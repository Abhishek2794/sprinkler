import {Dimensions, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');

const widthPercentageToDP = (widthPercent) => {
  const screenWidth = width;
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel((screenWidth * elemWidth) / 100);
};

const heightPercentageToDP = (heightPercentage) => {
  const screenHeight = height;
  const elemHeight = parseFloat(heightPercentage);
  return PixelRatio.roundToNearestPixel((screenHeight * elemHeight) / 100);
};
export const Dimension = {
  HEIGHT: heightPercentageToDP('100%'),
  HEIGHT_50: heightPercentageToDP('7.5'),
  HEIGHT_60: heightPercentageToDP('9'),
  HEIGHT_80: heightPercentageToDP('11'),
  HEIGHT_100: heightPercentageToDP('15'),
  HEIGHT_112: heightPercentageToDP('16.78'),
  HEIGHT_130: heightPercentageToDP('19.49'),
  HEIGHT_150: heightPercentageToDP('22.5'),
  HEIGHT_166: heightPercentageToDP('24.88'),
  HEIGHT_180: heightPercentageToDP('26.98'),
  HEIGHT_190: heightPercentageToDP('28.48'),
  HEIGHT_20: heightPercentageToDP('3'),
  HEIGHT_200: heightPercentageToDP('30'),
  HEIGHT_220: heightPercentageToDP('32.98'),
  HEIGHT_230: heightPercentageToDP('34.48'),
  HEIGHT_24: heightPercentageToDP('3.6'),
  HEIGHT_240: heightPercentageToDP('36'),
  HEIGHT_254: heightPercentageToDP('38'),
  HEIGHT_27: heightPercentageToDP('4'),
  HEIGHT_280: heightPercentageToDP('41.97'),
  HEIGHT_30: heightPercentageToDP('4.5'),
  HEIGHT_300: heightPercentageToDP('44.97'),
  HEIGHT_350: heightPercentageToDP('52.47'),
  HEIGHT_40: heightPercentageToDP('6'),
  HEIGHT_445: heightPercentageToDP('66.7'),
  HEIGHT_45: heightPercentageToDP('6.74'),
  HEIGHT_50: heightPercentageToDP('7.5'),
  HEIGHT_520: heightPercentageToDP('77.9'),
  HEIGHT_56: heightPercentageToDP('8.4'),
  HEIGHT_60: heightPercentageToDP('8.9'),
  HEIGHT_70: heightPercentageToDP('10.5'),
  HEIGHT_80: heightPercentageToDP('12'),
  HEIGHT_90: heightPercentageToDP('13.5'),
  WIDTH: widthPercentageToDP('100%'),
  WIDTH_100: widthPercentageToDP('26.6'),
  WIDTH_115: widthPercentageToDP('30.6'),
  WIDTH_135: widthPercentageToDP('36'),
  WIDTH_166: widthPercentageToDP('44.2'),
  WIDTH_194: widthPercentageToDP('51.7'),
  WIDTH_20: widthPercentageToDP('5.3'),
  WIDTH_24: widthPercentageToDP('6.4'),
  WIDTH_225: widthPercentageToDP('60'),
  WIDTH_230: widthPercentageToDP('61.3'),
  WIDTH_240: widthPercentageToDP('64'),
  WIDTH_280: widthPercentageToDP('74.66'),
  WIDTH_29: widthPercentageToDP('7.73'),
  WIDTH_30: widthPercentageToDP('8'),
  WIDTH_300: widthPercentageToDP('80'),
  WIDTH_317: widthPercentageToDP('84.5'),
  WIDTH_32: widthPercentageToDP('8.5'),
  WIDTH_338: widthPercentageToDP('90'),
  WIDTH_36: widthPercentageToDP('9.6'),
  WIDTH_48_75: widthPercentageToDP('13'),
  WIDTH_63: widthPercentageToDP('16.8'),
  WIDTH_97_5: widthPercentageToDP('26'),
};

export default Dimension;
