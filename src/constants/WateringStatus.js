/* eslint-disable import/prefer-default-export */
export const WateringStatus = {
  QUEUED: 0,
  STARTED: 1,
  CANCELED: 2,
  SKIPPED: 3,
  COMPLETED: 4,
};

export const WateringType = {
  QUICK_RUN: 0,
  MULTIPLE_RUN: 1,
  MANUAL_SCHEDULE: 2,
  SCHEDULE: 3,
};

export function getWateringStatus(status) {
  switch (status) {
    case WateringStatus.COMPLETED:
      return 'Completed';
    case WateringStatus.CANCELED:
      return 'Canceled';
    case WateringStatus.STARTED:
      return 'Ongoing';
    case WateringStatus.SKIPPED:
      return 'Skipped';
    default:
      return 'Queued';
  }
}

export function getWateringTypeName(type, schedule) {
  switch (type) {
    case WateringType.QUICK_RUN:
      return 'QUICK RUN';
    case WateringType.MULTIPLE_RUN:
      return 'MULTIPLE RUN';
    default:
      return schedule?.name || 'Current Watering';
  }
}
