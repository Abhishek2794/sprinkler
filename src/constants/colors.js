export const Colors = {
  BOTTOM_NAVIGATION: '#3D540C',
  CAM_BACKGROUND: '#6a6a6a',
  DANGER: '#D50000',
  FADE: '#FFFFFF50',
  FOOTER_BORDER_COLOR: '#979797',
  HEADER_COLOR: '#333',
  MARKER: '#b21010',
  PLACEHOLDER_TEXT: '#e7e7e7',
  PRIMARY_COLOR: '#9C27B0',
  PRIMARY_COLOR_DARK: '#4A148C',
  OFFLINE_STATUS: '#d6d6d6',
  ONLINE_STATUS: '#88ca3f',
  OVERLAY: '#edf0f4',
  SECONDARY_COLOR: '#303F9F',
  SCREEN_BACKGROUND: '#eeeeee',
  SWIPER_ACTIVE_DOT_COLOR: '#ffffff',
  SWIPER_DOT_COLOR: '#c3c1c1',
  TEXT_COLOR: '#000000',
  WEATHER_COLOR: '#F3FAB2',
};

export default Colors;
