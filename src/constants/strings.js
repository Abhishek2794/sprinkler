export const Strings = {
    EVEN_DAYS: ['Monday', 'Wednesday', 'Friday'],
    LOGS: [
        'Turning ON the Zone Sprinkler',
        'Turning OFF the Zone Sprinkler',
        'Zone Schedule Skipped based on weather data',
        'Zone Info Updated',
        'Zone Schedules Updated',
        'Zone Scheduled based on zone schedules data',
        'Zone Schedule skip based on weather data',
        'Manual Zone Scheduled as requested by the user',
        'Device Poll for Updates initiated',
    ],
    ODD_DAYS: ['Sunday', 'Tuesday', 'Thursday', 'Saturday'],
    WEEK_DAYS: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
};

export default Strings;
