import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const Fonts = {
    FONT_SIZE_12: width * 0.032,
    FONT_SIZE_14: width * 0.037,
    FONT_SIZE_15: width * 0.040,
    FONT_SIZE_16: width * 0.042,
    FONT_SIZE_18: width * 0.050,
    FONT_SIZE_20: width * 0.053,
    FONT_SIZE_40: width * 0.106,
    FONT_SIZE_45: width * 0.120,
    FONT_SIZE_64: width * 0.170,
    FONT_SIZE_75: width * 0.200,
    FONT_SIZE_95: width * 0.253,
};

export default Fonts;
