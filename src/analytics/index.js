import analytics from '@react-native-firebase/analytics';
import {logglyServices} from "../services";
import crashlytics from '@react-native-firebase/crashlytics';
import {trackEvent} from "appcenter-analytics";
import {trackError} from "appcenter-crashes";

export function logEvent(eventName, value) {
	analytics().logEvent(eventName, {...value});
	logglyServices.logEvent(value, eventName);
	trackEvent(eventName, value).then(data => {
		console.log("success", data);
	}).catch(err => {
		console.log("Err", err);
	});
}

export function recordError(errorName, error) {
	crashlytics().log(errorName);	
	crashlytics().recordError(error);
	trackError(errorName, error);
}