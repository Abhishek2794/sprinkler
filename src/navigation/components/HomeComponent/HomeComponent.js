import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ActiveScheduleScreen from '../../../routes/ActiveScheduleScreen';
import AddAddressScreen from '../../../routes/AddAddressScreen';
import AddDeviceScreen from '../../../routes/AddDeviceScreen';
import DeviceNameScreen from '../../../routes/DeviceNameScreen';
import DeviceNumberScreen from '../../../routes/DeviceNumberScreen';
import DeviceNumberErrorScreen from '../../../routes/DeviceNumberErrorScreen';
import HomeScreen from '../../../routes/HomeScreen';
import RegistrationFailureScreen from '../../../routes/RegistrationFailureScreen';
import SelectHomeWifiScreen from '../../../routes/SelectHomeWifiScreen';
import SetupControllerScreen from '../../../routes/SetupControllerScreen';
import SuccessMessageScreen from '../../../routes/SuccessMessageScreen';
import WifiPasswordScreen from '../../../routes/WifiPasswordScreen';
import TroubleShootScreen from '../../../routes/TroubleShootScreen';
import DeviceRemotelyDisabled from '../../../routes/DeviceRemotlyDisabled';
import ZoneSetupScreen1 from '../../../routes/ZoneSetup/screen-1';
import ZoneSetupScreen2 from '../../../routes/ZoneSetup/screen-2';
import ZoneSetupScreen3 from '../../../routes/ZoneSetup/screen-3';
import ZoneSetupScreen4 from '../../../routes/ZoneSetup/screen-4';
import ZoneSetupScreen5 from '../../../routes/ZoneSetup/screen-5';
import ZoneSetupScreen6 from '../../../routes/ZoneSetup/screen-6';
import ZoneSetupScreen7 from '../../../routes/ZoneSetup/screen-7';
import ZoneSetupScreen8 from '../../../routes/ZoneSetup/screen-8';
import ZoneSetupScreen9 from '../../../routes/ZoneSetup/screen-9';
import ZoneSetupScreen10 from '../../../routes/ZoneSetup/screen-10';
import ZoneSetupScreen11 from '../../../routes/ZoneSetup/screen-11';
import CropImageScreen from '../../../routes/EditZoneScreen/CropImageScreen';

const Stack = createStackNavigator();

const HomeComponent = () => (
  <Stack.Navigator headerMode="none" initialRouteName="Home">
    <Stack.Screen name="ActiveSchedule" component={ActiveScheduleScreen} />
    <Stack.Screen name="AddAddress" component={AddAddressScreen} />
    <Stack.Screen name="RemotelyDisabled" component={DeviceRemotelyDisabled} />
    <Stack.Screen name="AddDevice" component={AddDeviceScreen} />
    <Stack.Screen name="DeviceName" component={DeviceNameScreen} />
    <Stack.Screen name="DeviceNumber" component={DeviceNumberScreen} />
    <Stack.Screen
      name="DeviceNumberError"
      component={DeviceNumberErrorScreen}
    />
    <Stack.Screen name="Home" component={HomeScreen} />
    <Stack.Screen
      name="RegistrationFailure"
      component={RegistrationFailureScreen}
    />
    <Stack.Screen name="SelectHomeWifi" component={SelectHomeWifiScreen} />
    <Stack.Screen name="SetupController" component={SetupControllerScreen} />
    <Stack.Screen name="SuccessMessage" component={SuccessMessageScreen} />
    <Stack.Screen name="WifiPassword" component={WifiPasswordScreen} />
    <Stack.Screen name="TroubleShoot" component={TroubleShootScreen} />
    <Stack.Screen name="ZoneSetupScreen1" component={ZoneSetupScreen1} />
    <Stack.Screen name="ZoneSetupScreen2" component={ZoneSetupScreen2} />
    <Stack.Screen name="ZoneSetupScreen3" component={ZoneSetupScreen3} />
    <Stack.Screen name="ZoneSetupScreen4" component={ZoneSetupScreen4} />
    <Stack.Screen name="ZoneSetupScreen5" component={ZoneSetupScreen5} />
    <Stack.Screen name="ZoneSetupScreen6" component={ZoneSetupScreen6} />
    <Stack.Screen name="ZoneSetupScreen7" component={ZoneSetupScreen7} />
    <Stack.Screen name="ZoneSetupScreen8" component={ZoneSetupScreen8} />
    <Stack.Screen name="ZoneSetupScreen9" component={ZoneSetupScreen9} />
    <Stack.Screen name="ZoneSetupScreen10" component={ZoneSetupScreen10} />
    <Stack.Screen name="ZoneSetupScreen11" component={ZoneSetupScreen11} />
    <Stack.Screen name="CropImageScreen" component={CropImageScreen} />
  </Stack.Navigator>
);

export default HomeComponent;
