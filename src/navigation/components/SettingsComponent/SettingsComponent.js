import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ActiveScheduleScreen from '../../../routes/ActiveScheduleScreen';
import DeviceSettingScreen from '../../../routes/DeviceSettingScreen';
import EditAddressScreen from '../../../routes/EditAddressScreen';
import EditDeviceNameScreen from '../../../routes/EditDeviceNameScreen';
import EditUsernameScreen from '../../../routes/EditUsernameScreen';
import HistoryScreen from '../../../routes/HistoryScreen';
import NewHomeWifiScreen from '../../../routes/NewHomeWifiScreen';
import NewWifiPasswordScreen from '../../../routes/NewWifiPasswordScreen';
import ReRegistrationFailureScreen from '../../../routes/ReRegistrationFailureScreen';
import SettingsScreen from '../../../routes/SettingsScreen';
import UserAccountScreen from '../../../routes/UserAccountScreen';
import WifiResetScreen from '../../../routes/WifiResetScreen';
import NotificationPreference from '../../../routes/NotificationPreferences';
import ServerPreference from '../../../routes/ServerPreferences';
import DeviceInfoScreen from '../../../routes/DeviceInfoScreen';
import NetworkInfoScreen from '../../../routes/NetworkInfoScreen';
import MeasurementScreen from '../../../routes/MeasurementScreen';
import EditControllerWifiScreen from '../../../routes/EditControllerWifiScreen';

const Stack = createStackNavigator();

const SettingsComponent = () => (
  <Stack.Navigator headerMode="none" initialRouteName="Settings">
    <Stack.Screen name="ActiveSchedule" component={ActiveScheduleScreen} />
    <Stack.Screen name="DeviceSetting" component={DeviceSettingScreen} />
    <Stack.Screen name="EditAddress" component={EditAddressScreen} />
    <Stack.Screen name="EditDeviceName" component={EditDeviceNameScreen} />
    <Stack.Screen name="EditUsername" component={EditUsernameScreen} />
    <Stack.Screen name="History" component={HistoryScreen} />
    <Stack.Screen name="NewHomeWifi" component={NewHomeWifiScreen} />
    <Stack.Screen name="NewWifiPassword" component={NewWifiPasswordScreen} />
    <Stack.Screen
      name="ReRegistrationFailure"
      component={ReRegistrationFailureScreen}
    />
    <Stack.Screen name="Settings" component={SettingsScreen} />
    <Stack.Screen name="UserAccount" component={UserAccountScreen} />
    <Stack.Screen name="WifiReset" component={WifiResetScreen} />
    <Stack.Screen
      name="NotificationPreference"
      component={NotificationPreference}
    />
    <Stack.Screen name="ServerPreference" component={ServerPreference} />
    <Stack.Screen name="NetworkInfoScreen" component={NetworkInfoScreen} />
    <Stack.Screen name="DeviceInfoScreen" component={DeviceInfoScreen} />
    <Stack.Screen name="MeasurementScreen" component={MeasurementScreen} />
    <Stack.Screen
      name="EditControllerWifi"
      component={EditControllerWifiScreen}
    />
  </Stack.Navigator>
);

export default SettingsComponent;
