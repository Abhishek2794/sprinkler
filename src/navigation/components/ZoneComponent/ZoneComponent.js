import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import CropImageScreen from '../../../routes/EditZoneScreen/CropImageScreen';
import ActiveScheduleScreen from '../../../routes/ActiveScheduleScreen';
import EditNotesScreen from '../../../routes/EditNotesScreen';
import EditZoneScreen from '../../../routes/EditZoneScreen';
import EditZoneNameScreen from '../../../routes/EditZoneNameScreen';
import ZoneScreen from '../../../routes/ZoneScreen';
import MultipleRunScreen from '../../../routes/MultipleRunScreen';

const Stack = createStackNavigator();

const ZoneComponent = () => (
  <Stack.Navigator headerMode="none" initialRouteName="Zones">
    <Stack.Screen name="ActiveSchedule" component={ActiveScheduleScreen} />
    <Stack.Screen name="EditNotes" component={EditNotesScreen} />
    <Stack.Screen name="EditZone" component={EditZoneScreen} />
    <Stack.Screen name="EditZoneName" component={EditZoneNameScreen} />
    <Stack.Screen name="Zones" component={ZoneScreen} />
    <Stack.Screen name="CropImageScreen" component={CropImageScreen} />
    <Stack.Screen name="MultipleRun" component={MultipleRunScreen} />
  </Stack.Navigator>
);

export default ZoneComponent;
