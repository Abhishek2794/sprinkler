import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ActiveScheduleScreen from '../../../routes/ActiveScheduleScreen';
import EditDaysScreen from '../../../routes/EditDaysScreen';
import EditIntervalScreen from '../../../routes/EditIntervalScreen';
import EditScheduleScreen from '../../../routes/EditScheduleScreen';
import EditScheduleNameScreen from '../../../routes/EditScheduleNameScreen';
import EditTimeSlotScreen from '../../../routes/EditTimeSlotScreen';
import PickTimeslotScreen from '../../../routes/PickTimeslotScreen';
import ScheduleScreen from '../../../routes/ScheduleScreen';
import ScheduleOnDateScreen from '../../../routes/ScheduleOnDateScreen';
import ScheduleZoneScreen from '../../../routes/ScheduleZoneScreen';
import SelectDaysScreen from '../../../routes/SelectDaysScreen';
import SelectIntervalScreen from '../../../routes/SelectIntervalScreen';
import SelectNameScreen from '../../../routes/SelectNameScreen';
import SelectPeriodScreen from '../../../routes/SelectPeriodScreen';
import SelectZoneScreen from '../../../routes/SelectZoneScreen';
import WeatherIntelligence from '../../../routes/WeatherIntelligence';
import SmartCycle from '../../../routes/SmartCycle';

const Stack = createStackNavigator();

const ScheduleComponent = () => (
  <Stack.Navigator headerMode="none" initialRouteName="Schedules">
    <Stack.Screen name="ActiveSchedule" component={ActiveScheduleScreen} />
    <Stack.Screen name="EditDays" component={EditDaysScreen} />
    <Stack.Screen name="EditInterval" component={EditIntervalScreen} />
    <Stack.Screen name="EditSchedule" component={EditScheduleScreen} />
    <Stack.Screen name="EditScheduleName" component={EditScheduleNameScreen} />
    <Stack.Screen name="EditTimeSlot" component={EditTimeSlotScreen} />
    <Stack.Screen name="PickTimeslot" component={PickTimeslotScreen} />
    <Stack.Screen name="Schedules" component={ScheduleScreen} />
    <Stack.Screen name="ScheduleOnDate" component={ScheduleOnDateScreen} />
    <Stack.Screen name="ScheduleZone" component={ScheduleZoneScreen} />
    <Stack.Screen name="SelectDays" component={SelectDaysScreen} />
    <Stack.Screen name="SelectInterval" component={SelectIntervalScreen} />
    <Stack.Screen name="SelectName" component={SelectNameScreen} />
    <Stack.Screen name="SelectPeriod" component={SelectPeriodScreen} />
    <Stack.Screen name="SelectZone" component={SelectZoneScreen} />
    <Stack.Screen name="WeatherIntelligence" component={WeatherIntelligence} />
    <Stack.Screen name="SmartCycle" component={SmartCycle} />
  </Stack.Navigator>
);

export default ScheduleComponent;
