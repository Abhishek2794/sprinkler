/* eslint-disable no-useless-escape */
/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {createStackNavigator} from '@react-navigation/stack';
import watch from 'redux-watch';
import * as ActionCreators from '../actions';
import store from '../helpers/store';
import {appActions} from '../actions/app.actions';
import AppStack from './AppStack/AppStack';
import AuthStack from './AuthStack/AuthStack';
import LaunchStack from './LaunchStack';

const Stack = createStackNavigator();

class Navigator extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    const watcher = watch(store.getState, 'login.isLoggedIn');
    this.loginListener = store.subscribe(
      watcher((newValue) => {
        if (newValue === false) {
          store.dispatch(appActions.appLoaded(true));
          store.dispatch(appActions.checkLogin(false));
        }
      }),
    );
  }

  componentWillUnmount() {
    this.loginListener();
  }

  render() {
    const {isLoggedIn} = this.props;

    return (
      <Stack.Navigator headerMode="none">
        {isLoggedIn === null && (
          <Stack.Screen name="Launch" component={LaunchStack} />
        )}
        {isLoggedIn === true && (
          <Stack.Screen name="App" component={AppStack} />
        )}
        {isLoggedIn === false && (
          <Stack.Screen name="Auth" component={AuthStack} />
        )}
      </Stack.Navigator>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {docked} = state.docker;
  const {isLoggedIn} = state.login;
  return {device, docked, isLoggedIn};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Navigator);
