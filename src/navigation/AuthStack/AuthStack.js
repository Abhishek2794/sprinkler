import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ForgotPasswordScreen from '../../routes/ForgotPasswordScreen';
import LoginScreen from '../../routes/LoginScreen';
import SignUpScreen from '../../routes/SignupScreen';

const Stack = createStackNavigator();

const AuthStack = () => (
    <Stack.Navigator headerMode="none" initialRouteName="Login">
        <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
    </Stack.Navigator>
);

export default AuthStack;
