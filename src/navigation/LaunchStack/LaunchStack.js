import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from '../../routes/SplashScreen';
import LaunchScreen from '../../routes/LaunchScreen';

const Stack = createStackNavigator();

const LaunchStack = () => (
    <Stack.Navigator headerMode="none" initialRouteName="Splash">
        <Stack.Screen name="Launch" component={LaunchScreen} />
        <Stack.Screen name="Splash" component={SplashScreen} />
    </Stack.Navigator>
);

export default LaunchStack;
