/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import HomeComponent from '../components/HomeComponent';
import ScheduleComponent from '../components/ScheduleComponent';
import SettingsComponent from '../components/SettingsComponent';
import TabBar from '../../components/tabBar';
import ZoneComponent from '../components/ZoneComponent';
import {Colors} from '../../constants';

const styles = StyleSheet.create({
  image: {
    height: 24,
    width: 24,
  },
});

const Tab = createBottomTabNavigator();

const whiteListedRoutes = ['Home', 'Schedules', 'Settings', 'Zones'];

function isMainRoute(route) {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : route.name;
  return whiteListedRoutes.indexOf(routeName) > -1;
}

const AppStack = (mainProps) => (
  <Tab.Navigator
    initialRouteName="Home"
    tabBar={(props) => (
      <View>
        {mainProps.docked && isMainRoute(props) ? (
          <TabBar {...mainProps} />
        ) : null}
        <BottomTabBar {...props} />
      </View>
    )}
    tabBarOptions={{
      activeTintColor: Colors.BOTTOM_NAVIGATION,
      showLabel: true,
    }}>
    <Tab.Screen
      component={HomeComponent}
      name="Home"
      options={({route}) => ({
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../../../assets/navigation/home.png')}
              style={styles.image}
            />
          ) : (
            <Image
              source={require('../../../assets/navigation/homew.png')}
              style={styles.image}
            />
          ),
        tabBarLabel: 'Home',
        tabBarVisible: isMainRoute(route),
      })}
    />
    <Tab.Screen
      component={ScheduleComponent}
      name="Schedules"
      options={({route}) => ({
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../../../assets/navigation/calendar.png')}
              style={styles.image}
            />
          ) : (
            <Image
              source={require('../../../assets/navigation/calendarw.png')}
              style={styles.image}
            />
          ),
        tabBarLabel: 'Schedules',
        tabBarVisible: isMainRoute(route),
      })}
    />
    <Tab.Screen
      component={ZoneComponent}
      name="Zones"
      options={({route}) => ({
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../../../assets/navigation/sprinkler.png')}
              style={styles.image}
            />
          ) : (
            <Image
              source={require('../../../assets/navigation/sprinklerw.png')}
              style={styles.image}
            />
          ),
        tabBarLabel: 'Zones',
        tabBarVisible: isMainRoute(route),
      })}
    />
    <Tab.Screen
      component={SettingsComponent}
      name="Settings"
      options={({route}) => ({
        tabBarIcon: ({focused}) =>
          focused ? (
            <Image
              source={require('../../../assets/navigation/settings.png')}
              style={styles.image}
            />
          ) : (
            <Image
              source={require('../../../assets/navigation/settingsw.png')}
              style={styles.image}
            />
          ),
        tabBarLabel: 'Settings',
        tabBarVisible: isMainRoute(route),
      })}
    />
  </Tab.Navigator>
);

const mapStateToProps = (state) => {
  const {docked} = state.docker;
  return {docked};
};

export default connect(mapStateToProps, null)(AppStack);
