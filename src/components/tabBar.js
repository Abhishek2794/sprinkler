/* eslint-disable no-restricted-globals */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-extraneous-dependencies */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../actions';
import {appActions} from '../actions/app.actions';
import {userServices} from '../services';
import store from '../helpers/store';
import Overlay from './overlay';
import {WateringStatus} from '../constants';
import {userActions} from '../actions/user.actions';
class TabBar extends Component {
  constructor(props) {
    super(props);
    this.handlePress = this.handlePress.bind(this);
    this.setProgress = this.setProgress.bind(this);
    this.skipZone = this.skipZone.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopSchedule = this.stopSchedule.bind(this);
  }

  componentDidMount() {
    if (this.checkForValidSchedule()) {
      this.startTimer();
    }
  }

  componentWillUnmount() {
    clearInterval(this.progressTimer);
    clearTimeout(this.updateTimer);
  }

  setProgress() {
    const {scheduleObj} = this.props;
    const {_id, zones} = scheduleObj;
    const runningZone = zones.find(
      (item) => item.status === WateringStatus.STARTED,
    );
    if (runningZone) {
      if (moment(runningZone.endTime).diff(moment()) >= 0) {
        const startTime = moment(runningZone.startTime);
        const timeElapsed = moment().diff(startTime, 'seconds');
        console.log(timeElapsed);
        if (!isNaN(timeElapsed)) {
          store.dispatch(appActions.setProgressValue(timeElapsed + 1));
        }
      }
    } else {
      clearTimeout(this.progressTimer);
      clearTimeout(this.updateTimer);
      store.dispatch(appActions.setProgressValue(0));
    }
  }

  startTimer() {
    this.progressTimer = setInterval(() => {
      this.setProgress();
    }, 1000);
  }

  handlePress() {
    const {navigation} = this.props;
    navigation.navigate('ActiveSchedule');
    store.dispatch(userActions.dockView(false));
  }

  skipZone(wateringId, zoneId) {
    this.setState({loading: true, skipped: true});
    clearInterval(this.progressTimer);
    clearTimeout(this.updateTimer);
    const {device} = this.props;
    userServices
      .skipZone({serialNumber: device.serialNumber, wateringId, zoneId})
      .then(() => {})
      .catch((err) => {});
  }

  stopSchedule(wateringId) {
    clearInterval(this.progressTimer);
    clearTimeout(this.updateTimer);
    const {device} = this.props;
    userServices
      .stopQuickRun({serialNumber: device.serialNumber, wateringId})
      .then(() => {})
      .catch((err) => {});
  }

  checkForValidSchedule = () => {
    let response = true;
    const {scheduleObj} = this.props;
    if (scheduleObj === null || !scheduleObj._id) {
      response = false;
    } else if (moment(scheduleObj.endTime).diff(moment()) <= 0) {
      response = false;
    }
    if (response && scheduleObj.zones) {
      const runningZone = scheduleObj.zones.find(
        (item) => item.status === WateringStatus.STARTED,
      );
      response = runningZone ? true : false;
    }
    return response;
  };

  render() {
    if (this.checkForValidSchedule()) {
      return (
        <Overlay
          handlePress={this.handlePress}
          skipZone={this.skipZone}
          stopSchedule={this.stopSchedule}
        />
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleObj} = state.currentSchedule;
  return {device, scheduleObj};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TabBar);
