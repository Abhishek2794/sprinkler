/* eslint-disable no-underscore-dangle */
/* eslint-disable react/no-unused-state */
/* eslint-disable class-methods-use-this */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

class Cropper extends Component {
  componentDidMount() {
    this.getImage();
  }

  getImage() {
    const {image} = this.props;
    ImagePicker.openCropper({path: image.uri, height: 890, width: 2312})
      .then((response) => {
        this.props.onSubmit(response);
      })
      .catch((err) => {
        this.props.navigation.goBack();
      });
  }

  render() {
    return <View />;
  }
}

Cropper.propTypes = {
  image: PropTypes.shape({
    height: PropTypes.number.isRequired,
    uri: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default Cropper;
