/* eslint-disable react/sort-comp */
/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  SafeAreaView,
} from 'react-native';
import {Button} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ScrollPicker from 'react-native-wheel-scrollview-picker';
import {appConstants, Colors, Dimension, Fonts} from '../constants';

const styles = StyleSheet.create({
  container: {
    marginTop: 12,
    height: Dimension.HEIGHT_100,
  },
  modalHeader: {
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderBottomWidth: 0.5,
    borderBottomColor: '#999',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  duration: {
    fontSize: Fonts.FONT_SIZE_40,
    marginLeft: Dimension.WIDTH_20,
    marginRight: Dimension.WIDTH_20,
  },
  durationContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  footer: {
    height: Dimension.HEIGHT_50,
    marginTop: 8,
    justifyContent: 'center',
    fontSize: 15,
  },
  text: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
    fontWeight: 'bold',
  },
  timeInput: {
    alignItems: 'center',
  },
  timeInputText: {
    color: '#666',
    fontSize: 24,
    fontWeight: 'bold',
  },
  durationPickerWrapper: {
    marginRight: 12,
    textAlign: 'center',
  },
  durationPickerHeader: {
    fontSize: 13,
    opacity: 0.7,
  },
  durationPicker: {
    height: 85,
  },
  centerView: {
    flex: 1,
    justifyContent: 'center',
  },
  headerView: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

class DurationPicker extends Component {
  seconds = [];
  minutes = [];
  hours = [];
  constructor(props) {
    super(props);
    this.state = {
      duration: 15,
    };
    for (let i = 0; i < 60; i++) {
      this.seconds.push(i);
      this.minutes.push(i);
      if (i < 5) {
        this.hours.push(i);
      }
    }
    this.closeModal = this.closeModal.bind(this);
    this.setDuration = this.setDuration.bind(this);
    this.updateDuration = this.updateDuration.bind(this);
  }

  setDuration() {
    const {duration} = this.state;
    const {startQuickRun} = this.props;
    startQuickRun(duration);
  }

  updateDuration(type) {
    let {duration} = this.state;
    switch (type) {
      case appConstants.DECREMENT: {
        if (duration > 15) {
          duration -= 15;
          this.setState({duration});
        }
        break;
      }
      case appConstants.INCREMENT: {
        duration += 15;
        this.setState({duration});
        break;
      }
      default:
        break;
    }
  }

  componentWillUnmount() {
    this.closeModal();
  }

  closeModal() {
    const {closePopup} = this.props;
    closePopup();
  }

  renderItem = ({item}) => {
    return (
      <View>
        <Text style={{textAlign: 'center', fontSize: 18}}>{item}</Text>
      </View>
    );
  };

  UNSAFE_componentWillReceiveProps(prevProps, prevState) {
    if (prevProps.duration) {
      this.setState({
        duration: prevProps.duration,
      });
    }
  }

  render() {
    const {duration} = this.state;
    const {modalVisible, isMultipleRun} = this.props;
    const calHours = Math.floor(duration / 3600);
    const calMinutes = Math.floor((duration % 3600) / 60);
    const calSeconds = Math.floor((duration % 3600) % 60);
    return (
      <Modal
        transparent={true}
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => this.closeModal()}
        width={Dimension.WIDTH_338}>
        <SafeAreaView style={styles.centerView}>
          <View style={styles.headerView}>
            <View style={styles.modalHeader}>
              <Text style={{fontSize: 16, fontWeight: 'bold', opacity: 0.6}}>
                Set Duration
              </Text>
              <TouchableOpacity
                style={{paddingHorizontal: 4}}
                activeOpacity={0.5}
                onPress={() => this.closeModal()}>
                <Icon name="close-circle" size={22} color={'#666'} />
              </TouchableOpacity>
            </View>
            <View style={styles.container}>
              <TouchableOpacity
                style={styles.durationContainer}
                activeOpacity={1}>
                <View style={styles.durationPickerWrapper}>
                  <Text style={styles.durationPickerHeader}>Hours</Text>
                  <ScrollPicker
                    dataSource={this.hours}
                    selectedIndex={calHours}
                    renderItem={(data, index, isSelected) => {
                      return this.renderItem(data);
                    }}
                    onValueChange={(data, selectedIndex) => {
                      let duration = this.state.duration;
                      duration = duration - calHours * 3600;
                      duration = duration + data * 3600;
                      this.setState({
                        duration,
                      });
                    }}
                    wrapperHeight={70}
                    wrapperWidth={40}
                    wrapperBackground={'#FFFFFF'}
                    itemHeight={35}
                    highlightColor={'#cacaca'}
                    highlightBorderWidth={2}
                    activeItemColor={'#222121'}
                    itemColor={'#B4B4B4'}
                  />
                </View>
                <View style={styles.durationPickerWrapper}>
                  <Text style={styles.durationPickerHeader}>Minutes</Text>
                  <View style={styles.durationPicker}>
                    <ScrollPicker
                      dataSource={this.minutes}
                      selectedIndex={calMinutes}
                      renderItem={(data, index, isSelected) => {
                        return this.renderItem(data);
                      }}
                      onValueChange={(data, selectedIndex) => {
                        let duration = this.state.duration;
                        duration = duration - calMinutes * 60;
                        duration = duration + data * 60;
                        this.setState({
                          duration,
                        });
                      }}
                      wrapperHeight={70}
                      wrapperWidth={40}
                      wrapperBackground={'#fff'}
                      itemHeight={35}
                      highlightColor={'#cacaca'}
                      highlightBorderWidth={2}
                      activeItemColor={'#222121'}
                      itemColor={'#B4B4B4'}
                    />
                  </View>
                </View>
                <TouchableOpacity style={styles.durationPickerWrapper}>
                  <Text style={styles.durationPickerHeader}>Seconds</Text>
                  <ScrollPicker
                    dataSource={this.seconds}
                    selectedIndex={calSeconds}
                    renderItem={(data, index, isSelected) => {
                      return this.renderItem(data);
                    }}
                    onValueChange={(data, selectedIndex) => {
                      let duration = this.state.duration;
                      duration = duration - calSeconds;
                      duration = duration + data;
                      this.setState({
                        duration,
                      });
                    }}
                    wrapperHeight={70}
                    wrapperWidth={40}
                    wrapperBackground={'#FFFFFF'}
                    itemHeight={35}
                    highlightColor={'#cacaca'}
                    highlightBorderWidth={2}
                    activeItemColor={'#222121'}
                    itemColor={'#B4B4B4'}
                    nestedScrollEnabled={true}
                  />
                </TouchableOpacity>
              </TouchableOpacity>
            </View>
            <Button
              color="#fff"
              style={[
                styles.footer,
                {
                  backgroundColor: this.state.duration
                    ? Colors.SECONDARY_COLOR
                    : '#333',
                },
              ]}
              disabled={this.state.duration ? false : true}
              onPress={() => this.setDuration()}>
              {isMultipleRun ? 'Save' : 'Start'}
            </Button>
          </View>
        </SafeAreaView>
      </Modal>
    );
  }
}

export default DurationPicker;
