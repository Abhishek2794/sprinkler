import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import {
    Colors,
    Dimension,
    Fonts,
} from '../constants';

const styles = StyleSheet.create({
    city: {
        paddingTop: 5,
        color: '#9e9e9e',
        fontWeight: '300',
        fontSize: Fonts.FONT_SIZE_15,
    },
    condition: {
        color: '#9e9e9e',
        fontSize: Fonts.FONT_SIZE_15,
        fontWeight: '300',
        paddingTop: 10,
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    current: {
        alignItems: 'center',
        backgroundColor: Colors.WEATHER_COLOR,
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        width: '42%',
    },
    icon: {
        alignItems: 'center',
        height: Dimension.HEIGHT_30 + 10,
        justifyContent: 'center',
        width: '20%',
    },
    item: {
        borderBottomColor: Colors.PLACEHOLDER_TEXT,
        borderBottomWidth: 0.5,
        borderStyle: 'solid',
        flexDirection: 'row',
        height: Dimension.HEIGHT_30 + 10,
        justifyContent: 'space-between',
    },
    section: {
        backgroundColor: Colors.WEATHER_COLOR,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 10,
        width: '54%',
    },
    symbol: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_20,
        fontWeight: 'bold',
    },
    temp: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_40,
        fontWeight: '300',
    },
    text: {
        color: '#9e9e9e',
        fontWeight: '300',
        fontSize: Fonts.FONT_SIZE_12,
    },
    type: {
        alignItems: 'center',
        flexDirection: 'row',
        height: Dimension.HEIGHT_30 + 10,
        justifyContent: 'space-between',
        width: '80%',
    },
    val: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_14,
        fontWeight: '300',
    },
});

const WeatherCard = (props) => {
    const { weather } = props;
    return (
        <View>
            <View style={styles.container}>
                <View style={styles.current}>
                    <Text style={styles.temp}>{Math.round(weather.temp)}</Text>
                    <Text style={styles.symbol}>&deg;F</Text>
                </View>
                <View style={styles.section}>
                    <View style={styles.item}>
                        <View style={styles.icon}>
                            <Image source={require('../../assets/images/wind/wind.png')} />
                        </View>
                        <View style={styles.type}>
                            <Text style={styles.val}>Wind</Text>
                            <Text style={[styles.val, { paddingRight: 10 }]}>{`${weather.wind_speed} mph`}</Text>
                        </View>

                    </View>
                    <View style={styles.item}>
                        <View style={styles.icon}>
                            <Image source={require('../../assets/images/realFeel/realFeel.png')} />
                        </View>
                        <View style={styles.type}>
                            <Text style={styles.val}>Real Feel</Text>
                            <Text style={[styles.val, { paddingRight: 10 }]}>{`${Math.round(weather.feels_like)}\u2109`}</Text>
                        </View>
                    </View>
                    <View style={styles.item}>
                        <View style={styles.icon}>
                            <Image source={require('../../assets/images/humidity/humidity.png')} />
                        </View>
                        <View style={styles.type}>
                            <Text style={styles.val}>Humidity</Text>
                            <Text style={[styles.val, { paddingRight: 10 }]}>{`${weather.humidity} %`}</Text>
                        </View>
                    </View>
                    <View style={styles.item}>
                        <View style={styles.icon}>
                            <Image source={require('../../assets/images/barometer/barometer.png')} />
                        </View>
                        <View style={styles.type}>
                            <Text style={styles.val}>Pressure</Text>
                            <Text style={[styles.val, { paddingRight: 10 }]}>{`${Math.round(weather.pressure)} mb`}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
};

export default WeatherCard;
