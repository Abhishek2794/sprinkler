import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Colors, Fonts} from '../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

export const NextButton = ({navigation, screen}) => (
  <TouchableOpacity
    onPress={() => navigation.navigate(screen)}
    style={styles.button}>
    <Text style={styles.text}>Next</Text>
  </TouchableOpacity>
);

export default NextButton;
