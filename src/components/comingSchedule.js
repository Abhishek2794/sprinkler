import React from 'react';
import moment from 'moment';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { Dimension, Colors, Fonts } from '../constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        paddingLeft: Dimension.WIDTH_30,
        paddingRight: Dimension.WIDTH_30,
        paddingTop: Dimension.HEIGHT_30 / 2,
        paddingBottom: Dimension.HEIGHT_30 / 2,
    },
    dateTime: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_16,
    },
    schedule: {
        borderRadius: 8,
        shadowRadius: 18,
        shadowOpacity: 1,
        alignSelf: 'center',
        width: Dimension.WIDTH_338,
        marginTop: Dimension.HEIGHT_20,
        shadowOffset: { width: 0, height: 7 },
        shadowColor: 'rgba(203, 203, 203, 0.5)',
        backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
    scheduleName: {
        paddingTop: 10,
        fontWeight: '200',
        color: '#929292',
        fontSize: Fonts.FONT_SIZE_14,
    },
});

const ComingSchedule = ({ schedule }) => {
    const today = new Date();
    const tomorrow = new Date(today.setDate(today.getDate() + 1));
    const startTime = new Date(schedule.time);
    const day = moment().isSame(moment(schedule.time), 'day') ? 'Today' : (tomorrow.toDateString() === startTime.toDateString() ? 'Tomorrow' : `Upcoming (${moment(schedule.time).format('MMM Do')})`);
    return (
        <View style={styles.schedule}>
            <View style={styles.container}>
                <Text style={styles.dateTime}>{day}</Text>
                <Text style={styles.scheduleName}>{`Sprinklr will water ${schedule.scheduleName} schedule at ${moment(startTime).format('hh:mm A')} `}</Text>
            </View>
        </View>
    );
};

export default ComingSchedule;
