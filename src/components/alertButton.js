import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Modal, {
  ModalButton,
  ModalContent,
  ModalFooter,
  SlideAnimation,
} from 'react-native-modals';
import {Fonts, Dimension} from '../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    borderTopWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: '#d2d2d1',
  },
  text: {
    color: '#929292',
    textAlign: 'center',
    fontSize: Fonts.FONT_SIZE_20,
  },
});

export default class AlertButton extends Component {
  constructor(props) {
    super(props);
    this.delete = this.delete.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  delete() {
    const {onPress} = this.props;
    onPress();
  }

  closeModal() {
    const {closePopup} = this.props;
    closePopup();
  }

  render() {
    const {action, modalVisible, text} = this.props;
    return (
      <Modal
        modalAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        onTouchOutside={() => this.closeModal()}
        onHardwareBackPress={() => this.closeModal()}
        width={Dimension.WIDTH_338}
        visible={modalVisible}
        footer={
          <ModalFooter>
            <ModalButton
              text="Cancel"
              align="center"
              key="CancelButton"
              style={styles.button}
              textStyle={styles.text}
              onPress={() => this.closeModal()}
            />
            <ModalButton
              align="center"
              key="SetButton"
              style={styles.button}
              text={action}
              onPress={() => this.delete()}
              textStyle={[styles.text, {color: '#ff7575'}]}
            />
          </ModalFooter>
        }>
        <ModalContent>
          <View
            style={{
              marginTop: Dimension.HEIGHT_20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={styles.text}>{text}</Text>
          </View>
        </ModalContent>
      </Modal>
    );
  }
}
