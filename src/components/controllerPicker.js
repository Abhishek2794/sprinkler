import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal, {ModalContent, ModalTitle} from 'react-native-modals';
import {Colors, Dimension, Fonts} from '../constants';

const styles = StyleSheet.create({
  container: {
    paddingTop: Dimension.HEIGHT_20 / 2,
  },
  deviceName: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
  },
  footer: {
    marginTop: 10,
    borderTopWidth: 0.5,
    alignItems: 'center',
    flexDirection: 'row',
    width: Dimension.WIDTH,
    borderTopColor: '#d2d1d4',
    paddingTop: Dimension.HEIGHT_20,
  },
  footerText: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  name: {
    flex: 4,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: Dimension.HEIGHT_50,
  },
  statusIndicator: {
    flex: 1,
  },
  text: {
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_20,
  },
});

export default class ControllerPicker extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  onSelect(index) {
    const {switchController} = this.props;
    switchController(index);
  }

  closeModal() {
    const {closePopup} = this.props;
    closePopup();
  }

  handleClick() {
    const {addController} = this.props;
    addController();
  }

  render() {
    const {activeDevice, devices, modalVisible} = this.props;
    if (devices) {
      return (
        <Modal
          width={Dimension.WIDTH_338}
          visible={modalVisible}
          onTouchOutside={() => this.closeModal()}
          dialogTitle={
            <ModalTitle title="Contollers" textStyle={styles.text} />
          }>
          <ModalContent>
            <SafeAreaView style={styles.container}>
              {devices.map((device, index) => (
                <View key={device._id} style={styles.row}>
                  <View style={styles.statusIndicator}>
                    <Text
                      style={
                        device.isOnline === true
                          ? {
                              color: Colors.ONLINE_STATUS,
                              fontSize: Fonts.FONT_SIZE_20,
                            }
                          : {
                              color: Colors.OFFLINE_STATUS,
                              fontSize: Fonts.FONT_SIZE_20,
                            }
                      }>
                      {' '}
                      &#9679;{' '}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.onSelect(index)}
                    style={styles.name}>
                    <Text style={styles.deviceName}>{device.name}</Text>
                  </TouchableOpacity>
                  <View style={styles.statusIndicator}>
                    {activeDevice._id === device._id ? (
                      <Icon
                        name="done"
                        color={Colors.PRIMARY_COLOR}
                        size={24}
                      />
                    ) : null}
                  </View>
                </View>
              ))}
              <TouchableOpacity
                style={styles.footer}
                onPress={() => this.handleClick()}>
                <Icon
                  name="control-point"
                  color={Colors.PRIMARY_COLOR}
                  size={28}
                />
                <Text style={styles.footerText}>Add Controller</Text>
              </TouchableOpacity>
            </SafeAreaView>
          </ModalContent>
        </Modal>
      );
    }
    return <View />;
  }
}
