import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Modal, {
  ModalButton,
  ModalContent,
  ModalFooter,
} from 'react-native-modals';
import {Colors, Dimension, Fonts, appConstants} from '../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    borderWidth: 0.5,
    alignItems: 'center',
    borderStyle: 'solid',
    justifyContent: 'center',
    borderColor: Colors.FOOTER_BORDER_COLOR,
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  modalButton: {
    flex: 1,
    borderTopWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: '#d2d2d1',
  },
  modalText: {
    color: '#929292',
    textAlign: 'center',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
});

export default class CancelButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.close = this.close.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  close() {
    this.setState({modalVisible: false});
    const {navigation, routeAction} = this.props;
    if (routeAction === appConstants.ROUTE_GO_BACK) {
      navigation.goBack();
      return;
    }
    navigation.popToTop();
  }

  closeModal() {
    this.setState({modalVisible: false});
  }

  handleClick() {
    this.setState({modalVisible: true});
  }

  render() {
    const {modalVisible} = this.state;
    return (
      <View style={styles.button}>
        <Modal
          width={Dimension.WIDTH_338}
          visible={modalVisible}
          footer={
            <ModalFooter>
              <ModalButton
                text="Cancel"
                align="center"
                key="CancelButton"
                style={styles.modalButton}
                textStyle={styles.text}
                onPress={() => this.closeModal()}
              />
              <ModalButton
                align="center"
                key="SetButton"
                style={styles.modalButton}
                text="OK"
                onPress={() => this.close()}
                textStyle={[styles.text, {color: '#ff7575'}]}
              />
            </ModalFooter>
          }>
          <ModalContent>
            <View
              style={{
                marginTop: Dimension.HEIGHT_20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.modalText}>
                Are you sure you want to cancel the process?
              </Text>
            </View>
          </ModalContent>
        </Modal>
        <TouchableOpacity onPress={() => this.handleClick()}>
          <Text style={styles.text}>Cancel</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
