import React from 'react';
import moment from 'moment';
import { StyleSheet, Text, View } from 'react-native';
import { Dimension, Colors, Fonts } from '../constants';

const styles = StyleSheet.create({
    box: {
        flex: 1,
    },
    container: {
        alignSelf: 'center',
        backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
        borderRadius: 8,
        flex: 1,
        flexDirection: 'row',
        marginTop: Dimension.HEIGHT_20,
        paddingLeft: Dimension.WIDTH_30,
        paddingRight: Dimension.WIDTH_30,
        paddingTop: 10,
        shadowColor: 'rgba(203, 203, 203, 0.5)',
        shadowRadius: 18,
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 7 },
        width: Dimension.WIDTH_338,
    },
    scheduleHeader: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_16,
        paddingBottom: 10,
    },
    scheduleDetail: {
        color: '#929292',
        fontSize: Fonts.FONT_SIZE_14,
        marginRight: 5,
    },
});

const ScheduleHistory = ({ schedules, history }) => (
    <View style={styles.container}>
        {
            history.length > 0
                ? (
                    <View style={styles.box}>
                        <Text style={styles.scheduleHeader}>Last watered</Text>
                        {
                            (history).map((lw) => (
                                <View key={`${lw.scheduleName} ${lw.time}`} style={{ marginBottom: Dimension.HEIGHT_30 / 2 }}>
                                    <Text numberOfLines={1} style={styles.scheduleDetail}>{moment.unix(lw.time).format('dddd hh:mm A')}</Text>
                                    <Text numberOfLines={1} style={styles.scheduleDetail}>{lw.scheduleName}</Text>
                                </View>
                            ))
                        }
                    </View>
                )
                : null
        }
        {
            schedules.length > 0
                ? (
                    <View style={styles.box}>
                        <Text style={styles.scheduleHeader}>Next schedule</Text>
                        {
                            (schedules).map((us) => (
                                <View key={us.time} style={{ marginBottom: Dimension.HEIGHT_30 / 2 }}>
                                    <Text numberOfLines={1} style={styles.scheduleDetail}>
                                        {`${moment(new Date(us.time)).format('dddd hh:mm A')} `}
                                    </Text>
                                    <Text numberOfLines={1} style={styles.scheduleDetail}>
                                        {`water ${us.scheduleName}`}
                                    </Text>
                                </View>
                            ))
                        }
                    </View>
                )
                : null
        }
    </View>
);

export default ScheduleHistory;
