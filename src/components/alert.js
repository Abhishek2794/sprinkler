import { Alert } from 'react-native';

const alert = (title, message) => {
    Alert.alert(
        title,
        message,
        [
            { text: 'OK' },
        ],
        { cancelable: true },
    );
};

export default alert;
