import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import VectorIcon from 'react-native-vector-icons/AntDesign';
import {Colors, Fonts, Dimension} from '../constants';

const styles = StyleSheet.create({
  appBar: {
    flexDirection: 'row',
    height: Dimension.HEIGHT_60,
  },
  backNavigator: {
    alignItems: 'flex-start',
    flex: 2,
    height: Dimension.HEIGHT_60,
    justifyContent: 'center',
  },
  box: {
    flex: 1,
  },
  container: {
    height: Dimension.HEIGHT_60,
  },
  helpContainer: {
    alignItems: 'flex-end',
    flex: 2,
    height: Dimension.HEIGHT_60,
    justifyContent: 'center',
    paddingRight: 10,
  },
  icon: {
    marginLeft: 5,
    marginRight: 5,
  },
  title: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
    fontWeight: 'bold',
  },
  titleContainer: {
    alignItems: 'center',
    flex: 4,
    height: Dimension.HEIGHT_60,
    justifyContent: 'center',
  },
});

export const StatusBar = ({title, navigation, isBackNavigable}) => (
  <SafeAreaView>
    <View style={styles.container}>
      <LinearGradient
        style={styles.box}
        locations={[0.1, 1.0]}
        colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
        <View style={styles.appBar}>
          <View style={styles.backNavigator}>
            {isBackNavigable ? (
              <TouchableOpacity
                style={[styles.appBar, {alignItems: 'center'}]}
                onPress={() => navigation.goBack()}>
                <VectorIcon
                  color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                  name="back"
                  size={24}
                  style={styles.icon}
                />
              </TouchableOpacity>
            ) : null}
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title} numberOfLines={2}>
              {title}
            </Text>
          </View>
          <View style={styles.helpContainer}>
            <Icon
              name="ios-help-circle-outline"
              color={Colors.SWIPER_ACTIVE_DOT_COLOR}
              size={30}
            />
          </View>
        </View>
      </LinearGradient>
    </View>
  </SafeAreaView>
);

export default StatusBar;
