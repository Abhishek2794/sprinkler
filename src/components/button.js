import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Colors, Dimension, Fonts } from '../constants';

const styles = StyleSheet.create({
    button: {
        shadowOffset: {
            width: 0,
            height: 7,
        },
        borderRadius: 8,
        alignSelf: 'center',
        shadowColor: '#5d9f26',
        justifyContent: 'center',
        width: Dimension.WIDTH_300,
        height: Dimension.HEIGHT_50,
        backgroundColor: Colors.PRIMARY_COLOR_DARK,
    },
    buttonText: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: Fonts.FONT_SIZE_20,
        color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
});

const Button = ({ handleClick, text }) => (
    <TouchableOpacity
        style={styles.button}
        onPress={() => handleClick()}
    >
        <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
);

export default Button;
