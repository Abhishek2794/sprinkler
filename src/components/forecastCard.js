import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import moment from 'moment';
import { Colors, Dimension, Fonts } from '../constants';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: Dimension.HEIGHT_200,
        marginTop: Dimension.HEIGHT_20,
    },
    item: {
        marginBottom: 10,
        paddingBottom: 10,
        borderStyle: 'solid',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        height: Dimension.HEIGHT_30,
        borderBottomColor: Colors.PLACEHOLDER_TEXT,
    },
    text: {
        color: '#929292',
        fontSize: Fonts.FONT_SIZE_14,
    },
});

const ForecastCard = (props) => {
    const { forecast } = props;
    return (
        <View>
            <View style={styles.container}>
                {
                    forecast.map((prop) => (
                        <View key={prop.dt} style={styles.item}>
                            <View style={{ flex: 2 }}>
                                <Text numberOfLines={1} style={styles.text}>{moment.unix(prop.dt).format('dddd')}</Text>
                            </View>
                            <View style={{ flex: 1.5, alignItems: 'center' }}>
                                <Image style={{ width: Dimension.WIDTH_24, height: Dimension.WIDTH_24 }} source={{ uri: `http://openweathermap.org/img/w/${prop.weather[0].icon}.png` }} />
                            </View>
                            <View style={{ flex: 3 }}>
                                <Text numberOfLines={1} style={styles.text}>{prop.weather[0].description}</Text>
                            </View>
                            <View style={styles.tempRange}>
                                <Text style={[styles.text, { paddingLeft: 20 }]}>{`${Math.round(prop.temp.max)}/${Math.round(prop.temp.min)}\u2109`}</Text>
                            </View>
                        </View>
                    ))
                }
            </View>
        </View>
    );
};

export default ForecastCard;
