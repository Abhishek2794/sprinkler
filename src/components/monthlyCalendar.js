import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import config from '../helpers/config';
import {CalendarList} from 'react-native-calendars';
import Moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';

class MonthlyCalendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dates: {},
    };
    this.handleClick = this.handleClick.bind(this);
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (props.dates) {
      this.getMarkedDate(props.dates);
    }
  }

  handleClick(day) {
    const {handleClick} = this.props;
    handleClick(day.dateString);
  }

  getMarkedDate = (dates) => {
    if (dates) {
      const today = Moment().format('YYYY-MM-DD');
      dates[today] = {
        ...dates[today],
        marked: true,
        dotColor: '#7ed321',
        selected: true,
        selectedColor: '#429321',
      };
      AsyncStorage.getItem('@Sprinkler:HistoryLogs')
        .then((data) => {
          if (data) {
            const parsedData = JSON.parse(data);
            parsedData.logs.forEach((item) => {
              const date = moment(item.timestamp).format('YYYY-MM-DD');
              dates[date] = {
                marked: true,
                dotColor: '#7ed321',
                selected: true,
                selectedColor: '#007FFF',
              };
            });
            this.setState({
              dates,
            });
          } else {
            this.setState({
              dates,
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  render() {
    const startOfMonth = moment()
      .subtract(config.logHistoryDurationInDays, 'days')
      .format('YYYY-MM-DD');
    return (
      <CalendarList
        hideExtraDays
        horizontal
        onDayPress={(day) => this.handleClick(day)}
        markedDates={this.state.dates}
        minDate={startOfMonth}
        pastScrollRange={1}
        scrollEnabled
      />
    );
  }
}

const mapStateToProps = (state) => {
  const {dates} = state.calendarDates;
  return {dates};
};

export default connect(mapStateToProps)(MonthlyCalendar);
