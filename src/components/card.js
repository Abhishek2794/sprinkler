import React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import {Colors, Dimension} from '../constants';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 8,
    flexDirection: 'column',
    position: 'relative',
    padding: 24,
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    shadowOffset: {width: 0, height: 7},
    shadowOpacity: 1,
    shadowRadius: 18,
    flex: 1,
    marginTop: 24,
    marginBottom: Dimension.HEIGHT_70,
    width: '90%',
    alignItems: 'center',
  },
});

const Card = ({children}) => <View style={styles.container}>{children}</View>;

export default Card;
