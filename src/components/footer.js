import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors, Dimension } from '../constants';

const styles = StyleSheet.create({
    container: {
        bottom: 0,
        width: '100%',
        position: 'absolute',
        flexDirection: 'row',
        height: Dimension.HEIGHT_56,
        backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
});

const Footer = ({ children }) => <View style={styles.container}>{children}</View>;
export default Footer;
