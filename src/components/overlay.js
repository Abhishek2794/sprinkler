/* eslint-disable no-restricted-globals */
/* eslint-disable prefer-destructuring */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import watch from 'redux-watch';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ProgressBar} from 'react-native-paper';
import Spinner from 'react-native-spinkit';
import * as ActionCreators from '../actions';
import {
  Colors,
  Dimension,
  Fonts,
  WateringStatus,
  getWateringTypeName,
} from '../constants';
import store from '../helpers/store';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 5,
    borderWidth: 2,
    height: Dimension.HEIGHT_30,
    justifyContent: 'center',
    width: Dimension.WIDTH_63,
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    flexDirection: 'row',
  },
  buttonText: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_12,
  },
  countdown: {
    color: '#89CC00',
    fontSize: Fonts.FONT_SIZE_12,
  },
  image: {
    height: Dimension.HEIGHT_45,
    width: Dimension.WIDTH_115,
  },
  imageContainer: {
    alignItems: 'center',
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    height: Dimension.HEIGHT_45,
    justifyContent: 'center',
    marginBottom: 5,
    marginLeft: 5,
    marginTop: 5,
    width: Dimension.WIDTH_115,
  },
  imageSection: {
    alignItems: 'center',
    height: Dimension.HEIGHT_70,
    justifyContent: 'center',
    width: '30%',
  },
  indicator: {
    alignSelf: 'center',
    position: 'absolute',
    width: '90%',
    marginTop: 8,
  },
  infoContainer: {
    height: Dimension.HEIGHT_70,
    width: '70%',
  },
  innnerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  overlay: {
    alignItems: 'center',
    flexDirection: 'row',
    height: Dimension.HEIGHT_70,
    position: 'relative',
    width: '100%',
  },
  overlayContainer: {
    backgroundColor: '#3D550C',
    flexDirection: 'row',
    height: Dimension.HEIGHT_70,
    width: Dimension.WIDTH,
  },
  text: {
    color: '#89CC00',
  },
  textContainer: {
    flexDirection: 'column',
    paddingLeft: 15,
    paddingTop: 10,
  },
  timer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    width: '100%',
  },
});

class Overlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };

    this.handlePress = this.handlePress.bind(this);
    this.skipZone = this.skipZone.bind(this);
    this.stopSchedule = this.stopSchedule.bind(this);
  }

  getZoneImage = (zoneId) => {
    const {zones} = this.props;
    const zone = zones.find((item) => item.id === parseInt(zoneId));
    if (zone) {
      return zone.defaultImage;
    } else {
      return null;
    }
  };

  componentDidMount() {
    const watcher = watch(store.getState, 'currentSchedule.scheduleObj');
    this.unsubscribe = store.subscribe(
      watcher(() => {
        this.setState({loading: false});
      }),
    );
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  handlePress() {
    const {handlePress} = this.props;
    handlePress();
  }

  skipZone(wateringId, zoneId) {
    this.setState({loading: true});
    const {skipZone} = this.props;
    skipZone(wateringId, zoneId);
  }

  stopSchedule(wateringId) {
    this.setState({loading: true});
    const {stopSchedule} = this.props;
    stopSchedule(wateringId);
  }

  calculateTime(currentTime, totalDuration) {
    const mRemainingTime = totalDuration - currentTime;
    const response = {
      currentTime: moment().startOf('day').seconds(currentTime).format('mm:ss'),
      remainingTime: moment()
        .startOf('day')
        .seconds(mRemainingTime)
        .format('mm:ss'),
    };
    return response;
  }

  render() {
    let duration;
    const {loading} = this.state;
    const {scheduleObj, value} = this.props;
    const {_id, zones, status, type, schedule} = scheduleObj;
    const runningZone = zones.find(
      (item) => item.status === WateringStatus.STARTED,
    );
    if (runningZone) {
      duration = runningZone.scheduledDuration;
    } else {
      duration = 0;
    }
    const time = this.calculateTime(value, duration);
    const zoneImage = this.getZoneImage(runningZone._id);

    return (
      <View style={styles.overlayContainer}>
        <TouchableOpacity
          onPress={() => this.handlePress()}
          style={styles.overlay}>
          <View style={styles.imageSection}>
            <View style={styles.imageContainer}>
              <Image
                resizeMode="contain"
                source={
                  zoneImage
                    ? {uri: zoneImage}
                    : require('../../assets/images/zone-default/zone-default.png')
                }
                style={styles.image}
              />
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View style={styles.innnerContainer}>
              <View style={styles.textContainer}>
                <Text
                  style={styles.text}
                  numberOfLines={1}
                  ellipsizeMode="tail">{`${
                  runningZone.name
                } | ${getWateringTypeName(type, schedule)}`}</Text>
              </View>
              <View style={[{alignItems: 'flex-end'}, styles.textContainer]}>
                <View style={styles.buttonContainer}>
                  {loading ? (
                    <Spinner
                      color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      isVisible
                      size={20}
                      type="ThreeBounce"
                    />
                  ) : (
                    <TouchableOpacity
                      onPress={() => this.stopSchedule(_id)}
                      style={{marginRight: 12}}>
                      <MaterialIcon
                        name="stop-circle"
                        size={32}
                        color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      />
                    </TouchableOpacity>
                  )}
                  {loading ? (
                    <Spinner
                      color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      isVisible
                      size={20}
                      type="ThreeBounce"
                    />
                  ) : (
                    <TouchableOpacity
                      onPress={() => this.skipZone(_id, runningZone._id)}>
                      <MaterialIcon
                        name="skip-next-circle"
                        size={32}
                        color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
            <View style={styles.timer}>
              <Text style={styles.countdown}>{time.currentTime}</Text>
              <Text style={styles.countdown}>{time.remainingTime}</Text>
            </View>
            <ProgressBar
              progress={
                isNaN((value * 1000) / (duration * 1000))
                  ? 0
                  : (value * 1000) / (duration * 1000)
              }
              color={Colors.SWIPER_ACTIVE_DOT_COLOR}
              style={styles.indicator}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {scheduleObj} = state.currentSchedule;
  const {value} = state.progress;
  const {zones} = state.zones;
  return {scheduleObj, value, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Overlay);
