import React from 'react';
import {
    Image,
    TouchableOpacity,
} from 'react-native';

export const HelpButton = (
    <TouchableOpacity onPress={() => {}}>
        <Image source={require('../../assets/images/help/help.png')} />
    </TouchableOpacity>
);

export default HelpButton;
