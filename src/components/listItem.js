/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Colors, Dimension, Fonts } from '../constants';

const styles = StyleSheet.create({
    container: {
        flex: 2,
        flexDirection: 'column',
    },
    formEdit: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    formElement: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: '#dfdfe4',
        paddingTop: Dimension.HEIGHT_20 / 2,
        paddingBottom: Dimension.HEIGHT_20 / 2,
    },
    formField: {
        color: Colors.HEADER_COLOR,
        fontSize: Fonts.FONT_SIZE_16,
    },
    formFieldValue: {
        borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
        borderWidth: 2,
        color: '#929292',
        fontSize: Fonts.FONT_SIZE_14,
    },
});

export default class ListItem extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const { onClick, type } = this.props;
        onClick(type);
    }

    render() {
        const { text, title } = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.handleClick()}
                style={styles.formElement}
            >
                <View style={styles.container}>
                    <Text style={styles.formField}>{title}</Text>
                    <Text numberOfLines={1} style={styles.formFieldValue}>{text}</Text>
                </View>
                <View style={styles.formEdit}>
                    <Icon name="keyboard-arrow-right" size={24} color="#929292" />
                </View>
            </TouchableOpacity>
        );
    }
}
