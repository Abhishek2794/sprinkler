/* eslint-disable no-useless-escape */
/* eslint-disable class-methods-use-this */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import * as ActionCreators from '../actions';
import store from '../helpers/store';
import { appActions } from '../actions/app.actions';
import { userServices } from '../services/user.services';
import { Fonts, Colors, Dimension } from '../constants';

const styles = StyleSheet.create({
    addButton: {
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimension.WIDTH_135,
        height: Dimension.HEIGHT_50,
        marginTop: Dimension.HEIGHT_30,
        backgroundColor: Colors.PRIMARY_COLOR_DARK,
    },
    addButtonText: {
        fontWeight: '600',
        fontSize: Fonts.FONT_SIZE_15,
        color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
    container: {
        alignItems: 'center',
        marginTop: Dimension.HEIGHT_70,
    },
    logoutButton: {
        borderWidth: 2,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors.DANGER,
        width: Dimension.WIDTH_135,
        height: Dimension.HEIGHT_50,
        backgroundColor: 'transparent',
        marginTop: Dimension.HEIGHT_20 / 2,
    },
    logoutButtonText: {
        color: Colors.DANGER,
        fontSize: Fonts.FONT_SIZE_15,
    },
    message: {
        fontWeight: '600',
        textAlign: 'center',
        fontSize: Fonts.FONT_SIZE_15,
        color: Colors.PRIMARY_COLOR_DARK,
    },
});

class NoDevice extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleClick() {
        const { navigation } = this.props;
        navigation.navigate('AddDevice');
    }

    handleLogout() {
        const { userData } = this.props;
        const topic = userData.email.replace(/[&\/\\@#_,+()$~%.'":*?<>{}]/g, '-');
        messaging().unsubscribeFromTopic(topic);
        userServices.logout(userData.email)
            .then(() => store.dispatch(appActions.checkLogin(false)));
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/images/watering/watering.png')} />
                <Text style={styles.message}>Ahh! seems like you don&#8217;t</Text>
                <Text style={styles.message}>have any device associated</Text>
                <Text style={styles.message}>with your account.</Text>
                <TouchableOpacity
                    style={styles.addButton}
                    onPress={() => this.handleClick()}
                >
                    <Text style={styles.addButtonText}>ADD DEVICE</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.logoutButton}
                    onPress={() => this.handleLogout()}
                >
                    <Text style={styles.logoutButtonText}>LOGOUT</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { userData } = state.user;
    return { userData };
};

const mapDispatchToProps = (dispatch) => bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoDevice);
