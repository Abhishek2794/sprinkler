import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import {Colors, Dimension, Fonts} from '../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: Dimension.HEIGHT_40,
  },
  buttonText: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_14,
    color: Colors.PRIMARY_COLOR_DARK,
  },
  container: {
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    width: Dimension.WIDTH_225,
    height: Dimension.HEIGHT_280,
    marginRight: Dimension.WIDTH_24,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  divider: {
    height: 1,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: Colors.PLACEHOLDER_TEXT,
  },
  header: {
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_16,
    color: Colors.PRIMARY_COLOR_DARK,
  },
  image: {
    alignSelf: 'center',
    height: Dimension.HEIGHT_90,
    marginTop: 10,
    width: Dimension.WIDTH_100,
  },
  message: {
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_14,
  },
  section: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 12,
    marginBottom: Dimension.HEIGHT_20,
  },
  shape: {
    height: 2,
    marginTop: 7,
    marginLeft: 15,
    marginRight: 15,
    width: Dimension.WIDTH_20,
    backgroundColor: Colors.PRIMARY_COLOR_DARK,
  },
});

const InfoCard = () => (
  <View style={styles.section}>
    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/tips_image_1/tips_image_1.png')}
          style={styles.image}
        />
        <Text style={styles.header}>{`Water deeply${'\n'}and slowly`}</Text>
        <Text style={styles.shape}>-</Text>
        <Text numberOfLines={2} style={styles.message}>
          Water should access all parts of soil and roots for better results
        </Text>
        <Text style={styles.divider}>-</Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Know More</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/tips_image_2/tips_image_2.png')}
          style={styles.image}
        />
        <Text style={styles.header}>{`Find Average${'\n'}Moisture needs`}</Text>
        <Text style={styles.shape}>-</Text>
        <Text numberOfLines={3} style={styles.message}>
          Get useful tips of container plant watering
        </Text>
        <Text style={styles.divider}>-</Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Know More</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  </View>
);

export default InfoCard;
