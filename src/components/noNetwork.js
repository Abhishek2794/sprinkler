/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Colors } from '../constants';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
});
const NoNetwork = () => (
    <View style={styles.container}>
        <Icon name="wifi-off" size={40} color={Colors.OFFLINE_STATUS} />
        <Text>No Internet Connection</Text>
    </View>
);

export default NoNetwork;
