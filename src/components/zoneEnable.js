import React, {Component} from 'react';
import {StyleSheet, Text, SafeAreaView} from 'react-native';
import Modal, {
  ModalButton,
  ModalContent,
  ModalFooter,
  SlideAnimation,
} from 'react-native-modals';
import {Fonts, Dimension} from '../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    borderTopWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: '#d2d2d1',
  },
  text: {
    color: '#929292',
    textAlign: 'center',
    fontSize: Fonts.FONT_SIZE_20,
  },
});

export default class ZoneEnable extends Component {
  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const {onPress} = this.props;
    onPress();
  }

  closeModal() {
    const {closePopup} = this.props;
    closePopup();
  }

  render() {
    const {modalVisible} = this.props;
    return (
      <Modal
        modalAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        onTouchOutside={() => this.closeModal()}
        width={Dimension.WIDTH_338}
        visible={modalVisible}
        footer={
          <ModalFooter>
            <ModalButton
              align="center"
              key="CancelButton"
              onPress={() => this.closeModal()}
              style={styles.button}
              text="Dismiss"
              textStyle={styles.text}
            />
            <ModalButton
              align="center"
              key="SetButton"
              onPress={() => this.onClick()}
              style={styles.button}
              text="Enable"
              textStyle={[styles.text, {color: '#ff7575'}]}
            />
          </ModalFooter>
        }>
        <ModalContent>
          <SafeAreaView
            style={{
              marginTop: Dimension.HEIGHT_20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={styles.text}>
              This zone is disabled. Would you like to enable it?
            </Text>
          </SafeAreaView>
        </ModalContent>
      </Modal>
    );
  }
}
