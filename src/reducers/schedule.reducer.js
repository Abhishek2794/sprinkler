import { appConstants } from '../constants';

export default (state = { scheduleData: {} }, action) => {
    switch (action.type) {
    case appConstants.SCHEDULE_DATA_SUCCESS:
        return { scheduleData: action.data };
    case appConstants.SCHEDULE_DATA_FAILURE:
        return { schedule_error: action.error };
    default:
        return { ...state };
    }
};
