import { appConstants } from '../constants';

export default (state = { zones: [], zonesError: '' }, action) => {
    switch (action.type) {
    case appConstants.ZONES_DATA_SUCCESS:
        return { zones: action.data, zonesError: '' };
    case appConstants.ZONES_DATA_FAILURE:
        return { zones: [], zonesError: action.error };
    default:
        return { ...state };
    }
};
