import {appConstants} from '../constants';

export default (state = {online: false}, action) => {
  switch (action.type) {
    case appConstants.DEVICE_ONLINE_STATUS:
      return {online: action.data};
    case appConstants.SET_DEVICE_STATUS:
      return {
        online: action.data.isOnline,
      };
    default:
      return {...state};
  }
};
