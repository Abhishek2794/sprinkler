import { appConstants } from '../constants';

export default (state = { timeslotData: [] }, action) => {
    switch (action.type) {
    case appConstants.TIMESLOT:
        return { timeslotData: action.data };
    case appConstants.TIMESLOT_ERROR:
        return { timeslotError: action.error };
    default:
        return { ...state };
    }
};
