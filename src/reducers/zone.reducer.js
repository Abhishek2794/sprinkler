import { appConstants } from '../constants';

export default (state = { zone: {} }, action) => {
    switch (action.type) {
    case appConstants.ZONE_DATA:
        return { zone: action.data };
    default:
        return { ...state };
    }
};
