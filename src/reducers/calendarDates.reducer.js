import { appConstants } from '../constants';

export default (state = {}, action) => {
    switch (action.type) {
    case appConstants.CALENDAR_DATES:
        return { dates: action.dates };
    default:
        return state;
    }
};
