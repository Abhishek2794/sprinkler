import {appConstants} from '../constants';

export default (state = {schedules: null, schedules_error: ''}, action) => {
  switch (action.type) {
    case appConstants.SCHEDULES_DATA_SUCCESS:
      return {schedules: action.data};
    case appConstants.SCHEDULES_DATA_FAILURE:
      return {schedules_error: action.error};
    case appConstants.SCHEDULE_DELETE_SUCCESS:
      return {
        schedules: state.schedules.filter((item) => item.id !== action.data.id),
      };
    default:
      return {...state};
  }
};
