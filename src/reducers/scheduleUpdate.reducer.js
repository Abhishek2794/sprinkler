import { userConstants } from '../constants';

export default (state = {}, action) => {
    switch (action.type) {
    case userConstants.SCHEDULE_UPDATE_SUCCESS:
        return { update: true };
    case userConstants.SCHEDULE_UPDATE_FAILURE:
        return { update: false, error: action.error };
    default:
        return state;
    }
};
