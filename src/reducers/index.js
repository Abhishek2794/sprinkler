/* eslint-disable no-param-reassign */
import { combineReducers } from 'redux';
import { appConstants, userConstants } from '../constants';

import activeDevice from './activeDevice.reducer';
import app from './app.reducer';
import appLoaded from './appLoaded.reducer';
import authentication from './authentication.reducer';
import calendarDates from './calendarDates.reducer';
import controller from './controller.reducer';
import controllerRegistration from './controllerRegistration.reducer';
import currentSchedule from './currentSchedule.reducer';
import device from './device.reducer';
import deviceOnline from './deviceOnline.reducer';
import devices from './devices.reducer';
import docker from './docker.reducer';
import forecast from './forecast.reducer';
import login from './login.reducer';
import logout from './logout.reducer';
import logs from './logs.reducer';
import networkList from './networkList.reducer';
import noDevice from './noDevice.reducer';
import openWeather from './openWeather.reducer';
import progress from './progress.reducer';
import registration from './registration.reducer';
import schedule from './schedule.reducer';
import schedules from './schedules.reducer';
import scheduleCreate from './scheduleCreate.reducer';
import scheduleHistory from './scheduleHistory.reducer';
import scheduleUpdate from './scheduleUpdate.reducer';
import scheduleZones from './scheduleZones.reducer';
import scheduledZones from './scheduledZones.reducer';
import switchDevice from './switchDevice.reducer';
import timeslot from './timeslot.reducer';
import upcomingSchedule from './upcomingSchedule.reducer';
import user from './user.reducer';
import scheduleTimetable from './scheduleTimetable.reducer';
import weather from './weather.reducer';
import zone from './zone.reducer';
import zones from './zones.reducer';

const appReducer = combineReducers({
    activeDevice,
    app,
    appLoaded,
    authentication,
    calendarDates,
    controller,
    controllerRegistration,
    currentSchedule,
    device,
    deviceOnline,
    devices,
    docker,
    forecast,
    login,
    logout,
    logs,
    networkList,
    noDevice,
    openWeather,
    progress,
    registration,
    schedule,
    schedules,
    scheduleCreate,
    scheduleHistory,
    scheduleTimetable,
    scheduleUpdate,
    scheduleZones,
    scheduledZones,
    switchDevice,
    timeslot,
    upcomingSchedule,
    user,
    weather,
    zone,
    zones,
});

const rootReducer = (state, action) => {
    if (action.type === appConstants.DEVICE_RESET || action.type === userConstants.USER_LOGOUT) {
        state = undefined;
    }
    return appReducer(state, action);
};

export default rootReducer;
