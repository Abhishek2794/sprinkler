import {appConstants} from '../constants';

export default (state = {allDevices: []}, action) => {
  switch (action.type) {
    case appConstants.DEVICES_DATA_SUCCESS:
      return {allDevices: action.data};

    case appConstants.SET_DEVICE_STATUS:
      let allDevices = state.allDevices.slice();
      if (allDevices) {
        allDevices = allDevices.map((item) => {
          if (item._id == action.data._id) {
            return {
              ...action.data
            };
          } else {
            return item;
          }
        });
      }
      return {
        allDevices,
      };
    default:
      return {...state};
  }
};
