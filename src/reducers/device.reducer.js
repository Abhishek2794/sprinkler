import { appConstants } from '../constants';

export default (state = {}, action) => {
    switch (action.type) {
    case appConstants.DEVICE_DATA_SUCCESS:
        return { device_update: true };
    case appConstants.DEVICE_DATA_FAILURE:
        return { device_update: false };
    default:
        return state;
    }
};
