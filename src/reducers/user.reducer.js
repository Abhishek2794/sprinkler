import { userConstants } from '../constants';

export default (state = { userData: {}, userDataError: '' }, action) => {
    switch (action.type) {
    case userConstants.USER_DATA:
        return { userData: action.data };
    case userConstants.USER_DATA_ERROR:
        return { userData: {}, userDataError: action.error };
    default:
        return { ...state };
    }
};
