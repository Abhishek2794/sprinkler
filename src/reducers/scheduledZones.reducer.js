import { appConstants } from '../constants';

export default (state = {}, action) => {
    switch (action.type) {
    case appConstants.SCHEDULED_ZONES_DATA_SUCCESS:
        return { zones: action.data };
    case appConstants.SCHEDULED_ZONES_DATA_FAILURE:
        return { zones_error: action.error };
    default:
        return state;
    }
};
