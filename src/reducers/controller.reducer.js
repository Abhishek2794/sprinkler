import { appConstants } from '../constants';

export default (state = { controllerData: {} }, action) => {
    switch (action.type) {
    case appConstants.CONTROLLER_DATA_SUCCESS:
        return { controllerData: action.data };
    case appConstants.CONTROLLER_DATA_FAILURE:
        return { controllerData: action.error };
    default:
        return { ...state };
    }
};
