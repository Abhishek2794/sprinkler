import { appConstants } from '../constants';

export default (state = {}, action) => {
    switch (action.type) {
    case appConstants.SCHEDULE_ZONES_REQUEST:
        return { zones: action.zones };
    default:
        return state;
    }
};
