import { appConstants } from '../constants';

export default (state = { scheduleObj: {} }, action) => {
    switch (action.type) {
    case appConstants.CURRENT_SCHEDULE:
        return { scheduleObj: action.data };
    default:
        return { ...state };
    }
};
