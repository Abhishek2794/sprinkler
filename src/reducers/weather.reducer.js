import { appConstants } from '../constants';

export default (state = { weatherData: {}, weatherError: '' }, action) => {
    switch (action.type) {
    case appConstants.WEATHER_DATA_SUCCESS:
        return { weatherData: action.data };
    case appConstants.WEATHER_DATA_FAILURE:
        return { weatherError: action.error };
    default:
        return { ...state };
    }
};
