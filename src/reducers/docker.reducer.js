import { userConstants } from '../constants';

export default (state = { docked: true }, action) => {
    switch (action.type) {
    case userConstants.DOCK_VIEW:
        return { docked: action.status };
    default:
        return { ...state };
    }
};
