import { appConstants } from '../constants';

export default (state = { value: 0 }, action) => {
    switch (action.type) {
    case appConstants.PROGRESS_VALUE:
        return { value: action.data };
    default:
        return { ...state };
    }
};
