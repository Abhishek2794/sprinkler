import { userConstants } from '../constants';

export default (state = { schedule: {} }, action) => {
    switch (action.type) {
    case userConstants.SCHEDULE_CREATE_REQUEST:
        return { schedule: action.schedule };
    case userConstants.SCHEDULE_CREATE_SUCCESS:
        return { status: true };
    case userConstants.SCHEDULE_CREATE_FAILURE:
        return { status: false, error: action.error };
    default:
        return { ...state };
    }
};
