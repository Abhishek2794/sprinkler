import { appConstants } from '../constants';

export default (state = { networks: {}, error: '' }, action) => {
    switch (action.type) {
    case appConstants.NETWORK_LIST_SUCCESS:
        return { networks: action.data, error: '' };
    case appConstants.NETWORK_LIST_FAILURE:
        return { error: action.error, networks: {} };
    default:
        return { ...state };
    }
};
