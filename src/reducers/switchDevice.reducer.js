import { appConstants } from '../constants';

export default (state = { loadingDevice: false }, action) => {
    switch (action.type) {
    case appConstants.SWITCH_DEVICE:
        return { loadingDevice: action.data };
    default:
        return { ...state };
    }
};
