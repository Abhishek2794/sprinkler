import { appConstants } from '../constants';

export default (state = { status: false }, action) => {
    switch (action.type) {
    case appConstants.NO_DEVICE:
        return { status: action.status };
    default:
        return { ...state };
    }
};
