import {appConstants} from '../constants';

export default (state = {device: {}}, action) => {
  switch (action.type) {
    case appConstants.ACTIVE_DEVICE:
      return {device: action.data};
    case appConstants.SET_DEVICE_STATUS:
      const device = {
        ...action.data,
      };
      return {
        device,
      };
    default:
      return {...state};
  }
};
