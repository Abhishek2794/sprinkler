import {appConstants, userConstants} from '../constants';

export default (state = {timetable: [], timetableError: ''}, action) => {
  switch (action.type) {
    case userConstants.DOCK_VIEW:
      return {timetable: [], timetableError: ''};
    case appConstants.SCHEDULE_TIMETABLE_SUCCESS:
      return {timetable: action.data};
    case appConstants.SCHEDULE_TIMETABLE_FAILURE:
      return {timetable: [], timetableError: action.error};
    default:
      return {...state};
  }
};
