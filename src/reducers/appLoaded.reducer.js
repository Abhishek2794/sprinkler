import { appConstants } from '../constants';

export default (state = { loaded: false }, action) => {
    switch (action.type) {
    case appConstants.APP_LOADED:
        return { loaded: action.status };
    default:
        return { ...state };
    }
};
