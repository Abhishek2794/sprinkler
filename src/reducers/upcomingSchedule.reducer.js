import {appConstants} from '../constants';

export default (state = {scheduleRecord: []}, action) => {
  switch (action.type) {
    case appConstants.UPCOMING_SCHEDULES:
      let zoneName = '';
      let duration = 0;
      const upcoming = action.data.map((item) => {
        item.zones.forEach((zone) => {
          duration += zone.duration;
          zoneName += `${zone.value.name}, `;
        });
        return {
          ...item,
          zoneName,
          duration,
        };
      });
      return {scheduleRecord: upcoming};
    case appConstants.UPCOMING_SCHEDULES_ERROR:
      return {scheduleRecordError: action.error};
    default:
      return {...state};
  }
};
