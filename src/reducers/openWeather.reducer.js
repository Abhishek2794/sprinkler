import { appConstants } from '../constants';

export default (state = { openWeatherData: {} }, action) => {
    switch (action.type) {
    case appConstants.OPEN_WEATHER:
        return { openWeatherData: action.data };
    default:
        return { ...state };
    }
};
