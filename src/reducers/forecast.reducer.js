import { appConstants } from '../constants';

export default (state = { forecastData: {}, forecastError: '' }, action) => {
    switch (action.type) {
    case appConstants.FORECAST_DATA_SUCCESS:
        return { forecastData: action.data };
    case appConstants.FORECAST_DATA_FAILURE:
        return { forecastError: action.error };
    default:
        return { ...state };
    }
};
