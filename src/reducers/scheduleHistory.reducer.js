import { appConstants } from '../constants';

export default (state = { history: [] }, action) => {
    switch (action.type) {
    case appConstants.SCHEDULE_HISTORY_SUCCESS:
        return { history: action.data };
    case appConstants.SCHEDULE_HISTORY_FAILURE:
        return { historyError: action.error };
    default:
        return { ...state };
    }
};
