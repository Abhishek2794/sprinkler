import { appConstants } from '../constants';

export default (state = { history: [] }, action) => {
    switch (action.type) {
    case appConstants.DEVICE_LOGS:
        return { history: action.data };
    default:
        return { ...state };
    }
};
