import { userConstants } from '../constants';

export default (state = { logoutStatus: false }, action) => {
    switch (action.type) {
    case userConstants.USER_LOGOUT:
        return { logoutStatus: action.status };
    default:
        return { ...state };
    }
};
