import {userConstants} from '../constants';

export default (
  state = {boarded: false, networkError: false, lastRelease: '18 July 2023'},
  action,
) => {
  switch (action.type) {
    case userConstants.USER_BOARD_SUCCESS:
      return {...state, boarded: true};
    case userConstants.USER_BOARD_FAILURE:
      return {...state, boarded: false};
    case userConstants.NETWORK_ERROR:
      return {...state, networkError: action.status};
    default:
      return {...state};
  }
};
