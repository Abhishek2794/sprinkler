import { appConstants } from '../constants';

export default (state = {status: true}, action) => {
    switch (action.type) {
    case appConstants.CONTROLLER_DATA_SUCCESS:
        return { status: true, error: '' }; 
    case appConstants.CONTROLLER_REGISTER_SUCCESS:
        return { status: true, error: '' };
    case appConstants.CONTROLLER_REGISTER_FAILURE:
        return { status: false, error: action.error };
    default:
        return state;
    }
};
