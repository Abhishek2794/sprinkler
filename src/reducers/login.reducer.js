import { userConstants } from '../constants';

export default (state = { isLoggedIn: null }, action) => {
    switch (action.type) {
    case userConstants.USER_LOGGED_IN:
        return { isLoggedIn: action.status };
    default:
        return { ...state };
    }
};
