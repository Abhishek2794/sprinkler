import { userConstants } from '../constants';

export default (state = { error: '', login: false }, action) => {
    switch (action.type) {
    case userConstants.USER_LOGIN_SUCCESS:
        return { error: '', login: true, user: action.user };
    case userConstants.USER_LOGIN_FAILURE:
        return { login: false, error: action.error };
    default:
        return { ...state };
    }
};
