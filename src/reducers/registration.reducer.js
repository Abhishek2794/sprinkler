import { userConstants } from '../constants';

export default (state = { register: false, error: '' }, action) => {
    switch (action.type) {
    case userConstants.USER_REGISTRATION_SUCCESS:
        return { register: true, user: action.user, error: '' };
    case userConstants.USER_REGISTRATION_FAILURE:
        return { register: false, error: action.error };
    default:
        return { ...state };
    }
};
