import moment from 'moment';
import store from '../helpers/store';
import {logEvent} from '../analytics/index';

let slotsBooking = {};

export function generateCalenderPayload() {
  slotsBooking = {};
  const payload = [];
  const state = store.getState();
  const {device} = state.activeDevice;
  const {schedules} = state.schedules;
  const {zones} = state.zones;
  let schedulesCopy = JSON.parse(JSON.stringify(schedules));
  schedulesCopy = schedulesCopy.filter((item) => item.enabled);
  schedulesCopy = schedulesCopy.sort((a, b) => {
    const aTime = moment(a.startTime).format('HH:mm');
    const bTime = moment(b.startTime).format('HH:mm');
    if (moment(aTime, 'HH:mm').diff(moment(bTime, 'HH:mm'), 'seconds') >= 0) {
      return 1;
    } else {
      return -1;
    }
  });
  schedulesCopy.forEach((schedule) => {
    sortZones(schedule);
    schedule.zones.forEach((item) => {
      const zone = zones.find((ele) => ele._id === item.id);
      if (zone.enabled) {
        let days = findDaysDiff(schedule.startDate, schedule.endDate);
        days = days > 90 ? 90 : days;
        for (let i = 0; i < days; i++) {
          const date = moment(schedule.startDate).add(i, 'days');
          const slot = calculateTimeSlot(
            schedule.timeSlot,
            item.duration,
            date,
            schedule.days,
            schedule.endTime,
            schedule.startTime,
          );
          if (slot.startTime && slot.endTime) {
            payload.push({
              scheduleId: schedule._id,
              zoneId: zone._id,
              zoneNumber: zone.number,
              zoneName: zone.name,
              scheduleName: schedule.name,
              duration: item.duration,
              deviceId: device._id,
              masterValve: zone.masterValve,
              serialNumber: device.serialNumber,
              deviceAcknowledgement: null,
              ...slot,
            });
          }
        }
      }
    });
  });
  console.log(payload, slotsBooking);
  return {data: payload, serialNumber: device.serialNumber};
}

function sortZones(schedule) {
  switch (schedule.timeSlot) {
    case 0:
      // sort descending
      schedule.zones.sort((a, b) => b.id - a.id);
      break;
    case 1:
      // sort descending
      schedule.zones.sort((a, b) => b.id - a.id);
      break;
    case 2:
      // sort ascending
      schedule.zones.sort((a, b) => a.id - b.id);
      break;
    case 3:
      // sort ascending
      schedule.zones.sort((a, b) => a.id - b.id);
      break;
  }
}

function findDaysDiff(startDate, endDate) {
  const result = moment(endDate).diff(startDate, 'days');
  return result;
}

function calculateTimeSlot(timeSlot, duration, date, days, endTime, startTime) {
  let timeSlots = {};
  switch (timeSlot) {
    case 0:
      let beforeSunrise = moment(date);
      beforeSunrise = beforeSunrise.set({
        hour: '06',
        minute: '00',
        second: '00',
      });
      timeSlots = createSlotsBefore(beforeSunrise, duration, date, days);
      return timeSlots;
      break;
    case 1:
      let beforeSpecificTime = moment(date);
      const time = moment(endTime).toDate();
      beforeSpecificTime = beforeSpecificTime.set({
        hour: time.getHours(),
        minute: time.getMinutes(),
        second: '00',
      });
      timeSlots = createSlotsBefore(beforeSpecificTime, duration, date, days);
      return timeSlots;
      break;
    case 2:
      let aferSunset = moment(date);
      aferSunset = aferSunset.set({
        hour: '18',
        minute: '00',
        second: '00',
      });
      timeSlots = createSlotsAfter(aferSunset, duration, date, days);
      return timeSlots;
      break;
    case 3:
      let atSpecificTime = moment(date);
      const time2 = moment(startTime).toDate();
      atSpecificTime = atSpecificTime.set({
        hour: time2.getHours(),
        minute: time2.getMinutes(),
        second: '00',
      });
      timeSlots = createSlotsAfter(atSpecificTime, duration, date, days);
      return timeSlots;
      break;
  }
}

function createSlotsAfter(startTime, duration, date, days) {
  if (checkValidDay(days, date)) {
    const givenDate = moment(date).format('YYYY-MM-DD');
    if (slotsBooking[givenDate]) {
      let proposedTime = startTime;
      let depthIndex = 0;
      slotsBooking[givenDate].forEach((date, index) => {
        const to = moment(
          `${givenDate} ${date.to}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate();
        const from = moment(
          `${givenDate} ${date.from}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate();
        if (proposedTime >= from && proposedTime <= to) {
          proposedTime = to;
          depthIndex = index;
        }
      });
      const bookingFrom = moment(proposedTime).format('hh:mm:ss A');
      let bookingTo = moment(proposedTime).add(duration, 'seconds');
      bookingTo = moment(bookingTo).format('hh:mm:ss A');
      const currentSlots = slotsBooking[givenDate];
      slotsBooking = {
        ...slotsBooking,
        [givenDate]: [
          ...currentSlots.slice(0, depthIndex + 1),
          {from: bookingFrom, to: bookingTo},
          ...currentSlots.slice(depthIndex + 1),
        ],
      };
      return {
        day: parseInt(moment(givenDate).format('d')) + 1,
        startTime: moment(
          `${givenDate} ${bookingFrom}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
        endTime: moment(
          `${givenDate} ${bookingTo}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
      };
    } else {
      const from = moment(startTime).format('hh:mm:ss A');
      let to = moment(startTime).add(duration, 'seconds');
      to = moment(to).format('hh:mm:ss A');
      slotsBooking = {
        ...slotsBooking,
        [moment(startTime).format('YYYY-MM-DD')]: [
          {
            from,
            to,
          },
        ],
      };
      const formattedDate = moment(date).format('YYYY-MM-DD');
      return {
        day: parseInt(moment(date).format('d')) + 1,
        startTime: moment(
          `${formattedDate} ${from}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
        endTime: moment(
          `${formattedDate} ${to}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
      };
    }
  } else {
    return 0;
  }
}

function createSlotsBefore(startTime, duration, date, days) {
  if (checkValidDay(days, date)) {
    const givenDate = moment(date).format('YYYY-MM-DD');
    if (slotsBooking[givenDate]) {
      let proposedTime = startTime;
      let depthIndex = 0;
      slotsBooking[givenDate].forEach((date, index) => {
        const to = moment(
          `${givenDate} ${date.to}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate();
        const from = moment(
          `${givenDate} ${date.from}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate();
        if (proposedTime >= from && proposedTime <= to) {
          proposedTime = from;
          depthIndex = index;
        }
      });
      const bookingTo = moment(proposedTime).format('hh:mm:ss A');
      let bookingFrom = moment(proposedTime).subtract(duration, 'seconds');
      bookingFrom = moment(bookingFrom).format('hh:mm:ss A');
      const currentSlots = slotsBooking[givenDate];
      slotsBooking = {
        ...slotsBooking,
        [givenDate]: [
          ...currentSlots.slice(0, depthIndex + 1),
          {from: bookingFrom, to: bookingTo},
          ...currentSlots.slice(depthIndex + 1),
        ],
      };
      return {
        day: parseInt(moment(givenDate).format('d')) + 1,
        startTime: moment(
          `${givenDate} ${bookingFrom}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
        endTime: moment(
          `${givenDate} ${bookingTo}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
      };
    } else {
      const to = moment(startTime).format('hh:mm:ss A');
      let from = moment(startTime).subtract(duration, 'seconds');
      from = moment(from).format('hh:mm:ss A');
      slotsBooking = {
        ...slotsBooking,
        [moment(startTime).format('YYYY-MM-DD')]: [
          {
            from,
            to,
          },
        ],
      };
      const formattedDate = moment(date).format('YYYY-MM-DD');
      return {
        day: parseInt(moment(date).format('d')) + 1,
        startTime: moment(
          `${formattedDate} ${from}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
        endTime: moment(
          `${formattedDate} ${to}`,
          'YYYY-MM-DD HH:mm:ss A',
        ).toDate(),
      };
    }
  } else {
    return 0;
  }
}

function checkValidDay(days, date) {
  if (moment(date).diff(moment(), 'day') >= 0) {
    switch (days) {
      case 'Tuesday,Thursday,Saturday':
        if (parseInt(moment(date).format('d')) % 2 !== 0) {
          return true;
        } else {
          return false;
        }
        break;
      case 'Monday,Wednesday,Friday,Sunday':
        if (parseInt(moment(date).format('d')) % 2 === 0) {
          return true;
        } else {
          return false;
        }
        break;
      case 'Everyday':
        return true;
        break;
      default:
        const everyday = days.split(',');
        const currentDay = moment(date).format('dddd');
        if (everyday.includes(currentDay)) {
          return true;
        } else {
          return false;
        }
    }
  }
}
