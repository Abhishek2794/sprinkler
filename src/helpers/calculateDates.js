import moment from 'moment';
import {Colors} from '../constants';

function setCalendarDates(dates, color = Colors.PRIMARY_COLOR_DARK) {
  console.log('here', dates);
  let markedDates = {};
  for (let i = 0; i < dates.length; i += 1) {
    const key = moment(dates[i]).format('YYYY-MM-DD');
    const date = {
      [key]: {
        marked: true,
        dotColor: color,
      },
    };
    markedDates = Object.assign(markedDates, date);
  }

  return markedDates;
}

export default setCalendarDates;
