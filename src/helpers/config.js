import AsyncStorage from '@react-native-async-storage/async-storage';

async function fetchServerUrl() {
  const data = {
    name: 'Production',
    ip: 'http://greeninity.com:4000',
  };
  await AsyncStorage.removeItem('@Sprinkler:Server');
  const server = await AsyncStorage.getItem('@Sprinkler:Server');
  if (server) {
    const userServer = JSON.parse(server);
    return userServer.ip;
  } else {
    await AsyncStorage.setItem('@Sprinkler:Server', JSON.stringify(data));
    return data.ip;
  }
}

export async function config() {
  const ip = await fetchServerUrl();
  return {
    apiUrl: ip,
    controllerUrl: 'http://192.168.1.1',
    googleApiKey: 'AIzaSyCOswNk0arW7zJFnHT8t8j4KOFIUXF5i1g',
    googleApiUrl: 'https://maps.googleapis.com/maps/api/geocode/json',
    hereApiUrl: 'https://weather.cit.api.here.com/weather/1.0/report.json',
    hereAppCode: 'dJGau-E3DLLn4LTcIf7ODw',
    hereAppId: 'WJDXdps8FeVfsAACEo7e',
    logglyApiKey: '71a6b176-a423-4b76-a23c-6eeb1c903bfa',
    weatherApiKey: '00d5f6e844d89ddec79812d551e240f7',
    weatherApiUrl: 'https://api.openweathermap.org',
    historyApiUrl: 'https://history.openweathermap.org',
    logHistoryDurationInDays: 30,
  };
}

export default config;
