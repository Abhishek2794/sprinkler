import auth from '@react-native-firebase/auth';

export default function firebaseToken() {
    return (
        auth().currentUser.getIdToken()
            .then((token) => {
                const tokenString = token.toString();
                return tokenString;
            })
    );
}
