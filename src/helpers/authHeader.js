import {Platform} from 'react-native';
import VersionInfo from 'react-native-version-info';

export default function authHeader(token) {
  return {
    'Content-Type': 'application/json',
    'X-Authorization-Firebase': token,
    appType: Platform.OS,
    version: VersionInfo.appVersion,
    timezone: new Date().getTimezoneOffset(),
  };
}
