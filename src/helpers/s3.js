import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import {S3, GetObjectCommand, DeleteObjectCommand} from '@aws-sdk/client-s3';
import {getSignedUrl} from '@aws-sdk/s3-request-presigner';

export const s3Credentials = {
  endpoint: 'https://nyc3.digitaloceanspaces.com',
  region: 'nyc3',
  accessKey: 'TILCF66U46CDFJQHJIDE',
  secretKey: 'XgEG29YHl4KRNgK3C4htqhriNz4kvtXbMx6ZsyA9VIk',
  bucket: 'sprinkler-zone-images',
};

export const s3Client = new S3({
  endpoint: s3Credentials.endpoint,
  region: s3Credentials.region,
  credentials: {
    accessKeyId: s3Credentials.accessKey,
    secretAccessKey: s3Credentials.secretKey,
  },
});

async function getZoneImage(imageUrls) {
  if (Array.isArray(imageUrls) && imageUrls.length) {
    let defaultZone = imageUrls.find((item) => item.isDefault);
    if (!defaultZone) {
      defaultZone = imageUrls.slice(0, 1);
    }
    try {
      const response = await getSignedUrl(
        s3Client,
        new GetObjectCommand({
          Bucket: s3Credentials.bucket,
          Key: defaultZone.key,
        }),
      );
      return response;
    } catch (err) {
      return null;
    }
  }
  return null;
}

async function getImageViaKey(key) {
  if (key) {
    try {
      const response = await getSignedUrl(
        s3Client,
        new GetObjectCommand({
          Bucket: s3Credentials.bucket,
          Key: key,
        }),
      );
      return response;
    } catch (err) {
      return null;
    }
  }
  return null;
}

async function deleteImage(key) {
  try {
    await s3Client.send(
      new DeleteObjectCommand({
        Bucket: s3Credentials.bucket,
        Key: key,
      }),
    );
    return true;
  } catch (err) {
    return null;
  }
}

export {getZoneImage, getImageViaKey, deleteImage};
