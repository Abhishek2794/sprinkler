export function checkFirmwareVersion(newVersion, currentVersion) {
	let allowUpdate = false;
	if (newVersion) {
		const newVersionArr = newVersion.split(".").map(e => parseInt(e));
		const currentVersionArr = currentVersion.split(".").map(e => parseInt(e));
		if (newVersionArr.length === currentVersionArr.length) {
			newVersionArr.forEach((item, index) => {
				if (item > currentVersionArr[index]) {
					allowUpdate = true;
					return allowUpdate;
				}
			})
		}
	}
	return allowUpdate;
}