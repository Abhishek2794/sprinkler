import { userServices } from '../services';
import { appActions } from './app.actions';
import { userConstants, appConstants } from '../constants';

function dockView(status) {
    function update(data) { return { type: userConstants.DOCK_VIEW, status: data }; }

    return (dispatch) => {
        dispatch(update(status));
    };
}

function login(user) {
    function success(data) { return { type: userConstants.USER_LOGIN_SUCCESS, user: data }; }
    function failure(error) { return { type: userConstants.USER_LOGIN_FAILURE, error }; }

    return (dispatch) => userServices.login(user)
        .then(
            (res) => dispatch(success(res)),
            (error) => {
                dispatch(failure(error.toString()));
                dispatch(failure(''));
            },
        );
}

function logout(email) {
    function loggedOut() { return { type: userConstants.USER_LOGIN_FAILURE, error: '' }; }
    function success() { return { type: userConstants.USER_LOGOUT, status: true }; }
    function failure() { return { type: userConstants.USER_LOGOUT, status: false }; }

    return (dispatch) => userServices.logout(email)
        .then(async () => {
            dispatch(success());
            dispatch(loggedOut());
        })
        .catch(() => dispatch(failure()));
}

function register(user) {
    function success(data) { return { type: userConstants.USER_REGISTRATION_SUCCESS, user: data }; }
    function failure(error) { return { type: userConstants.USER_REGISTRATION_FAILURE, error }; }

    return (dispatch) => userServices.register(user)
        .then(
            (res) => dispatch(success(res)),
            (error) => {
                dispatch(failure(error.toString()));
                dispatch(failure(''));
            },
        );
}

function updateDevice(device) {
    function success() { return { type: appConstants.DEVICE_DATA_SUCCESS }; }
    function failure() { return { type: appConstants.DEVICE_LOGS_FAILURE }; }

    return (dispatch) => userServices.updateDevice(device)
        .then(() => {
            dispatch(success());
            dispatch(appActions.fetchDeviceList());
        })
        .catch(() => dispatch(failure()));
}

function updateSchedule(deviceId, schedule) {
    function success() { return { type: userConstants.SCHEDULE_UPDATE_SUCCESS }; }
    function failure(error) { return { type: userConstants.SCHEDULE_UPDATE_FAILURE, error }; }

    return (dispatch) => userServices.updateSchedule(deviceId, schedule)
        .then(() => dispatch(success()))
        .catch((error) => dispatch(failure(error)));
}

function createSchedule(deviceId, schedule) {
    function success() { return { type: userConstants.SCHEDULE_CREATE_SUCCESS }; }
    function failure(error) { return { type: userConstants.SCHEDUEL_CREATE_FAILURE, error }; }

    return (dispatch) => userServices.createSchedule(deviceId, schedule)
        .then(() => dispatch(success()))
        .catch((error) => dispatch(failure(error)));
}

function updateScheduleZones(zones) {
    function success() { return { type: userConstants.SCHEDULE_ZONE_UPDATE_SUCCESS }; }
    function failure(error) { return { type: userConstants.SCHEDULE_ZONE_UPDATE_FAILURE, error }; }

    return (dispatch) => userServices.updateScheduleZones(zones)
        .then(() => dispatch(success()))
        .catch((error) => dispatch(failure(error)));
}

function updateUserDetails(user) {
    function success() { return { type: userConstants.USER_UPDATE_SUCCESS }; }
    function failure() { return { type: userConstants.USER_UPDATE_FAILURE }; }

    return (dispatch) => userServices.updateUserDetails(user)
        .then(() => dispatch(success()))
        .catch(() => dispatch(failure()));
}

export const userActions = {
    dockView,
    login,
    logout,
    register,
    updateDevice,
    createSchedule,
    updateSchedule,
    updateUserDetails,
    updateScheduleZones,
};

export default userActions;
