/* eslint-disable no-use-before-define */

import AsyncStorage from '@react-native-async-storage/async-storage';
import {appServices} from '../services/app.services';
import {userConstants, appConstants} from '../constants';
import {recordError} from '../analytics';
import {getZoneImage} from '../helpers/s3';

function appLoaded(status) {
  function success(data) {
    return {type: appConstants.APP_LOADED, status: data};
  }
  return (dispatch) => {
    dispatch(success(status));
  };
}

function boardUser() {
  function success() {
    return {type: userConstants.USER_BOARD_SUCCESS};
  }
  return (dispatch) => {
    appServices.boardUser().then(() => dispatch(success()));
  };
}

function checkBoarding() {
  function success() {
    return {type: userConstants.USER_BOARD_SUCCESS};
  }
  function failure() {
    return {type: userConstants.USER_BOARD_FAILURE};
  }
  return (dispatch) => {
    appServices.checkBoarding().then((status) => {
      if (status) {
        dispatch(success());
      } else {
        dispatch(failure());
      }
    });
  };
}

function checkLogin(status) {
  function request(data) {
    return {type: userConstants.USER_LOGGED_IN, status: data};
  }
  return (dispatch) => {
    dispatch(request(status));
  };
}

function networkError(status) {
  function request(data) {
    return {type: userConstants.NETWORK_ERROR, status: data};
  }
  return (dispatch) => {
    dispatch(request(status));
  };
}

function createSchedule(schedule) {
  function request(data) {
    return {type: userConstants.SCHEDULE_CREATE_REQUEST, schedule: data};
  }
  return (dispatch) => {
    dispatch(request(schedule));
  };
}

function deviceListStatus(status) {
  function success(data) {
    return {type: appConstants.NO_DEVICE, status: data};
  }
  return (dispatch) => {
    dispatch(success(status));
  };
}

function fetchActiveDevice() {
  function onlineStatus(data) {
    return {type: appConstants.DEVICE_ONLINE_STATUS, data};
  }
  function openWeatherData(data) {
    return {type: appConstants.OPEN_WEATHER, data};
  }
  function activeDeviceAction(data) {
    return {type: appConstants.ACTIVE_DEVICE, data};
  }
  function zones(data) {
    return {type: appConstants.ZONES_DATA_SUCCESS, data};
  }
  function user(data) {
    return {type: userConstants.USER_DATA, data};
  }
  function allDevices(data) {
    return {type: appConstants.DEVICES_DATA_SUCCESS, data};
  }
  function setSchedule(data) {
    return {type: appConstants.CURRENT_SCHEDULE, data};
  }
  function setUpcomingSchedule(data) {
    return {type: appConstants.UPCOMING_SCHEDULES, data};
  }
  function setSchedule(data) {
    return {type: appConstants.SCHEDULES_DATA_SUCCESS, data};
  }
  return (dispatch) => {
    appServices
      .fetchDeviceList()
      .then((devices) => {
        dispatch(checkLogin(true));
        dispatch(allDevices(devices));
        dispatch(networkError(false));
        const activeDevice = devices.find((device) => device.selected);
        if (activeDevice) {
          dispatch(activeDeviceAction(activeDevice));
          dispatch(onlineStatus(activeDevice.isOnline));
        }
        appServices
          .fetchUserDetails()
          .then((data) => {
            dispatch(user(data));
            appServices
              .fetchCurrentSchedule()
              .then((data) => {
                if (data) {
                  dispatch(setSchedule(data));
                }
              })
              .catch((err) => {
                console.log('Fetch current schedule error', err);
              });
          })
          .catch((err) => console.log('user data error', err));
        if (activeDevice) {
          const [lng, lat] = activeDevice.address.location.coordinates;
          appServices
            .fetchOpenWeatherData({lat, lng})
            .then((data) => dispatch(openWeatherData(data)))
            .catch((err) => console.log('weather data error', err));
          appServices
            .fetchSchedules(activeDevice._id)
            .then((data) => dispatch(setSchedule(data)))
            .catch((err) => console.log('Error while fetching schedule', err));
          appServices
            .fetchZones(activeDevice._id)
            .then(async (data) => {
              const deviceZones = data.map((zone) => {
                return {
                  ...zone,
                  imageUrl: zone.imageUrl,
                };
              });
              const zoneImages = deviceZones.map(
                async (zone) => await getZoneImage(zone.imageUrl),
              );
              const allImages = await Promise.all(zoneImages);
              const updatedZone = allImages.map((defaultImage, index) => {
                return {
                  ...deviceZones[index],
                  defaultImage,
                };
              });
              dispatch(zones(updatedZone));
            })
            .catch((err) => console.log('zones error', err));
          appServices
            .fetchUpcomingSchedules(activeDevice._id)
            .then((data) => dispatch(setUpcomingSchedule(data)))
            .catch((err) => console.log('Upcoming schedule error', err));
        }
      })
      .catch((err) => {
        console.log('active device error', err);
      });
  };
}

function fetchCalendarDates(id) {
  function success(data) {
    return {type: appConstants.CALENDAR_DATES, dates: data};
  }
  return (dispatch) => {
    appServices.fetchCalendarDates(id).then((data) => dispatch(success(data)));
  };
}

function fetchCurrentSchedule(id) {
  function failure() {
    return {type: appConstants.CURRENT_SCHEDULE, data: {}};
  }
  function success(data) {
    return {type: appConstants.CURRENT_SCHEDULE, data};
  }
  return (dispatch) => {
    appServices
      .fetchCurrentSchedule(id)
      .then((data) => {
        dispatch(success(data));
      })
      .catch(() => dispatch(failure()));
  };
}

function fetchDeviceList() {
  function success(data) {
    return {type: appConstants.DEVICES_DATA_SUCCESS, data};
  }
  return (dispatch) => {
    appServices.fetchDeviceList().then((devices) => {
      dispatch(success(devices));
    });
  };
}

function fetchDeviceLogs(id) {
  function success(data) {
    return {type: appConstants.DEVICE_LOGS, data};
  }
  return (dispatch) => {
    appServices
      .fetchDeviceLogs(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => console.log(error));
  };
}

function saveToken(token) {
  AsyncStorage.setItem('@Sprinkler:deviceToken', token);
}

function fetchSchedule(schedule = null) {
  function success(data) {
    return {type: appConstants.SCHEDULE_DATA_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.SCHEDULE_DATA_FAILURE, error};
  }
  return (dispatch) => {
    if (schedule && schedule._id) {
      dispatch(success(schedule));
    } else {
      dispatch(failure(schedule));
    }
  };
}

function fetchScheduledZones(id) {
  function success(data) {
    return {type: appConstants.SCHEDULED_ZONES_DATA_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.SCHEDULED_ZONES_DATA_FAILURE, error};
  }
  return (dispatch) => {
    appServices
      .fetchScheduledZones(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchScheduleHistory(id) {
  function success(data) {
    return {type: appConstants.SCHEDULE_HISTORY_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.SCHEDULE_HISTORY_FAILURE, error};
  }
  return (dispatch) => {
    appServices
      .fetchScheduleHistory(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchSchedules(id) {
  function success(data) {
    return {type: appConstants.SCHEDULES_DATA_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.SCHEDULES_DATA_FAILURE, error};
  }
  return (dispatch) => {
    appServices
      .fetchSchedules(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchScheduleTimeTable(deviceId, timestamp) {
  function success(data) {
    return {type: appConstants.SCHEDULE_TIMETABLE_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.SCHEDULE_TIMETABLE_FAILURE, error};
  }
  return (dispatch) => {
    appServices
      .fetchScheduleTimeTable(deviceId, timestamp)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchTimeslot(id) {
  function success(data) {
    return {type: appConstants.TIMESLOT, data};
  }
  function failure(error) {
    return {type: appConstants.TIMESLOT_ERROR, error};
  }
  return (dispatch) => {
    appServices
      .fetchTimeslot(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchUpcomingSchedules(id) {
  function success(data) {
    return {type: appConstants.UPCOMING_SCHEDULES, data};
  }
  function failure(error) {
    return {type: appConstants.UPCOMING_SCHEDULES_ERROR, error};
  }
  return (dispatch) => {
    appServices
      .fetchUpcomingSchedules(id)
      .then((data) => dispatch(success(data)))
      .catch((error) => dispatch(failure(error)));
  };
}

function fetchUserDetails() {
  function success(data) {
    return {type: userConstants.USER_DATA, data};
  }
  function failure(error) {
    return {type: userConstants.USER_DATA_ERROR, error};
  }
  return (dispatch) => {
    appServices
      .fetchUserDetails()
      .then((data) => {
        dispatch(success(data));
      })
      .catch((error) => dispatch(failure(error)));
  };
}

function assignZone(zone) {
  return {type: appConstants.ZONE_DATA, data: zone};
}

function fetchZone(deviceId, zoneNumber) {
  function success(data) {
    return {type: appConstants.ZONE_DATA, data};
  }
  return (dispatch) => {
    appServices.fetchZone(deviceId, zoneNumber).then(async (data) => {
      let zone = {...data};
      if (zone.imageUrl) {
        try {
          const imageUrl = JSON.parse(zone.imageUrl);
          const defaultImage = await getZoneImage(zone.imageUrl);
          zone = {
            ...zone,
            imageUrl,
            defaultImage,
          };
        } catch (err) {
          zone = {
            ...zone,
            imageUrl: [],
          };
        }
      } else {
        zone = {
          ...zone,
          imageUrl: [],
        };
      }
      dispatch(success(data));
    });
  };
}

function updateZone(zoneData) {
  function success(data) {
    return {type: appConstants.ZONE_DATA, data};
  }
  return (dispatch) => {
    dispatch(success(zoneData));
  };
}

function fetchZones(id, zoneId = 0) {
  function updateZone(data) {
    return {type: appConstants.ZONE_DATA, data};
  }
  function success(data) {
    return {type: appConstants.ZONES_DATA_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.ZONES_DATA_FAILURE, error};
  }
  return (dispatch) => {
    appServices
      .fetchZones(id)
      .then(async (data) => {
        const zoneImages = data.map(
          async (zone) => await getZoneImage(zone.imageUrl),
        );
        const allImages = await Promise.all(zoneImages);
        const updatedZone = allImages.map((defaultImage, index) => {
          return {
            ...data[index],
            defaultImage,
          };
        });
        if (zoneId) {
          const activeZone = updatedZone.find((zone) => zone._id === zoneId);
          dispatch(updateZone(activeZone));
        }
        dispatch(success(updatedZone));
      })
      .catch((error) => dispatch(failure(error)));
  };
}

function registerController(controller) {
  function reset() {
    return {type: appConstants.CONTROLLER_REGISTER_RESET};
  }
  function success() {
    return {type: appConstants.CONTROLLER_REGISTER_SUCCESS};
  }
  function failure(error) {
    return {type: appConstants.CONTROLLER_REGISTER_FAILURE, error};
  }
  const {serialNumber} = controller;
  return (dispatch) => {
    appServices
      .registerController(serialNumber)
      .then((deviceToken) => {
        saveToken(deviceToken.token);
        appServices
          .addController(controller)
          .then((data) => {
            dispatch(success());
          })
          .catch((error) => {
            recordError('Error: Add Controller', error);
            dispatch(failure(error));
            dispatch(reset());
          });
      })
      .catch((error) => {
        recordError(
          'Error: Register Controller',
          'Device is already registered',
        );
        dispatch(failure('Device is already registered'));
        dispatch(reset());
      });
  };
}

function setControllerData(controller) {
  function success(data) {
    return {type: appConstants.CONTROLLER_DATA_SUCCESS, data};
  }
  function failure(error) {
    return {type: appConstants.CONTROLLER_DATA_FAILURE, error};
  }
  return (dispatch) => {
    if (controller) {
      dispatch(success(controller));
    } else {
      dispatch(failure(undefined));
    }
  };
}

function updateDeviceInfo(value) {
  function success(data) {
    return {type: appConstants.SET_DEVICE_STATUS, data};
  }
  return (dispatch) => {
    dispatch(success(value));
  };
}

function updateCurrentSchedule(value) {
  function success(data) {
    return {type: appConstants.CURRENT_SCHEDULE, data};
  }
  return (dispatch) => {
    dispatch(success(value));
  };
}

function setProgressValue(value) {
  function success(data) {
    return {type: appConstants.PROGRESS_VALUE, data};
  }
  return (dispatch) => {
    dispatch(success(value));
  };
}

function switchDevice(status) {
  function success(data) {
    return {type: appConstants.SWITCH_DEVICE, data};
  }
  return (dispatch) => {
    dispatch(success(status));
  };
}

function updateActiveDevice(device = null) {
  function onlineStatus(data) {
    return {type: appConstants.DEVICE_ONLINE_STATUS, data};
  }
  function success(data) {
    return {type: appConstants.ACTIVE_DEVICE, data};
  }
  return (dispatch) => {
    if (device) {
      dispatch(success(device));
      dispatch(onlineStatus(device.isOnline));
    }
  };
}

function logout() {
  function success() {
    return {type: userConstants.USER_LOGOUT};
  }
  return (dispatch) => {
    dispatch(success());
  };
}

function multipleQuickRun(payload) {
  function success(data) {
    return {type: appConstants.MULTIPLE_ZONE_RUN_SUCCESS, data};
  }
  function failure(data) {
    return {type: appConstants.MULTIPLE_ZONE_RUN_FAILURE, data};
  }
  return (dispatch) => {
    appServices
      .multipleQuickRun(payload)
      .then((response) => {
        dispatch(success('Success'));
      })
      .catch((err) => {
        failure(err.message);
      });
  };
}

function updateActiveDeviceState(paylaod) {
  function success(data) {
    return {type: appConstants.ACTIVE_DEVICE, data};
  }
  return (dispatch) => {
    dispatch(success(paylaod));
  };
}

export const appActions = {
  appLoaded,
  boardUser,
  checkBoarding,
  checkLogin,
  createSchedule,
  deviceListStatus,
  fetchActiveDevice,
  fetchCalendarDates,
  fetchCurrentSchedule,
  fetchDeviceList,
  fetchDeviceLogs,
  fetchSchedule,
  fetchSchedules,
  fetchScheduleHistory,
  fetchScheduledZones,
  fetchTimeslot,
  fetchScheduleTimeTable,
  fetchUpcomingSchedules,
  fetchUserDetails,
  fetchZone,
  fetchZones,
  registerController,
  setControllerData,
  updateDeviceInfo,
  setProgressValue,
  switchDevice,
  updateActiveDevice,
  logout,
  multipleQuickRun,
  updateActiveDeviceState,
  updateZone,
  assignZone,
  updateCurrentSchedule,
};

export default appActions;
