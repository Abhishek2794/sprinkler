import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { Colors, Fonts, Dimension } from '../../constants';
import * as Helpers from '../../helpers/modules';

const styles = StyleSheet.create({
    button: {
        borderRadius: 5,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimension.WIDTH_338,
        height: Dimension.HEIGHT_50,
        marginTop: Dimension.HEIGHT_30,
        backgroundColor: Colors.PRIMARY_COLOR_DARK,
    },
    buttonText: {
        fontWeight: '600',
        fontSize: Fonts.FONT_SIZE_15,
        color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    image: {
        alignSelf: 'center',
        marginTop: Dimension.HEIGHT_50,
        marginBottom: Dimension.HEIGHT_60,
    },
    text: {
        color: '#929292',
        fontWeight: '600',
        alignSelf: 'center',
        fontSize: Fonts.FONT_SIZE_20,
    },
    transparentButton: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        marginTop: Dimension.HEIGHT_20,
    },
});

export default class SetupFailedScreen extends Component {
    constructor(props) {
        super(props);
        this.handleNavigation = this.handleNavigation.bind(this);
    }

    handleNavigation(screen) {
        const { navigation } = this.props;
        navigation.navigate(screen);
    }

    render() {
        const { navigation } = this.props;
        return (
            <View style={{}}>
                <Helpers.StatusBar
                    subtitle=""
                    isBackNavigable={false}
                    child={Helpers.HelpButton}
                    title="Registration Failed"
                    navigation={navigation}
                />

                <View>
                    <Image
                        style={styles.image}
                        source={require('../../../assets/images/registered/registered.png')}
                    />
                    <Text style={styles.text}>Sorry it seems like this</Text>
                    <Text style={styles.text}>serial number is already</Text>
                    <Text style={styles.text}>registered in our system.</Text>

                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonText}> CONTACT SUPPORT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.transparentButton}
                        onPress={() => this.handleNavigation('DeviceNumber')}
                    >
                        <Text style={[styles.buttonText, { color: Colors.PRIMARY_COLOR_DARK }]}>CHANGE SERIAL NUMBER</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
