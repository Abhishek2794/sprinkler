/* eslint-disable class-methods-use-this */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prefer-stateless-function */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Helpers from '../../helpers/modules';
import * as ActionCreators from '../../actions';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  skipButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderColor: '#59a62b',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  home: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    color: '#9e9e9e',
    fontWeight: '600',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: Fonts.FONT_SIZE_20,
    marginTop: Dimension.HEIGHT_70,
  },
});

class SuccessMessageScreen extends Component {
  constructor(props) {
    super(props);
    this.checkDeviceLocation = this.checkDeviceLocation.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentDidMount() {
    store.dispatch(appActions.updateActiveDevice());
  }

  async checkDeviceLocation(type) {
    const postalCode = await AsyncStorage.getItem('@Sprinkler:postalCode');
    const {allDevices} = this.props;
    const filteredDevices = allDevices.filter(
      (device) => device.zipCode === postalCode,
    );
    if (filteredDevices.length > 0) {
      Alert.alert(
        'Sprinkler',
        `You have ${filteredDevices.length} devices nearby. Would you like to copy the configuration of any of the devices?`,
        [
          {
            text: 'Cancel',
            onPress: () => {
              this.handleNavigation();
            },
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              const {navigation} = this.props;
              navigation.navigate('CopyConfiguration');
              store.dispatch(appActions.updateActiveDevice());
              store.dispatch(appActions.fetchDeviceList());
            },
          },
        ],
        {cancelable: false},
      );
      return;
    }
    this.handleNavigation(type);
  }

  handleNavigation(type) {
    store.dispatch(appActions.fetchActiveDevice());
    if (type) {
      this.props.navigation.navigate('ZoneSetupScreen1');
    } else {
      store.dispatch(appActions.checkLogin(null));
    }
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle=""
          title="Add Device"
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <Image
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/paired/paired.png')}
          />
          <Text
            style={
              styles.text
            }>{`Yay!${'\n'}your device${'\n'}is successfully paired.`}</Text>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.checkDeviceLocation(0)}
            style={styles.skipButton}>
            <Text style={[styles.home, {color: '#59a62b'}]}>Skip </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.checkDeviceLocation(1)}
            style={styles.button}>
            <Text style={styles.home}>Zone Setup</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  return {allDevices};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SuccessMessageScreen);
