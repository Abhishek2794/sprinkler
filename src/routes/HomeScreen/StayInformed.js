import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors} from '../../constants';
import {Card, Text, Caption, Button} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 12,
    marginBottom: 12,
  },
  cardSection: {
    width: '49%',
    padding: 4,
    flexGrow: 1,
  },
  card: {
    flex: 1,
  },
  cardImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
    backgroundColor: '#fafafa',
  },
  actionBtn: {
    alignSelf: 'flex-end',
    flex: 1,
    alignItems: 'flex-end',
  },
});

class StayInformed extends React.Component {
  render() {
    const {zones, navigation} = this.props;
    return (
      <>
        <View style={styles.container}>
          <View style={styles.cardSection}>
            <Card style={[styles.card]}>
              <View style={styles.cardImage}>
                <MaterialCommunityIcons
                  name="watering-can"
                  size={34}
                  color={'#689F38'}
                />
              </View>
              <Card.Content style={{minHeight: 50}}>
                <Text>Configure Zones</Text>
                <Caption>Configure the zones for the controller</Caption>
              </Card.Content>
              <Card.Actions style={styles.actionBtn}>
                <Button
                  color={'#689F38'}
                  onPress={() => navigation.navigate('ZoneSetupScreen1')}>
                  Start
                </Button>
              </Card.Actions>
            </Card>
          </View>
          <View style={[styles.cardSection, {marginLeft: 12}]}>
            <Card style={styles.card}>
              <View style={styles.cardImage}>
                <MaterialCommunityIcons
                  name="calendar-clock"
                  size={34}
                  color={'#7B1FA2'}
                />
              </View>
              <Card.Content style={{minHeight: 50}}>
                <Text>Create Schedule</Text>
                <Caption>Configure new schedule for the controller</Caption>
              </Card.Content>
              <Card.Actions style={styles.actionBtn}>
                <Button
                  color={'#7B1FA2'}
                  onPress={async () => {
                    await AsyncStorage.setItem('addSchedule', 'true');
                    navigation.navigate('Schedules');
                  }}>
                  Create
                </Button>
              </Card.Actions>
            </Card>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.cardSection}>
            <Card style={styles.card}>
              <View style={styles.cardImage}>
                <MaterialCommunityIcons
                  name="wifi-strength-1-alert"
                  size={34}
                  color={'#E64A19'}
                />
              </View>

              <Card.Content style={{minHeight: 50}}>
                <Text>Poor Wifi Signal</Text>
                <Caption>You controller is having poor wifi signals.</Caption>
              </Card.Content>
              <Card.Actions style={styles.actionBtn}>
                <Button color={'#E64A19'}>Troubleshoot</Button>
              </Card.Actions>
            </Card>
          </View>
          <View style={[styles.cardSection, {marginLeft: 12}]}>
            <Card style={styles.card}>
              <View style={styles.cardImage}>
                <MaterialCommunityIcons
                  name="cloud-download"
                  size={34}
                  color={'#1976D2'}
                />
              </View>
              <Card.Content style={{minHeight: 50}}>
                <Text>Update App</Text>
                <Caption>New app version is available, please update</Caption>
              </Card.Content>
              <Card.Actions style={styles.actionBtn}>
                <Button color={'#1976D2'}>Update</Button>
              </Card.Actions>
            </Card>
          </View>
        </View>
      </>
    );
  }
}

export default StayInformed;
