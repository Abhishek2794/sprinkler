import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import store from '../../helpers/store';
import {userActions} from '../../actions';
import {appServices} from '../../services';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';
import {Colors, Dimension} from '../../constants';
import ForecastCard from '../../components/forecastCard';
import moment from 'moment';
import NoNetwork from '../../components/noNetwork';
import {
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  Text,
  Image,
  RefreshControl,
  TouchableOpacity,
  Alert,
} from 'react-native';
import InfoCard from '../../components/blog';
import {appActions} from '../../actions/app.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import NetInfo from '@react-native-community/netinfo';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import {logEvent} from '../../analytics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {checkFirmwareVersion} from '../../helpers/firmwareVersionCheck';
import FirmwareUpgrade from '../firmwareUpgrade';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f6f7',
    flex: 1,
  },
  header: {},
  userInfoBox: {
    height: 180,
    paddingHorizontal: 24,
    paddingVertical: 42,
  },
  avatarImage: {
    height: 52,
    width: 52,
  },
  headline: {
    color: '#fff',
    fontSize: 15,
  },
  userName: {
    fontSize: 20,
    fontWeight: '600',
    color: '#fff',
    marginBottom: 8,
  },
  weatherBox: {
    position: 'absolute',
    backgroundColor: '#fff',
    paddingVertical: 12,
    paddingHorizontal: 16,
    left: 24,
    height: 160,
    top: 120,
    width: 325,
    shadowColor: '#fff',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 100,
  },
  currentDate: {
    fontSize: 16,
    color: '#666',
    opacity: 0.7,
    fontWeight: '500',
    marginBottom: 8,
  },
  weather: {
    fontSize: 18,
    color: '#333',
    fontWeight: '500',
    paddingBottom: 8,
    opacity: 0.85,
    textTransform: 'capitalize',
  },
  border: {
    marginTop: 8,
    marginBottom: 16,
    borderBottomWidth: 0.5,
    borderBottomColor: '#dadada',
  },
  body: {
    marginTop: 110,
    paddingLeft: 24,
  },
  bodyView: {
    marginBottom: 16,
  },
  bodyHeader: {
    fontSize: 16,
    fontWeight: '500',
    color: '#333',
    opacity: 0.8,
  },
  forecastSection: {
    marginRight: 24,
    borderRadius: 8,
    shadowOpacity: 1,
    shadowRadius: 18,
    alignSelf: 'center',
    width: Dimension.WIDTH_338,
    paddingLeft: Dimension.WIDTH_20,
    paddingRight: Dimension.WIDTH_20,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  addDeviceBtn: {
    flexDirection: 'row',
    padding: 8,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: Colors.PRIMARY_COLOR,
    minWidth: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  device: {
    marginTop: 16,
    marginRight: 16,
    paddingVertical: 12,
    paddingHorizontal: 16,
    backgroundColor: '#fff',
    width: 240,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#dadada',
  },
  deviceName: {
    fontSize: 16,
    fontWeight: '500',
    color: '#333',
    marginVertical: 5,
  },
  deviceState: {
    opacity: 0.7,
    fontSize: 12,
  },
  deviceBox: {
    flexDirection: 'row',
  },
});

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      firmwareUpgrade: false,
    };
  }
  componentDidMount() {
    const {navigation, userData, device, allDevices} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      logEvent('HOME_SCREEN', {screen: 'HOME_SCREEN'});
      store.dispatch(userActions.dockView(true));
      NetInfo.fetch().then((state) => {
        if (!state.isConnected) {
          Alert.alert(
            'No Network',
            'App is not able to connect with the network, check your network connection',
            [
              {
                text: 'Close',
              },
            ],
            {cancelable: false},
          );
        }
      });
    });
    appServices.registerForPushNotifications();
    this.setState({
      firmwareUpgrade: checkFirmwareVersion(
        device.availableFirmwareUpdateVersion,
        device.currentFirmware,
      ),
    });
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userData?.id !== this.props.userData?.id) {
      const user = this.props.userData;
      crashlytics().log('User signed in.');
      Promise.all([
        crashlytics().setUserId(user.id.toString()),
        crashlytics().setAttributes({
          name: user.nickname,
          email: user.email,
        }),
        analytics().setUserId(user.id.toString()),
        analytics().setUserProperty('email', user.email),
        analytics().setUserProperty('userName', user.nickname),
      ]).then((data) => {
        console.log('Initialised Crash Analytics');
      });
    }
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    const {device} = this.props;
    store.dispatch(appActions.updateActiveDevice());
    store.dispatch(appActions.fetchDeviceList());
    store.dispatch(appActions.fetchCurrentSchedule(device._id));
    store.dispatch(appActions.fetchScheduleHistory(device._id));
    this.setState({refreshing: false});
  };

  switchController = (index) => {
    const {allDevices} = this.props;
    store.dispatch(appActions.switchDevice(true));
    AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
    appServices.setActiveDevice(allDevices[index]).then(() => {
      logEvent('DEVICE_ACTIVATED', {
        id: allDevices[index]?.id,
        name: allDevices[index]?.name,
      });
      store.dispatch(appActions.fetchActiveDevice());
    });
  };

  checkIfAnyActiveController = (device, allDevices) => {
    if (!device.name) {
      let index = 0;
      allDevices.forEach((item, index) => {
        if (item.isOnline) {
          index = index;
        }
      });
      this.switchController(index);
    }
  };

  render() {
    const {refreshing} = this.state;
    const {
      loadingDevice,
      userData,
      openWeatherData,
      allDevices,
      device,
      networkError,
    } = this.props;
    if (userData.id) {
      this.checkIfAnyActiveController(device, allDevices);
    }
    if (networkError) {
      return <NoNetwork />;
    }
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <Spinner visible={!userData.id} />
        <View style={styles.header}>
          <LinearGradient
            style={styles.userInfoBox}
            locations={[0.1, 1.0]}
            colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: 325,
              }}>
              <View>
                <Text style={styles.userName}>Hello {userData.nickname},</Text>
                <Text style={styles.headline}>
                  Manage your water resource quickly!
                </Text>
              </View>
              <View>
                <Image
                  style={styles.avatarImage}
                  source={require('../../../assets/images/avatar/avatar.png')}
                />
              </View>
            </View>
          </LinearGradient>
        </View>
        <View style={styles.weatherBox}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.currentDate}>
                {moment().format('Do MMM, YYYY')}
              </Text>
              <Text style={styles.weather}>
                {openWeatherData?.current?.weather[0]?.description}
              </Text>
            </View>
            <View>
              <Icon name="cloud" color={'#999'} size={50} />
            </View>
          </View>
          <View style={styles.border} />

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.weather}>
                {openWeatherData?.current?.temp + '\u2109'}
              </Text>
              <Text style={styles.currentDate}>Temperature</Text>
            </View>
            <View>
              <Text style={styles.weather}>
                {openWeatherData?.current?.humidity}
              </Text>
              <Text style={styles.currentDate}>Humidity</Text>
            </View>
            <View>
              <Text style={styles.weather}>
                {openWeatherData?.current?.wind_speed}
              </Text>
              <Text style={styles.currentDate}>Wind Speed</Text>
            </View>
          </View>
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefresh}
            />
          }>
          <View style={styles.body}>
            <View style={styles.bodyView}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginRight: 24,
                  alignItems: 'center',
                }}>
                <Text style={styles.bodyHeader}>My Devices</Text>
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => this.props.navigation.navigate('AddDevice')}>
                  <LinearGradient
                    locations={[0.1, 1.0]}
                    colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}
                    style={styles.addDeviceBtn}>
                    <Icon name="add" size={18} style={styles.headline} />
                    <Text style={styles.headline}>New Device</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={styles.deviceBox}>
                  {allDevices.map((item, index) => {
                    const isActive = device._id === item.id;
                    return (
                      <View key={item.id} style={styles.device}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <Image
                            style={styles.avatarImage}
                            source={require('../../../assets/images/device/device.png')}
                          />
                          <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {
                              if (!isActive) {
                                Alert.alert(
                                  'Switch Device',
                                  'Do you really want to connect with this device?',
                                  [
                                    {
                                      text: 'Dismiss',
                                    },
                                    {
                                      text: 'Yes',
                                      onPress: () =>
                                        this.switchController(index),
                                    },
                                  ],
                                  {cancelable: false},
                                );
                              }
                            }}
                            style={styles.addDeviceBtn}>
                            <Text style={{color: Colors.SECONDARY_COLOR}}>
                              {isActive ? 'Activated' : 'Touch to connect'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        <Text style={styles.deviceName}>{item.name}</Text>
                        <Text style={styles.deviceState}>
                          {!item.isOnline
                            ? `Device is offline since ${moment(
                                item.offlineTime,
                              ).fromNow()}`
                            : 'Device is online!'}
                        </Text>
                      </View>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
            <View style={styles.bodyView}>
              <Text style={styles.bodyHeader}>Upcoming Weather Report</Text>
            </View>
            <View style={styles.forecastSection}>
              {openWeatherData.daily ? (
                <ForecastCard forecast={openWeatherData.daily.slice(1, 6)} />
              ) : null}
            </View>
            <View style={{marginTop: 12}}>
              <Text style={styles.bodyHeader}>Stay Informed</Text>
              <InfoCard />
            </View>
          </View>
        </ScrollView>
        <FirmwareUpgrade
          device={device}
          visible={this.state.firmwareUpgrade}
          serialNumber={device.serialNumber}
          onClose={() =>
            this.setState({
              firmwareUpgrade: false,
            })
          }
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {history} = state.scheduleHistory;
  const {loadingDevice} = state.switchDevice;
  const {online} = state.deviceOnline;
  const {openWeatherData} = state.openWeather;
  const {scheduleRecord} = state.upcomingSchedule;
  const {userData} = state.user;
  const {networkError} = state.app;
  return {
    allDevices,
    device,
    history,
    loadingDevice,
    online,
    openWeatherData,
    scheduleRecord,
    userData,
    networkError,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
