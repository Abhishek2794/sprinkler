import React, {useReducer} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import store from '../../helpers/store';
import {userActions} from '../../actions';
import {appServices} from '../../services';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import NoNetwork from '../../components/noNetwork';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  RefreshControl,
  Alert,
  Modal,
  FlatList,
  TouchableOpacity,
  Platform,
  SafeAreaView,
} from 'react-native';
import {appActions} from '../../actions/app.actions';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NetInfo from '@react-native-community/netinfo';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import {logEvent} from '../../analytics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {checkFirmwareVersion} from '../../helpers/firmwareVersionCheck';
import FirmwareUpgrade from '../firmwareUpgrade';
import {
  Subheading,
  Caption,
  Title,
  Button,
  Card,
  Text,
} from 'react-native-paper';
import StayInformed from './StayInformed';
import NoDevice from '../../components/noDevice';
import * as Helpers from '../../helpers/modules';
import EventSource from 'react-native-sse';
import 'react-native-url-polyfill/auto';
import config from '../../helpers/config';
import firebaseToken from '../../helpers/token';
import {SseEvents} from '../../helpers/sseEvents';
import {getTimeSlotString, formatTime} from './helper';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f6f7',
    flex: 1,
  },
  header: {
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#dadada',
    borderBottomWidth: 1.5,
  },
  headerTitle: {
    opacity: 0.7,
    letterSpacing: 1,
  },
  offline: {
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#dadada',
    borderBottomWidth: 1,
  },
  addController: {
    margin: 12,
  },
  controllerCard: {
    padding: 8,
    borderColor: '#dadada',
    borderWidth: 0.5,
    marginBottom: 12,
    backgroundColor: '#fcfcfc',
  },
  carouselView: {
    padding: 12,
    backgroundColor: '#eee',
  },
  weatherCard: {
    backgroundColor: '#fff',
    padding: 8,
    borderColor: '#dadada',
    borderWidth: 1,
    borderRadius: 4,
    marginRight: 8,
    elevation: 1,
  },
  schedules: {
    marginTop: 12,
    flexDirection: 'row',
    flex: 1,
  },
  letterSpacing: {
    letterSpacing: 0.8,
  },
});

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      firmwareUpgrade: false,
      viewAllDevice: false,
    };
  }
  componentDidMount() {
    const {navigation, userData, device, allDevices} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      this.initialize = false;
      this.setState({viewAllDevice: false});
      logEvent('HOME_SCREEN', {screen: 'HOME_SCREEN'});
      store.dispatch(userActions.dockView(true));
      NetInfo.fetch().then((state) => {
        if (!state.isConnected) {
          Alert.alert(
            'No Network',
            'App is not able to connect with the network, check your network connection',
            [
              {
                text: 'Close',
              },
            ],
            {cancelable: false},
          );
        }
      });
    });
    appServices.registerForPushNotifications();
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userData?._id !== this.props.userData?._id) {
      const user = this.props.userData;
      const {device} = this.props;
      this.sseIntegration(user._id);
      crashlytics().log('User signed in.');
      Promise.all([
        crashlytics().setUserId(user._id.toString()),
        crashlytics().setAttributes({
          name: user.nickname,
          email: user.email,
        }),
        analytics().setUserId(user._id.toString()),
        analytics().setUserProperty('email', user.email),
        analytics().setUserProperty('userName', user.nickname),
      ]).then((data) => {
        console.log('Initialised Crash Analytics');
      });
      if (device) {
        const newFirmware =
          device.availableFirmwareUpdateVersion || device.currentFirmware;
        const result = checkFirmwareVersion(
          newFirmware,
          device.currentFirmware,
        );
        if (result) {
          this.setState({
            firmwareUpgrade: true,
          });
        }
      }
    }
  }

  async sseIntegration(loggedInUserId) {
    const appConfig = await config();
    const token = await firebaseToken();
    const url = new URL(
      `${appConfig.apiUrl}/auth/user/${loggedInUserId}/events`,
    );
    this.eventSource = new EventSource(url, {
      headers: {
        'X-Authorization-Firebase': token,
        appType: Platform.OS,
      },
    });
    const listener = (event) => {
      if (event.type === 'open') {
        console.log('SSE connection Successful');
      } else if (event.type === 'message') {
        const {eventName, payload, userId} = JSON.parse(event.data);
        if (userId === loggedInUserId) {
          switch (eventName) {
            case SseEvents.CONTROLLER_UPDATE:
              store.dispatch(appActions.updateDeviceInfo(payload));
              break;
            case SseEvents.WATERING_UPDATE:
              console.log('WATERING_UPDATE', payload);
              store.dispatch(appActions.updateCurrentSchedule(payload));
              break;
            default:
              console.log('Unhandled Event', eventName);
          }
        }
      } else if (event.type === 'error') {
        console.error('Connection error:', event.message);
      } else if (event.type === 'exception') {
        console.error('Error:', event.message, event.error);
      }
    };

    this.eventSource.addEventListener('open', listener);
    this.eventSource.addEventListener('message', listener);
    this.eventSource.addEventListener('error', listener);
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    const {device} = this.props;
    store.dispatch(appActions.updateActiveDevice(device));
    store.dispatch(appActions.fetchDeviceList());
    store.dispatch(appActions.fetchCurrentSchedule(device._id));
    store.dispatch(appActions.fetchUpcomingSchedules(device._id));
    this.setState({refreshing: false});
  };

  switchController = (index) => {
    const {allDevices} = this.props;
    store.dispatch(appActions.switchDevice(true));
    AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
    appServices.setActiveDevice(allDevices[index]).then(() => {
      logEvent('DEVICE_ACTIVATED', {
        id: allDevices[index]?.id,
        name: allDevices[index]?.name,
      });
      store.dispatch(appActions.fetchActiveDevice());
    });
  };

  checkIfAnyActiveController = (device, allDevices) => {
    if (!device.name) {
      let index = 0;
      allDevices.forEach((item, index) => {
        if (item.isOnline) {
          index = index;
        }
      });
      this.switchController(index);
    }
  };

  _renderItem = ({item, index}) => {
    const weather = item.weather[0];
    const temp = item.temp;
    return (
      <View style={styles.weatherCard}>
        <Subheading style={{alignSelf: 'center'}}>
          {moment.unix(item.dt).format('DD/MM')}
        </Subheading>
        <Image
          style={{width: 50, height: 50, alignSelf: 'center'}}
          source={{
            uri: `http://openweathermap.org/img/w/${weather.icon}.png`,
          }}
        />
        <Caption style={{textTransform: 'capitalize', alignSelf: 'center'}}>
          {weather.main}
        </Caption>
        <Text style={{alignSelf: 'center', opacity: 0.7}}>{`${Math.round(
          temp.min,
        )}\u2109  |  ${Math.round(temp.max)}\u2109`}</Text>
      </View>
    );
  };

  render() {
    const {refreshing, viewAllDevice} = this.state;
    const {
      userData,
      openWeatherData,
      allDevices,
      device,
      networkError,
      navigation,
      scheduleRecord,
      history,
      zones,
    } = this.props;
    if (networkError) {
      return <NoNetwork />;
    }
    const records = scheduleRecord.slice();
    const record = records.length ? records[0] : null;
    if (!userData._id) {
      return <Spinner visible={true} />;
    } else if (allDevices.length > 0) {
      return (
        <SafeAreaView style={styles.container}>
          <View style={styles.header}>
            <SimpleLineIcons
              name="arrow-up-circle"
              size={22}
              style={{opacity: 0.7}}
              onPress={() => this.setState({viewAllDevice: true})}
            />
            <View style={{marginLeft: 16}}>
              <Title style={styles.headerTitle}>{device.name}</Title>
              <Caption>{device.serialNumber}</Caption>
            </View>
          </View>
          <Modal
            animationType="slide"
            transparent={false}
            visible={viewAllDevice}
            onRequestClose={() => {
              this.setState({viewAllDevice: false});
            }}>
            <SafeAreaView>
              <View style={styles.header}>
                <SimpleLineIcons
                  name="arrow-down-circle"
                  size={22}
                  style={{opacity: 0.7}}
                  onPress={() => this.setState({viewAllDevice: false})}
                />
                <View style={{marginLeft: 16}}>
                  <Title style={styles.headerTitle}>My Controllers</Title>
                  <Caption>
                    You can add new or switch between controllers
                  </Caption>
                </View>
              </View>
              <View style={styles.addController}>
                <Button
                  icon="plus-circle"
                  onPress={() => {
                    this.initialize = true;
                    this.setState({viewAllDevice: false});
                    navigation.navigate('AddDevice');
                  }}
                  mode="contained">
                  Add controller
                </Button>
              </View>
              <ScrollView style={{padding: 12, backgroundColor: '#f5f6f7'}}>
                {allDevices.map((item, index) => {
                  return (
                    <Card
                      key={item._id}
                      style={[
                        styles.controllerCard,
                        {
                          borderColor: '#303F9F',
                          borderWidth: item._id == device._id ? 1 : 0,
                        },
                      ]}>
                      <TouchableOpacity
                        onPress={() => {
                          if (item._id !== device._id) {
                            this.setState({viewAllDevice: false});
                            this.switchController(index);
                          }
                        }}>
                        <Card.Title
                          title={item.name}
                          subtitle={item.locationString}
                          subtitleNumberOfLines={1}
                        />
                        <Card.Content>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <MaterialCommunityIcons
                              name={item.isOnline ? 'wifi' : 'wifi-off'}
                              size={22}
                              color={item.isOnline ? '#388E3C' : '#D32F2F'}
                            />
                            <Subheading style={{marginHorizontal: 16}}>
                              {item.isOnline ? 'Online' : 'Offline'}
                            </Subheading>
                            {!item.isOnline ? (
                              <Caption>{`(since ${moment(
                                item.offlineSince,
                              ).fromNow()})`}</Caption>
                            ) : null}
                          </View>
                        </Card.Content>
                      </TouchableOpacity>
                    </Card>
                  );
                })}
              </ScrollView>
            </SafeAreaView>
          </Modal>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            {!device.isOnline && userData._id ? (
              <View style={styles.offline}>
                <MaterialCommunityIcons
                  name="wifi-off"
                  size={22}
                  color="#D32F2F"
                />
                <Subheading style={{marginHorizontal: 16}}>Offline</Subheading>
                <Caption>{`(since ${moment(
                  device.offlineSince,
                ).fromNow()})`}</Caption>
              </View>
            ) : null}
            <View style={styles.carouselView}>
              <FlatList
                horizontal={true}
                data={openWeatherData.daily}
                renderItem={this._renderItem}
                keyExtractor={(item) => item.dt.toString()}
              />
              <View style={styles.schedules}>
                <View>
                  <Text style={styles.letterSpacing}>Upcoming Schedule</Text>
                  {record ? (
                    <Caption>
                      {`Watering of ${
                        record.zoneName
                      } is scheduled for ${formatTime(
                        record.duration,
                      )}. Schedule runs on ${record.days}, ${getTimeSlotString(
                        record,
                      )}`}
                    </Caption>
                  ) : (
                    <Caption>Not Available</Caption>
                  )}
                </View>
              </View>
            </View>
            <View style={{paddingVertical: 12}}>
              <StayInformed zones={zones} navigation={navigation} />
            </View>
          </ScrollView>
          <FirmwareUpgrade
            device={device}
            visible={this.state.firmwareUpgrade}
            deviceId={device._id}
            onClose={() =>
              this.setState({
                firmwareUpgrade: false,
              })
            }
          />
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <Helpers.StatusBar
            child={Helpers.HelpButton}
            isBackNavigable={false}
            navigation={navigation}
            subtitle="No Device Found"
            title="Schedules"
          />
          <NoDevice navigation={navigation} />
        </SafeAreaView>
      );
    }
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {history} = state.scheduleHistory;
  const {loadingDevice} = state.switchDevice;
  const {online} = state.deviceOnline;
  const {openWeatherData} = state.openWeather;
  const {scheduleRecord} = state.upcomingSchedule;
  const {userData} = state.user;
  const {networkError} = state.app;
  const {zones} = state.zones;
  return {
    allDevices,
    device,
    history,
    loadingDevice,
    online,
    openWeatherData,
    scheduleRecord,
    userData,
    networkError,
    zones,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
