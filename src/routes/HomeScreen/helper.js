import moment from 'moment';

export function formatTime(duration) {
  const totalTime = duration;
  const calHours = Math.floor(totalTime / 3600);
  const calMinutes = Math.floor((totalTime % 3600) / 60);
  const calSeconds = Math.floor((totalTime % 3600) % 60);
  let timeString = calHours ? `${calHours} H` : '';
  timeString = calMinutes ? `${timeString} ${calMinutes} M` : '0 M';
  timeString = calSeconds
    ? `${timeString} ${calSeconds} S`
    : `${timeString} 0 S`;
  return timeString;
}

export function getTimeSlotString(schedule) {
  switch (schedule.timeSlot) {
    case 0:
      return 'Ends Before Sunrise ';
    case 1:
      const eTime = moment(schedule.endTime).format('hh:mm A');
      return `Ends Before ${eTime}`;
    case 2:
      return 'Starts After Sunset';
    case 3:
      const sTime = moment(schedule.startTime).format('hh:mm A');
      return `Starts At ${sTime}`;
    default:
      return '';
  }
}
