/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  RefreshControl,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MarqueeText from 'react-native-marquee';
import VectorIcon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import InfoCard from '../../components/blog';
import NoDevice from '../../components/noDevice';
import {appActions} from '../../actions/app.actions';
import ForecastCard from '../../components/forecastCard';
import WeatherCard from '../../components/weatherCard';
import ComingSchedule from '../../components/comingSchedule';
import {Colors, Fonts, Dimension} from '../../constants';
import ScheduleHistory from '../../components/scheduleHistory';
import ControllerPicker from '../../components/controllerPicker';
import {appServices} from '../../services';
import {userActions} from '../../actions';

const styles = StyleSheet.create({
  box: {
    flex: 1,
  },
  boxContainer: {
    alignItems: 'center',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 10,
    flexDirection: 'row',
    height: Dimension.HEIGHT_40,
    justifyContent: 'space-between',
  },
  container: {
    flexDirection: 'row',
    paddingTop: Dimension.HEIGHT_20 / 2,
  },
  forecastSection: {
    borderRadius: 8,
    shadowOpacity: 1,
    shadowRadius: 18,
    alignSelf: 'center',
    width: Dimension.WIDTH_338,
    marginTop: Dimension.HEIGHT_20,
    paddingLeft: Dimension.WIDTH_20,
    paddingRight: Dimension.WIDTH_20,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  marqueeContainer: {
    width: '70%',
    height: Dimension.HEIGHT_40,
  },
  name: {
    paddingLeft: 5,
    paddingTop: 5,
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.TEXT_COLOR,
  },
  navigationBar: {
    height: Dimension.HEIGHT_60,
  },
  statusBar: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: Dimension.HEIGHT_60,
    paddingLeft: Dimension.WIDTH_20,
    paddingRight: Dimension.WIDTH_20 / 2,
    width: Dimension.WIDTH,
  },
  viewPort: {
    flex: 1,
    backgroundColor: Colors.SCREEN_BACKGROUND,
  },
  weatherSection: {
    borderRadius: 8,
    shadowOpacity: 1,
    shadowRadius: 18,
    alignSelf: 'center',
    width: Dimension.WIDTH_338,
    marginTop: Dimension.HEIGHT_20,
    paddingLeft: Dimension.WIDTH_20 / 2,
    paddingRight: Dimension.WIDTH_20 / 2,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      refreshing: false,
    };
    this.addController = this.addController.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.switchController = this.switchController.bind(this);
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      store.dispatch(userActions.dockView(true));
    });
    appServices.registerForPushNotifications();
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  onRefresh() {
    this.setState({refreshing: true});
    const {device} = this.props;
    store.dispatch(appActions.updateActiveDevice());
    store.dispatch(appActions.fetchDeviceList());
    store.dispatch(appActions.fetchCurrentSchedule(device._id));
    store.dispatch(appActions.fetchScheduleHistory(device._id));
    this.setState({refreshing: false});
  }

  addController() {
    const {navigation} = this.props;
    this.setState({modalVisible: false});
    navigation.navigate('AddDevice');
  }

  handleClick() {
    this.setState({modalVisible: true});
  }

  closeModal() {
    this.setState({modalVisible: false});
  }

  switchController(index) {
    const {allDevices} = this.props;
    this.setState({modalVisible: false});
    store.dispatch(appActions.switchDevice(true));
    appServices.setActiveDevice(allDevices[index]).then(() => {
      store.dispatch(appActions.fetchActiveDevice());
    });
  }

  render() {
    const {modalVisible, refreshing} = this.state;
    const {
      allDevices,
      device,
      history,
      loadingDevice,
      navigation,
      online,
      openWeatherData,
      scheduleRecord,
      userData,
    } = this.props;
    const {current, daily} = openWeatherData;

    if (allDevices.length > 0) {
      return (
        <View style={styles.viewPort}>
          <StatusBar hidden />
          <Spinner visible={loadingDevice} />
          <View style={styles.navigationBar}>
            <LinearGradient
              style={styles.box}
              locations={[0.1, 1.0]}
              colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
              <View style={styles.statusBar}>
                <TouchableOpacity
                  onPress={() => this.handleClick()}
                  style={styles.boxContainer}>
                  <Text
                    style={[
                      {fontSize: Fonts.FONT_SIZE_18},
                      {
                        color: online
                          ? Colors.ONLINE_STATUS
                          : Colors.OFFLINE_STATUS,
                      },
                    ]}>
                    {' '}
                    &#9679;{' '}
                  </Text>
                  <View style={styles.marqueeContainer}>
                    <MarqueeText
                      loop
                      marqueeDelay={200}
                      marqueeOnStart
                      marqueeResetDelay={200}
                      style={styles.name}>
                      {device.name}
                    </MarqueeText>
                  </View>
                  <Icon
                    name="arrow-drop-down"
                    color={Colors.TEXT_COLOR}
                    size={40}
                  />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </View>
          <ControllerPicker
            devices={allDevices}
            activeDevice={device}
            modalVisible={modalVisible}
            closePopup={this.closeModal}
            addController={this.addController}
            switchController={this.switchController}
          />
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={styles.weatherSection}>
              {current ? <WeatherCard weather={current} /> : null}
            </View>
            {scheduleRecord.length > 0 ? (
              <ComingSchedule schedule={scheduleRecord[0]} />
            ) : null}
            {scheduleRecord.length > 1 || history.length > 0 ? (
              <ScheduleHistory
                history={history}
                schedules={scheduleRecord.slice(1)}
              />
            ) : null}
            {/* <InfoCard /> */}
          </ScrollView>
        </View>
      );
    } else if (userData.id) {
      return (
        <View style={styles.viewPort}>
          <Helpers.StatusBar
            child={Helpers.HelpButton}
            isBackNavigable={false}
            navigation={navigation}
            title="No Device Found"
          />
          <NoDevice navigation={navigation} />
        </View>
      );
    } else {
      return <Spinner visible={true} />;
    }
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {history} = state.scheduleHistory;
  const {loadingDevice} = state.switchDevice;
  const {online} = state.deviceOnline;
  const {openWeatherData} = state.openWeather;
  const {scheduleRecord} = state.upcomingSchedule;
  const {userData} = state.user;
  return {
    allDevices,
    device,
    history,
    loadingDevice,
    online,
    openWeatherData,
    scheduleRecord,
    userData,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
