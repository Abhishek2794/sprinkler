import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import alert from '../../components/alert';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  header: {
    marginBottom: 5,
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  radioText: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: -3,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class SelectIntervalScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: null,
      selectedIndex: -1,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    const {navigation, schedule} = this.props;
    const {days, selectedIndex} = this.state;
    if (days == null) {
      alert('ATTENTION', 'Select atleast one interval');
      return;
    }
    if (selectedIndex === '3') {
      navigation.navigate('SelectDays');
      return;
    }

    store.dispatch(appActions.createSchedule({...schedule, days}));
    navigation.navigate('SelectPeriod');
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Select Interval"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View>
            <Text
              style={
                styles.header
              }>{`Which days do you want to${'\n'} run this schedule?`}</Text>
            <RadioGroup
              size={16}
              style={{marginTop: Dimension.HEIGHT_20}}
              onSelect={(index, value) => {
                switch (value) {
                  case '0':
                    this.setState({days: 'Monday,Wednesday,Friday,Sunday', selectedIndex: value});
                    break;
                  case '1':
                    this.setState({
                      days: 'Tuesday,Thursday,Saturday',
                      selectedIndex: value,
                    });
                    break;
                  case '2':
                    this.setState({days: 'Everyday', selectedIndex: value});
                    break;
                  case '3':
                    this.setState({days: '', selectedIndex: value});
                    break;
                  default:
                    break;
                }
              }}
              color={Colors.PRIMARY_COLOR_DARK}>
              <RadioButton value="0">
                <Text style={styles.radioText}>Odd Days</Text>
              </RadioButton>
              <RadioButton value="1">
                <Text style={styles.radioText}>Even Days</Text>
              </RadioButton>
              <RadioButton value="2">
                <Text style={styles.radioText}>Every day</Text>
              </RadioButton>
              <RadioButton value="3">
                <Text style={styles.radioText}>Specific days</Text>
              </RadioButton>
            </RadioGroup>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {schedule} = state.scheduleCreate;
  return {schedule};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectIntervalScreen);
