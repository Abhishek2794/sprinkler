/* eslint-disable react/no-unused-state */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {ScrollView, StyleSheet, Text, View, Image} from 'react-native';
import {Colors} from '../../constants';
import * as ActionCreators from '../../actions';
import Spinner from 'react-native-spinkit';
import {StatusBar} from '../../components/statusBar';
import {HelpButton} from '../../components/helpButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 12,
    paddingVertical: 16,
    borderBottomColor: '#999',
    borderBottomWidth: 0.5,
  },
  headerDate: {
    fontSize: 16,
  },
  weatherInfo: {
    marginTop: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  infoHeader: {
    opacity: 0.5,
  },
  infoText: {
    fontSize: 16,
  },
  inlineBox: {
    flexDirection: 'row',
  },
  whiteContainer: {
    margin: 8,
    backgroundColor: '#fff',
    paddingVertical: 16,
    paddingHorizontal: 12,
    borderRadius: 8,
  },
  bodyHeader: {
    paddingTop: 12,
    paddingHorizontal: 20,
    fontSize: 18,
    fontWeight: 'bold',
    opacity: 0.7,
  },
});

class ScheduleOnDateScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wateringInfo: null,
    };
  }
  componentDidMount() {
    this.dataAdapter();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.timetable.length !== this.props.timetable.length)
      this.dataAdapter();
  }
  dataAdapter = () => {
    const {timetable, route} = this.props;
    let watering = {};
    if (!timetable.length) {
      watering = null;
    }
    timetable.forEach((item) => {
      if (watering[item.scheduleId]) {
        const temp = watering[item.scheduleId].slice();
        watering = {
          ...watering,
          [item.scheduleId]: temp.concat(item),
        };
      } else {
        watering = {
          ...watering,
          [item.scheduleId]: [item],
        };
      }
    });
    let watered = {};
    AsyncStorage.getItem('@Sprinkler:HistoryLogs').then((data) => {
      let historyLogs = data;
      if (historyLogs) {
        historyLogs = JSON.parse(historyLogs);
        const logs = historyLogs.logs.filter(
          (item) =>
            moment(item.timestamp).format('YYYY-MM-DD') ===
            moment(route.params.timestamp).format('YYYY-MM-DD'),
        );
        if (!logs.length) {
          watered = null;
        }
        logs.forEach((item) => {
          if (watered[item.scheduleId]) {
            const temp = watered[item.scheduleId].slice();
            watered = {
              ...watered,
              [item.scheduleId]: temp.concat(item),
            };
          } else {
            watered = {
              ...watered,
              [item.scheduleId]: [item],
            };
          }
        });
        setTimeout(() => {
          this.setState({
            wateringInfo: {
              watered,
              watering,
            },
          });
        }, 1000);
      } else {
        setTimeout(() => {
          this.setState({
            wateringInfo: {
              watered,
              watering,
            },
          });
        }, 1000);
      }
    });
  };

  durationFormatter = (duration) => {
    const calHours = Math.floor(duration / 3600);
    const calMinutes = Math.floor((duration % 3600) / 60);
    const calSeconds = Math.floor((duration % 3600) % 60);
    let timeString = '';
    timeString = calHours ? `${calHours} hrs ` : '';
    timeString += calMinutes ? `${calMinutes} min ` : '';
    timeString += calSeconds ? `${calSeconds} sec ` : '';
    return timeString;
  };

  render() {
    const {navigation, openWeatherData, route, timetable} = this.props;
    const {params} = route;
    const {timestamp} = params;
    const {daily, timezone} = openWeatherData;
    let current = null;
    const dayOffset = moment(timestamp).diff(moment(), 'day');
    if (dayOffset >= 0 || dayOffset < 7) {
      current = daily[dayOffset];
    }
    console.log(this.state);
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <StatusBar
          subtitle=""
          title="Schedules"
          child={HelpButton}
          isBackNavigable
          navigation={navigation}
        />
        <View style={styles.header}>
          <View>
            <View style={styles.weatherInfo}>
              <Text style={styles.headerDate}>
                {moment(timestamp).format('ll')}
              </Text>
              <Text style={{opacity: 0.6}}>Time Zone: {timezone}</Text>
            </View>
            {current ? (
              <View>
                <View style={styles.weatherInfo}>
                  <Text style={{fontSize: 20, textTransform: 'capitalize'}}>
                    {current.weather[0].description}
                  </Text>
                  <Image
                    style={{width: 60, height: 60, alignSelf: 'center'}}
                    source={{
                      uri: `http://openweathermap.org/img/w/${current.weather[0].icon}.png`,
                    }}
                  />
                </View>
                <View style={styles.weatherInfo}>
                  <View>
                    <Text style={styles.infoHeader}>Temperature</Text>
                    <Text style={styles.infoText}>
                      {current.temp.day + ' ' + '\u2109'}
                    </Text>
                  </View>
                  <View>
                    <Text style={styles.infoHeader}>Humidity</Text>
                    <Text style={styles.infoText}>{current.humidity}</Text>
                  </View>
                  <View>
                    <Text style={styles.infoHeader}>Wind Speed</Text>
                    <Text style={styles.infoText}>{current.wind_speed}</Text>
                  </View>
                  <View>
                    <Text style={styles.infoHeader}>Dew</Text>
                    <Text style={styles.infoText}>{current.dew_point}</Text>
                  </View>
                </View>
              </View>
            ) : null}
          </View>
        </View>
        <ScrollView>
          {!this.state.wateringInfo ? (
            <View
              style={{
                height: 200,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Spinner
                color={Colors.PRIMARY_COLOR}
                isVisible
                size={60}
                type="ThreeBounce"
              />
            </View>
          ) : null}
          {this.state.wateringInfo?.watering ? (
            <View>
              <Text style={styles.bodyHeader}>Watering</Text>
              {Object.keys(this.state.wateringInfo.watering).map(
                (id, index) => {
                  const data = {
                    name: this.state.wateringInfo.watering[id][0].scheduleName,
                    timeStamp: new Date(
                      this.state.wateringInfo.watering[id][0].startTime,
                    ),
                  };
                  let duration = 0;
                  return (
                    <View
                      style={styles.whiteContainer}
                      key={`watering_${index}`}>
                      <View style={styles.inlineBox}>
                        <Icon name="watering-can" size={24} color={'#3d5afe'} />
                        <View style={{marginLeft: 8}}>
                          <Text
                            style={{
                              fontSize: 18,
                              fontWeight: 'bold',
                              opacity: 0.7,
                            }}>
                            {data.name}
                          </Text>
                          <Text style={{fontSize: 12, opacity: 0.6}}>
                            {moment(data.timeStamp).format('MMM DD, hh:mm A')}
                          </Text>
                        </View>
                      </View>

                      {this.state.wateringInfo.watering[id].map(
                        (item, index) => {
                          duration += item.duration;
                          return (
                            <View
                              key={`${item.startTime}_${index}_watering`}
                              style={[styles.inlineBox, {paddingTop: 6}]}>
                              <Text style={{flex: 1}}>{item.zoneName}</Text>
                              <Text style={{flex: 1}}>
                                {this.durationFormatter(item.duration)}
                              </Text>
                            </View>
                          );
                        },
                      )}
                      <View style={[styles.inlineBox, {paddingTop: 6}]}>
                        <Text
                          style={{flex: 1, fontWeight: 'bold', opacity: 0.7}}>
                          Total Duration
                        </Text>
                        <Text style={{flex: 1}}>
                          {this.durationFormatter(duration)}
                        </Text>
                      </View>
                    </View>
                  );
                },
              )}
            </View>
          ) : null}
          {this.state.wateringInfo?.watered ? (
            <View>
              <Text style={styles.bodyHeader}>Watered Logs</Text>
              {Object.keys(this.state.wateringInfo.watered).map((id, index) => {
                const data = {
                  name:
                    this.state.wateringInfo.watered[id][0].scheduleText ||
                    'Quick Run',
                  timeStamp: this.state.wateringInfo.watered[id][0].timestamp,
                };
                let duration = 0;
                return (
                  <View style={styles.whiteContainer} key={`watered_${index}`}>
                    <View style={styles.inlineBox}>
                      <Icon
                        name="watering-can"
                        size={24}
                        color={Colors.SWIPER_DOT_COLOR}
                      />
                      <View style={{marginLeft: 8}}>
                        <Text
                          style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            opacity: 0.7,
                          }}>
                          {data.name}
                        </Text>
                        <Text style={{fontSize: 12, opacity: 0.6}}>
                          {moment(this.props.route.params.timestamp).format(
                            'MMM DD YYYY',
                          )}
                        </Text>
                      </View>
                    </View>

                    {this.state.wateringInfo.watered[id].map((item, index) => {
                      if (item.duration) {
                        duration += item.duration;
                        return (
                          <View
                            style={{paddingTop: 6}}
                            key={`${item.startTime}_${index}_watered`}>
                            <Text>{item.zoneText}</Text>
                          </View>
                        );
                      } else {
                        return null;
                      }
                    })}
                    <View style={[styles.inlineBox, {paddingTop: 6}]}>
                      <Text style={{flex: 1, fontWeight: 'bold', opacity: 0.7}}>
                        Total Duration
                      </Text>
                      <Text style={{flex: 1}}>
                        {this.durationFormatter(duration)}
                      </Text>
                    </View>
                  </View>
                );
              })}
            </View>
          ) : null}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {openWeatherData} = state.openWeather;
  const {timetable} = state.scheduleTimetable;
  return {openWeatherData, timetable};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScheduleOnDateScreen);
