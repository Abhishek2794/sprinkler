/* eslint-disable class-methods-use-this */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import ListItem from '../../components/listItem';
import {Colors, Fonts, Dimension, appConstants} from '../../constants';
import store from '../../helpers/store';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  header: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
    marginBottom: 5,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class SelectPeriodScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      endDatePicker: false,
      endDateString: 'Never',
      endDate: this.getEndDate(new Date()),
      startDate: new Date(),
      startDatePicker: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.setEndDate = this.setEndDate.bind(this);
    this.setStartDate = this.setStartDate.bind(this);
  }

  onSubmit() {
    const {endDate, startDate} = this.state;
    endDate.setHours(0, 0, 0, 0);
    startDate.setHours(0, 0, 0, 0);
    if (startDate >= endDate) {
      alert('ATTENTION', 'Start or End Date Invalid');
      return;
    }

    const {navigation, schedule} = this.props;
    store.dispatch(
      appActions.createSchedule({...schedule, endDate, startDate}),
    );
    navigation.navigate('WeatherIntelligence');
  }

  getEndDate(date) {
    const year = date.getFullYear() + 10;
    const month = date.getMonth();
    const day = date.getDate();
    return new Date(year, month, day);
  }

  setEndDate(event, date) {
    if (date !== undefined) {
      this.setState({
        endDate: date,
        endDatePicker: false,
        endDateString: date.toDateString(),
      });
    } else {
      this.setState({endDatePicker: false});
    }
  }

  setStartDate(event, date) {
    if (date !== undefined) {
      this.setState({startDate: date, startDatePicker: false});
    } else {
      this.setState({startDatePicker: false});
    }
  }

  handleClick(type) {
    switch (type) {
      case appConstants.START_DATE:
        this.setState({startDatePicker: true});
        break;
      case appConstants.END_DATE:
        this.setState({endDatePicker: true});
        break;
      default:
        break;
    }
  }

  render() {
    const {navigation} = this.props;
    const {
      startDate,
      startDatePicker,
      endDate,
      endDatePicker,
      endDateString,
    } = this.state;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          child={Helpers.HelpButton}
          isBackNavigable
          title="Create Schedule"
          subtitle="Select Start End Dates"
          navigation={navigation}
        />

        {startDatePicker && (
          <RNDateTimePicker
            value={startDate}
            maximumDate={endDate}
            minimumDate={new Date()}
            mode="date"
            is24Hour={false}
            display="default"
            onChange={this.setStartDate}
          />
        )}

        {endDatePicker && (
          <RNDateTimePicker
            value={endDate}
            minimumDate={startDate}
            mode="date"
            is24Hour={false}
            display="default"
            onChange={this.setEndDate}
          />
        )}

        <Helpers.Card>
          <Text style={styles.header}>
            Choose start and end date for this schedule
          </Text>
          <ListItem
            title="Choose Start Date"
            onClick={this.handleClick}
            type={appConstants.START_DATE}
            text={
              moment(startDate).isSame(moment(), 'day')
                ? 'Today'
                : startDate.toDateString()
            }
          />

          <ListItem
            text={endDateString}
            title="Choose End Date"
            onClick={this.handleClick}
            type={appConstants.END_DATE}
          />
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {schedule} = state.scheduleCreate;
  return {schedule};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SelectPeriodScreen);
