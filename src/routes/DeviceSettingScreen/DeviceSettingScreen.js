/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-extraneous-dependencies */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Switch,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {logglyServices, userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import AlertButton from '../../components/alertButton';
import {Colors, Fonts, Dimension} from '../../constants';
import {logEvent, recordError} from '../../analytics';
import {Snackbar} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    backgroundColor: '#fff',
    margin: 24,
    borderRadius: 8,
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_14,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  formLabel: {
    flex: 2,
    flexDirection: 'column',
  },
  section: {
    flex: 2,
    flexDirection: 'column',
  },
});

class DeviceSettingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteModal: false,
      resetModal: false,
      unlinkModal: false,
      coldReset: false,
      masterValve: false,
      animating: false,
      rainsensor1: false,
      rainsensor2: false,
      error: '',
    };

    this.unlinkAccount = this.unlinkAccount.bind(this);
    this.removeDevice = this.removeDevice.bind(this);
    this.closeDeletePopup = this.closeDeletePopup.bind(this);
    this.closeResetPopup = this.closeResetPopup.bind(this);
    this.closeUnlinkPopup = this.closeUnlinkPopup.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.resetNetwork = this.resetNetwork.bind(this);
  }

  closeDeletePopup() {
    this.setState({deleteModal: false});
  }

  closeResetPopup() {
    return this.setState({resetModal: false});
  }

  closeUnlinkPopup() {
    this.setState({unlinkModal: false});
  }

  handleNavigation(screen) {
    logEvent(screen, {screen});
    const {device, navigation, online} = this.props;
    if (online) {
      navigation.navigate(screen, {device});
      return;
    }
    alert(device.name, 'Device is Offline');
  }

  removeDevice() {
    this.resetData();
    const {device} = this.props;
    userServices
      .removeDevice(device)
      .then(async () => {
        this.setState({animating: false});
        const {userData} = this.props;
        await AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
        logglyServices.logEvent({user: userData.email, device}, 'removeDevice');
        store.dispatch(appActions.checkLogin(null));
        logEvent('FACTORY_RESET', {screen: 'FACTORY_RESET'});
      })
      .catch((err) => {
        this.setState({animating: false});
        alert('Error', 'Cannot delete device');
        logEvent('FACTORY_RESET_ERROR', {
          error: 'Error while factory reset',
          screen: 'FACTORY_RESET',
        });
        recordError('Error: Factory Reset', err);
      });
  }

  resetNetwork() {
    this.resetData();
    const {device, navigation, online, userData} = this.props;
    if (online) {
      userServices
        .resetDeviceNetwork(device)
        .then(() => {
          this.setState({animating: false});
          logglyServices.logEvent(
            {user: userData.email, device},
            'resetNetwork',
          );
          logEvent('COLD_RESET', {screen: 'COLD_RESET'});
          navigation.navigate('NewHomeWifi');
        })
        .catch((err) => {
          this.setState({animating: false});
          logEvent('COLD_RESET_ERROR', {
            error: 'Error while cold reset',
            screen: 'COLD_RESET',
          });
          recordError('Error: COLD Reset', err);
        });
      return;
    }
    this.setState({animating: false});
    navigation.navigate('NewHomeWifi');
  }

  resetData = () => {
    this.setState({
      deleteModal: false,
      resetModal: false,
      unlinkModal: false,
      coldReset: false,
      masterValve: false,
      animating: false,
      rainsensor1: false,
      rainsensor2: false,
    });
  };

  unlinkAccount() {
    this.resetData();
    const {device} = this.props;
    userServices
      .deRegisterDevice(device)
      .then(() => {
        this.setState({animating: false});
        const {userData} = this.props;
        logglyServices.logEvent(
          {user: userData.email, device},
          'unlinkAccount',
        );
        store.dispatch(appActions.checkLogin(null));
      })
      .catch((error) => {
        this.setState({animating: false});
        alert('Error', error.toString());
      });
  }

  checkDeviceOnlineStatus = (showPopUp = true) => {
    const {online} = this.props;
    if (!online) {
      if (showPopUp) {
        alert(
          'Device is Offline',
          'The controller device is currently offline hence aborting the operation',
        );
      } else {
        return false;
      }
      this.setState({
        animating: false,
      });
    } else {
      return true;
    }
  };

  onMasterValveUpdate = () => {
    this.setState({animating: true});
    const {masterValve} = this.state;
    const {device} = this.props;
    if (this.checkDeviceOnlineStatus())
      userServices
        .updateDevice({
          ...device,
          masterValve: !masterValve,
        })
        .then(() => {
          logEvent('MasterValveUpdate', {masterValve});
          store.dispatch(
            appActions.updateActiveDevice({
              ...device,
              masterValve: !masterValve,
            }),
          );
          this.setState({masterValve: !masterValve, animating: false});
        })
        .catch((err) => {
          this.setState({animating: false});
          recordError('Error while updating master valve', err);
        });
  };

  onSensorUpdate = (type) => {
    this.setState({animating: true});
    const {device} = this.props;
    const payload = {
      ...device,
      sense1: type === 1 ? !this.state.rainsensor1 : device.sense1,
      sense2: type === 2 ? !this.state.rainsensor2 : device.sense2,
    };
    if (this.checkDeviceOnlineStatus())
      userServices
        .updateDevice(payload)
        .then(() => {
          store.dispatch(appActions.updateActiveDevice(payload));
          this.setState({
            rainsensor1:
              type === 1 ? !this.state.rainsensor1 : this.state.rainsensor1,
            rainsensor2:
              type === 2 ? !this.state.rainsensor2 : this.state.rainsensor2,
            animating: false,
          });
        })
        .catch((err) => {
          console.log(err);
          this.setState({
            animating: false,
            error: `Error while updating rain sensor ${err}`,
          });
          recordError(`Error in updating rainsensor ${type}`, err);
        });
  };

  componentDidMount() {
    const {device} = this.props;
    console.log(device);
    this.setState({
      masterValve: device.masterValve,
      rainsensor1: device.sense1,
      rainsensor2: device.sense2,
    });
  }

  render() {
    const {device, navigation} = this.props;
    const {
      animating,
      deleteModal,
      resetModal,
      unlinkModal,
      coldReset,
    } = this.state;
    console.log(this.state.rainsensor1, this.state.rainsensor2);
    return (
      <>
        <Helpers.StatusBar
          subtitle=""
          isBackNavigable
          child={Helpers.HelpButton}
          title="Controller Settings"
          navigation={navigation}
        />
        <ScrollView style={styles.container}>
          <Spinner visible={animating} />

          <AlertButton
            action="OK, Reset"
            onPress={this.resetNetwork}
            closePopup={this.closeResetPopup}
            modalVisible={resetModal}
            text={`Please make sure you are nearby to the controller "${device.name}" and have active wifi network. Are you sure you want to reset the network?`}
          />

          <AlertButton
            action="OK, Remove"
            onPress={this.removeDevice}
            closePopup={this.closeDeletePopup}
            modalVisible={deleteModal}
            text="Are you sure you want to remove the device?"
          />

          <AlertButton
            action="OK, Remove"
            onPress={this.unlinkAccount}
            closePopup={() => {
              this.setState({
                coldReset: false,
              });
            }}
            modalVisible={coldReset}
            text="Device is offline, Are you sure you want to remove the device?"
          />

          <AlertButton
            action="Yes"
            onPress={this.unlinkAccount}
            closePopup={this.closeUnlinkPopup}
            modalVisible={unlinkModal}
            text="You will be unable to access this device. Do you want to continue?"
          />

          <Snackbar
            visible={this.state.error}
            onDismiss={() => this.setState({error: ''})}
            action={{
              label: 'Close',
              onPress: () => {
                this.setState({error: ''});
              },
            }}>
            {this.state.error}
          </Snackbar>

          <View style={styles.subContainer}>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => this.handleNavigation('EditDeviceName')}>
              <View style={styles.section}>
                <Text style={styles.formField}>Name</Text>
                <Text style={styles.formFieldValue}>{device.name}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => this.handleNavigation('EditAddress')}>
              <View style={styles.section}>
                <Text style={styles.formField}>Address</Text>
                <Text numberOfLines={1} style={styles.formFieldValue}>
                  {device.address.fullAddress}
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.formElement} onPress={() => {}}>
              <View style={styles.section}>
                <Text style={styles.formField}>Device</Text>
                <Text style={styles.formFieldValue}>Copy Configuration</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <View style={styles.formElement}>
              <View style={styles.section}>
                <Text style={styles.formField}>Master Valve</Text>
                <Text style={styles.formFieldValue}>
                  Global Master Valve Status
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={this.state.masterValve}
                  onValueChange={this.onMasterValveUpdate}
                />
              </View>
            </View>
            <View style={styles.formElement}>
              <View style={styles.section}>
                <Text style={styles.formField}>Rain Sensor</Text>
                <Text style={styles.formFieldValue}>
                  Status of Rain Sensor 1
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={this.state.rainsensor1}
                  onValueChange={this.onSensorUpdate.bind(this, 1)}
                />
              </View>
            </View>
            <View style={styles.formElement}>
              <View style={styles.section}>
                <Text style={styles.formField}>Rain Sensor</Text>
                <Text style={styles.formFieldValue}>
                  Status of Rain Sensor 2
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={this.state.rainsensor2}
                  onValueChange={this.onSensorUpdate.bind(this, 2)}
                />
              </View>
            </View>
            {/* <TouchableOpacity style={styles.formElement} onPress={() => navigation.navigate('NotificationPreference')}>
              <View style={styles.section}>
                <Text style={styles.formField}>Notification Preferences</Text>
                <Text style={styles.formFieldValue}>
                  Configure the notification settings
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity> */}
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => {
                this.setState({resetModal: true});
              }}>
              <View style={styles.formLabel}>
                <Text style={styles.formField}>Wifi Network</Text>
                <Text style={styles.formFieldValue}>Change Wifi Settings</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => {
                if (this.checkDeviceOnlineStatus()) {
                  this.setState({unlinkModal: true});
                }
              }}>
              <View style={styles.formLabel}>
                <Text style={styles.formField}>Cold Reset</Text>
                <Text style={styles.formFieldValue}>Unlink Account</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => {
                if (this.checkDeviceOnlineStatus(false)) {
                  this.setState({deleteModal: true});
                } else {
                  this.setState({coldReset: true});
                }
              }}>
              <View style={styles.formLabel}>
                <Text style={styles.formField}>Factory Reset</Text>
                <Text style={styles.formFieldValue}>Remove Device</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {online} = state.deviceOnline;
  const {userData} = state.user;
  return {device, online, userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeviceSettingScreen);
