import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Modal,
  SafeAreaView,
} from 'react-native';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {appConstants, Colors, Fonts} from '../../constants';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Button, Text} from 'react-native-paper';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  header: {
    marginBottom: 5,
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  radioText: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: -5,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 24,
    backgroundColor: '#f5f6f7',
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#aaa',
  },
  textContainer: {
    paddingHorizontal: 16,
    alignItems: 'center',
    paddingVertical: 24,
  },
});

class PickTimeslotScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeSlot: null,
      endTime: new Date(),
      endTimePicker: false,
      startTime: new Date(),
      startTimePicker: false,
      showInfo: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.setEndTime = this.setEndTime.bind(this);
    this.setStartTime = this.setStartTime.bind(this);
  }

  onSubmit() {
    const screen = 'SelectZone';
    const {navigation, schedule} = this.props;
    const {endTime, startTime, timeSlot} = this.state;
    console.log(timeSlot);
    if (timeSlot === null) {
      alert('ATTENTION', 'Please Choose Timeslot');
      return;
    }
    switch (timeSlot) {
      case 0:
        store.dispatch(appActions.createSchedule({...schedule, timeSlot}));
        break;
      case 1:
        store.dispatch(
          appActions.createSchedule({
            ...schedule,
            timeSlot,
            endTime: moment(endTime).toISOString(),
          }),
        );
        break;
      case 2:
        store.dispatch(appActions.createSchedule({...schedule, timeSlot}));
        break;
      case 3:
        store.dispatch(
          appActions.createSchedule({
            ...schedule,
            timeSlot,
            startTime: moment(startTime).toISOString(),
          }),
        );
        break;
      default:
        break;
    }
    navigation.navigate(screen);
  }

  onSelected(timeSlot, type) {
    this.setState({timeSlot});
    switch (type) {
      case appConstants.START_AT_SPECIFIC_TIME:
        this.setState({startTimePicker: true});
        break;
      case appConstants.END_BEFORE_SPECIFIC_TIME:
        this.setState({endTimePicker: true});
        break;
      default:
        break;
    }
  }

  setEndTime(event, date) {
    if (date !== undefined) {
      this.setState({endTime: date, endTimePicker: false});
    } else {
      this.setState({endTimePicker: false, timeSlot: null});
    }
  }

  setStartTime(event, date) {
    if (date !== undefined) {
      const updatedDate = new Date(date).setSeconds('00');
      this.setState({startTime: updatedDate, startTimePicker: false});
    } else {
      this.setState({startTimePicker: false, timeSlot: null});
    }
  }

  render() {
    const {navigation} = this.props;
    const {
      endTime,
      startTime,
      endTimePicker,
      startTimePicker,
      timeSlot,
      showInfo,
    } = this.state;
    console.log('here2');
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Pick Timeslot"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        {startTimePicker && (
          <RNDateTimePicker
            value={startTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setStartTime}
          />
        )}
        {endTimePicker && (
          <RNDateTimePicker
            value={endTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setEndTime}
          />
        )}
        <Helpers.Card>
          <View>
            <Text style={styles.header}>Pick a time slot for the schedule</Text>
            <RadioGroup
              size={16}
              color={Colors.PRIMARY_COLOR_DARK}
              selectedIndex={timeSlot}
              onSelect={(index, value) => this.onSelected(index, value)}>
              <RadioButton
                disabled={timeSlot === 1}
                value={appConstants.END_BEFORE_SUNRISE}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.radioText}>End before sunrise</Text>
                  {timeSlot === 1 ? (
                    <MaterialIcons
                      name="info-outline"
                      size={16}
                      color={'#17a2b8'}
                      onPress={() => {
                        this.setState({
                          showInfo: true,
                        });
                      }}
                      style={{marginLeft: 8, paddingHorizontal: 4}}
                    />
                  ) : null}
                </View>
              </RadioButton>
              <RadioButton
                disabled={timeSlot === 1}
                value={appConstants.END_BEFORE_SPECIFIC_TIME}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.radioText}>End before specific time</Text>
                  {timeSlot === 1 ? (
                    <MaterialIcons
                      name="info-outline"
                      size={16}
                      color={'#17a2b8'}
                      onPress={() => {
                        this.setState({
                          showInfo: true,
                        });
                      }}
                      style={{marginLeft: 8, paddingHorizontal: 4}}
                    />
                  ) : null}
                </View>
              </RadioButton>
              <RadioButton value={appConstants.START_AFTER_SUNSET}>
                <Text style={styles.radioText}>Start after sunset</Text>
              </RadioButton>
              <RadioButton value={appConstants.START_AT_SPECIFIC_TIME}>
                <Text style={styles.radioText}>Start at specific time</Text>
              </RadioButton>
            </RadioGroup>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
        <Modal
          animationType="fade"
          transparent={true}
          visible={showInfo}
          collapsable={true}
          onRequestClose={() => {
            this.setState({
              showInfo: false,
            });
          }}>
          <SafeAreaView style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={styles.textContainer}>
                <Text style={{letterSpacing: 0.16}}>
                  Heads up! We do not recommend having more then one "End
                  Before" schedule, as it can cause irregular behavior. To
                  prevent this behavior, you can select a different run time
                  option, or make sure your "End Before" schedules do not back
                  up into the previous day.
                </Text>
              </View>
              <Button
                onPress={() => {
                  this.setState({showInfo: false});
                }}
                mode="contained"
                color={Colors.SECONDARY_COLOR}
                style={{
                  borderTopLeftRadius: 0,
                  borderTopRightRadius: 0,
                  height: 48,
                  justifyContent: 'center',
                }}>
                Ok
              </Button>
            </View>
          </SafeAreaView>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {schedule} = state.scheduleCreate;
  return {device, schedule};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PickTimeslotScreen);
