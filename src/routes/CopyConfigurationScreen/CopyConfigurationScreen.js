import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {logglyServices, userServices} from '../../services';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    justifyContent: 'space-between',
    paddingTop: Dimension.HEIGHT_20,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  message: {
    alignSelf: 'center',
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_18,
    marginTop: Dimension.HEIGHT_20,
  },
  name: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class CopyConfigurationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false,
      isCopying: false,
      message: 'Copying configuration...',
      selectedIndex: -1,
    };

    this.handleNavigation = this.handleNavigation.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    this.setState({isCopying: true});
    const {selectedIndex} = this.state;
    const {allDevices, device} = this.props;
    const data = {
      deviceIdTo: device._id,
      deviceIdFrom: allDevices.filter(
        (dev) => dev.id !== device._id && dev.zipCode === device.zipCode,
      )[selectedIndex].id,
    };

    userServices.copyConfiguration(data).then(() => {
      this.setState({copied: true, isCopying: false});
      const {userData} = this.props;
      logglyServices.logEvent(
        {user: userData.email, data},
        'copyConfiguration',
      );
    });
  }

  handleNavigation() {
    const {navigation} = this.props;
    navigation.popToTop();
  }

  render() {
    const {copied, isCopying, message, selectedIndex} = this.state;
    const {allDevices, device, navigation} = this.props;
    if (isCopying) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            title="Copy Configuration"
            isBackNavigable
            subtitle="Choose Device"
            child={Helpers.HelpButton}
            navigation={navigation}
          />
          <Helpers.Card>
            <ActivityIndicator />
            <Text style={styles.message}>{message}</Text>
          </Helpers.Card>
        </View>
      );
    }
    if (copied) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            title="Copy Configuration"
            isBackNavigable
            subtitle="Choose Device"
            child={Helpers.HelpButton}
            navigation={navigation}
          />
          <Helpers.Card>
            <Text style={styles.message}>
              Configuration copied successfully.
            </Text>
          </Helpers.Card>
          <Helpers.Footer>
            <TouchableOpacity
              onPress={() => this.handleNavigation()}
              style={styles.button}>
              <Text style={styles.text}>Home</Text>
            </TouchableOpacity>
          </Helpers.Footer>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Copy Configuration"
          isBackNavigable={false}
          subtitle="Choose Device"
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <Text style={styles.header}>Select device to copy</Text>
          {allDevices
            .filter(
              (dev) => dev.id !== device._id && dev.zipCode === device.zipCode,
            )
            .map((dev, index) => (
              <TouchableOpacity
                key={dev}
                onPress={() => this.setState({selectedIndex: index})}>
                <View style={styles.listItem}>
                  <Text style={styles.name}>{dev.name}</Text>
                  {index === selectedIndex && (
                    <Icon name="done" color={Colors.PRIMARY_COLOR} size={24} />
                  )}
                </View>
              </TouchableOpacity>
            ))}
        </Helpers.Card>
        <Helpers.Footer>
          <Helpers.CancelButton navigation={navigation} />
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Copy</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {userData} = state.user;
  return {
    device,
    allDevices,
    userData,
  };
};

export default connect(mapStateToProps, null)(CopyConfigurationScreen);
