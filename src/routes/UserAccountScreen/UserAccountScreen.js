/* eslint-disable no-useless-escape */
/* eslint-disable import/no-extraneous-dependencies */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import messaging from '@react-native-firebase/messaging';
import Card from '../../components/card';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import AlertButton from '../../components/alertButton';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TextInput} from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_14,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  formLabel: {
    flex: 2,
    flexDirection: 'column',
  },
  largeContainer: {
    flex: 2,
    flexDirection: 'column',
  },
});

class UserAccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoutModal: false,
    };

    this.closeLogoutPopup = this.closeLogoutPopup.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.logout = this.logout.bind(this);
  }

  closeLogoutPopup() {
    this.setState({logoutModal: false});
  }

  handleNavigation(type) {
    const {navigation} = this.props;
    navigation.navigate('EditUsername', {type});
  }

  logout() {
    this.setState({logoutModal: false});
    const {userData} = this.props;
    if (userServices.email) {
      const topic = userData.email.replace(/[&\/\\@#_,+()$~%.'":*?<>{}]/g, '-');
      messaging().unsubscribeFromTopic(topic);
      userServices.logout(userData.email).then(() => {
        AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
        store.dispatch(appActions.checkLogin(false));
        store.dispatch(appActions.logout());
      });
    } else {
      userServices.logout().then(() => {
        AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
        store.dispatch(appActions.checkLogin(false));
        store.dispatch(appActions.logout());
      });
    }
  }

  render() {
    const {navigation, userData} = this.props;
    const {logoutModal} = this.state;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle=""
          title="User Account"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <AlertButton
          action="OK, Logout"
          onPress={this.logout}
          closePopup={this.closeLogoutPopup}
          modalVisible={logoutModal}
          text="Are you sure you want to Logout?"
        />

        <Card>
          <TouchableOpacity
            style={styles.formElement}
            onPress={() => this.handleNavigation('Name')}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Name</Text>
              <Text style={styles.formFieldValue}>{userData.nickname}</Text>
            </View>
            <View style={styles.formEdit}>
              <Icon name="keyboard-arrow-right" size={24} color="#929292" />
            </View>
          </TouchableOpacity>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Email Address</Text>
              <Text style={styles.formFieldValue}>{userData.email}</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.formElement}
            onPress={() => this.handleNavigation('Phone Number')}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Phone Number</Text>
              <Text style={styles.formFieldValue}>{userData.phoneNumber}</Text>
            </View>
            <View style={styles.formEdit}>
              <Icon name="keyboard-arrow-right" size={24} color="#929292" />
            </View>
          </TouchableOpacity>
          <View
            style={{paddingTop: 10, paddingBottom: 10, flexDirection: 'row'}}>
            <TouchableOpacity
              style={styles.formLabel}
              onPress={() => this.setState({logoutModal: true})}>
              <Text
                numberOfLines={1}
                style={[styles.formField, {color: '#ff7575'}]}>
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </Card>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {userData} = state.user;
  return {userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserAccountScreen);
