/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  PermissionsAndroid,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {config} from '../../helpers/config';
import alert from '../../components/alert';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {appServices} from '../../services/app.services';
import {userServices} from '../../services';

const styles = StyleSheet.create({
  address: {
    alignItems: 'center',
    height: Dimension.HEIGHT_100,
    justifyContent: 'center',
  },
  addressContainer: {
    borderBottomWidth: 1,
    flexDirection: 'column',
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  addressText: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  box: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  card: {
    marginTop: 10,
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    alignSelf: 'center',
    position: 'relative',
    flexDirection: 'column',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_520,
    paddingTop: Dimension.HEIGHT_40,
    paddingLeft: Dimension.WIDTH_30,
    paddingRight: Dimension.WIDTH_30,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  label: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  map: {
    alignSelf: 'stretch',
    height: Dimension.HEIGHT_445 - Dimension.HEIGHT_45,
    width: Dimension.WIDTH,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditAddressScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationString: null,
      coordinates: {latitude: 28.7041, longitude: 77.1025},
      listViewDisplayed: true,
      loading: true,
      submitting: false,
      ZipCode: '',
    };
    this.askLocationPermission = this.askLocationPermission.bind(this);
    this.checkLocationPermission = this.checkLocationPermission.bind(this);
    this.fetchGeoLocation = this.fetchGeoLocation.bind(this);
    this.handleMapRegionChange = this.handleMapRegionChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.saveLocation = this.saveLocation.bind(this);
  }

  componentDidMount() {
    const {device} = this.props;
    if (device.address.fullAddress) {
      this.setState({
        locationString: device.address.fullAddress,
        coordinates: {
          latitude: parseFloat(device.address.location.coordinates[1]),
          longitude: parseFloat(device.address.location.coordinates[0]),
        },
        loading: false,
      });
    } else {
      this.checkLocationPermission();
    }
  }

  onSubmit() {
    this.setState({submitting: true});
    const {locationString, coordinates, ZipCode} = this.state;
    const {latitude, longitude} = coordinates;
    if (locationString === null) {
      alert('Error', 'Location not set');
      return;
    }
    const {device} = this.props;
    if (device.isOnline)
      userServices
        .updateDevice({
          ...device,
          address: {
            location: {
              coordinates: [longitude, latitude],
            },
            locationKey: device.address.locationKey,
            fullAddress: locationString,
            zipCode: ZipCode,
          },
        })
        .then(() => {
          store.dispatch(appActions.updateActiveDevice());
          this.setState({submitting: false}, () => {
            alert('Success', 'Location Updated');
          });
        })
        .catch(() => {
          this.setState({submitting: false}, () => {
            setTimeout(() => {
              alert('Error', 'Could not update location');
            }, 1000);
          });
        });
  }

  async checkLocationPermission() {
    const status = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (status) {
      this.fetchGeoLocation();
      return;
    }
    this.askLocationPermission();
  }

  async askLocationPermission() {
    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      this.fetchGeoLocation();
      return;
    }
    alert('Error', 'Please grant permission to access location');
  }

  async fetchGeoLocation() {
    Geolocation.getCurrentPosition(
      async (info) => {
        const {coords} = info;
        const {latitude, longitude} = coords;
        const addresses = await appServices.reverseGeocode({
          latitude,
          longitude,
        });
        this.setState(
          {
            coordinates: {latitude, longitude},
            loading: false,
            locationString: addresses[0].formatted_address,
            ZipCode: parseInt(addresses[0].address.postalCode, 10),
          },
          () => {
            this.locationRef.setAddressText(addresses[0].formatted_address);
          },
        );
      },
      () => {
        Alert.alert(
          'Warning',
          'Not able to fetch your current location',
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({loading: false});
              },
            },
          ],
          {cancelable: true},
        );
      },
    );
  }

  async handleMapRegionChange(mapRegion) {
    const {latitude, longitude} = mapRegion;
    const addresses = await appServices.reverseGeocode({latitude, longitude});
    this.setState(
      {
        coordinates: mapRegion,
        loading: false,
        locationString: addresses[0].formatted_address,
        ZipCode: parseInt(addresses[0].address.postalCode, 10),
      },
      () => {
        this.locationRef.setAddressText(addresses[0].formatted_address);
      },
    );
  }

  async saveLocation(data, details) {
    const {description} = data;
    const {geometry} = details;
    const {location} = geometry;
    const {lat, lng} = location;
    const addresses = await appServices.reverseGeocode({
      latitude: parseFloat(lat),
      longitude: parseFloat(lng),
    });
    await AsyncStorage.setItem(
      '@Sprinkler:postalCode',
      addresses[0].address.postalCode,
    );
    this.setState({
      locationString: description,
      coordinates: {latitude: parseFloat(lat), longitude: parseFloat(lng)},
      listViewDisplayed: false,
      ZipCode: parseInt(addresses[0].address.postalCode, 10),
    });
  }

  render() {
    const {navigation} = this.props;
    const {coordinates, listViewDisplayed, loading, submitting} = this.state;
    const {latitude, longitude} = coordinates;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle="Address"
          title="Add Device"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        {loading ? (
          <Spinner visible />
        ) : (
          <View style={styles.container}>
            <Spinner visible={submitting} />
            <View style={styles.address}>
              <GooglePlacesAutocomplete
                autoFocus={false}
                enablePoweredByContainer={false}
                fetchDetails
                GoogleReverseGeocodingQuery={{
                  key: config.googleApiKey,
                  language: 'en',
                }}
                listViewDisplayed={listViewDisplayed}
                minLength={3}
                onPress={this.saveLocation}
                placeholder="Type your address"
                query={{
                  key: config.googleApiKey,
                  language: 'en',
                  types: 'geocode',
                }}
                ref={(ref) => {
                  this.locationRef = ref;
                }}
                returnKeyType="search"
                styles={{
                  textInputContainer: {
                    backgroundColor: '#fff',
                    borderBottomColor: Colors.PRIMARY_COLOR_DARK,
                    borderColor: '#fff',
                    borderTopColor: '#fff',
                    borderWidth: 2,
                    height: Dimension.HEIGHT_50,
                    width: '100%',
                  },
                }}
                textInputProps={{
                  onFocus: () => this.setState({listViewDisplayed: true}),
                }}
              />
            </View>
            <View style={styles.map}>
              <MapView
                provider={PROVIDER_GOOGLE}
                region={{
                  latitude,
                  longitude,
                  latitudeDelta: 0.0022,
                  longitudeDelta: 0.0021,
                }}
                showsMyLocationButton
                style={styles.container}
                onRegionChangeComplete={this.handleMapRegionChange}
              />
              <Icon
                size={32}
                color={Colors.PRIMARY_COLOR_DARK}
                name="location-on"
                style={{
                  zIndex: 3,
                  top: '50%',
                  left: '50%',
                  position: 'absolute',
                }}
              />
            </View>
          </View>
        )}
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Confirm Location</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  return {device};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditAddressScreen);
