import {connect} from 'react-redux';
import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {userServices} from '../../services';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.DANGER,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  home: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    color: '#9e9e9e',
    fontWeight: '600',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: Fonts.FONT_SIZE_20,
    marginTop: Dimension.HEIGHT_70,
  },
});

class DeviceNumberErrorScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
    };
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  handleNavigation() {
    this.setState({animating: true});
    const {controllerData, navigation} = this.props;
    const {serialNumber} = controllerData;
    userServices
      .deleteRegistration({serialNumber})
      .then(() => {
        this.setState({animating: false});
        navigation.navigate('DeviceNumber');
      })
      .catch((err) => {
        this.setState({animating: false});
        navigation.goBack();
      });
  }

  render() {
    const {animating} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Spinner visible={animating} />
        <Helpers.StatusBar
          subtitle=""
          title="Add Device"
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <Image
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/unpaired/unpaired.png')}
          />
          <Text
            style={
              styles.text
            }>{`Oops!${'\n'}The device already Activated.`}</Text>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.handleNavigation()}
            style={styles.button}>
            <Text style={styles.home}>Unlink Device</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  return {controllerData};
};

export default connect(mapStateToProps, null)(DeviceNumberErrorScreen);
