/* eslint-disable react/no-did-update-set-state */
/* eslint-disable no-unused-vars */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-useless-escape */
/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import moment from 'moment';
import equal from 'fast-deep-equal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import {logglyServices, userServices, appServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import ListItem from '../../components/listItem';
import {appActions} from '../../actions/app.actions';
import {userActions} from '../../actions/user.actions';
import AlertButton from '../../components/alertButton';
import {generateCalenderPayload} from '../../helpers/create-calender';
import {Colors, Fonts, Dimension, appConstants} from '../../constants';
import {Button} from 'react-native-paper';

const styles = StyleSheet.create({
  box: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  button: {
    flex: 1,
    borderTopWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: '#d2d2d1',
    height: Dimension.HEIGHT_100 / 2,
  },
  card: {
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    alignSelf: 'center',
    width: Dimension.WIDTH_338,
    paddingLeft: Dimension.WIDTH_30,
    paddingRight: Dimension.WIDTH_30,
    marginTop: Dimension.HEIGHT_20 / 2,
    paddingTop: Dimension.HEIGHT_20 / 2,
    marginBottom: 20,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  colorCircle: {
    borderRadius: 25,
    width: Dimension.WIDTH_32,
    height: Dimension.WIDTH_32,
  },
  container: {
    borderRadius: 8,
    backgroundColor: '#f8f8f8',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_240,
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  footer: {
    borderTopWidth: 0.5,
    flexDirection: 'row',
    borderTopColor: '#d2d2d1',
    height: Dimension.HEIGHT_45,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_14,
  },
  formLabel: {
    flex: 2,
    flexDirection: 'column',
  },
  header: {
    alignItems: 'center',
    borderBottomWidth: 0.5,
    justifyContent: 'center',
    borderBottomColor: '#d2d2d1',
    height: Dimension.HEIGHT_45,
  },
  manualSchedule: {
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_60,
    justifyContent: 'space-between',
    marginTop: Dimension.HEIGHT_20,
    paddingLeft: Dimension.WIDTH_30,
    paddingRight: Dimension.WIDTH_30,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  rectangle: {
    flex: 2,
    flexDirection: 'column',
  },
  text: {
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_20,
  },
  footer: {
    height: 50,
  },
  deleteButton: {
    justifyContent: 'center',
    height: 50,
    borderRadius: 0,
  },
});

class EditScheduleScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deleteModal: false,
      endDatePicker: false,
      endTime: new Date(),
      endTimePicker: false,
      startDatePicker: false,
      startTime: new Date(),
      startTimePicker: false,
      loading: false,
    };

    this.checkSheduleStatus = this.checkSheduleStatus.bind(this);
    this.closeDeletePopup = this.closeDeletePopup.bind(this);
    this.dailySchedule = this.dailySchedule.bind(this);
    this.deleteSchedule = this.deleteSchedule.bind(this);
    this.handleDateTime = this.handleDateTime.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.onChangeValue = this.onChangeValue.bind(this);
    this.setEndDate = this.setEndDate.bind(this);
    this.setEndTime = this.setEndTime.bind(this);
    this.setStartDate = this.setStartDate.bind(this);
    this.setStartTime = this.setStartTime.bind(this);
    this.startManualSchedule = this.startManualSchedule.bind(this);
    this.updateSchedule = this.updateSchedule.bind(this);
  }

  componentDidUpdate(prevProps) {
    const {scheduleData} = this.props;
    if (!equal(scheduleData, prevProps.scheduleData)) {
      const {endTime, startTime} = scheduleData;
      const et = new Date(0);
      et.setUTCSeconds(endTime);
      const st = new Date(0);
      st.setUTCSeconds(startTime);
      this.setState({endTime: et, startTime: st});
    }
  }

  onChangeValue(field, val) {
    const {scheduleData} = this.props;
    this.updateSchedule({...scheduleData, [field]: val});
  }

  setEndDate(event, date) {
    this.setState({endDatePicker: false});
    if (date !== undefined) {
      const {scheduleData} = this.props;
      this.updateSchedule({...scheduleData, endDate: new Date(date)});
    }
  }

  setEndTime(event, date) {
    this.setState({endTimePicker: false});
    if (date !== undefined) {
      const {scheduleData} = this.props;
      this.updateSchedule({...scheduleData, endTime: moment(date)});
    }
  }

  setStartDate(event, date) {
    this.setState({startDatePicker: false});
    if (date !== undefined) {
      const {scheduleData} = this.props;
      this.updateSchedule({...scheduleData, startDate: new Date(date)});
    }
  }

  setStartTime(event, date) {
    this.setState({startTimePicker: false});
    if (date !== undefined) {
      const {scheduleData} = this.props;
      this.updateSchedule({...scheduleData, startTime: moment(date)});
    }
  }

  checkSheduleStatus() {
    // const {scheduleData, scheduleObj} = this.props;
    // const {id, name} = scheduleData;
    // if (
    //   Object.keys(scheduleObj).length === 0 &&
    //   scheduleObj.constructor === Object
    // ) {
    //   this.setState({deleteModal: true});
    //   return;
    // }
    // const {currentlyRunningScheduleDto} = scheduleObj;
    // const {scheduleId} = currentlyRunningScheduleDto;

    // if (scheduleId === id) {
    //   alert('Attention', `${name} is running and cannot be deleted.`);
    //   return;
    // }

    this.setState({deleteModal: true});
  }

  dailySchedule(timestamp) {
    const {device, navigation} = this.props;
    store.dispatch(appActions.fetchScheduleTimeTable(device._id, timestamp));
    navigation.navigate('ScheduleAgenda', {timestamp});
  }

  handleDateTime(type) {
    switch (type) {
      case appConstants.END_DATE:
        this.setState({endDatePicker: true});
        break;
      case appConstants.END_TIME:
        this.setState({endTimePicker: true});
        break;
      case appConstants.START_DATE:
        this.setState({startDatePicker: true});
        break;
      case appConstants.START_TIME:
        this.setState({startTimePicker: true});
        break;
      default:
        break;
    }
  }

  handleNavigation(screen) {
    // const {scheduleData} = this.props;
    // if (screen !== 'EditScheduleName') {
    //   const {id} = scheduleData;
    //   const {device} = this.props;
    //   appServices.checkSchedule(device._id, id).then((res) => {
    //     if (res) {
    //       alert('Sprinker', "Can't edit schedule now");
    //       return;
    //     }
    //     const {navigation} = this.props;
    //     navigation.navigate(screen);
    //   });
    //   return;
    // }
    const {navigation} = this.props;
    navigation.navigate(screen);
  }

  closeDeletePopup() {
    this.setState({deleteModal: false});
  }

  deleteSchedule() {
    this.setState({loading: true});
    const {scheduleData} = this.props;
    this.setState({deleteModal: false});
    userServices
      .deleteSchedule(scheduleData)
      .then(() => {
        const {device} = this.props;
        store.dispatch(appActions.fetchSchedules(device._id));
        setTimeout(() => {
          console.log('Edit Schedule');
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                const {navigation} = this.props;
                this.setState({loading: false});
                navigation.goBack();
              });
          }
        }, 2000);
      })
      .catch(() => {
        this.setState({loading: false}, () => {
          alert('Error', 'Cannot delete schedule');
        });
      });
  }

  startManualSchedule() {
    const {scheduleData, device} = this.props;
    const payload = {
      scheduleId: scheduleData._id,
      manualSchedule: true,
      deviceId: device._id,
      name: scheduleData.name,
      scheduleZones: scheduleData.zones.map((item) => ({
        duration: item.duration,
        zone: item.value,
      })),
    }
    userServices
      .addManualSchedule(payload)
      .then((res) => {
        const {navigation} = this.props;
        store.dispatch(userActions.dockView(false));
        navigation.navigate('ActiveSchedule');
      })
      .catch((error) => {
        alert('Sprinkler', error.errorMessage);
      });
  }

  updateSchedule(schedule) {
    this.setState({loading: true});
    const {device} = this.props;
    userServices
      .updateSchedule(device._id, schedule)
      .then((updatedSchedule) => {
        const {userData} = this.props;
        logglyServices.logEvent(
          {user: userData.email, schedule},
          'updateSchedule',
        );
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(appActions.fetchSchedule(updatedSchedule));
        store.dispatch(appActions.fetchZones(device._id));
        setTimeout(() => {
          console.log('Edit Schedule');
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                store.dispatch(appActions.fetchCalendarDates(id));
              });
          }
        }, 2000);
        this.setState({loading: false});
      })
      .catch(() => {
        this.setState({loading: false}, () => {
          alert('Error', 'Cannot update schedule');
        });
      });
  }

  render() {
    const {navigation, scheduleData} = this.props;

    if (
      Object.keys(scheduleData).length === 0 &&
      scheduleData.constructor === Object
    ) {
      return null;
    }

    const {deleteModal, loading} = this.state;

    const {endDate, startDate} = scheduleData;

    const {endTime, startTime} = this.state;

    const {
      endDatePicker,
      endTimePicker,
      startDatePicker,
      startTimePicker,
    } = this.state;

    const [yyE, mmE, ddE] = endDate.split('T')[0].split('-');
    const [yyS, mmS, ddS] = startDate.split('T')[0].split('-');
    return (
      <View style={styles.box}>
        <Spinner visible={loading} />

        <Helpers.StatusBar
          isBackNavigable
          title="Edit Schedule"
          subtitle={scheduleData.name}
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <AlertButton
          action="OK, Delete"
          onPress={this.deleteSchedule}
          closePopup={this.closeDeletePopup}
          modalVisible={deleteModal}
          text="Are you sure you want to delete the schedule?"
        />

        {startDatePicker && (
          <RNDateTimePicker
            value={new Date(startDate)}
            maximumDate={new Date(yyE, mmE - 1, ddE)}
            minimumDate={new Date()}
            mode="date"
            is24Hour={false}
            display="default"
            onChange={this.setStartDate}
          />
        )}

        {startTimePicker && (
          <RNDateTimePicker
            value={startTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setStartTime}
          />
        )}

        {endDatePicker && (
          <RNDateTimePicker
            value={new Date(endDate)}
            minimumDate={new Date(yyS, mmS - 1, parseInt(ddS) + 1)}
            mode="date"
            is24Hour={false}
            display="default"
            onChange={this.setEndDate}
          />
        )}

        {endTimePicker && (
          <RNDateTimePicker
            value={endTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setEndTime}
          />
        )}

        <ScrollView>
          <TouchableOpacity
            onPress={() => this.startManualSchedule()}
            style={styles.manualSchedule}>
            <Text style={styles.formField}>Run Schedule Now</Text>
            <Icon
              name="play-circle-filled"
              size={36}
              color={Colors.PRIMARY_COLOR_DARK}
            />
          </TouchableOpacity>
          <View style={styles.card}>
            <View style={styles.formElement}>
              <View style={styles.formLabel}>
                <Text style={styles.formField}>Enabled</Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={scheduleData.enabled}
                  trackColor={Colors.PRIMARY_COLOR_DARK}
                  onValueChange={() =>
                    this.onChangeValue('enabled', !scheduleData.enabled)
                  }
                />
              </View>
            </View>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => this.handleNavigation('EditScheduleName')}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Name</Text>
                <Text style={styles.formFieldValue}>{scheduleData.name}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleNavigation('EditTimeSlot')}
              style={styles.formElement}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Time Slot</Text>
                <Text numberOfLines={1} style={styles.formFieldValue}>
                  {scheduleData.timeSlot === 0 ? 'End before Sunrise' : null}
                  {scheduleData.timeSlot === 1
                    ? 'End before specific time'
                    : null}
                  {scheduleData.timeSlot === 2 ? 'Start After Sunset' : null}
                  {scheduleData.timeSlot === 3
                    ? 'Start at specific time'
                    : null}
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleNavigation('ScheduleZone')}
              style={styles.formElement}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Active Zones</Text>
                <Text numberOfLines={1} style={styles.formFieldValue}>
                  {scheduleData.scheduleZoneNames.join(', ')}
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <ListItem
              title="Start Date"
              text={moment(scheduleData.startDate).format('MMMM Do YYYY')}
              type={appConstants.START_DATE}
              onClick={this.handleDateTime}
            />
            <ListItem
              title="End Date"
              text={moment(scheduleData.endDate).format('MMMM Do YYYY')}
              type={appConstants.END_DATE}
              onClick={this.handleDateTime}
            />
            {scheduleData.timeSlot === 1 ? (
              <ListItem
                title="End Time"
                text={moment(scheduleData.endTime).format('hh:mm A')}
                type={appConstants.END_TIME}
                onClick={this.handleDateTime}
              />
            ) : scheduleData.timeSlot === 3 ? (
              <ListItem
                title="Start Time"
                text={moment(scheduleData.startTime).format('hh:mm A')}
                type={appConstants.START_TIME}
                onClick={this.handleDateTime}
              />
            ) : null}
            <ListItem
              onClick={() => this.handleNavigation('EditInterval')}
              text={scheduleData.days}
              title="Day Interval"
              type="navigator"
            />
            <View style={styles.formElement}>
              <View style={styles.formLabel}>
                <Text style={styles.formField}>Smart Watering</Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={scheduleData.smartWatering}
                  trackColor={Colors.PRIMARY_COLOR_DARK}
                  onValueChange={() =>
                    this.onChangeValue(
                      'smartWatering',
                      !scheduleData.smartWatering,
                    )
                  }
                />
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Button
            mode="contained"
            onPress={() => this.checkSheduleStatus()}
            style={styles.deleteButton}
            color={'#E53935'}>
            Delete Schedule
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleData} = state.schedule;
  const {scheduleObj} = state.currentSchedule;
  const {userData} = state.user;
  return {
    device,
    scheduleData,
    scheduleObj,
    userData,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditScheduleScreen);
