/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Alert,
  Image,
} from 'react-native';
import equal from 'fast-deep-equal';
import Icon from 'react-native-vector-icons/MaterialIcons';
import wifi from 'react-native-android-wifi';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {Colors, Dimension, Fonts} from '../../constants';
import {appServices} from '../../services';
import WifiManager from 'react-native-wifi-reborn';
import {logEvent, recordError} from '../../analytics';
import {Button, Caption, TextInput} from 'react-native-paper';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  card: {
    marginTop: Dimension.HEIGHT_100,
  },
  container: {
    flex: 1,
  },
  helperCard: {
    paddingHorizontal: 16,
    paddingVertical: 24,
    backgroundColor: '#fff',
    margin: 12,
    borderRadius: 4,
    minHeight: 400,
  },
  errorText: {
    color: '#000',
    marginLeft: 12,
    paddingTop: 4,
    paddingBottom: 12,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  subHeader: {
    margin: 8,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: 16,
  },
  image: {
    alignSelf: 'center',
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    justifyContent: 'space-between',
    paddingTop: Dimension.HEIGHT_20,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  name: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  tryButton: {
    borderRadius: 5,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
    width: Dimension.WIDTH_135,
    height: Dimension.HEIGHT_50,
    marginTop: Dimension.HEIGHT_45,
  },
  tryButtonText: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
  },
  wifi: {
    alignSelf: 'center',
  },
  caption: {
    paddingHorizontal: 26,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

class SelectHomeWifiScreen extends Component {
  fetchNetwork = true;
  constructor(props) {
    super(props);
    this.state = {
      ssids: [],
      selectedSSID: '',
      noDeviceFound: false,
      loader: false,
      showErrorScreen: false,
    };
    this.getNetworkList = this.getNetworkList.bind(this);
    this.scanWifi = this.scanWifi.bind(this);
  }

  componentDidMount() {
    if (this.fetchNetwork) {
      this.getNetworkList();
    }
    this.fetchNetwork = false;
  }

  componentDidUpdate(prevProps) {
    const {controllerRegistration, navigation} = this.props;
    if (!equal(controllerRegistration, prevProps.controllerRegistration)) {
      const {status} = controllerRegistration;
      if (status === false) {
        navigation.navigate('SetupFailed');
      }
    }
  }

  onSubmit = () => {
    const {selectedSSID} = this.state;
    if (selectedSSID === '') {
      alert('Error', 'Please select your network');
      return;
    }
    const {controllerData, navigation} = this.props;
    store.dispatch(
      appActions.setControllerData({...controllerData, ssid: selectedSSID}),
    );
    navigation.navigate('WifiPassword');
  };

  async getNetworkList() {
    if (Platform.OS === 'android') {
      wifi.loadWifiList(
        async (wifiStringList) => {
          const networks = await JSON.parse(wifiStringList);
          const ssids = [];
          networks.forEach((network) => {
            if (
              !network.SSID.startsWith('Sprinkler') &&
              network.frequency < 2500
            ) {
              ssids.push(network.SSID);
            }
          });
          this.scanWifi();
          this.setState({ssids});
        },
        (error) => {
          this.setState({
            noDeviceFound: true,
          });
          alert('Sprinkler', error);
        },
      );
    } else {
      const {controllerData} = this.props;
      const {serialNumber} = controllerData;
      WifiManager.connectToProtectedSSID(
        `Sprinkler${serialNumber.split('AMEBAA')[1]}`,
        'password',
        false,
      ).then(
        (data) => {
          console.log('Connected successfully!');
          appServices
            .fetchNetworkList()
            .then((networks) => {
              const {ssids} = networks;
              if (Array.isArray(ssids) && ssids.length)
                this.checkSprinkerWifi(ssids);
              const controllerSsid = `Sprinkler${
                serialNumber.split('AMEBAA')[1]
              }`;
              const filterSsids = ssids.filter(
                (item) => item !== controllerSsid,
              );
              this.setState(
                {
                  iosNetworkError: false,
                  ssids: filterSsids,
                  showErrorScreen: filterSsids.length ? false : true,
                },
                () => {
                  logEvent('SELECT_WIFI', {
                    screen: 'SELECT_WIFI',
                    availableWifi: JSON.stringify(this.state.ssids),
                  });
                },
              );
            })
            .catch((err) => {
              this.setState({
                noDeviceFound: true,
              });
              recordError('Error: Wifi List', err);
            });
        },
        (error) => {
          this.setState({
            noDeviceFound: true,
          });
          recordError('Error: Wifi List', error);
        },
      ),
        (error) => {
          this.setState({
            noDeviceFound: true,
          });
          recordError('Error: Wifi List', error);
          console.log(error);
        };
    }
  }

  scanWifi() {
    wifi.reScanAndLoadWifiList(
      (wifiStringList) => {
        const networks = JSON.parse(wifiStringList);
        console.log('network', networks);
        const {ssids} = this.state;
        if (Array.isArray(networks) && networks.length)
          this.checkSprinkerWifi(networks);
        networks.forEach((network) => {
          if (
            ssids.indexOf(network.SSID) < 0 &&
            !network.SSID.startsWith('Sprinkler')
          ) {
            ssids.push(network.SSID);
          }
        });
        this.setState(
          {ssids, showErrorScreen: ssids.length ? false : true},
          () => {
            logEvent('SELECT_WIFI', {
              screen: 'SELECT_WIFI',
              availableWifi: JSON.stringify(ssids),
            });
          },
        );
      },
      (error) => {
        this.setState({
          noDeviceFound: true,
        });
        alert('Sprinkler', error);
      },
    );
  }

  checkSprinkerWifi = (networks) => {
    let isAvailable = false;
    const {controllerData} = this.props;
    const {serialNumber} = controllerData;
    const controllerSsid = `Sprinkler${serialNumber.split('AMEBAA')[1]}`;
    if (Platform.OS === 'android') {
      isAvailable = networks.find((item) => item.SSID === controllerSsid);
    } else {
      isAvailable = true;
    }
    if (!isAvailable) {
      this.setState({
        noDeviceFound: true,
      });
    } else {
      this.setState({
        noDeviceFound: false,
      });
    }
  };

  render() {
    const {navigation, error} = this.props;
    const {selectedSSID, ssids, noDeviceFound, showErrorScreen} = this.state;
    // if (showErrorScreen || noDeviceFound || error) {
    //   return (
    //     <View style={styles.container}>
    //       <Helpers.StatusBar
    //         title={
    //           !ssids.length && !noDeviceFound
    //             ? 'Add Device'
    //             : 'Add Device Error'
    //         }
    //         isBackNavigable={true}
    //         subtitle="Device Setup"
    //         child={Helpers.HelpButton}
    //         navigation={navigation}
    //       />
    //       <View style={styles.helperCard}>
    //         {!ssids.length && !noDeviceFound ? (
    //           <View style={{justifyContent: 'center', height: 100}}>
    //             <ActivityIndicator
    //               animating={true}
    //               color={Colors.SECONDARY_COLOR}
    //             />
    //           </View>
    //         ) : (
    //           <View>
    //             <Text style={{fontSize: 16, textAlign: 'center'}}>
    //               App is not able to detect the sprinkler controller device over
    //               wifi
    //             </Text>
    //             <Image
    //               source={require('../../../assets/images/wifi-error/wifi-error.png')}
    //               style={{marginVertical: 16, alignSelf: 'center'}}
    //             />
    //             <Text style={{fontSize: 15, opacity: 0.8, fontWeight: 'bold'}}>
    //               Possible Errors
    //             </Text>
    //             <Text style={styles.errorText}>
    //               1. Controller is switched off
    //             </Text>
    //             <Text style={styles.errorText}>
    //               2. Controller is already associated with a user account
    //             </Text>
    //             <Text style={styles.errorText}>
    //               3. Controller is not available over wifi network
    //             </Text>
    //             <Text style={styles.errorText}>
    //               {error ? `4. ${error}` : null}
    //             </Text>
    //           </View>
    //         )}
    //       </View>
    //       {!ssids.length && !noDeviceFound ? (
    //         <Helpers.Footer>
    //           <TouchableOpacity onPress={() => {}} style={styles.button}>
    //             <Text style={styles.text}>Next</Text>
    //           </TouchableOpacity>
    //         </Helpers.Footer>
    //       ) : (
    //         <Helpers.Footer>
    //           {Platform.OS === 'android' ? (
    //             <TouchableOpacity
    //               onPress={() => {
    //                 this.setState({noDeviceFound: false});
    //                 if (Platform.OS === 'android') {
    //                   this.scanWifi();
    //                 } else {
    //                   this.getNetworkList();
    //                 }
    //               }}
    //               style={styles.button}>
    //               <Text style={styles.text}>ReScan Wifi</Text>
    //             </TouchableOpacity>
    //           ) : null}
    //           <TouchableOpacity
    //             onPress={() => this.props.navigation.navigate('DeviceNumber')}
    //             style={[
    //               styles.button,
    //               {borderLeftColor: '#fff', borderLeftWidth: 1},
    //             ]}>
    //             <Text style={styles.text}>Device Number</Text>
    //           </TouchableOpacity>
    //         </Helpers.Footer>
    //       )}
    //     </View>
    //   );
    // } else {
    //   return (
    //     <View style={styles.container}>
    //       <Helpers.StatusBar
    //         title="Add Device"
    //         isBackNavigable
    //         subtitle="Device Setup"
    //         child={Helpers.HelpButton}
    //         navigation={navigation}
    //       />

    //       <Helpers.Card>
    //         <Text style={styles.header}>Select Your Home Wi-Fi</Text>
    //         <ScrollView
    //           style={[styles.container, {marginBottom: 30}]}
    //           showsVerticalScrollIndicator={true}>
    //           {ssids.map((ssid) => (
    //             <TouchableOpacity
    //               key={ssid}
    //               onPress={() => this.setState({selectedSSID: ssid})}>
    //               <View style={styles.listItem}>
    //                 <Text style={styles.name}>{ssid}</Text>
    //                 {ssid === selectedSSID && (
    //                   <Icon
    //                     name="done"
    //                     color={Colors.PRIMARY_COLOR}
    //                     size={24}
    //                   />
    //                 )}
    //               </View>
    //             </TouchableOpacity>
    //           ))}
    //           {ssids.length ? null : (
    //             <View>
    //               <View style={{justifyContent: 'center', height: 100}}>
    //                 <ActivityIndicator
    //                   animating={true}
    //                   color={Colors.SECONDARY_COLOR}
    //                 />
    //               </View>
    //             </View>
    //           )}
    //         </ScrollView>
    //       </Helpers.Card>
    //       <View style={styles.caption}>
    //         <View>
    //           <Caption>Scroll Down in the list to view the network</Caption>
    //           <Caption>Note: 5G Wifi network are not serviceable</Caption>
    //         </View>
    //         <Button
    //           mode="text"
    //           color={'#212121'}
    //           onPress={() => {
    //             this.setState({noDeviceFound: false});
    //             if (Platform.OS === 'android') {
    //               this.scanWifi();
    //             } else {
    //               this.getNetworkList();
    //             }
    //           }}>
    //           Re-Scan
    //         </Button>
    //       </View>
    //       <Helpers.Footer>
    //         <TouchableOpacity onPress={this.onSubmit} style={styles.button}>
    //           <Text style={styles.text}>Next</Text>
    //         </TouchableOpacity>
    //       </Helpers.Footer>
    //     </View>
    //   );
    // }
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Add Device"
          isBackNavigable
          subtitle="Device Setup"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <TextInput
            label="Not able to find your wifi?"
            placeholder="Enter your wifi display name"
            mode="outlined"
            onChangeText={(text) => this.setState({selectedSSID: text})}
          />
          <Text style={[styles.header, {marginTop: 12}]}>
            Select Your Home Wi-Fi
          </Text>
          <ScrollView
            style={[styles.container, {marginBottom: 30}]}
            showsVerticalScrollIndicator={true}>
            {ssids.map((ssid) => (
              <TouchableOpacity
                key={ssid}
                onPress={() => this.setState({selectedSSID: ssid})}>
                <View style={styles.listItem}>
                  <Text style={styles.name}>{ssid}</Text>
                  {ssid === selectedSSID && (
                    <Icon name="done" color={Colors.PRIMARY_COLOR} size={24} />
                  )}
                </View>
              </TouchableOpacity>
            ))}
            {ssids.length ? null : (
              <View>
                <View style={{justifyContent: 'center', height: 100}}>
                  <ActivityIndicator
                    animating={true}
                    color={Colors.SECONDARY_COLOR}
                  />
                </View>
              </View>
            )}
          </ScrollView>
        </Helpers.Card>
        <View style={styles.caption}>
          <View>
            <Caption>Scroll Down in the list to view the network</Caption>
            <Caption>Note: 5G Wifi network are not serviceable</Caption>
          </View>
          <Button
            mode="text"
            color={'#212121'}
            onPress={() => {
              this.setState({noDeviceFound: false});
              if (Platform.OS === 'android') {
                this.scanWifi();
              } else {
                this.getNetworkList();
              }
            }}>
            Re-Scan
          </Button>
        </View>
        <Helpers.Footer>
          <TouchableOpacity onPress={this.onSubmit} style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  const {error} = state.controllerRegistration;
  return {controllerData, error};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectHomeWifiScreen);
