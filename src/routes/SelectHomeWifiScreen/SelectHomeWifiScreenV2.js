import {connect} from 'react-redux';
import React, {useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import {
  View,
  StyleSheet,
  Alert,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import store from '../../helpers/store';
import {Subheading, Button, Caption, Text, TextInput} from 'react-native-paper';
import * as Helpers from '../../helpers/modules';
import WifiManager from 'react-native-wifi-reborn';
import alert from '../../components/alert';
import {appServices} from '../../services';
import LottieView from 'lottie-react-native';
import Geolocation from '@react-native-community/geolocation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../constants';
import {appActions} from '../../actions/app.actions';

const DEVICE_ERROR = {
  notFound: 1,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  listContainer: {
    padding: 12,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#dadada',
    borderBottomWidth: 0.5,
  },
  listText: {
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
});

const SelectHomeWifiScreen = ({navigation, error, controllerData}) => {
  const [connectionError, setConError] = useState(0);
  const [networks, setNetworks] = useState([]);
  useEffect(() => {
    setTimeout(() => {
      getWifiNetwork();
    }, 3000);
  }, []);

  function getWifiNetwork() {
    try {
      const {serialNumber} = controllerData;
      Geolocation.getCurrentPosition(
        () => {
          try {
            WifiManager.connectToProtectedSSID(
              `Sprinkler${serialNumber.split('AMEBAA')[1]}`,
              'password',
              false,
            ).then(
              async () => {
                const networks = await appServices.fetchNetworkList();
                setNetworks(networks.ssids);
              },
              (conErr) => {
                setConError(DEVICE_ERROR.notFound);
                alert('Wifi Connection Error', conErr.toString());
              },
            );
          } catch (conErr) {
            setConError(DEVICE_ERROR.notFound);
            alert('Wifi Connection Error', conErr.toString());
          }
        },
        (error) => {
          Alert.alert(
            error.message,
            'Looks like your GPS is Off, hence not able to proceed',
            [
              {
                text: 'I have turned on GPS',
                onPress: () => {
                  getWifiNetwork();
                },
              },
            ],
            {cancelable: true},
          );
        },
      );
    } catch (err) {
      alert('Error', err);
    }
  }
  const updateSsid = (ssid) => {
    store.dispatch(appActions.setControllerData({...controllerData, ssid}));
  };
  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.listContainer}
        onPress={() => updateSsid(item)}>
        <Subheading style={styles.listText}>
          {index + 1}. {item}
        </Subheading>
        {controllerData.ssid === item ? (
          <Icon name="check-box" color={Colors.PRIMARY_COLOR_DARK} size={24} />
        ) : (
          <Icon
            name="check-box-outline-blank"
            color={Colors.PRIMARY_COLOR}
            size={24}
          />
        )}
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <Helpers.StatusBar
        title="Wifi Selection"
        isBackNavigable
        subtitle="Device Setup"
        child={Helpers.HelpButton}
        navigation={navigation}
      />
      {connectionError || error ? (
        <Helpers.Card>
          <View style={{flex: 1, width: '100%'}}>
            <LottieView
              source={
                error
                  ? require('../../../assets/animation/Failed.json')
                  : require('../../../assets/animation/NotFound.json')
              }
              autoPlay
              loop
            />
          </View>
          <View>
            {error ? (
              <Subheading>
                Controller registration failed on server. Please contact the
                support
              </Subheading>
            ) : (
              <Subheading>
                App is not able to track controller in the vicinity. Please make
                sure that the controller is ON and nearby
              </Subheading>
            )}
          </View>
        </Helpers.Card>
      ) : (
        <Helpers.Card>
          {networks.length ? (
            <ScrollView>
              <View style={styles.row}>
                <Icon name="wifi" color={Colors.PRIMARY_COLOR} size={18} />
                <Subheading style={{marginLeft: 8}}>
                  Select your home wifi
                </Subheading>
              </View>
              <View>
                <FlatList
                  style={{
                    marginVertical: 12,
                    maxHeight: 400,
                    minWidth: 300,
                  }}
                  data={networks}
                  renderItem={renderItem}
                  keyExtractor={(item) => item}
                />
              </View>
              <View>
                <Caption>Wifi hidden or not discoverable?</Caption>
                <TextInput
                  label="Provide Ssid Manually"
                  mode="outlined"
                  style={{marginTop: 8, height: 42}}
                  onChangeText={(text) => updateSsid(text)}
                />
              </View>
            </ScrollView>
          ) : (
            <View>
              <View style={{flex: 1}}>
                <LottieView
                  source={require('../../../assets/animation/HomeWifiSearch.json')}
                  autoPlay
                  loop
                />
              </View>
              <Caption>Searching wifi networks around you</Caption>
            </View>
          )}
        </Helpers.Card>
      )}
      <Helpers.Footer>
        <Button
          style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
          onPress={() => navigation.navigate('WifiPassword')}
          mode="contained"
          disabled={!controllerData.ssid}>
          <Subheading
            style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
            Continue
          </Subheading>
        </Button>
      </Helpers.Footer>
    </View>
  );
};

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  const {error} = state.controllerRegistration;
  return {controllerData, error};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectHomeWifiScreen);
