import moment from 'moment';

const PF = {
  'Cool season grass': 0.8,
  'Warm Season Grass': 0.6,
  Shrubs: 0.5,
  Trees: 0.5,
  Annual: 0.5,
  Perennial: 0.5,
  Xeriscape: 0.5,
  Garden: 0.5,
};

const MonthWisePf = {
  January: 0.61,
  February: 0.64,
  March: 0.75,
  April: 1.04,
  May: 0.95,
  June: 0.88,
  July: 0.94,
  August: 0.86,
  September: 0.74,
  October: 0.75,
  November: 0.69,
  December: 0.6,
};

const ET = {
  January: 0.05,
  February: 0.07,
  March: 0.1,
  April: 0.14,
  May: 0.17,
  June: 0.19,
  July: 0.2,
  August: 0.17,
  September: 0.17,
  October: 0.12,
  November: 0.06,
  December: 0.04,
};

const LA = 46.45;

const wateringRate = {
  'Fixed Spray Head': 1.5,
  'Rotor Head': 1,
  'Rotary Nozzle': 0.7,
  Mister: 0.6,
  Bubbler: 0.5,
  Emitter: 0.5,
  'Drip Line': 0.3,
};

export function calculateTime(zone) {
  const month = moment().format('MMMM');
  let calPF = MonthWisePf[month];
  const calET = ET[month];
  let calRate = wateringRate['Rotary Nozzle'];
  if (zone.vegetationType) {
    calPF = PF[zone.vegetationType] || calPF;
  }
  if (zone.sprayHeadType) {
    calRate = wateringRate[zone.sprayHeadType] || calRate;
  }
  const time = (calET * calPF * LA * 0.623) / calRate;
  return Math.round(time * 60);
}
