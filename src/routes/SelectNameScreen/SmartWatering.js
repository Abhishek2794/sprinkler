import React, {useEffect} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {Colors, Dimension, Fonts, appConstants} from '../../constants';
import Icon from 'react-native-vector-icons/AntDesign';
import {Grayscale} from 'react-native-color-matrix-image-filters';
import {calculateTime} from './wateringAlgo';

const styles = StyleSheet.create({
  zoneList: {
    marginTop: 10,
    alignSelf: 'center',
    flexDirection: 'column',
  },
  zone: {
    marginBottom: 10,
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    shadowColor: '#000',
    flexDirection: 'row',
    height: Dimension.HEIGHT_130,
    shadowOffset: {width: 0, height: 12},
    backgroundColor: Colors.TEXT_COLOR,
  },
  zoneContainer: {
    height: Dimension.HEIGHT_130,
    left: 0,
    position: 'absolute',
    top: 0,
    width: Dimension.WIDTH_338,
    zIndex: 2,
  },
  zoneContent: {
    flex: 1,
    flexDirection: 'row',
  },
  zoneControl: {
    height: Dimension.HEIGHT_130,
    justifyContent: 'center',
  },
  zoneDuration: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
    marginVertical: 12,
    fontWeight: 'bold',
  },
  zoneIndex: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zoneName: {
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_16,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
});

const SmartWatering = ({zones, allZones, smartWatering, onUpdate}) => {
  useEffect(() => {
    if (smartWatering) {
      const zoneCopy = zones.map((item) => {
        const zone = allZones.find(ele => ele._id === item.id);
        const calDuration = calculateTime(zone);
        return {
          ...item,
          duration: calDuration,
        };
      });
      onUpdate(zoneCopy);
    }
  }, []);
  const updateZoneDuration = (index, zoneOperator) => {
    const zonesCopy = zones.slice();
    switch (zoneOperator) {
      case appConstants.INCREMENT:
        zonesCopy[index].duration += 15;
        break;
      case appConstants.DECREMENT:
        if (zonesCopy[index].duration > 15) {
          zonesCopy[index].duration -= 15;
        }
        break;
      default:
        break;
    }
    onUpdate(zonesCopy);
  };
  const getZoneImage = (zoneId) => {
    const zone = allZones.find((item) => item._id === parseInt(zoneId));
    return zone.defaultImage;
  };
  return (
    <View>
      <View style={styles.zoneList}>
        {zones.map((item, index) => {
          const {id, duration} = item;
          const zone = allZones.find(ele => ele._id === id);
          const zoneImage = getZoneImage(id);
          return (
            <View key={zone._id} style={styles.zone}>
              <View style={styles.zoneContainer}>
                <View style={styles.row}>
                  <View style={styles.zoneControl}>
                    <Text style={styles.zoneName} numberOfLines={1}>
                      {zone.name}
                    </Text>
                    <Text style={styles.zoneIndex}>
                      {zone.number < 10
                        ? `Zone 0${zone.number}`
                        : `Zone ${zone.number}`}
                    </Text>
                  </View>
                  <View style={styles.durationControl}>
                    <TouchableWithoutFeedback
                      onPress={() =>
                        updateZoneDuration(index, appConstants.INCREMENT)
                      }>
                      <Icon
                        name="pluscircle"
                        size={30}
                        color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      />
                    </TouchableWithoutFeedback>
                    <Text style={styles.zoneDuration}>{`${Math.floor(
                      duration / 60,
                    )}:${duration % 60 === 0 ? '00' : duration % 60}`}</Text>
                    <TouchableWithoutFeedback
                      onPress={() =>
                        updateZoneDuration(index, appConstants.DECREMENT)
                      }>
                      <Icon
                        name="minuscircle"
                        size={30}
                        color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      />
                    </TouchableWithoutFeedback>
                  </View>
                </View>
              </View>
              <Grayscale key={zone.number} amount={0}>
                <Image
                  resizeMode="cover"
                  style={{
                    width: Dimension.WIDTH_338,
                    height: Dimension.HEIGHT_130,
                  }}
                  source={
                    zoneImage
                      ? {uri: zoneImage}
                      : require('../../../assets/images/zone-default/zone-default.png')
                  }
                />
              </Grayscale>
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default SmartWatering;
