/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Modal,
  Alert,
  SafeAreaView,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import {logglyServices, userServices, appServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {Caption, Switch, Title, Subheading, Button} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {generateCalenderPayload} from '../../helpers/create-calender';
import {ScrollView} from 'react-native-gesture-handler';
import moment from 'moment';
import SmartWatering from './SmartWatering';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  box: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  container: {
    flex: 1,
    position: 'relative',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  smartWatering: {
    paddingVertical: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  caption: {
    width: '75%',
  },
  modalHeader: {
    paddingVertical: 16,
    paddingHorizontal: 12,
    borderBottomColor: Colors.PRIMARY_COLOR,
    backgroundColor: Colors.PRIMARY_COLOR,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 12,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
  },
  borderBottom: {
    padding: 12,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
  },
  title: {
    letterSpacing: 1.4,
    fontWeight: 'bold',
    fontSize: 12,
    textTransform: 'uppercase',
  },
  rowHeader: {
    padding: 12,
    backgroundColor: '#f5f6f7',
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '50%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

class SelectNameScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      smartWatering: false,
      name: '',
      error: '',
      showPreview: false,
      showZones: false,
      showTimings: false,
    };
    this.onSave = this.onSave.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(name) {
    if (name) {
      this.setState({name, error: ''});
      return;
    }
    this.setState({name, error: 'Please enter schedule name'});
  }

  onSubmit() {
    const {name} = this.state;
    if (name === '') {
      this.setState({error: 'Please enter schedule name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({error: 'Name cannot have special characters'});
    }
  }

  showPreview = () => {
    const {name, showTimings} = this.state;
    const {schedules} = this.props;
    if (!name) {
      this.setState({error: 'Please enter schedule name'});
      return;
    }
    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({error: 'Name cannot have special characters'});
      return;
    }
    const isDuplicate = schedules.find((item) => item.name === name.trim());
    if (isDuplicate) {
      this.setState({error: 'Duplicate schedule name not allowed !!'});
      return;
    }
    if (showTimings) {
      this.setState({showTimings: false, showPreview: true});
    } else {
      this.setState({showTimings: true, showPreview: false});
    }
  };

  onSave() {
    const {name, smartWatering} = this.state;
    this.setState({animating: true, showPreview: false});

    const {device, navigation, schedule} = this.props;

    userServices
      .createSchedule(device._id, {
        ...schedule,
        name,
        enabled: true,
        smartWatering,
      })
      .then(() => {
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(appActions.fetchZones(device._id));
        setTimeout(() => {
          const payload = generateCalenderPayload();
          console.log('Create Schedule');
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                console.log('Here');
                store.dispatch(appActions.fetchCalendarDates(device._id));
                store.dispatch(appActions.createSchedule(undefined));
                this.setState({animating: false});
              });
          }
        }, 2000);
        navigation.navigate('Schedules');
      })
      .catch(() => {
        this.setState({animating: false});
        alert('Error', 'Could not save schedule');
      });
  }

  getTimeslot = (schedule) => {
    switch (schedule.timeSlot) {
      case 0:
        return (
          <View style={styles.row}>
            <Text style={styles.title}>End Time</Text>
            <Subheading>{'06:00:00 AM'}</Subheading>
          </View>
        );
      case 1:
        return (
          <View style={styles.row}>
            <Text style={styles.title}>End Time</Text>
            <Subheading>
              {moment(schedule.endTime).format('hh:mm:ss A')}
            </Subheading>
          </View>
        );
      case 2:
        return (
          <View style={styles.row}>
            <Text style={styles.title}>Start Time</Text>
            <Subheading>{'06:00:00 PM'}</Subheading>
          </View>
        );
      case 3:
        return (
          <View style={styles.row}>
            <Text style={styles.title}>Start Time</Text>
            <Subheading>
              {moment(schedule.startTime).format('hh:mm:ss A')}
            </Subheading>
          </View>
        );
    }
  };

  discardSchedule = () =>
    Alert.alert(
      'Confirmation',
      'Are you sure you want to discard this schedule?',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: () => this.props.navigation.navigate('Schedules'),
        },
      ],
    );

  render() {
    const {navigation, schedule, zones} = this.props;
    const {
      animating,
      error,
      name,
      showPreview,
      showZones,
      showTimings,
      smartWatering,
    } = this.state;
    return (
      <View style={styles.container}>
        <Spinner visible={animating} />

        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Choose Name"
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        {showTimings ? (
          <View style={{flex: 1, paddingBottom: 60}}>
            <Subheading style={{paddingHorizontal: 20}}>
              Selected Zones
            </Subheading>
            <ScrollView style={{flex: 1}}>
              <SmartWatering
                zones={schedule.zones}
                allZones={zones}
                smartWatering={smartWatering}
                onUpdate={(data) => {
                  store.dispatch(
                    appActions.createSchedule({
                      ...schedule,
                      zones: data,
                    }),
                  );
                }}
              />
            </ScrollView>
          </View>
        ) : (
          <Helpers.Card>
            <View style={styles.formElement}>
              <View style={styles.box}>
                <Text style={styles.formField}>Schedule Name</Text>
                <TextInput
                  autoFocus
                  maxLength={250}
                  numberOfLines={1}
                  editable
                  returnKeyType="done"
                  value={name}
                  contextMenuHidden
                  selectTextOnFocus={false}
                  keyboardType="ascii-capable"
                  ref={(ref) => {
                    this.editName = ref;
                  }}
                  onSubmitEditing={() => this.onSubmit()}
                  onChangeText={(text) => this.onChangeText(text)}
                  style={[
                    styles.formFieldValue,
                    {
                      borderBottomColor: error
                        ? Colors.DANGER
                        : Colors.SWIPER_ACTIVE_DOT_COLOR,
                    },
                  ]}
                />
              </View>
              {error ? <Text style={styles.errorText}>{error}</Text> : null}
              <View style={styles.smartWatering}>
                <View style={styles.caption}>
                  <Text style={styles.formField}>Allow Smart Watering</Text>
                  <Caption>
                    Smart watering technique uses weather forecast to decide for
                    watering a zone and helps in cost and water saving.
                  </Caption>
                </View>
                <Switch
                  color={Colors.PRIMARY_COLOR_DARK}
                  value={this.state.smartWatering}
                  onValueChange={() => {
                    const {device} = this.props;
                    if (false) {
                      alert(
                        'Operation denied',
                        'Rain sensors are enabled hence smart watering is disabled',
                      );
                    } else {
                      this.setState({smartWatering: !this.state.smartWatering});
                    }
                  }}
                />
              </View>
            </View>
          </Helpers.Card>
        )}

        <Helpers.Footer>
          <TouchableOpacity onPress={this.showPreview} style={styles.button}>
            <Text style={styles.text}>
              {showTimings ? 'Review Schedule' : 'Zone Timings'}
            </Text>
          </TouchableOpacity>
        </Helpers.Footer>
        {schedule ? (
          <Modal
            animationType="slide"
            transparent={false}
            visible={showPreview}
            onRequestClose={() => {
              this.setState({showPreview: false});
            }}>
            <SafeAreaView>
              <View style={styles.modalHeader}>
                <Icon
                  name="arrow-circle-down"
                  size={30}
                  color="#FFF"
                  onPress={() => this.setState({showPreview: false})}
                />
                <Subheading
                  style={{
                    textTransform: 'uppercase',
                    fontSize: 16,
                    color: '#fff',
                    fontWeight: 'bold',
                    letterSpacing: 1,
                  }}>
                  Schedule Preview
                </Subheading>
                <Icon name="help-outline" size={30} color="#FFF" />
              </View>
              <ScrollView>
                <View style={styles.modalView}>
                  <View style={styles.row}>
                    <Text style={styles.title}>Name</Text>
                    <Subheading>{name}</Subheading>
                  </View>
                  <View style={styles.borderBottom}>
                    <Text style={styles.title}>Zones & Durations</Text>
                    <View style={{paddingTop: 8}}>
                      {schedule.zones.map((item) => {
                        const totalTime = item.duration;
                        const calHours = Math.floor(totalTime / 3600);
                        const calMinutes = Math.floor((totalTime % 3600) / 60);
                        const calSeconds = Math.floor((totalTime % 3600) % 60);
                        let timeString = calHours ? `${calHours} H` : '';
                        timeString = calMinutes
                          ? `${timeString} ${calMinutes} M`
                          : '0 M';
                        timeString = calSeconds
                          ? `${timeString} ${calSeconds} S`
                          : `${timeString} 0 S`;
                        const zone = zones.find((ele) => ele._id === item.id);
                        return (
                          <View style={{flexDirection: 'row'}} key={zone._id}>
                            <Text style={{width: '30%', letterSpacing: 1}}>
                              {zone.name}
                            </Text>
                            <Text>{timeString}</Text>
                          </View>
                        );
                      })}
                    </View>
                  </View>
                  <View style={styles.rowHeader}>
                    <Text style={{letterSpacing: 1.5, fontSize: 12}}>
                      TIMING
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.title}>Days</Text>
                    <Subheading>
                      {schedule.days.replace('Interval', 'Days')}
                    </Subheading>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.title}>Start Date</Text>
                    <Subheading>
                      {moment(schedule.startDate).format('ddd Do MMM, YYYY')}
                    </Subheading>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.title}>End Date</Text>
                    <Subheading>
                      {moment(schedule.endDate).format('ddd Do MMM, YYYY')}
                    </Subheading>
                  </View>
                  {this.getTimeslot(schedule)}
                  <View style={styles.rowHeader}>
                    <Text style={{letterSpacing: 1.5, fontSize: 12}}>
                      Plus Feature
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.title}>Skips</Text>
                    <Subheading>{`Rain Skip`}</Subheading>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.title}>Cycle/Soak</Text>
                    <Subheading>{`Smart Cycle`}</Subheading>
                  </View>
                </View>
              </ScrollView>
              <View style={styles.footer}>
                <Button
                  mode="outlined"
                  onPress={this.discardSchedule}
                  style={[styles.footerBtn, {marginRight: 4}]}
                  color={Colors.PRIMARY_COLOR}>
                  Discard
                </Button>
                <Button
                  mode="contained"
                  onPress={() => this.onSave()}
                  style={[styles.footerBtn]}
                  color={Colors.SECONDARY_COLOR}>
                  Create Schedule
                </Button>
              </View>
            </SafeAreaView>
          </Modal>
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {schedule} = state.scheduleCreate;
  const {userData} = state.user;
  const {schedules} = state.schedules;
  const {zones} = state.zones;
  return {device, schedule, userData, schedules, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SelectNameScreen);
