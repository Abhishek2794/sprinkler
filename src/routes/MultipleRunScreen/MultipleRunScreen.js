import React from 'react';
import {Subheading, Button} from 'react-native-paper';
import {View, ScrollView, Image, StyleSheet, Switch} from 'react-native';
import {HelpButton} from '../../components/helpButton';
import {StatusBar} from '../../components/statusBar';
import {connect} from 'react-redux';
import {Colors} from '../../constants/colors';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import DurationPicker from '../../components/durationPicker';
import {appServices} from '../../services';
import alert from '../../components/alert';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingTop: 8,
  },
  zoneList: {
    borderColor: '#e0e0e0',
    borderWidth: 0.5,
    paddingHorizontal: 8,
    paddingVertical: 12,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  runBtn: {
    height: 50,
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});

class MultipleRunScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeZones: this.props.zones
        .filter((item) => item.enabled)
        .map((item) => {
          return {
            ...item,
            checked: false,
            duration: 30,
          };
        }),
      modalVisible: false,
      zoneId: 0,
      zoneDuration: 0,
    };
  }
  updateZone = (id, key, value) => {
    this.setState({
      activeZones: this.state.activeZones.map((item) => {
        if (id === item._id) {
          return {
            ...item,
            [key]: value,
          };
        } else return item;
      }),
    });
  };
  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  };
  setZoneDuration = (duration) => {
    this.setState({
      activeZones: this.state.activeZones.map((item) => {
        if (this.state.zoneId === item._id) {
          return {
            ...item,
            duration,
          };
        } else return item;
      }),
      zoneId: 0,
      modalVisible: false,
      zoneDuration: 0,
    });
  };
  durationFormatter = (duration) => {
    const calHours = Math.floor(duration / 3600);
    const calMinutes = Math.floor((duration % 3600) / 60);
    const calSeconds = Math.floor((duration % 3600) % 60);
    let durationString = '';
    if (calHours) {
      durationString = `${calHours} H `;
    }
    if (calMinutes) {
      durationString += `${calMinutes} M `;
    }
    if (calSeconds) {
      durationString += `${calSeconds} S`;
    }
    if (!durationString) {
      return '0 S';
    }
    return durationString;
  };
  totalDuration = () => {
    let duration = 0;
    this.state.activeZones.forEach((item) => {
      if (item.checked) duration += item.duration;
    });
    return this.durationFormatter(duration);
  };
  totalSelected = () => {
    let total = 0;
    this.state.activeZones.forEach((item) => {
      if (item.checked) total += 1;
    });
    return total;
  };
  selectAll = () => {
    this.setState({
      activeZones: this.state.activeZones.map((item) => {
        return {
          ...item,
          checked: true,
        };
      }),
    });
  };
  startRun = () => {
    const {device, navigation} = this.props;
    const selectedZones = this.state.activeZones.filter((item) => item.checked);
    if (!selectedZones.length) {
      alert('Error', 'Please select zone(s) to water!');
      return;
    }
    const payload = {
      name: 'Multiple Run',
      enabled: 1,
      deviceId: device._id,
      scheduleZones: selectedZones.map((item) => {
        return {
          duration: item.duration,
          zone: item,
        };
      }),
    };
    appServices
      .multipleQuickRun(payload)
      .then((data) => {
        navigation.navigate('ActiveSchedule');
      })
      .catch((err) => {
        console.log(err);
      });
  };
  render() {
    const {navigation} = this.props;
    const {activeZones, modalVisible, zoneDuration} = this.state;
    return (
      <View style={{flex: 1}}>
        <DurationPicker
          duration={zoneDuration}
          isMultipleRun={true}
          closePopup={this.closeModal}
          modalVisible={modalVisible}
          startQuickRun={this.setZoneDuration}
        />
        <StatusBar
          title={'Select Zones'}
          child={HelpButton}
          isBackNavigable
          navigation={navigation}
        />
        <View style={styles.header}>
          <View style={{alignItems: 'center'}}>
            <Subheading>{this.totalSelected()} Selected</Subheading>
            <Button color="#1565C0" onPress={this.selectAll}>
              Select All
            </Button>
          </View>
          <View style={{alignItems: 'center'}}>
            <Subheading>Total Time</Subheading>
            <Button color="#1565C0">{this.totalDuration()}</Button>
          </View>
        </View>
        <ScrollView style={styles.container}>
          {activeZones.map((item) => {
            return (
              <View key={item._id} style={styles.zoneList}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Switch
                    trackColor={Colors.SECONDARY_COLOR}
                    value={item.checked}
                    onValueChange={() => {
                      this.updateZone(item._id, 'checked', !item.checked);
                    }}
                  />
                  <Image
                    source={
                      item.defaultImage
                        ? {uri: item.defaultImage}
                        : require('../../../assets/images/zone-default/zone-default.png')
                    }
                    style={{height: 100, width: 100, marginRight: 12}}
                  />
                  <Subheading>{item.name}</Subheading>
                </View>
                <View>
                  {item.checked ? (
                    <Button
                      color="#1565C0"
                      style={{borderColor: '#1565C0'}}
                      onPress={() => {
                        this.setState({
                          zoneId: item._id,
                          modalVisible: true,
                          zoneDuration: item.duration,
                        });
                      }}
                      mode="outlined">
                      {this.durationFormatter(item.duration)}
                    </Button>
                  ) : null}
                </View>
              </View>
            );
          })}
        </ScrollView>
        <Button
          icon="arrow-right-drop-circle"
          style={styles.runBtn}
          color={Colors.SECONDARY_COLOR}
          onPress={this.startRun}
          labelStyle={{fontSize: 18}}
          mode="contained">
          Quick Run
        </Button>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zones} = state.zones;
  return {device, userData, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MultipleRunScreen);
