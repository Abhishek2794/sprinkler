/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ActivityIndicator,
  PermissionsAndroid,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import wifi from 'react-native-android-wifi';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {Colors, Dimension, Fonts} from '../../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  card: {
    marginTop: Dimension.HEIGHT_100,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: '#929292',
    fontWeight: '600',
    alignSelf: 'center',
    fontSize: Fonts.FONT_SIZE_20,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  image: {
    alignSelf: 'center',
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    justifyContent: 'space-between',
    paddingTop: Dimension.HEIGHT_20,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  name: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  tryButton: {
    borderRadius: 5,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
    width: Dimension.WIDTH_135,
    height: Dimension.HEIGHT_50,
    marginTop: Dimension.HEIGHT_45,
  },
  tryButtonText: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
  },
  wifi: {
    alignSelf: 'center',
  },
});

class NewHomeWifiScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ssids: [],
      selectedSSID: '',
    };
    this.checkWifiPermission = this.checkWifiPermission.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.scanWifi = this.scanWifi.bind(this);
  }

  componentDidMount() {
    this.checkWifiPermission();
  }

  onSubmit() {
    const {selectedSSID} = this.state;
    if (selectedSSID === '') {
      alert('Error', 'Please select your network');
      return;
    }
    const {controllerData, navigation} = this.props;
    store.dispatch(
      appActions.setControllerData({...controllerData, ssid: selectedSSID}),
    );
    navigation.navigate('NewWifiPassword');
  }

  async checkWifiPermission() {
    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Sprinkler',
        message: 'Please grant permission to access wifi',
      },
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      wifi.loadWifiList(
        async (wifiStringList) => {
          const networks = await JSON.parse(wifiStringList);
          const ssids = [];
          networks.forEach((network) => {
            if (ssids.indexOf(network.SSID) < 0) {
              ssids.push(network.SSID);
            }
          });
          this.scanInterval = setInterval(() => {
            this.scanWifi();
          }, 2000);
          this.setState({ssids});
        },
        (error) => {
          console.log(error);
        },
      );
    }
  }

  scanWifi() {
    wifi.reScanAndLoadWifiList(
      (wifiStringList) => {
        const networks = JSON.parse(wifiStringList);
        const {ssids} = this.state;
        networks.forEach((network) => {
          if (ssids.indexOf(network.SSID) < 0) {
            ssids.push(network.SSID);
          }
        });
        this.setState({ssids});
      },
      (error) => {
        console.log(error);
      },
    );
  }

  render() {
    const {navigation} = this.props;
    const {selectedSSID, ssids} = this.state;
    if (ssids.length === 0) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            title="Reset Network"
            isBackNavigable
            subtitle=""
            child={Helpers.HelpButton}
            navigation={navigation}
          />
          <Helpers.Card>
            <ActivityIndicator />
          </Helpers.Card>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Reset Network"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Text style={styles.header}>Select home Wi-Fi</Text>
          {ssids.map((ssid) => (
            <TouchableOpacity
              key={ssid}
              onPress={() => this.setState({selectedSSID: ssid})}>
              <View style={styles.listItem}>
                <Text style={styles.name}>{ssid}</Text>
                {ssid === selectedSSID && (
                  <Icon name="done" color={Colors.PRIMARY_COLOR} size={24} />
                )}
              </View>
            </TouchableOpacity>
          ))}
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  return {controllerData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NewHomeWifiScreen);
