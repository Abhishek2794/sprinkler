import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {Colors, Fonts, appConstants} from '../../constants';
import {userServices, appServices} from '../../services';
import {generateCalenderPayload} from '../../helpers/create-calender';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  radioText: {
    marginTop: -5,
    fontWeight: '300',
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditTimeSlotScreen extends Component {
  constructor(props) {
    super(props);
    const {scheduleData} = this.props;
    const {timeSlot} = scheduleData;
    this.state = {
      endTime: new Date(),
      endTimePicker: false,
      startTime: new Date(),
      startTimePicker: false,
      submitting: false,
      timeSlot,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.setEndTime = this.setEndTime.bind(this);
    this.setStartTime = this.setStartTime.bind(this);
  }

  componentDidMount() {
    const {scheduleData} = this.props;
    const {endTime, startTime} = scheduleData;
    const et = endTime ? new Date(endTime) : new Date();
    const st = startTime ? new Date(startTime) : new Date();
    this.setState({endTime: et, startTime: st});
  }

  onSubmit() {
    this.setState({submitting: true});
    let scheduleUpdate;
    const {scheduleData} = this.props;
    const {endTime, startTime, timeSlot} = this.state;
    switch (timeSlot) {
      case 0:
        scheduleUpdate = {...scheduleData, timeSlot};
        break;
      case 1:
        scheduleUpdate = {
          ...scheduleData,
          timeSlot,
          endTime: null,
        };
        break;
      case 2:
        scheduleUpdate = {...scheduleData, timeSlot};
        break;
      case 3:
        scheduleUpdate = {
          ...scheduleData,
          timeSlot,
          startTime: moment(startTime),
        };
        break;
      default:
        alert('Error', 'No Timeslot chosen');
        return;
    }
    const {device, navigation} = this.props;
    console.log(scheduleUpdate);
    userServices
      .updateSchedule(device._id, scheduleUpdate)
      .then((updatedSchedule) => {
        store.dispatch(appActions.fetchSchedule(updatedSchedule));
        store.dispatch(appActions.fetchSchedules(device._id));
        setTimeout(() => {
          console.log('Edit Schedule, here');
          const payload = generateCalenderPayload();
          console.log(payload);
          this.setState({submitting: false});
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                store.dispatch(appActions.fetchCalendarDates(device._id));
              });
          }
          navigation.goBack();
        }, 2000);
      })
      .catch(() =>
        this.setState({submitting: false}, () => {
          alert('Error', 'Could not update schedule');
        }),
      );
  }

  onSelected(timeSlot, type) {
    this.setState({timeSlot});
    switch (type) {
      case appConstants.START_AT_SPECIFIC_TIME:
        this.setState({startTimePicker: true});
        break;
      case appConstants.END_BEFORE_SPECIFIC_TIME:
        this.setState({endTimePicker: true});
        break;
      default:
        break;
    }
  }

  setEndTime(event, date) {
    if (date !== undefined) {
      this.setState({endTime: date, endTimePicker: false});
    } else {
      const {scheduleData} = this.props;
      const {timeSlot} = scheduleData;
      this.setState({endTimePicker: false, timeSlot});
    }
  }

  setStartTime(event, date) {
    if (date !== undefined) {
      const updatedDate = new Date(date).setSeconds('00');
      this.setState({startTime: updatedDate, startTimePicker: false});
    } else {
      const {scheduleData} = this.props;
      const {timeSlot} = scheduleData;
      this.setState({startTimePicker: false, timeSlot});
    }
  }

  render() {
    const {navigation, scheduleData} = this.props;
    const timeslotData = scheduleData.timeSlot;
    const {
      endTime,
      endTimePicker,
      startTime,
      startTimePicker,
      submitting,
      timeSlot,
    } = this.state;
    console.log('here');
    return (
      <View style={styles.container}>
        <Spinner visible={submitting} />
        <Helpers.StatusBar
          title="Edit Schedule"
          isBackNavigable
          subtitle="Pick Timeslot"
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        {startTimePicker && (
          <RNDateTimePicker
            value={startTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setStartTime}
          />
        )}

        {endTimePicker && (
          <RNDateTimePicker
            value={endTime}
            mode="time"
            is24Hour={false}
            display="default"
            onChange={this.setEndTime}
          />
        )}

        <Helpers.Card>
          <View>
            <Text style={styles.header}>Pick a time slot for the schedule</Text>
            <RadioGroup
              size={20}
              selectedIndex={timeSlot}
              color={Colors.PRIMARY_COLOR_DARK}
              onSelect={(index, value) => this.onSelected(index, value)}>
              <RadioButton
                value={appConstants.END_BEFORE_SUNRISE}
                disabled={timeslotData === 1}>
                <Text style={styles.radioText}>End before sunrise</Text>
              </RadioButton>
              <RadioButton
                disabled={timeslotData === 1}
                value={appConstants.END_BEFORE_SPECIFIC_TIME}>
                <Text style={styles.radioText}>End before specific time</Text>
              </RadioButton>
              <RadioButton value={appConstants.START_AFTER_SUNSET}>
                <Text style={styles.radioText}>Start after sunset</Text>
              </RadioButton>
              <RadioButton value={appConstants.START_AT_SPECIFIC_TIME}>
                <Text style={styles.radioText}>Start at specific time</Text>
              </RadioButton>
            </RadioGroup>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleData} = state.schedule;
  return {device, scheduleData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditTimeSlotScreen);
