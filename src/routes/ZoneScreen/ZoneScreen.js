import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ImageBackground,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import {logglyServices, userServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import NoDevice from '../../components/noDevice';
import {appActions} from '../../actions/app.actions';
import {userActions} from '../../actions/user.actions';
import {appConstants, Colors, Dimension, Fonts} from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  schedule: {
    fontSize: Fonts.FONT_SIZE_14,
    marginLeft: Dimension.WIDTH_24,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  toggle: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontWeight: '500',
  },
  zone: {
    elevation: 24,
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    shadowColor: '#000',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_130,
    marginBottom: Dimension.HEIGHT_30,
    shadowOffset: {width: 0, height: 12},
    backgroundColor: Colors.TEXT_COLOR,
  },
  zoneDetails: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: Dimension.WIDTH_24,
    marginRight: Dimension.WIDTH_24,
  },
  zoneList: {
    flexDirection: 'column',
    paddingLeft: Dimension.WIDTH_20,
    paddingRight: Dimension.WIDTH_20,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_100,
  },
  zoneName: {
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_16,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zoneNumber: {
    fontSize: Fonts.FONT_SIZE_16,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class ZoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideDisabled: false,
      loading: false,
      refreshing: false,
    };
    this.handleNavigation = this.handleNavigation.bind(this);
    this.hideDisabled = this.hideDisabled.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      store.dispatch(userActions.dockView(true));
    });
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  onRefresh() {
    this.setState({refreshing: true});
    const {device} = this.props;
    store.dispatch(appActions.fetchZones(device._id));
    this.setState({refreshing: false});
  }

  handleNavigation(zone) {
    const {device} = this.props;
    store.dispatch(appActions.fetchZone(device._id, zone.number));
    const {navigation} = this.props;
    setTimeout(() => {
      navigation.navigate('EditZone');
    }, 1000);
  }

  hideDisabled() {
    const {hideDisabled} = this.state;
    this.setState({hideDisabled: !hideDisabled});
  }

  toggleZone(index) {
    this.setState({loading: true});
    const {zones} = this.props;
    zones[index].enabled = !zones[index].enabled;
    userServices
      .updateZone(zones[index])
      .then(() => {
        const {device, userData} = this.props;
        logglyServices.logEvent(
          {user: userData.email, zone: zones[index]},
          'updateZone',
        );
        store.dispatch(appActions.fetchZones(device._id));
        store.dispatch(appActions.fetchCalendarDates(device._id));
        this.setState({loading: false});
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Error', error.toString());
      });
  }

  render() {
    const {hideDisabled, loading, refreshing} = this.state;

    let {zones} = this.props;
    const {allDevices, navigation} = this.props;
    if (hideDisabled) {
      zones = zones.filter((zone) => zone.enabled);
    }
    const zoneToggle = (
      <TouchableOpacity onPress={() => this.hideDisabled()}>
        <Text style={styles.toggle}>
          {hideDisabled ? 'Show Disabled' : 'Hide Disabled'}
        </Text>
      </TouchableOpacity>
    );
    if (allDevices.length > 0) {
      return (
        <View>
          <Spinner visible={loading} />
          <Helpers.StatusBar
            child={zoneToggle}
            isBackNavigable={false}
            navigation={navigation}
            subtitle="16 Zones Enabled"
            title="Zones"
          />

          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={styles.zoneList}>
              {zones
                ? zones.map((zone, index) => (
                    <TouchableOpacity
                      key={zone.number}
                      style={styles.zone}
                      onPress={() => this.handleNavigation(zone)}>
                      <ImageBackground
                        resizeMode="cover"
                        source={
                          zone.imageUrl == null
                            ? require('../../../assets/images/zone-default/zone-default.png')
                            : {uri: zone.imageUrl}
                        }
                        style={[
                          styles.container,
                          {
                            height: Dimension.HEIGHT_130,
                          },
                        ]}>
                        <View style={{width: Dimension.WIDTH_63}}>
                          <Switch
                            style={{marginTop: Dimension.HEIGHT_20}}
                            value={zone.enabled}
                            trackColor={Colors.PRIMARY_COLOR_DARK}
                            onValueChange={() => this.toggleZone(index)}
                          />
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <View style={styles.zoneDetails}>
                            <Text
                              style={styles.zoneName}>{`${zone.name} | `}</Text>
                            <Text style={styles.zoneNumber}>
                              {zone.number < 10
                                ? `Zone 0${zone.number}`
                                : `Zone ${zone.number}`}
                            </Text>
                          </View>
                          <Icon
                            name="keyboard-arrow-right"
                            size={24}
                            color="#929292"
                            style={{marginRight: Dimension.WIDTH_20}}
                          />
                        </View>
                        <Text style={styles.schedule}>{`Last Watered: ${
                          zone.lastWatered == null
                            ? ' Never'
                            : moment(zone.lastWatered).format('lll')
                        }`}</Text>
                        <Text style={styles.schedule}>{`Next Schedule: ${
                          zone.upcomingWatering == null
                            ? 'Never'
                            : moment(zone.upcomingWatering).format('lll')
                        }`}</Text>
                      </ImageBackground>
                    </TouchableOpacity>
                  ))
                : null}
            </View>
          </ScrollView>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          child={Helpers.HelpButton}
          isBackNavigable={false}
          navigation={navigation}
          subtitle="No Device Found"
          title="Zones"
        />
        <NoDevice navigation={navigation} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zones} = state.zones;
  return {
    allDevices,
    device,
    userData,
    zones,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ZoneScreen);
