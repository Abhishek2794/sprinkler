import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  RefreshControl,
  ScrollView,
} from 'react-native';
import ZoneCard from './ZoneCard';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import {logglyServices, userServices, appServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import NoDevice from '../../components/noDevice';
import {appActions} from '../../actions/app.actions';
import {userActions} from '../../actions/user.actions';
import {logEvent} from '../../analytics';
import {Colors} from '../../constants/colors';
import {Caption} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {generateCalenderPayload} from '../../helpers/create-calender';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabs: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  tabItem: {
    width: '33.33%',
    padding: 12,
    backgroundColor: '#fff',
  },
  tabItemActive: {
    width: '33.33%',
    padding: 12,
    backgroundColor: '#eee',
  },
  centerAilgn: {
    textAlign: 'center',
  },
  zones: {
    marginTop: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  zoneMenu: {
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderTopColor: '#e0e0e0',
    borderTopWidth: 1,
  },
  menuBtn: {
    alignItems: 'center',
  },
});

class ZoneScreenV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      index: 0,
      routes: [
        {
          index: 0,
          title: 'ALL',
        },
        {
          index: 1,
          title: 'ACTIVE',
        },
        {
          index: 2,
          title: 'INACTIVE',
        },
      ],
    };
    this.handleNavigation = this.handleNavigation.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      store.dispatch(userActions.dockView(true));
      logEvent('ZONE_SCREEN', {screen: 'ZONE_SCREEN'});
    });
    this.onRefresh();
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  onRefresh() {
    this.setState({refreshing: true});
    const {device} = this.props;
    if (device._id) store.dispatch(appActions.fetchZones(device._id));
    this.setState({refreshing: false});
  }

  handleNavigation(zone) {
    store.dispatch(appActions.assignZone(zone));
    const {navigation} = this.props;
    navigation.navigate('EditZone');
  }

  toggleZone(id) {
    this.setState({loading: true});
    const zone = {...this.props.zones.find((item) => item._id === id)};
    zone.enabled = !zone.enabled;
    userServices
      .updateZone(zone)
      .then(() => {
        const {device, userData, schedules} = this.props;
        logglyServices.logEvent(
          {user: userData.email, zone: zone},
          'updateZone',
        );
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(appActions.fetchZones(device._id));
        let calculateCalender = false;
        setTimeout(() => {
          schedules.forEach((schedule) => {
            if (!schedule.temporary) {
              schedule.zones.forEach((ele) => {
                if (ele.value._id === id) {
                  calculateCalender = true;
                }
              });
            }
          });
          if (calculateCalender) {
            const payload = generateCalenderPayload();
            if (payload) {
              appServices
                .createCalenderForController(device.serialNumber, payload.data)
                .then(() => {
                  this.setState({loading: false});
                  store.dispatch(appActions.fetchCalendarDates(device._id));
                });
            }
          } else {
            this.setState({loading: false});
          }
        }, 4000);
      })
      .catch((error) => {
        console.log(error);
        this.setState({loading: false});
        alert('Error', error.toString());
      });
  }

  render() {
    const {loading, refreshing, routes, index} = this.state;
    let {zones} = this.props;
    let zonesArr = zones;
    const {navigation, allDevices} = this.props;
    if (index === 1) {
      zonesArr = zones.filter((item) => item.enabled === true);
      logEvent('ACTIVE_ZONES', {
        activeZones: zonesArr.length,
        screen: 'ACTIVE_ZONES',
      });
    } else if (index === 2) {
      zonesArr = zones.filter((item) => item.enabled === false);
      logEvent('INACTIVE_ZONES', {
        inActiveZones: zonesArr.length,
        screen: 'INACTIVE_ZONES',
      });
    }
    if (!allDevices.length) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            child={Helpers.HelpButton}
            isBackNavigable={false}
            navigation={navigation}
            subtitle="No Device Found"
            title="Zones"
          />
          <NoDevice navigation={navigation} />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <Spinner visible={loading} />
          <Helpers.StatusBar
            isBackNavigable={false}
            navigation={navigation}
            subtitle="16 Zones Enabled"
            title="Zones"
          />
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={styles.zones}>
              {zonesArr.map((item) => {
                return (
                  <ZoneCard
                    zone={item}
                    key={item._id}
                    imageUrl={item.defaultImage}
                    toggleZone={(id) => this.toggleZone(id)}
                    handleNavigation={() => this.handleNavigation(item)}
                  />
                );
              })}
            </View>
          </ScrollView>
          <View style={styles.zoneMenu}>
            <TouchableOpacity
              style={styles.menuBtn}
              activeOpacity={0.5}
              onPress={() => {
                this.setState({
                  index: this.state.index === 1 ? 0 : 1,
                });
              }}>
              <Icon
                name={this.state.index === 1 ? 'step-backward' : 'eye'}
                color={Colors.SECONDARY_COLOR}
                size={24}
              />
              <Caption>
                Show {this.state.index === 1 ? 'All' : 'Active'}
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuBtn}
              activeOpacity={0.5}
              onPress={() => {
                this.setState({
                  index: this.state.index === 2 ? 0 : 2,
                });
              }}>
              <Icon
                name={this.state.index === 2 ? 'step-backward' : 'eye-off'}
                color={Colors.SECONDARY_COLOR}
                size={24}
              />
              <Caption>
                Show {this.state.index === 2 ? 'All' : 'Inactive'}
              </Caption>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.menuBtn}
              activeOpacity={0.5}
              onPress={() => {
                const {zones, device} = this.props;
                const activeZones = zones.filter((item) => item.enabled);
                if (!device.isOnline) {
                  alert(
                    'Controller is offline',
                    'Please turn on the controller device and make sure its connected with the wifi',
                  );
                  return;
                } else if (!activeZones.length) {
                  alert(
                    'No Active Zones',
                    'Please set the zones to active mode',
                  );
                  return;
                } else {
                  navigation.navigate('MultipleRun');
                }
              }}>
              <Icon
                name="arrow-right-drop-circle"
                color={Colors.SECONDARY_COLOR}
                size={24}
              />
              <Caption>Quick Run</Caption>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zones} = state.zones;
  const {online} = state.deviceOnline;
  const {schedules} = state.schedules;
  return {
    allDevices,
    device,
    userData,
    zones,
    online,
    schedules,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ZoneScreenV2);
