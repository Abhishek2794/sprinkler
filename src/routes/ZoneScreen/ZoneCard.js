import React from 'react';
import moment from 'moment';
import {Dimension, Colors} from '../../constants';
import {
  View,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Switch,
  TouchableOpacity,
} from 'react-native';
import {Text, Subheading} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width / 2,
    padding: 10,
  },
  zone: {
    borderRadius: 6,
    backgroundColor: '#fff',
    paddingBottom: 12,
  },
  zoneInfo: {
    paddingLeft: 8,
    paddingTop: 6,
  },
  zoneInfoWithSwitch: {
    paddingHorizontal: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    fontSize: 13,
    color: '#333',
  },
  value: {
    fontSize: 15,
    color: '#000',
  },
  icon: {
    backgroundColor: '#f6f6f6',
    borderRadius: 20,
    alignSelf: 'flex-end',
    marginTop: 32,
    marginRight: 8,
  },
});

class ZoneCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enabled: false,
    };
  }
  async componentDidMount() {
    const {zone} = this.props;
    this.setState({
      enabled: zone.enabled,
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.enabled !== this.props.zone.enabled) {
      this.setState({
        enabled: this.props.zone.enabled,
      });
    }
  }
  render() {
    const {zone, imageUrl} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.zone}>
          <TouchableOpacity
            onPress={() => this.props.handleNavigation()}
            activeOpacity={0.5}>
            <ImageBackground
              resizeMode="cover"
              source={imageUrl ? {uri: imageUrl} : require('../../../assets/images/zone-default/zone-default.png')}
              style={{
                height: Dimension.HEIGHT_80,
              }}>
              <View
                style={{
                  backgroundColor: !zone.enabled
                    ? 'rgba(224,224,224,0.5)'
                    : 'rgba(224,224,224,0)',
                  height: Dimension.HEIGHT_80,
                }}>
                <Icon
                  name="chevron-right"
                  size={24}
                  color="#999"
                  style={styles.icon}
                />
              </View>
            </ImageBackground>
            <View style={styles.zoneInfo}>
              <Subheading style={{fontWeight: "bold"}}>{zone.name}</Subheading>
            </View>
          </TouchableOpacity>
          <View style={styles.zoneInfoWithSwitch}>
            <Text style={styles.value}>
              {zone.enabled ? 'Active' : 'Inactive'}
            </Text>
            <Switch
              value={this.state.enabled}
              trackColor={Colors.PRIMARY_COLOR_DARK}
              onValueChange={() => {
                this.setState({
                  enabled: !this.state.enabled,
                });
                this.props.toggleZone(zone._id);
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default ZoneCard;
