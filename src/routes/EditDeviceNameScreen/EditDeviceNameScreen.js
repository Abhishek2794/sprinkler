/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import {Colors, Fonts, Dimension} from '../../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  largeContainer: {
    flex: 2,
    flexDirection: 'column',
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditDeviceNameScreen extends Component {
  constructor(props) {
    super(props);
    const {route} = this.props;
    this.state = {
      name: route.params['device'].name,
      error: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(name) {
    if (name) {
      this.setState({name, error: ''});
      return;
    }
    this.setState({name, error: 'Invalid Name'});
  }

  onSubmit() {
    const {name} = this.state;
    if (!name) {
      this.setState({error: 'Invalid Name'});
      return;
    }
    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({error: 'Name cannot have special characters'});
      return;
    }
    this.setState({error: ''});
    const {device, navigation} = this.props;
    userServices.updateDevice({...device, name}).then(() => {
      navigation.goBack();
      store.dispatch(appActions.updateActiveDevice());
    });
  }

  render() {
    const {navigation} = this.props;
    const {name, error} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Edit Device"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.largeContainer}>
              <Text style={styles.formField}>Device Name</Text>
              <TextInput
                autoFocus
                value={name}
                maxLength={250}
                numberOfLines={1}
                editable
                returnKeyType="done"
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.onChangeText(text)}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: error
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
          </View>
          {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </Helpers.Card>

        <Helpers.Footer>
          <TouchableOpacity
            disabled={error}
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  return {device};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditDeviceNameScreen);
