/* eslint-disable no-useless-escape */
import LinearGradient from 'react-native-linear-gradient';
import React, {Component} from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import alert from '../../components/alert';
import {userServices} from '../../services';
import { Button } from 'react-native-paper';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts} from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    fontWeight: '600',
    marginBottom: 10,
    paddingLeft: 10,
  },
  form: {
    marginTop: 44,
    marginHorizontal: 24,
  },
  header: {
    alignSelf: 'center',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_64,
    fontWeight: 'bold',
    marginTop: Dimension.HEIGHT_100,
  },
  signup: {
    marginTop: 50,
  },
  text: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: 18,
  },
  textInput: {
    borderColor: Colors.PRIMARY_COLOR_DARK,
    borderRadius: 8,
    borderStyle: 'solid',
    borderWidth: 2,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
    fontWeight: '300',
    height: Dimension.HEIGHT_50,
    marginBottom: Dimension.HEIGHT_20,
    paddingLeft: Dimension.WIDTH_20,
    width: Dimension.WIDTH_317,
  },
  loginBtn: {
    height: 56,
    justifyContent: 'center',
    fontSize: 20,
    width: '96%',
  },
});

export default class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {},
      errors: {},
      emailSent: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.changeText = this.changeText.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  onSubmit() {
    const {errors, form} = this.state;
    if (!('email' in form)) {
      errors.email = 'Email ID is required';
      this.setState({errors});
      return;
    }
    if (
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(
        form.email,
      )
    ) {
      errors.email = 'Email ID is invalid';
      this.setState({errors});
      return;
    }

    Keyboard.dismiss();
    userServices
      .sendResetMail(form.email)
      .then(() => this.setState({emailSent: true}))
      .catch((error) => alert('Error', error.toString()));
  }

  changeText(text, field) {
    const {errors, form} = this.state;
    if (!text) {
      Reflect.deleteProperty(form, field);
      errors[field] = `${field} is required`;
      this.setState({errors, form});
      return;
    }
    form[field] = text;
    delete errors[field];
    this.setState({errors, form});
  }

  handleNavigation(screen) {
    const {navigation} = this.props;
    navigation.navigate(screen);
  }

  render() {
    const {errors, emailSent} = this.state;
    const {navigation} = this.props;

    return (
      <LinearGradient
        locations={[0.1, 1.0]}
        style={{flex: 1}}
        colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
        <Helpers.StatusBar
          title="Forget Password"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <KeyboardAvoidingView
          behavior="height"
          keyboardVerticalOffset={10}
          styles={styles.container}>
          <Text style={styles.header}>Sprinkler</Text>
          <View style={styles.form}>
            <TextInput
              blurOnSubmit={false}
              placeholder="Email ID"
              returnKeyType="done"
              style={styles.textInput}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              onSubmitEditing={() => this.onSubmit()}
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              onChangeText={(text) => this.changeText(text, 'email')}
            />
            {errors.email ? (
              <Text style={styles.errorText}>{errors.email}</Text>
            ) : null}
            {emailSent ? (
              <Text
                style={[
                  styles.text,
                  {alignSelf: 'center'},
                ]}>{`Kindly check your mail ${'\n'}for password reset link.`}</Text>
            ) : (
              <Button
                mode="contained"
                onPress={this.onSubmit}
                color={Colors.SECONDARY_COLOR}
                style={styles.loginBtn}>
                <Text style={{fontSize: 18, letterSpacing: 1}}>Send Link</Text>
              </Button>
            )}
            <View style={styles.signup}>
              <Text style={styles.text}>Already have an account?</Text>
              <Button
                color={'#fff'}
                mode="outlined"
                style={{marginTop: 16}}
                onPress={() => this.handleNavigation('Login')}>
                Login
              </Button>
            </View>
          </View>
        </KeyboardAvoidingView>
      </LinearGradient>
    );
  }
}
