/* eslint-disable import/no-extraneous-dependencies */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {appActions} from '../../actions/app.actions';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    marginTop: Dimension.HEIGHT_30,
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class NewWifiPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formError: false,
      password: '',
      visible: false,
    };
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeText(password) {
    if (password) {
      this.setState({password, formError: false});
      return;
    }
    this.setState({password, formError: true});
  }

  onSubmit() {
    const {password} = this.state;
    if (!password) {
      this.setState({formError: true});
      return;
    }
    const {controllerData, navigation} = this.props;
    store.dispatch(appActions.setControllerData({...controllerData, password}));
    navigation.navigate('EditControllerWifi');
  }

  render() {
    const {controllerData, navigation} = this.props;
    const {ssid} = controllerData;
    const {formError, password, visible} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Reset Network"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Text style={styles.header}>{`Enter password for ${ssid}`}</Text>
          <View style={styles.formElement}>
            <View style={{flex: 2, flexDirection: 'column'}}>
              <Text style={styles.formField}>Password</Text>
              <TextInput
                autoFocus
                maxLength={250}
                value={password}
                numberOfLines={1}
                returnKeyType="done"
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                secureTextEntry={!visible}
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.onChangeText(text)}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: formError
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
            <View style={styles.formEdit}>
              {visible ? (
                <TouchableOpacity
                  onPress={() => this.setState({visible: false})}>
                  <Icon color="#929292" name="eye" size={24} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.setState({visible: true})}>
                  <Icon color="#929292" name="eye-off" size={24} />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {controllerData} = state.controller;
  return {controllerData, device};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewWifiPasswordScreen);
