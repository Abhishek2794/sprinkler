/* eslint-disable import/no-extraneous-dependencies */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, View, Keyboard} from 'react-native';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {logEvent} from '../../analytics';
import {
  Subheading,
  TextInput,
  Button,
  Title,
  Caption,
} from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
});

class NewWifiPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      visible: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeText(password) {
    this.setState({password});
  }

  onSubmit() {
    Keyboard.dismiss();
    const {password} = this.state;
    if (!password) {
      return;
    }
    const {controllerData, navigation, userData} = this.props;
    store.dispatch(
      appActions.setControllerData({
        ...controllerData,
        password,
        userId: userData._id,
      }),
    );
    navigation.navigate('EditControllerWifi');
  }

  render() {
    const {controllerData, navigation} = this.props;
    const {ssid} = controllerData;
    const {password, visible} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Provide Password"
          isBackNavigable
          subtitle="Wifi Password"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Title>Enter network's password</Title>
          <View style={{width: '100%', marginVertical: 16}}>
            <TextInput
              autoFocus
              maxLength={250}
              value={password}
              numberOfLines={1}
              returnKeyType="done"
              secureTextEntry={!visible}
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              onChangeText={(text) => this.onChangeText(text)}
              mode="outlined"
              label="Enter network password"
              placeholder="Enter password"
              right={
                <TextInput.Icon
                  name={visible ? 'eye-off' : 'eye'}
                  onPress={() => this.setState({visible: !visible})}
                />
              }
            />
          </View>
          <Caption>Selected Network: {ssid}</Caption>
        </Helpers.Card>
        <Helpers.Footer>
          <Button
            style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
            onPress={() => this.onSubmit()}
            mode="contained"
            disabled={!password}>
            <Subheading
              style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
              Link Controller
            </Subheading>
          </Button>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  const {userData} = state.user;
  return {controllerData, userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewWifiPasswordScreen);
