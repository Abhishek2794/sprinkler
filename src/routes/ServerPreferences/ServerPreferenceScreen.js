import React from 'react';
import * as Helpers from '../../helpers/modules';
import {Subheading, DataTable, Caption, Snackbar} from 'react-native-paper';
import {View, StyleSheet, Switch} from 'react-native';
import {connect} from 'react-redux';
import {Colors} from '../../constants';
import Spinner from 'react-native-loading-spinner-overlay';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import store from '../../helpers/store';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';

class ServerPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      type: '',
    };
  }

  async componentDidMount() {
    const server = await AsyncStorage.getItem('@Sprinkler:Server');
    if (server) {
      const userServer = JSON.parse(server);
      this.setState({type: userServer.name});
    }
  }

  logout() {
    const {userData} = this.props;
    const topic = userData.email.replace(/[&\/\\@#_,+()$~%.'":*?<>{}]/g, '-');
    messaging().unsubscribeFromTopic(topic);
    userServices.logout(userData.email).then(async () => {
      await AsyncStorage.removeItem('@Sprinkler:HistoryLogs');
      this.setState({loading: false});
      store.dispatch(appActions.checkLogin(false));
      store.dispatch(appActions.logout());
    });
  }

  async updateServerConfig(config) {
    this.setState({loading: true});
    await AsyncStorage.setItem('@Sprinkler:Server', JSON.stringify(config));
    this.logout();
  }

  render() {
    const {navigation} = this.props;
    const {loading, type} = this.state;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          isBackNavigable
          child={Helpers.HelpButton}
          title="Servers"
          navigation={navigation}
        />
        <Spinner visible={loading} />

        <View style={styles.continer}>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>
                <Subheading style={styles.tableHeader}>
                  Available Server
                </Subheading>
              </DataTable.Title>
              <DataTable.Title numeric>
                <Subheading style={styles.tableHeader}> Status</Subheading>
              </DataTable.Title>
            </DataTable.Header>
            <DataTable.Row>
              <DataTable.Cell>Production</DataTable.Cell>
              <DataTable.Cell numeric>
                <Switch
                  color={Colors.SECONDARY_COLOR}
                  value={type === 'Production'}
                  onValueChange={async (value) => {
                    const data = {
                      name: 'Production',
                      ip: 'http://mizzle.org:8081',
                    };
                    if (value) await this.updateServerConfig(data);
                  }}
                />
              </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
              <DataTable.Cell>Staging</DataTable.Cell>
              <DataTable.Cell numeric>
                <Switch
                  color={Colors.SECONDARY_COLOR}
                  value={type === 'Staging'}
                  onValueChange={async (value) => {
                    const data = {
                      name: 'Staging',
                      ip: 'http://sprinklerocean.com:8081',
                    };
                    if (value) await this.updateServerConfig(data);
                  }}
                />
              </DataTable.Cell>
            </DataTable.Row>
          </DataTable>
          <Caption>
            Note: You have to re-login after toggling the server.
          </Caption>
        </View>
        <Snackbar
          visible={this.state.error || this.state.toast}
          onDismiss={() => this.setState({error: '', toast: ''})}
          style={{backgroundColor: this.state.error ? '#d32f2f' : '#00796B'}}>
          {this.state.error || this.state.toast}
        </Snackbar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  continer: {
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  tableHeader: {
    fontSize: 15,
    color: '#000',
    fontWeight: 'bold',
  },
});

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  return {device, userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ServerPreference);
