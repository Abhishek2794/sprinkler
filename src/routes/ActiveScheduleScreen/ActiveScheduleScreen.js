import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  AppState,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  BackHandler,
  Alert,
} from 'react-native';
import equal from 'fast-deep-equal';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-spinkit';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import VectorIcon from 'react-native-vector-icons/AntDesign';
import {ProgressBar} from 'react-native-paper';
import store from '../../helpers/store';
import {
  Colors,
  Dimension,
  Fonts,
  WateringStatus,
  getWateringTypeName,
  getWateringStatus,
} from '../../constants';
import * as ActionCreators from '../../actions';
import {appActions} from '../../actions/app.actions';
import {userServices} from '../../services';
import {logEvent} from '../../analytics';

const styles = StyleSheet.create({
  appBar: {
    alignItems: 'center',
    flexDirection: 'row',
    height: Dimension.HEIGHT_60,
  },
  backNavigator: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
    bottom: 0,
    height: Dimension.HEIGHT_60,
    justifyContent: 'center',
    position: 'absolute',
    width: Dimension.WIDTH,
  },
  container: {
    flex: 1,
  },
  countdown: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_12,
  },
  current: {
    position: 'absolute',
    top: Dimension.HEIGHT_90,
    width: Dimension.WIDTH_338,
  },
  helpContainer: {
    flex: 2,
    paddingRight: 10,
    alignItems: 'flex-end',
  },
  icon: {
    marginLeft: 5,
    marginRight: 5,
  },
  imageBackground: {
    flex: 1,
    height: Dimension.HEIGHT_130,
  },
  indicator: {
    width: '100%',
  },
  loadingContainer: {
    alignItems: 'center',
    height: Dimension.HEIGHT_150,
    justifyContent: 'center',
  },
  message: {
    alignSelf: 'center',
    fontSize: 16,
  },
  schedule: {
    fontSize: Fonts.FONT_SIZE_12,
    fontWeight: 'bold',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  scroll: {
    marginBottom: Dimension.HEIGHT_50,
  },
  skipButton: {
    alignItems: 'center',
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 5,
    borderWidth: 2,
    justifyContent: 'center',
    height: Dimension.HEIGHT_30,
    width: Dimension.WIDTH_100,
  },
  skipText: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  skipZone: {
    alignItems: 'flex-end',
    paddingRight: Dimension.WIDTH_20,
    paddingTop: Dimension.WIDTH_20,
    position: 'absolute',
    top: Dimension.HEIGHT_40,
    width: '100%',
  },
  statusContainer: {
    height: Dimension.HEIGHT_60,
  },
  subtitle: {
    marginTop: 15,
    fontWeight: '300',
    alignSelf: 'center',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  timer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
    width: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  titleContainer: {
    flex: 4,
    alignItems: 'center',
  },
  zone: {
    elevation: 24,
    height: Dimension.HEIGHT_130,
    marginTop: Dimension.HEIGHT_20,
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 12},
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    width: Dimension.WIDTH_338,
  },
  zoneDetails: {
    alignItems: 'center',
    backgroundColor: Colors.HEADER_COLOR,
    flexDirection: 'row',
    height: Dimension.HEIGHT_30,
    justifyContent: 'space-between',
    opacity: 0.75,
    paddingLeft: Dimension.WIDTH_24,
    paddingRight: Dimension.WIDTH_24,
    position: 'absolute',
    top: 0,
    width: '100%',
  },
  zoneList: {
    flexDirection: 'column',
    paddingLeft: Dimension.WIDTH_20,
    paddingRight: Dimension.WIDTH_20,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_100,
  },
  zoneName: {
    fontSize: Fonts.FONT_SIZE_16,
    fontWeight: 'bold',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zoneNumber: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_15,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class ActiveScheduleScreen extends Component {
  isOffline = false;
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      skipped: false,
    };
    this.fetchScheduleStatus = this.fetchScheduleStatus.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.handlePress = this.handlePress.bind(this);
    this.setProgress = this.setProgress.bind(this);
    this.skipZone = this.skipZone.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopSchedule = this.stopSchedule.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    BackHandler.addEventListener('hardwareBackPress', this.handlePress);
    clearInterval(this.progressTimer);
    const {scheduleObj} = this.props;
    if (
      Object.keys(scheduleObj).length === 0 &&
      scheduleObj.constructor === Object
    ) {
      this.offlineDuration = true;
      this.checkIfDeviceOffline();
      return;
    }
    this.offlineDuration = false;
    this.startTimer();
  }

  checkIfDeviceOffline = () => {
    const {device} = this.props;
    setTimeout(() => {
      if (this.offlineDuration) {
        store.dispatch(appActions.fetchCurrentSchedule(device._id));
      }
    }, 5000);
    setTimeout(() => {
      if (this.offlineDuration) {
        Alert.alert('Timeout Error', 'Looks like controller is offline', [
          {
            text: 'Return Back',
            onPress: () => {
              this.props.navigation.goBack();
            },
          },
        ]);
      }
    }, 10000);
  };

  componentDidUpdate(prevProps) {
    const {scheduleObj} = this.props;
    if (!equal(prevProps.scheduleObj, scheduleObj)) {
      clearInterval(this.progressTimer);
      clearTimeout(this.updateTimer);
      if (
        Object.keys(scheduleObj).length === 0 &&
        scheduleObj.constructor === Object
      ) {
        this.offlineDuration = true;
        this.checkIfDeviceOffline();
        return;
      }
      this.offlineDuration = false;
      this.startTimer();
    }
  }

  componentWillUnmount() {
    this.offlineDuration = false;
    AppState.removeEventListener('change', this.handleAppStateChange);
    BackHandler.removeEventListener('hardwareBackPress', this.handlePress);
    clearTimeout(this.progressTimer);
    clearTimeout(this.updateTimer);
  }

  setProgress() {
    const {scheduleObj} = this.props;
    if (!this.checkForValidSchedule()) {
      this.offlineDuration = true;
      this.checkIfDeviceOffline();
      return;
    }
    this.offlineDuration = false;
    const {_id, zones} = scheduleObj;
    const runningZone = zones.find(
      (item) => item.status === WateringStatus.STARTED,
    );
    if (runningZone) {
      if (moment(runningZone.endTime).diff(moment()) >= 0) {
        const startTime = moment(runningZone.startTime);
        const timeElapsed = moment().diff(startTime, 'seconds');
        console.log(timeElapsed);
        if (!isNaN(timeElapsed)) {
          store.dispatch(appActions.setProgressValue(timeElapsed + 1));
        }
      }
    } else {
      clearTimeout(this.progressTimer);
      clearTimeout(this.updateTimer);
      store.dispatch(appActions.setProgressValue(0));
    }
  }

  fetchScheduleStatus() {
    const {device} = this.props;
    clearInterval(this.progressTimer);
    store.dispatch(appActions.setProgressValue(0));
    setTimeout(() => {
      store.dispatch(appActions.fetchCurrentSchedule(device._id));
    }, 2000);
  }

  handleAppStateChange(nextAppState) {
    clearInterval(this.progressTimer);
    if (nextAppState === 'active') {
      const {scheduleObj} = this.props;
      if (
        Object.keys(scheduleObj).length === 0 &&
        scheduleObj.constructor === Object
      ) {
        this.offlineDuration = true;
        this.checkIfDeviceOffline();
        return;
      }
      this.offlineDuration = false;
      this.startTimer();
    }
  }

  handlePress() {
    this.props.navigation.goBack();
  }

  skipZone(wateringId, zoneId) {
    this.setState({loading: true, skipped: true});
    clearInterval(this.progressTimer);
    clearTimeout(this.updateTimer);
    const {device} = this.props;
    userServices
      .skipZone({serialNumber: device.serialNumber, wateringId, zoneId})
      .then(() => this.setState({loading: false, skipped: false}))
      .catch((err) => {});
  }

  startTimer() {
    this.progressTimer = setInterval(() => {
      this.setProgress();
    }, 1000);
  }

  stopSchedule(wateringId) {
    this.setState({loading: true});
    clearInterval(this.progressTimer);
    clearTimeout(this.updateTimer);
    const {device} = this.props;
    userServices
      .stopQuickRun({serialNumber: device.serialNumber, wateringId})
      .then(() => {
        this.props.navigation.goBack();
      })
      .catch((err) => {});
  }

  calculateTime(currentTime, totalDuration) {
    const mRemainingTime = totalDuration - currentTime;
    const response = {
      currentTime: moment().startOf('day').seconds(currentTime).format('mm:ss'),
      remainingTime: moment()
        .startOf('day')
        .seconds(mRemainingTime)
        .format('mm:ss'),
    };
    return response;
  }

  checkForValidSchedule = () => {
    let response = true;
    const {scheduleObj} = this.props;
    if (scheduleObj === null || !scheduleObj._id) {
      response = false;
    }
    return response;
  };

  getZoneImage = (zoneId) => {
    const {zones} = this.props;
    const zone = zones.find((item) => item.id === zoneId);
    if (zone) {
      return zone.defaultImage;
    }
    return null;
  };

  render() {
    const {loading} = this.state;
    const {scheduleObj, value} = this.props;

    if (!this.checkForValidSchedule()) {
      return (
        <View
          style={[
            styles.container,
            {backgroundColor: Colors.SCREEN_BACKGROUND},
          ]}>
          <View style={styles.statusContainer}>
            <LinearGradient
              style={styles.container}
              locations={[0.1, 1.0]}
              colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
              <View style={styles.appBar}>
                <View style={styles.backNavigator}>
                  <TouchableOpacity
                    style={{flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.handlePress()}>
                    <VectorIcon
                      color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      name="back"
                      size={24}
                      style={styles.icon}
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>Active Schedule</Text>
                </View>
                <View style={styles.helpContainer}>
                  <Ionicons
                    name="ios-help-circle-outline"
                    color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                    size={30}
                  />
                </View>
              </View>
            </LinearGradient>
          </View>
          <View style={styles.loadingContainer}>
            <Spinner
              color={Colors.TEXT_COLOR}
              isVisible
              size={40}
              type="ThreeBounce"
            />
          </View>
        </View>
      );
    } else {
      const {_id, zones, status, type, schedule} = scheduleObj;
      const runningZone = zones.find(
        (item) => item.status === WateringStatus.STARTED,
      );
      const time = runningZone
        ? this.calculateTime(value, runningZone.scheduledDuration)
        : {};
      const {skipped} = this.state;
      return (
        <View
          style={[
            styles.container,
            {backgroundColor: Colors.SCREEN_BACKGROUND, position: 'relative'},
          ]}>
          <View style={styles.statusContainer}>
            <LinearGradient
              style={styles.container}
              locations={[0.1, 1.0]}
              colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
              <View style={styles.appBar}>
                <View style={styles.backNavigator}>
                  <TouchableOpacity
                    style={{flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => this.handlePress()}>
                    <VectorIcon
                      color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                      name="back"
                      size={24}
                      style={styles.icon}
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>
                    {getWateringTypeName(type, schedule)}
                  </Text>
                </View>
                <View style={styles.helpContainer}>
                  <Ionicons
                    name="ios-help-circle-outline"
                    color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                    size={30}
                  />
                </View>
              </View>
            </LinearGradient>
          </View>
          <View style={styles.zoneList}>
            <ScrollView style={styles.scroll}>
              {zones.map((item) => {
                const zoneImage = this.getZoneImage(parseInt(item._id));
                return (
                  <View key={item._id} style={styles.zone}>
                    <ImageBackground
                      source={
                        zoneImage
                          ? {uri: zoneImage}
                          : require('../../../assets/images/zone-default/zone-default.png')
                      }
                      style={styles.imageBackground}>
                      <View style={styles.zoneDetails}>
                        <Text style={styles.zoneName}>{`${item.name} | ${
                          Math.floor(item.scheduledDuration / 60) > 0
                            ? `${Math.floor(item.scheduledDuration / 60)} min`
                            : ''
                        } ${
                          item.scheduledDuration % 60 === 0
                            ? ''
                            : `${item.scheduledDuration % 60} sec`
                        }`}</Text>
                        <Text style={styles.schedule}>
                          Status: {getWateringStatus(item.status)}
                        </Text>
                      </View>
                      {item.status === WateringStatus.COMPLETED ||
                      item.status === WateringStatus.SKIPPED ? (
                        <View style={styles.skipZone}>
                          <MaterialIcon
                            name="done"
                            size={30}
                            color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                          />
                        </View>
                      ) : item.status === WateringStatus.STARTED ? (
                        <View style={styles.skipZone}>
                          <TouchableOpacity
                            disabled={skipped}
                            onPress={() => this.skipZone(_id, item._id)}
                            style={styles.skipButton}>
                            {loading ? (
                              <Spinner
                                color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                                isVisible
                                size={20}
                                type="ThreeBounce"
                              />
                            ) : (
                              <Text style={styles.skipText}>Skip</Text>
                            )}
                          </TouchableOpacity>
                        </View>
                      ) : null}
                    </ImageBackground>
                    {item.status === WateringStatus.STARTED ? (
                      <View style={styles.current}>
                        <View style={styles.timer}>
                          <Text style={styles.countdown}>
                            {time.currentTime}
                          </Text>
                          <Text style={styles.countdown}>
                            {time.remainingTime}
                          </Text>
                        </View>
                        <ProgressBar
                          progress={
                            isNaN(
                              (value * 1000) /
                                (runningZone.scheduledDuration * 1000),
                            )
                              ? 0
                              : (value * 1000) /
                                (runningZone.scheduledDuration * 1000)
                          }
                          color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                          style={styles.indicator}
                        />
                      </View>
                    ) : null}
                  </View>
                );
              })}
            </ScrollView>
          </View>
          <TouchableOpacity
            disabled={status === WateringStatus.STARTED ? false : true}
            onPress={() => this.stopSchedule(_id)}
            style={styles.button}>
            <Text style={styles.text}>Stop Schedule</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleObj} = state.currentSchedule;
  const {value} = state.progress;
  const {zones} = state.zones;
  return {device, scheduleObj, value, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActiveScheduleScreen);
