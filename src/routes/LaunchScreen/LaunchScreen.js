/* eslint-disable class-methods-use-this */
import { connect } from 'react-redux';
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';
import { bindActionCreators } from 'redux';
import store from '../../helpers/store';
import { appActions } from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import { Colors, Dimension, Fonts } from '../../constants';

const styles = StyleSheet.create({
    button: {
        bottom: 0,
        borderRadius: 8,
        shadowRadius: 11,
        shadowOpacity: 1,
        position: 'absolute',
        alignItems: 'center',
        shadowColor: '#5d9f26',
        justifyContent: 'center',
        width: Dimension.WIDTH,
        height: Dimension.HEIGHT_70,
        shadowOffset: { width: 0, height: 7 },
        backgroundColor: Colors.PRIMARY_COLOR_DARK,
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: Fonts.FONT_SIZE_20,
        color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
    container: {
        flex: 1,
        position: 'relative',
    },
    separator: {
        fontSize: Fonts.FONT_SIZE_75,
    },
    text: {
        fontWeight: '100',
        fontSize: Fonts.FONT_SIZE_45,
        marginLeft: Dimension.WIDTH_29,
        marginTop: Dimension.HEIGHT_100,
        color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    },
});

class LaunchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paginationVisible: true,
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.togglePagination = this.togglePagination.bind(this);
    }

    onSubmit() {
        AsyncStorage.setItem('@Sprinkler:OnBoard', 'true');
        const user = auth().currentUser;
        store.dispatch(appActions.checkLogin(user !== null));
    }

    togglePagination(index) {
        if (index === 2) {
            this.setState({ paginationVisible: false });
            return;
        }
        this.setState({ paginationVisible: true });
    }

    render() {
        const { paginationVisible } = this.state;
        return (
            <LinearGradient
                locations={[0.1, 1.0]}
                style={styles.container}
                colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}
            >
                <Swiper
                    loop={false}
                    showsButtons={false}
                    style={styles.page}
                    dotColor={Colors.SWIPER_DOT_COLOR}
                    onIndexChanged={this.togglePagination}
                    showsPagination={paginationVisible}
                    activeDotColor={Colors.SWIPER_ACTIVE_DOT_COLOR}
                >
                    <Page01 />
                    <Page02 />
                    <Page03 handleClick={this.onSubmit} />
                </Swiper>
            </LinearGradient>
        );
    }
}

const Page01 = () => (
    <View style={styles.container}>
        <Text style={styles.text}>{`Hey!${'\n'}Welcome to${'\n'}Sprinkler`}</Text>
    </View>
);

const Page02 = () => (
    <View style={styles.container}>
        <Text style={styles.text}>{`We${'\n'}Recommend${'\n'}to Water${'\n'}atleast`}</Text>
        <Text style={[styles.text, { fontWeight: 'bold' }]}>
        02 times
            <Text style={[styles.separator, { fontWeight: 'normal' }]}>/</Text>
            week
        </Text>
    </View>
);

const Page03 = ({ handleClick }) => (
    <View style={styles.container}>
        <Text style={styles.text}>
            {`And, for best${'\n'}results${'\n'}Schedule${'\n'}`}
        </Text>
        <Text style={[styles.text, { fontWeight: 'bold' }]}>Sprinkler</Text>
        <TouchableOpacity
            style={styles.button}
            onPress={() => handleClick()}
        >
            <Text style={styles.buttonText}>Get Started</Text>
        </TouchableOpacity>
    </View>
);

const mapDispatchToProps = (dispatch) => bindActionCreators(ActionCreators, dispatch);

export default connect(null, mapDispatchToProps)(LaunchScreen);
