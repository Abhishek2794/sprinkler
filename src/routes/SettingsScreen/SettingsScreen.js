/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/jsx-one-expression-per-line */
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {bindActionCreators} from 'redux';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import MarqueeText from 'react-native-marquee';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {userActions} from '../../actions/user.actions';
import {Colors, Fonts, Dimension} from '../../constants';
import ControllerPicker from '../../components/controllerPicker';
import {appServices} from '../../services';
import VersionInfo from 'react-native-version-info';
import {logEvent} from '../../analytics';
import FirmwareUpgrade from '../firmwareUpgrade';
import {checkFirmwareVersion} from '../../helpers/firmwareVersionCheck';

const styles = StyleSheet.create({
  box: {
    flex: 1,
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#000000',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: Dimension.WIDTH_20 / 2,
    paddingRight: Dimension.WIDTH_20 / 2,
    width: Dimension.WIDTH_166,
    height: Dimension.HEIGHT_30,
  },
  dropdown: {
    width: Dimension.WIDTH_30,
    height: Dimension.WIDTH_30,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_14,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  formLabel: {
    flex: 2,
    flexDirection: 'column',
  },
  infoContainer: {
    marginLeft: Dimension.WIDTH_29,
    marginRight: Dimension.WIDTH_29,
  },
  inner: {
    alignItems: 'center',
    borderRadius: 8,
    height: Dimension.HEIGHT_170,
    marginBottom: 7,
    marginTop: 7,
    marginLeft: 10,
    marginRight: 10,
    minHeight: Dimension.HEIGHT_130,
  },
  marqueeContainer: {
    width: '70%',
    height: Dimension.HEIGHT_24,
  },
  outer: {
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    alignSelf: 'center',
    width: Dimension.WIDTH_338,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  serialNumber: {
    marginTop: 5,
    fontWeight: '100',
    fontSize: Fonts.FONT_SIZE_12,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_16,
    color: '#fff',
  },
});

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      firmwareVersionCheck: false,
      firmwareUpgrade: false,
    };
    this.closeModal = this.closeModal.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.addController = this.addController.bind(this);
    this.switchController = this.switchController.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentDidMount() {
    const {navigation, device} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', () => {
      store.dispatch(userActions.dockView(true));
      logEvent('SETTING_SCREEN', {screen: 'SETTING_SCREEN'});
    });
    this.setState({
      firmwareVersionCheck: checkFirmwareVersion(
        device.availableFirmwareUpdateVersion,
        device.currentFirmware,
      ),
    });
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  handleNavigation(screen) {
    switch (screen) {
      case 'History':
        logEvent('DEVICE_LOGS', {screen: 'DEVICE_LOGS'});
        break;
      case 'UserAccount':
        logEvent('Account_Settings', {screen: 'Account_Settings'});
        break;
      default:
        logEvent(screen, {screen});
    }
    const {navigation} = this.props;
    navigation.navigate(screen);
  }

  addController() {
    const {navigation} = this.props;
    this.setState({modalVisible: false});
    navigation.navigate('AddDevice');
  }

  handleClick() {
    this.setState({modalVisible: true});
  }

  closeModal() {
    this.setState({modalVisible: false});
  }

  switchController(index) {
    const {allDevices} = this.props;
    this.setState({modalVisible: false});
    const activeDevice = allDevices[index];
    appServices.setActiveDevice(activeDevice).then(() => {
      store.dispatch(appActions.fetchActiveDevice());
    });
  }

  render() {
    const {modalVisible, firmwareVersionCheck} = this.state;
    const {allDevices, device, navigation, online, lastRelease} = this.props;
    const noDevice = allDevices.length === 0;
    return (
      <View style={styles.box}>
        {noDevice ? (
          <Helpers.StatusBar
            child={Helpers.HelpButton}
            isBackNavigable={true}
            navigation={navigation}
            subtitle="No Device Found"
            title="Settings"
          />
        ) : (
          <Helpers.StatusBar
            subtitle=""
            title="Settings"
            isBackNavigable={true}
            child={Helpers.HelpButton}
            navigation={navigation}
          />
        )}

        {noDevice ? null : (
          <ControllerPicker
            devices={allDevices}
            activeDevice={device}
            modalVisible={modalVisible}
            closePopup={this.closeModal}
            addController={this.addController}
            switchController={this.switchController}
          />
        )}

        <ScrollView>
          <View style={styles.outer}>
            <LinearGradient
              locations={[0.1, 1.0]}
              style={styles.inner}
              colors={['#5c5c5c', '#000000']}>
              <Image
                style={{marginTop: 14, marginBottom: 7}}
                source={require('../../../assets/images/sprinkler/sprinkler.png')}
              />
              {noDevice ? null : (
                <TouchableOpacity
                  style={styles.container}
                  onPress={() => {
                    this.handleClick();
                  }}>
                  <View style={styles.marqueeContainer}>
                    <MarqueeText
                      loop
                      marqueeDelay={500}
                      marqueeOnStart
                      marqueeResetDelay={500}
                      style={styles.text}>
                      {device.name}
                    </MarqueeText>
                  </View>
                  <Icon
                    name="arrow-drop-down"
                    color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                    size={30}
                  />
                </TouchableOpacity>
              )}
              {noDevice ? null : (
                <Text style={styles.serialNumber}>{device.serialNumber}</Text>
              )}
            </LinearGradient>
            {noDevice ? null : (
              <View style={styles.infoContainer}>
                <View style={styles.formElement}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>App Version</Text>
                    <Text style={styles.formFieldValue}>
                      {VersionInfo.appVersion} released on {lastRelease}
                    </Text>
                  </View>
                </View>
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => this.handleNavigation('DeviceSetting')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Controller Settings</Text>
                    <Text style={styles.formFieldValue}>{device.name}</Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
                {/* <View style={styles.formElement}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Firmware Version</Text>
                    <Text style={styles.formFieldValue}>
                      {device.currentFirmware ? device.currentFirmware : 'N/A'}
                    </Text>
                  </View>
                  {firmwareVersionCheck ? (
                    <View style={styles.formEdit}>
                      <Button
                        icon="update"
                        mode="outlined"
                        color={Colors.PRIMARY_COLOR}
                        onPress={() => {
                          this.setState({
                            firmwareUpgrade: true,
                          });
                        }}>
                        Update
                      </Button>
                    </View>
                  ) : null}
                </View>
                <View style={styles.formElement}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Hardware Version</Text>
                    <Text style={styles.formFieldValue}>
                      {device.hardwareVersion ? device.hardwareVersion : 'N/A'}
                    </Text>
                  </View>
                </View> */}
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => this.handleNavigation('DeviceInfoScreen')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Device Info</Text>
                    <Text style={styles.formFieldValue}>
                      View controller informations
                    </Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => this.handleNavigation('History')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Device Logs</Text>
                    <Text style={styles.formFieldValue}>Watering History</Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => this.handleNavigation('MeasurementScreen')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Measurement Units</Text>
                    <Text style={styles.formFieldValue}>
                      Configure measurements preferences
                    </Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => this.handleNavigation('NetworkInfoScreen')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Network Info</Text>
                    <Text style={styles.formFieldValue}>
                      View controller network informations
                    </Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.formElement}
                  onPress={() => navigation.navigate('NotificationPreference')}>
                  <View style={styles.formLabel}>
                    <Text style={styles.formField}>Notification Settings</Text>
                    <Text style={styles.formFieldValue}>
                      Configure the notification settings
                    </Text>
                  </View>
                  <View style={styles.formEdit}>
                    <Icon
                      name="keyboard-arrow-right"
                      size={24}
                      color="#929292"
                    />
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
          <View style={styles.outer}>
            <View style={styles.infoContainer}>
              <TouchableOpacity
                style={styles.formElement}
                onPress={() => this.handleNavigation('UserAccount')}>
                <View style={styles.formLabel}>
                  <Text style={styles.formField}>Account Settings</Text>
                  <Text style={styles.formFieldValue}>Edit Profile</Text>
                </View>
                <View style={styles.formEdit}>
                  <Icon name="keyboard-arrow-right" size={24} color="#929292" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.formElement}
                onPress={() => navigation.navigate('ServerPreference')}>
                <View style={styles.formLabel}>
                  <Text style={styles.formField}>Server Preference</Text>
                  <Text style={styles.formFieldValue}>
                    Configure server preferences
                  </Text>
                </View>
                <View style={styles.formEdit}>
                  <Icon name="keyboard-arrow-right" size={24} color="#929292" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.formElement}>
                <View style={styles.formLabel}>
                  <Text style={styles.formField}>Help Centre</Text>
                  <Text style={styles.formFieldValue}>FAQ, Contact Us</Text>
                </View>
                <View style={styles.formEdit}>
                  <Icon name="keyboard-arrow-right" size={24} color="#929292" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <FirmwareUpgrade
          device={device}
          visible={this.state.firmwareUpgrade}
          serialNumber={device.serialNumber}
          onClose={() =>
            this.setState({
              firmwareUpgrade: false,
            })
          }
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {online} = state.deviceOnline;
  const {lastRelease} = state.app;
  return {allDevices, device, online, lastRelease};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
