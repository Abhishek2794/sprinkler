import {connect} from 'react-redux';
import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title} from 'react-native-paper';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  screenImg: {
    borderRadius: 8,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '50%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

class ZoneSetupScreen11 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  setupZone = () => {
    const {navigation} = this.props;
    navigation.navigate('ZoneSetupScreen1');
  };
  abortSetUp = () => {
    store.dispatch(appActions.checkLogin(null));
  };
  render() {
    const {navigation, route} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Setup Success"
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Great Job!</Title>
          <Subheading>You have successfully set up your zone</Subheading>
        </View>
        <View style={styles.screenHolder}>
          <Image
            style={styles.screenImg}
            height={300}
            width={350}
            source={require('../../../assets/images/zone-setup/screen-11.png')}
          />
        </View>
        <View style={styles.footer}>
          <Button
            mode="outlined"
            onPress={this.abortSetUp}
            style={[styles.footerBtn, {marginRight: 4}]}
            color={Colors.PRIMARY_COLOR}>
            Exit Setup
          </Button>
          <Button
            mode="contained"
            disabled={this.state.loading}
            loading={this.state.loading}
            onPress={this.setupZone}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Next Zone
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  return {
    device,
  };
};

export default connect(mapStateToProps, null)(ZoneSetupScreen11);
