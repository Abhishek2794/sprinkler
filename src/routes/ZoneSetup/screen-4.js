import {connect} from 'react-redux';
import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title} from 'react-native-paper';
import {logEvent} from '../../analytics';
import {userServices} from '../../services';
import alert from '../../components/alert';
import {StackActions} from '@react-navigation/native';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  screenImg: {
    borderRadius: 8,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '50%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

const DURATION = 60;

class ZoneSetupScreen4 extends Component {
  interval = null;
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      timer: DURATION,
    };
  }
  componentDidMount() {
    const {zone} = this.props;
    const {deviceId, _id, number} = zone;
    userServices
      .startQuickRun({
        zoneId: _id,
        zoneNumber: number,
        deviceId,
        duration: DURATION,
      })
      .then(() => {
        setTimeout(() => {
          this.setState({loading: false});
        }, 4000);
        this.interval = setInterval(() => {
          this.setState({
            timer: this.state.timer - 1,
          });
        }, 1000);
      })
      .catch((err) => alert('Sprinkler Error', 'Device Is Offline'));
  }
  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
  wateringSuccess = () => {
    const {zone, navigation, device, scheduleObj} = this.props;
    const {_id} = scheduleObj;
    this.setState({loading: true});
    userServices
      .stopQuickRun({serialNumber: device.serialNumber, wateringId: _id})
      .then(() => {
        setTimeout(() => {
          this.setState({loading: false});
          clearInterval(this.interval);
          navigation.dispatch(StackActions.replace('ZoneSetupScreen3'));
          navigation.navigate('ZoneSetupScreen5', {zone});
        }, 2000);
      })
      .catch((err) => {
        this.setState({loading: false});
        alert(
          'Controller Error',
          `Not able to stop the watering due to ${err}`,
        );
      });
  };
  notWatering = () => {
    const {navigation} = this.props;
    clearInterval(this.interval);
    navigation.navigate('ZoneSetupScreen1');
  };
  render() {
    const {navigation, zone} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Zone Setup"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Is {zone.name} watering?</Title>
          <Subheading>The selected zone should be watering</Subheading>
        </View>
        <View style={styles.screenHolder}>
          <Title style={{fontSize: 30}}>{this.state.timer}</Title>
        </View>
        <View style={styles.footer}>
          <Button
            mode="outlined"
            onPress={this.notWatering}
            style={[styles.footerBtn, {marginRight: 4}]}
            color={Colors.PRIMARY_COLOR}>
            No
          </Button>
          <Button
            mode="contained"
            disabled={this.state.loading}
            loading={this.state.loading}
            onPress={this.wateringSuccess}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Yes
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleObj} = state.currentSchedule;
  const {zone} = state.zone;
  return {
    device,
    scheduleObj,
    zone,
  };
};

export default connect(mapStateToProps, null)(ZoneSetupScreen4);
