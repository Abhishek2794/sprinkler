import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';
import {appConstants, Colors} from '../../constants';
import * as Helpers from '../../helpers/modules';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import {Subheading, Text, Title, Button} from 'react-native-paper';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  scrollContainer: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  zoneItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 12,
    borderBottomColor: '#dadada',
    borderBottomWidth: 1,
  },
  zoneImageHolder: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '100%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 0,
  },
});

class ZoneSetupScreen1 extends Component {
  componentDidMount() {
    const {device} = this.props;
    store.dispatch(appActions.fetchZones(device._id));
  }
  render() {
    const {navigation, zones} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Zone Setup"
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Setup your zones</Title>
          <Subheading>Select each zone to test and configure</Subheading>
        </View>
        <ScrollView style={styles.scrollContainer}>
          {zones.map((item) => {
            return (
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => {
                  store.dispatch(appActions.updateZone(item));
                  if (item.enabled) {
                    navigation.navigate('ZoneSetupScreen3');
                  } else {
                    navigation.navigate('ZoneSetupScreen2');
                  }
                }}
                style={styles.zoneItem}
                key={item.id}>
                <View style={styles.zoneImageHolder}>
                  <Image
                    source={
                      item.defaultImage
                        ? {uri: item.defaultImage}
                        : require('../../../assets/images/zone-default/zone-default.png')
                    }
                    style={{
                      height: 60,
                      width: 60,
                      marginRight: 12,
                      opacity: item.enabled ? 1 : 0.5,
                    }}
                  />
                  <Text>{item.name}</Text>
                </View>
                <Icon name="chevron-right" size={22} />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View style={styles.footer}>
          <Button
            mode="contained"
            onPress={() => {
              navigation.navigate('Home');
            }}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Skip Setup
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {zones} = state.zones;
  const {device} = state.activeDevice;
  return {
    zones,
    device,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ZoneSetupScreen1);
