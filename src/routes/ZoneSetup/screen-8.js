import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Switch,
  TouchableOpacity,
} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title, RadioButton, Text} from 'react-native-paper';
import {logEvent} from '../../analytics';
import {userServices} from '../../services';
import alert from '../../components/alert';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    padding: 16,
    flex: 1,
  },
  scrollItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    alignItems: 'center',
    borderBottomColor: '#dadada',
    borderBottomWidth: 1,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 8,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '100%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

class ZoneSetupScreen8 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      initialised: false,
      zoneSetUpInfo: {},
      selectedValue: '',
      isEdit: false,
    };
  }
  componentDidMount() {
    const {route} = this.props;
    this.setState({
      selectedValue: route.params.selectedValue || '',
      isEdit: route.params.isEdit || false,
      zoneSetUpInfo: route.params.zoneSetUpInfo,
      initialised: true,
    });
  }
  updateSoilType = (value) => {
    if (value !== this.state.selectedValue) {
      this.setState({
        selectedValue: value,
      });
    } else {
      this.setState({
        selectedValue: '',
      });
    }
  };

  onNext = () => {
    this.setState({loading: true});
    const {zoneSetUpInfo, selectedValue, isEdit} = this.state;
    const {route, navigation, device} = this.props;
    const zone = route.params.zone;
    zone.soilType = selectedValue;
    userServices.updateZone(zone).then(() => {
      store.dispatch(appActions.fetchZones(device._id, zone._id));
      this.setState({loading: false});
      if (isEdit) {
        navigation.navigate('EditZone');
      } else {
        navigation.navigate('ZoneSetupScreen9', {zone, zoneSetUpInfo});
      }
    });
  };

  render() {
    const {navigation, route} = this.props;
    const {zoneSetUpInfo, initialised, selectedValue, isEdit} = this.state;
    console.log(zoneSetUpInfo);
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Soil Type"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Confirm your soil type.</Title>
          <Subheading>
            We have selected most common soil type at your location
          </Subheading>
        </View>
        <View style={styles.screenHolder}>
          {initialised ? (
            <ScrollView>
              {zoneSetUpInfo.soilType.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.item}
                    onPress={() => this.updateSoilType(item.soilType)}>
                    <Text>{item.soilType}</Text>
                    <Switch
                      value={item.soilType === selectedValue}
                      onValueChange={() => this.updateSoilType(item.soilType)}
                    />
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : (
            <ActivityIndicator animating={!initialised} />
          )}
        </View>
        <View style={styles.footer}>
          <Button
            mode="contained"
            onPress={this.onNext}
            disabled={this.state.loading || !this.state.selectedValue}
            loading={this.state.loading}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            {isEdit ? 'Update' : 'Next'}
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  return {
    device,
  };
};

export default connect(mapStateToProps, null)(ZoneSetupScreen8);
