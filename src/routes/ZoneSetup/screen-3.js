import {connect} from 'react-redux';
import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors} from '../../constants';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title} from 'react-native-paper';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  screenImg: {
    borderRadius: 8,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '50%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

class ZoneSetupScreen3 extends Component {
  testZone = () => {
    const {navigation} = this.props;
    navigation.navigate('ZoneSetupScreen4');
  };
  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Zone Setup"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>First, let's test this zone</Title>
          <Subheading>
            Let's check to make sure this zone is set up correctly. Walk to
            zone, then tab "Test Zone"
          </Subheading>
        </View>
        <View style={styles.screenHolder}>
          <Image
            style={styles.screenImg}
            height={240}
            width={350}
            source={require('../../../assets/images/zone-setup/screen-3.png')}
          />
        </View>
        <View style={styles.footer}>
          <Button
            mode="outlined"
            onPress={() => navigation.navigate('ZoneSetupScreen1')}
            style={[styles.footerBtn, {marginRight: 4}]}
            color={Colors.PRIMARY_COLOR}>
            Back
          </Button>
          <Button
            mode="contained"
            onPress={this.testZone}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Test Zone
          </Button>
        </View>
      </View>
    );
  }
}

export default connect(null, null)(ZoneSetupScreen3);
