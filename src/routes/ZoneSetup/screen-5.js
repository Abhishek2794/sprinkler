import {connect} from 'react-redux';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  ImageBackground,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title, TextInput, Caption} from 'react-native-paper';
import {userServices} from '../../services';
import ActionSheet from 'react-native-actionsheet';
import Permissions, {RESULTS, PERMISSIONS} from 'react-native-permissions';
import * as ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    padding: 16,
    flex: 1,
  },
  screenImg: {
    borderRadius: 8,
    marginVertical: 24,
    height: 300,
    width: '100%',
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '100%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
  contentBottomRight: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  contentCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  camera: {
    borderRadius: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimension.WIDTH_63,
    height: Dimension.HEIGHT_27,
    backgroundColor: '#fff',
    marginRight: 6,
  },
  edit: {
    marginLeft: 5,
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_12,
    color: '#000',
    letterSpacing: 1.5,
  },
});

class ZoneSetupScreen5 extends Component {
  interval = null;
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      zoneName: this.props.route.params.zone.name,
      error: '',
      showImageLoader: false,
      zoneSetUpInfo: {},
    };
  }

  componentDidMount() {
    userServices.getZoneSetupInfo().then((data) => {
      this.setState({
        zoneSetUpInfo: data,
      });
    });
  }

  setUpVegetation = () => {
    this.setState({loading: true});
    const {navigation, device, zone} = this.props;
    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(this.state.zoneName)) {
      this.setState({error: 'Zone name cannot have special characters'});
    } else {
      zone.name = this.state.zoneName;
      userServices.updateZone(zone).then(() => {
        store.dispatch(appActions.fetchZone(device._id, zone.number));
        store.dispatch(appActions.fetchZones(device._id));
        this.setState({loading: false});
        navigation.navigate('ZoneSetupScreen6', {
          zone,
          zoneSetUpInfo: this.state.zoneSetUpInfo,
        });
      });
    }
  };

  async enableCamera(source) {
    const camera =
      Platform.OS == 'ios'
        ? PERMISSIONS.IOS.CAMERA
        : PERMISSIONS.ANDROID.CAMERA;
    if (source) {
      this.enableStorage(source);
    } else {
      const status = await Permissions.check(camera);
      if (status === RESULTS.GRANTED) {
        this.enableStorage(source);
      } else {
        const permission = await Permissions.request(camera);
        switch (permission) {
          case RESULTS.UNAVAILABLE:
            alert(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
            alert('Camera permission is denied');
            break;
          case RESULTS.LIMITED:
            alert('Camera permission is limited');
            break;
          case RESULTS.GRANTED:
            this.enableStorage(source);
            break;
          case RESULTS.BLOCKED:
            alert('The permission is denied and not requestable anymore');
            break;
          default:
            alert('Unexpected error while access the camera');
            break;
        }
      }
    }
  }

  async enableStorage(source) {
    const storage =
      Platform.OS == 'ios'
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE;
    const status = await Permissions.check(storage);
    if (status === RESULTS.GRANTED) {
      this.pickImage(source);
    } else {
      const permission = await Permissions.request(storage);
      switch (permission) {
        case RESULTS.UNAVAILABLE:
          alert(
            'This feature is not available (on this device / in this context)',
          );
          break;
        case RESULTS.DENIED:
          alert('Storage permission is denied');
          break;
        case RESULTS.LIMITED:
          alert('Storage permission is limited');
          break;
        case RESULTS.GRANTED:
          this.pickImage(source);
          break;
        case RESULTS.BLOCKED:
          alert('The permission is denied and not requestable anymore');
          break;
        default:
          alert('Unexpected error while access the Storage');
          break;
      }
    }
  }

  async pickImage(source) {
    if (source === 0) {
      ImagePicker.launchCamera({quality: 1}, (result) => {
        if (!result.didCancel) {
          this.cropImage(result);
        }
      });
    }
    if (source === 1) {
      ImagePicker.launchImageLibrary({quality: 1}, (result) => {
        if (!result.didCancel) {
          this.cropImage(result);
        }
      });
    }
  }

  cropImage(result) {
    const {navigation} = this.props;
    navigation.navigate('CropImageScreen', {image: result});
  }

  render() {
    const {navigation, zone} = this.props;
    const {showImageLoader} = this.state;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Zone Setup"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Add a name and photo to your zone</Title>
          <Subheading>
            Give your zone a meaningful name; for instance, "Front Yard Grass"
          </Subheading>
        </View>
        <View style={styles.screenHolder}>
          <TextInput
            label="Zone Name"
            value={this.state.zoneName}
            mode="outlined"
            onChangeText={(value) => {
              this.setState({
                error: '',
                zoneName: value,
              });
            }}
          />
          {this.state.error ? <Caption>{this.state.error}</Caption> : null}
          <ImageBackground
            onLoad={() => this.setState({showImageLoader: false})}
            style={[styles.screenImg]}
            source={
              zone.defaultImage
                ? {uri: zone.defaultImage}
                : require('../../../assets/images/zone-default/zone-default.png')
            }>
            <View
              style={[
                {flex: 1, marginBottom: 8},
                showImageLoader
                  ? styles.contentCenter
                  : styles.contentBottomRight,
              ]}>
              {showImageLoader ? (
                <ActivityIndicator size="large" />
              ) : (
                <TouchableOpacity
                  style={styles.camera}
                  onPress={() => this.ActionSheet.show()}>
                  <Icon name="edit" size={15} color={'#000'} />
                  <Text style={styles.edit}>EDIT</Text>
                </TouchableOpacity>
              )}
            </View>
          </ImageBackground>
        </View>
        <View style={styles.footer}>
          <Button
            mode="contained"
            disabled={this.state.loading}
            loading={this.state.loading}
            onPress={this.setUpVegetation}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Next
          </Button>
        </View>
        <ActionSheet
          ref={(ref) => {
            this.ActionSheet = ref;
          }}
          title="Select Option"
          options={['Take Photo', 'Choose from Library', 'Cancel']}
          destructiveButtonIndex={2}
          onPress={(userAction) => this.enableCamera(userAction)}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {zone} = state.zone;
  return {
    device,
    zone,
  };
};

export default connect(mapStateToProps, null)(ZoneSetupScreen5);
