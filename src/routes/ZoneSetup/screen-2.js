import {connect} from 'react-redux';
import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';
import {Colors} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as Helpers from '../../helpers/modules';
import {Subheading, Title} from 'react-native-paper';
import {logEvent} from '../../analytics';
import {userServices} from '../../services';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  screenHolder: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  screenImg: {
    borderRadius: 8,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '50%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

class ZoneSetupScreen2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }
  activeZone = () => {
    const {zone, navigation} = this.props;
    this.setState({loading: true});
    logEvent('ZONE_UPDATE', {enabled: true, screen: 'ZONE_SETUP'});
    const updatedZone = {...zone, enabled: true};
    userServices.updateZone(updatedZone).then(() => {
      this.setState({loading: false});
      store.dispatch(appActions.updateZone(updatedZone));
      navigation.navigate('ZoneSetupScreen3');
    });
  };
  render() {
    const {navigation, zone} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Zone Setup"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Let's setup {zone.name}</Title>
          <Subheading>
            To enable to zone press Next button or press back to abort the
            operation
          </Subheading>
        </View>
        <View style={styles.screenHolder}>
          <Image
            style={styles.screenImg}
            height={200}
            width={350}
            source={require('../../../assets/images/zone-setup/screen-2.png')}
          />
        </View>
        <View style={styles.footer}>
          <Button
            mode="outlined"
            onPress={() => navigation.navigate('ZoneSetupScreen1')}
            style={[styles.footerBtn, {marginRight: 4}]}
            color={Colors.PRIMARY_COLOR}>
            Back
          </Button>
          <Button
            mode="contained"
            disabled={this.state.loading}
            loading={this.state.loading}
            onPress={this.activeZone}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Next
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {zone} = state.zone;
  return {
    device,
    zone
  };
};

export default connect(mapStateToProps, null)(ZoneSetupScreen2);
