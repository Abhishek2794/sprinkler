import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import * as Helpers from '../../helpers/modules';
import {logEvent} from '../../analytics';
import LottieView from 'lottie-react-native';
import {Caption, Title, Button, Subheading} from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default class AddDeviceScreen extends Component {
  constructor(props) {
    super(props);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentDidMount() {
    logEvent('ADD_CONTROLLER_SCREEN', {screen: 'ADD_CONTROLLER_SCREEN'});
  }

  handleNavigation(screen) {
    const {navigation} = this.props;
    navigation.navigate(screen);
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle=""
          title="Add Device"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <View>
            <Title style={{textAlign: 'center'}}>
              Setup your controller to sprinkler your thirsty garden
            </Title>
          </View>
          <View style={{flex: 1, width: '100%'}}>
            <LottieView
              source={require('../../../assets/animation/Launch.json')}
              autoPlay
              loop
            />
          </View>
          <Caption>Expected setup time ~1 min</Caption>
        </Helpers.Card>
        <Helpers.Footer>
          <Button
            style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
            onPress={() => navigation.navigate('AddAddress')}
            mode="contained">
            <Subheading
              style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
              Start Setup
            </Subheading>
          </Button>
        </Helpers.Footer>
      </View>
    );
  }
}
