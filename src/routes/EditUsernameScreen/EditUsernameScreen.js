/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import {logglyServices, userServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  box: {
    flex: 2,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditUsernameScreen extends Component {
  constructor(props) {
    super(props);
    const {userData} = this.props;
    this.state = {
      animating: false,
      error: '',
      nickname: userData.nickname,
      phone: userData.phoneNumber,
    };
    this.onSave = this.onSave.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(value, type) {
    if (type === "Name") {
      this.setState({nickname: value, error: ''});
      return;
    }
    if (type === "Phone Number") {
      this.setState({phone: value, value, error: ''});
      return;
    }
  }

  onSubmit() {
    const {nickname} = this.state;
    if (nickname === '') {
      this.setState({error: 'Invalid Name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(nickname)) {
      this.setState({error: 'Name cannot have special characters'});
    }
  }

  onSave() {
    this.setState({animating: true});
    const {nickname, phone} = this.state;
    if (!nickname) {
      this.setState({animating: false, error: 'Invalid User Name'});
      return;
    }

    const {navigation, userData} = this.props;

    userServices
      .updateUserDetails({...userData, nickname, phoneNumber: phone})
      .then(() => {
        this.setState({animating: false});
        store.dispatch(appActions.fetchUserDetails());
        navigation.goBack();
      })
      .catch((error) => {
        this.setState({animating: false});
        alert('ERROR', error.toString());
      });
  }

  render() {
    const {navigation, route} = this.props;
    const {animating, error, nickname, phone} = this.state;
    const type = route.params.type === 'Name' ? 0 : 1;
    return (
      <View style={styles.container}>
        <Spinner visible={animating} />

        <Helpers.StatusBar
          isBackNavigable
          title="Edit User"
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.box}>
              <Text style={styles.formField}>{route.params.type}</Text>
              <TextInput
                autoFocus
                maxLength={250}
                numberOfLines={1}
                editable
                returnKeyType="done"
                value={type ? phone : nickname}
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType={type ? 'phone-pad' : 'ascii-capable'}
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.onChangeText(text, route.params.type)}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: error
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
          </View>
          {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity onPress={() => this.onSave()} style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {userData} = state.user;
  return {userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditUsernameScreen);
