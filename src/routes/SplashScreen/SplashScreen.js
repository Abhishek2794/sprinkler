import React, {Component} from 'react';
import {StyleSheet, Text} from 'react-native';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import LinearGradient from 'react-native-linear-gradient';
import * as ActionCreators from '../../actions';
import {appActions} from '../../actions/app.actions';
import {appServices} from '../../services';
import {Colors, Fonts} from '../../constants';
import store from '../../helpers/store';
import {logEvent} from '../../analytics';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    alignSelf: 'center',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_64,
  },
});

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.checkBoarding = this.checkBoarding.bind(this);
  }

  componentDidMount() {
    logEvent('SPLASH_SCREEN', {screen: 'SplashScreen'});
    this.checkBoarding();
  }

  checkBoarding() {
    const {navigation} = this.props;
    appServices.checkBoarding().then((status) => {
      if (status) {
        const user = auth().currentUser;
        if (user === null) {
          store.dispatch(appActions.checkLogin(false));
          return;
        }
        store.dispatch(appActions.fetchActiveDevice());
        return;
      }
      navigation.navigate('Launch');
    });
  }

  render() {
    return (
      <LinearGradient
        locations={[0.1, 1.0]}
        style={styles.container}
        colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
        <Text style={styles.text}>Sprinkler</Text>
      </LinearGradient>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(null, mapDispatchToProps)(SplashScreen);
