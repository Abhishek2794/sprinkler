/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import {logglyServices, userServices, appServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {generateCalenderPayload} from '../../helpers/create-calender';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  box: {
    flex: 2,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditScheduleNameScreen extends Component {
  constructor(props) {
    super(props);
    const {scheduleData} = this.props;
    this.state = {
      animating: false,
      error: '',
      name: scheduleData.name,
    };
    this.onSave = this.onSave.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(name) {
    if (name) {
      this.setState({name, error: ''});
      return;
    }
    this.setState({name, error: 'Invalid Name'});
  }

  onSubmit() {
    const {name} = this.state;
    if (name === '') {
      this.setState({error: 'Invalid Name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({error: 'Name cannot have special characters'});
    }
  }

  onSave() {
    this.setState({animating: true});
    const {name} = this.state;
    if (!name) {
      this.setState({animating: false, error: 'Invalid Name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({
        animating: false,
        error: 'Name cannot have special characters',
      });
      return;
    }

    const {device, navigation, scheduleData, userData} = this.props;

    userServices
      .updateSchedule(device._id, {...scheduleData, name})
      .then((updatedSchedule) => {
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(
          appActions.fetchSchedule(updatedSchedule),
        );
        setTimeout(() => {
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                this.setState({animating: false});
              });
          }
          navigation.goBack();
        }, 2000);
      })
      .catch((error) => {
        this.setState({animating: false});
        alert('ERROR', error.toString());
      });
  }

  render() {
    const {navigation} = this.props;
    const {animating, error, name} = this.state;
    return (
      <View style={styles.container}>
        <Spinner visible={animating} />

        <Helpers.StatusBar
          isBackNavigable
          title="Edit Schedule"
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.box}>
              <Text style={styles.formField}>Schedule Name</Text>
              <TextInput
                autoFocus
                maxLength={250}
                numberOfLines={1}
                editable
                returnKeyType="done"
                value={name}
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                ref={(ref) => {
                  this.editName = ref;
                }}
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.onChangeText(text)}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: error
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
          </View>
          {error ? <Text style={styles.errorText}>Invalid Name</Text> : null}
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity onPress={() => this.onSave()} style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleData} = state.schedule;
  const {userData} = state.user;
  return {
    device,
    scheduleData,
    userData,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditScheduleNameScreen);
