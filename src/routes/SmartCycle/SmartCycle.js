import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, Image} from 'react-native';
import {Colors} from '../../constants';
import * as Helpers from '../../helpers/modules';
import {Subheading, Text, Title, Button, RadioButton} from 'react-native-paper';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  scrollContainer: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  scrollView: {
    alignItems: 'center',
  },
  flexedView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '100%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 0,
  },
});

class SmartCycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '2',
    };
  }
  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Create Schedule"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Would you like to add Smart Cycle?</Title>
          <Subheading>
            Smart Cycle helps prevent runoff by breaking up watering time,
            allowing water to soak into the soil before adding more
          </Subheading>
        </View>
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.scrollView}>
            <Image
              source={require('../../../assets/images/smart-watering/smart-watering.png')}
            />
          </View>
          <View style={{marginTop: 12}}>
            <RadioButton.Group
              value={this.state.selectedValue}
              onValueChange={(value) => {
                this.setState({ selectedValue: value });
                if (value === "1") {
                  /**
                   * Open popup modal to canputre timing
                   */
                }
              }}>
              <RadioButton.Item label="Manual Cycle and Soak" value="1" />
              <RadioButton.Item label="Smart Cycle" value="2" />
              <RadioButton.Item label="No Cycle and Soak" value="3" />
            </RadioButton.Group>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Button
            mode="contained"
            onPress={() => {
              navigation.navigate('SelectName');
            }}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Next
          </Button>
        </View>
      </View>
    );
  }
}

export default SmartCycle;
