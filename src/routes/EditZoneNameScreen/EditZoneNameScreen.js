/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import {logglyServices, userServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import {logEvent, recordError} from '../../analytics';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  box: {
    flex: 2,
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditZoneNameScreen extends Component {
  constructor(props) {
    super(props);
    const {zone} = this.props;
    this.state = {
      animating: false,
      error: '',
      name: zone.name,
      zone,
    };
    this.onSave = this.onSave.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(name) {
    if (name) {
      this.setState({name, error: ''});
      return;
    }
    this.setState({name, error: 'Invalid Name'});
  }

  onSubmit() {
    const {name} = this.state;
    if (!name) {
      this.setState({error: 'Invalid Name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({error: 'Name cannot have special characters'});
    }
  }

  onSave() {
    this.setState({animating: true});
    const {name} = this.state;
    if (!name) {
      this.setState({animating: false, error: 'Invalid Name'});
      return;
    }

    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({
        animating: false,
        error: 'Name cannot have special characters',
      });
      return;
    }

    const {zone} = this.state;
    const {navigation, userData} = this.props;
    logEvent('ZONE_UPDATE', {editedName: name, screen: 'EDIT_ZONE'});
    userServices
      .updateZone({...zone, name: name.replace(/[^\w\s]/gi, '')})
      .then(() => {
        this.setState({animating: false});
        logglyServices.logEvent({user: userData.email, zone}, 'EditZone');
        const {device} = this.props;
        store.dispatch(appActions.fetchZones(device._id, zone._id));
        navigation.goBack();
      })
      .catch((err) => {
        alert('Error', err.errorMessage);
        this.setState({animating: false});
        logEvent('ZONE_UPDATE', {
          error: 'Error while updating zone name',
          screen: 'EDIT_ZONE',
        });
        recordError('Error: Zone Name', err);
      });
  }

  render() {
    const {navigation} = this.props;
    const {animating, error, name} = this.state;
    return (
      <View style={styles.container}>
        <Spinner visible={animating} />

        <Helpers.StatusBar
          isBackNavigable
          title="Edit Zone"
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.box}>
              <Text style={styles.formField}>Zone Name</Text>
              <TextInput
                autoFocus
                maxLength={250}
                numberOfLines={1}
                editable
                returnKeyType="done"
                value={name}
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.onChangeText(text)}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: error
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
          </View>
          {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity onPress={() => this.onSave()} style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zone} = state.zone;
  return {device, userData, zone};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditZoneNameScreen);
