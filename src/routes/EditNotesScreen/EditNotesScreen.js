/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import {Colors, Fonts, Dimension} from '../../constants';
import {logEvent, recordError} from '../../analytics';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  largeContainer: {
    flex: 2,
    flexDirection: 'column',
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditNotesScreen extends Component {
  constructor(props) {
    super(props);
    const {zone} = this.props;
    this.state = {
      error: '',
      notes: zone.notes,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    const {navigation, zone} = this.props;
    const {notes} = this.state;
    logEvent('ZONE_UPDATE', {editedNote: notes, screen: 'EDIT_ZONE'});
    userServices
      .updateZone({...zone, notes})
      .then(() => {
        const {device} = this.props;
        store.dispatch(appActions.fetchZones(device._id, zone._id));
        navigation.goBack();
      })
      .catch((err) => {
        logEvent('ZONE_UPDATE', {
          error: 'Error while updating zone notes',
          screen: 'EDIT_ZONE',
        });
        recordError('Error: Zone Note', err);
      });
  }

  render() {
    const {navigation} = this.props;
    const {error, notes} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Edit Notes"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.largeContainer}>
              <Text style={styles.formField}>Notes</Text>
              <TextInput
                autoFocus
                value={notes}
                maxLength={250}
                multiline
                numberOfLines={6}
                editable
                returnKeyType="done"
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                onSubmitEditing={() => this.onSubmit()}
                onChangeText={(text) => this.setState({notes: text})}
                style={[
                  styles.formFieldValue,
                  {
                    borderBottomColor: error
                      ? Colors.DANGER
                      : Colors.SWIPER_ACTIVE_DOT_COLOR,
                  },
                ]}
              />
            </View>
          </View>
          {error ? <Text style={styles.errorText}>{error}</Text> : null}
        </Helpers.Card>

        <Helpers.Footer>
          <TouchableOpacity
            disabled={error !== ''}
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {zone} = state.zone;
  return {device, zone};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditNotesScreen);
