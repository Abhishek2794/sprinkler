/* eslint-disable no-prototype-builtins */
/* eslint-disable camelcase */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import Permissions, {RESULTS, PERMISSIONS} from 'react-native-permissions';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {config} from '../../helpers/config';
import alert from '../../components/alert';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {appServices} from '../../services/app.services';
import {logEvent, recordError} from '../../analytics';
import {Button, Subheading} from 'react-native-paper';

const styles = StyleSheet.create({
  address: {
    alignItems: 'center',
    height: Dimension.HEIGHT_100,
    justifyContent: 'center',
  },
  addressContainer: {
    borderBottomWidth: 1,
    flexDirection: 'column',
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  addressText: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  box: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  card: {
    marginTop: 10,
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    alignSelf: 'center',
    position: 'relative',
    flexDirection: 'column',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_520,
    paddingTop: Dimension.HEIGHT_40,
    paddingLeft: Dimension.WIDTH_30,
    paddingRight: Dimension.WIDTH_30,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  label: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  map: {
    alignSelf: 'stretch',
    height: Dimension.HEIGHT_445 - Dimension.HEIGHT_45,
    width: Dimension.WIDTH,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class AddAdressScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      coordinates: {latitude: 28.7041, longitude: 77.1025},
      listViewDisplayed: true,
      loading: true,
    };
    this.askLocationPermission = this.askLocationPermission.bind(this);
    this.checkLocationPermission = this.checkLocationPermission.bind(this);
    this.fetchGeoLocation = this.fetchGeoLocation.bind(this);
    this.handleMapRegionChange = this.handleMapRegionChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.promptUserToTurnLocation = this.promptUserToTurnLocation.bind(this);
    this.saveLocation = this.saveLocation.bind(this);
  }

  componentDidMount() {
    this.checkLocationPermission();
  }

  onSubmit() {
    const {address} = this.state;
    if (address === null) {
      alert('Error', 'Location not set');
      return;
    }
    const {navigation} = this.props;
    navigation.navigate('DeviceNumber');
  }

  async checkLocationPermission() {
    const permission =
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_ALWAYS
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    Permissions.check(permission).then((status) => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      switch (status) {
        case RESULTS.UNAVAILABLE:
          this.askLocationPermission();
          this.setState({
            loading: false,
          });
          break;
        case RESULTS.DENIED:
          this.askLocationPermission();
          break;
        case RESULTS.LIMITED:
          this.askLocationPermission();
          break;
        case RESULTS.GRANTED:
          this.fetchGeoLocation();
          break;
        case RESULTS.BLOCKED:
          this.askLocationPermission();
          this.setState({
            loading: false,
          });
          break;
        default:
          this.askLocationPermission();
          break;
      }
    });
  }

  async askLocationPermission() {
    const permission =
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    Permissions.request(permission).then((status) => {
      if (status === RESULTS.GRANTED) {
        this.fetchGeoLocation();
      } else if (status === RESULTS.UNAVAILABLE) {
        alert('Error', 'The feature is not available on this device');
        return;
      } else if (status === RESULTS.DENIED) {
        alert('Error', 'The permission to access the location is denied');
        return;
      } else if (status === RESULTS.BLOCKED) {
        alert('Error', 'The permission is denied and not requestable anymore');
        return;
      } else {
        alert('Error', 'Not able to access device location');
      }
    });
  }

  promptUserToTurnLocation() {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    })
      .then(() => {
        this.fetchGeoLocation();
      })
      .catch((err) => {
        alert(`Error: ${err.message}, Code: ${err.code}`);
      });
  }

  async fetchGeoLocation() {
    Geolocation.getCurrentPosition(
      async (info) => {
        const {controllerData} = this.props;
        const {coords} = info;
        const {latitude, longitude} = coords;
        appServices
          .reverseGeocode({latitude, longitude})
          .then((addresses) => {
            this.setState(
              {
                address: addresses[0].formatted_address,
                coordinates: {latitude, longitude},
                loading: false,
              },
              () => {
                this.locationRef.setAddressText(addresses[0].formatted_address);
              },
            );
            store.dispatch(
              appActions.setControllerData({
                ...controllerData,
                address: {
                  location: {
                    coordinates: [longitude, latitude],
                  },
                  fullAddress: addresses[0].formatted_address,
                  zipCode: parseInt(addresses[0].address.postalCode, 10),
                  locationKey: '',
                },
              }),
            );
          })
          .catch(() => alert('Error', 'Could not fetch Location Data'));
      },
      (error) => {
        Alert.alert(
          error.message,
          'Looks like your GPS is Off, hence not able to fetch your location',
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({loading: false});
              },
            },
          ],
          {cancelable: true},
        );
      },
    );
  }

  async handleMapRegionChange(mapRegion) {
    const {latitude, longitude} = mapRegion;
    appServices.reverseGeocode({latitude, longitude}).then((addresses) => {
      this.setState(
        {
          address: addresses[0].formatted_address,
          coordinates: mapRegion,
          loading: false,
        },
        () => {
          this.locationRef.setAddressText(addresses[0].formatted_address);
        },
      );
      const {controllerData} = this.props;
      store.dispatch(
        appActions.setControllerData({
          ...controllerData,
          address: {
            location: {
              coordinates: [longitude, latitude],
            },
            fullAddress: addresses[0].formatted_address,
            zipCode: parseInt(addresses[0].address.postalCode, 10),
            locationKey: '',
          },
        }),
      );
    });
  }

  async saveLocation(data, details) {
    const {description} = data;
    const {geometry} = details;
    const {location} = geometry;
    const {lat, lng} = location;
    appServices
      .reverseGeocode({latitude: parseFloat(lat), longitude: parseFloat(lng)})
      .then(async (addresses) => {
        await AsyncStorage.setItem(
          '@Sprinkler:postalCode',
          addresses[0].address.postalCode,
        );
        const {controllerData} = this.props;
        store.dispatch(
          appActions.setControllerData({
            ...controllerData,
            address: {
              location: {
                coordinates: [longitude, latitude],
              },
              fullAddress: addresses[0].formatted_address,
              zipCode: parseInt(addresses[0].address.postalCode, 10),
              locationKey: '',
            },
          }),
        );
        logEvent('LOCATION_ADDED', {
          location: description,
          screen: 'LOCATION_ADDED',
        });
        this.setState({
          address: addresses[0].formatted_address,
          coordinates: {latitude: parseFloat(lat), longitude: parseFloat(lng)},
          listViewDisplayed: false,
        });
      })
      .catch((err) => {
        recordError('Error: Add Location', err);
      });
  }

  render() {
    const {navigation} = this.props;
    const {coordinates, listViewDisplayed, loading} = this.state;
    const {latitude, longitude} = coordinates;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle="Address"
          title="Add Device"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.container}>
          <Spinner visible={loading} />
          <View style={styles.address}>
            <GooglePlacesAutocomplete
              autoFocus={false}
              enablePoweredByContainer={false}
              fetchDetails
              GoogleReverseGeocodingQuery={{
                key: config.googleApiKey,
                language: 'en',
              }}
              minLength={3}
              onPress={this.saveLocation}
              placeholder="Type your address"
              query={{
                key: config.googleApiKey,
                language: 'en',
                types: 'geocode',
              }}
              ref={(ref) => {
                this.locationRef = ref;
              }}
              returnKeyType="search"
              styles={{
                textInputContainer: {
                  backgroundColor: '#fff',
                  borderBottomColor: Colors.PRIMARY_COLOR_DARK,
                  borderColor: '#fff',
                  borderTopColor: '#fff',
                  borderWidth: 2,
                  height: Dimension.HEIGHT_50,
                  width: '100%',
                },
              }}
              textInputProps={{
                onFocus: () => this.setState({listViewDisplayed: true}),
              }}
            />
          </View>
          {!loading && (
            <View style={styles.map}>
              <MapView
                provider={PROVIDER_GOOGLE}
                region={{
                  latitude,
                  longitude,
                  latitudeDelta: 0.0022,
                  longitudeDelta: 0.0021,
                }}
                showsMyLocationButton
                style={styles.container}
                onRegionChangeComplete={this.handleMapRegionChange}
              />
              <Icon
                size={32}
                color={'#D50000'}
                name="location-on"
                style={{
                  zIndex: 3,
                  top: '50%',
                  left: '50%',
                  position: 'absolute',
                }}
              />
            </View>
          )}
        </View>
        <Helpers.Footer>
          <Button
            style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
            onPress={() => this.onSubmit()}
            mode="contained">
            <Subheading
              style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
              Confirm Location
            </Subheading>
          </Button>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  return {controllerData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddAdressScreen);
