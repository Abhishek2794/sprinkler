/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, ScrollView, View, Switch} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appActions} from '../../actions/app.actions';
import {Colors, Fonts, Dimension} from '../../constants';
import {logEvent} from '../../analytics';
import {Button, Subheading, Text, TextInput, Caption} from 'react-native-paper';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  errorText: {
    color: Colors.DANGER,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formField2: {
    color: '#9e9e9e',
    fontSize: 15,
    color: '#888',
  },
  formFieldValue: {
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  largeContainer: {
    flex: 2,
    flexDirection: 'column',
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  section: {
    marginTop: 12,
  },
});

class DeviceNameScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: false,
      error: '',
      name: '',
      masterValve: false,
      rainsensor1: false,
      rainsensor2: false,
    };

    AsyncStorage.getItem('@Sprinkler:deviceName').then((value) =>
      this.setState({name: value}),
    );
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(name) {
    if (name) {
      this.setState({disabled: false, error: '', name});
      return;
    }
    this.setState({disabled: true, error: 'Enter Device Name', name});
  }

  onSubmit() {
    const {name, masterValve, rainsensor1, rainsensor2} = this.state;
    if (!name) {
      this.setState({disabled: true, error: 'Enter Device Name'});
      return;
    }
    if (/[-!$%^&*()_+|~=`{}\[\]:\/;<>?,.@#"]/.test(name)) {
      this.setState({
        disabled: true,
        error: 'Name cannot have special characters',
      });
      return;
    }
    this.setState({disabled: true, error: false});
    const {controllerData, navigation} = this.props;
    store.dispatch(
      appActions.setControllerData({
        ...controllerData,
        name,
        masterValve,
        sense1: rainsensor1,
        sense2: rainsensor2,
      }),
    );
    AsyncStorage.setItem('@Sprinkler:deviceName', name);
    logEvent('DEVICE_NAME', {
      screen: 'DEVICE_NAME',
      name,
      masterValve,
      sense1: rainsensor1,
      sense2: rainsensor2,
    });
    navigation.navigate('SelectHomeWifi');
    this.setState({disabled: false});
  }

  render() {
    const {navigation} = this.props;
    const {disabled, error, name} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Add Device"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <ScrollView>
            <View style={styles.formElement}>
              <View style={styles.largeContainer}>
                <TextInput
                  autoFocus
                  value={name}
                  maxLength={250}
                  numberOfLines={1}
                  editable
                  returnKeyType="done"
                  contextMenuHidden
                  selectTextOnFocus={false}
                  keyboardType="ascii-capable"
                  ref={(ref) => {
                    this.editName = ref;
                  }}
                  onSubmitEditing={() => this.onSubmit()}
                  onChangeText={(text) => this.onChangeText(text)}
                  label="Device Name"
                  placeholder="Enter Name"
                  mode="outlined"
                />
              </View>
            </View>
            {error ? <Caption style={styles.errorText}>{error}</Caption> : null}
            <View style={styles.formElement}>
              <View style={[styles.largeContainer, {marginVertical: 12}]}>
                <Subheading>Master Valve</Subheading>
                <Text style={[styles.formField, {marginVertical: 8}]}>
                  Master valve control is available globally for each device,
                  and locally for each zone. When master valve control for the
                  device is toggled off, no zones will be watered. If on,
                  watering is determined by the status of the zone level master
                  valve control.
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.formField2}>Toggle Master Valve</Text>
                  <Switch
                    value={this.state.masterValve}
                    trackColor={Colors.PRIMARY_COLOR_DARK}
                    onValueChange={() => {
                      this.setState({
                        masterValve: !this.state.masterValve,
                      });
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={styles.section}>
              <Subheading>Rain Sensor</Subheading>
              <Text style={[styles.formField, {marginVertical: 8}]}>
                Rain sensors helps in optimize the watering duration by
                analyzing the rain in the area. You can enable the setting if
                your controller is connected with rain sensors
              </Text>
              <View style={styles.formElement}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <Text style={styles.formField2}>Rain Sensor 1</Text>
                  <Switch
                    value={this.state.rainsensor1}
                    trackColor={Colors.PRIMARY_COLOR_DARK}
                    onValueChange={() => {
                      this.setState({
                        rainsensor1: !this.state.rainsensor1,
                      });
                    }}
                  />
                </View>
              </View>
              <View style={styles.formElement}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <Text style={styles.formField2}>Rain Sensor 2</Text>
                  <Switch
                    value={this.state.rainsensor2}
                    trackColor={Colors.PRIMARY_COLOR_DARK}
                    onValueChange={() => {
                      this.setState({
                        rainsensor2: !this.state.rainsensor2,
                      });
                    }}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </Helpers.Card>
        <Helpers.Footer>
          <Button
            style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
            onPress={() => this.onSubmit()}
            disabled={disabled}
            mode="contained">
            <Subheading
              style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
              Select Wifi
            </Subheading>
          </Button>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  return {controllerData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeviceNameScreen);
