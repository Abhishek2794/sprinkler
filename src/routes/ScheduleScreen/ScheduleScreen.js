/* eslint-disable import/no-extraneous-dependencies */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  Modal,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import store from '../../helpers/store';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import NoDevice from '../../components/noDevice';
import {StatusBar} from '../../components/statusBar';
import {appActions} from '../../actions/app.actions';
import {userActions} from '../../actions/user.actions';
import {HelpButton} from '../../components/helpButton';
import {Colors, Dimension, Fonts} from '../../constants';
import MonthlyCalendar from '../../components/monthlyCalendar';
import {logEvent} from '../../analytics';
import {getLogs} from '../HistoryScreen/helper';
import {Title, Subheading, Caption} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';

const styles = StyleSheet.create({
  arrow: {
    width: Dimension.WIDTH_24 / 2,
    height: Dimension.WIDTH_24 / 2,
  },
  block: {
    alignItems: 'center',
    flexDirection: 'column',
    marginBottom: Dimension.HEIGHT_100,
    marginTop: Dimension.HEIGHT_30,
  },
  button: {
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY_COLOR_DARK,
    borderRadius: Dimension.WIDTH_100 / 4,
    bottom: Dimension.HEIGHT_30 / 2,
    elevation: 30,
    height: Dimension.WIDTH_100 / 2,
    justifyContent: 'center',
    position: 'absolute',
    right: Dimension.WIDTH_30 / 2,
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 7},
    shadowRadius: 18,
    width: Dimension.WIDTH_100 / 2,
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.PRIMARY_COLOR_DARK,
  },
  card: {
    borderRadius: 8,
    shadowRadius: 18,
    shadowOpacity: 1,
    flexDirection: 'row',
    marginHorizontal: Dimension.HEIGHT_20,
    marginBottom: Dimension.HEIGHT_20,
    shadowOffset: {width: 0, height: 7},
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  circle: {
    height: Dimension.WIDTH_24,
    width: Dimension.WIDTH_24,
  },
  container: {
    flex: 1,
    position: 'relative',
  },
  editSchedule: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingRight: 5,
  },
  eyeContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimension.WIDTH_48_75,
  },
  hintText: {
    paddingTop: 10,
    color: '#929292',
    paddingBottom: 10,
    fontWeight: '300',
    alignSelf: 'center',
    fontSize: Fonts.FONT_SIZE_20,
  },
  scheduleName: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
    textAlign: 'left',
    marginRight: 8,
  },
  scheduleStatus: {
    textAlign: 'left',
    fontSize: Fonts.FONT_SIZE_14,
  },
  scheduleTime: {
    color: '#929292',
    textAlign: 'left',
    fontSize: Fonts.FONT_SIZE_14,
  },
  toggle: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  modalView: {
    paddingVertical: 16,
    paddingHorizontal: 12,
  },
  modalHeader: {
    paddingVertical: 16,
    paddingHorizontal: 12,
    borderBottomColor: Colors.PRIMARY_COLOR,
    backgroundColor: Colors.PRIMARY_COLOR,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentHolder: {
    marginVertical: 16,
  },
  content: {
    borderColor: '#ddd',
    borderWidth: 1,
    alignItems: 'center',
    marginBottom: 30,
    borderRadius: 4,
    padding: 16,
    backgroundColor: '#f7f7f7',
  },
});

class ScheduleScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      selectSchedule: false,
    };
    this.dailySchedule = this.dailySchedule.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  componentDidMount() {
    const {navigation, device} = this.props;
    this.blurListener = navigation.addListener('blur', () => {
      store.dispatch(userActions.dockView(false));
    });
    this.focusListener = navigation.addListener('focus', async () => {
      getLogs(device);
      const data = await AsyncStorage.getItem('addSchedule');
      if (data) {
        this.setState({
          selectSchedule: true,
        });
        AsyncStorage.removeItem('addSchedule');
      }
      store.dispatch(userActions.dockView(true));
      if (device._id) {
        store.dispatch(appActions.fetchCalendarDates(device._id));
        store.dispatch(appActions.fetchSchedules(device._id));
      }
      logEvent('SCHEDULES_SCREEN', {screen: 'SCHEDULES_SCREEN'});
    });
  }

  componentWillUnmount() {
    this.blurListener();
    this.focusListener();
  }

  onRefresh() {
    const {device} = this.props;
    this.setState({refreshing: true});
    if (device._id) {
      store.dispatch(appActions.fetchSchedules(device._id));
      store.dispatch(appActions.fetchCalendarDates(device._id));
    }
    this.setState({refreshing: false});
  }

  createSchedule(scheduleType) {
    const {schedules, schedule} = this.props;
    if (schedules.length > 16) {
      Alert.alert(
        'Schedule Limit Exceeds',
        'You can create a maximum of 16 schedules',
        [{text: 'OK'}],
      );
    } else {
      const {navigation} = this.props;
      store.dispatch(appActions.createSchedule({...schedule, scheduleType}));
      this.setState({selectSchedule: false});
      logEvent('CREATE_SCHEDULES', {button_click: 'CREATE_SCHEDULES'});
      navigation.navigate('PickTimeslot');
    }
  }

  dailySchedule(timestamp) {
    const {device, navigation} = this.props;
    logEvent('VIEW_SCHEDULE', {screen: 'VIEW_SCHEDULE'});
    store.dispatch(appActions.fetchScheduleTimeTable(device._id, timestamp));
    navigation.navigate({
      name: 'ScheduleOnDate',
      params: {timestamp},
    });
  }

  handleClick(index) {
    const {navigation, schedules} = this.props;
    store.dispatch(appActions.fetchSchedule(schedules[index]));
    navigation.navigate('EditSchedule');
  }

  render() {
    const {allDevices, navigation, schedules} = this.props;
    const {refreshing, selectSchedule} = this.state;
    if (allDevices.length > 0) {
      return (
        <View style={styles.container}>
          <StatusBar
            subtitle=""
            title="Schedules"
            child={HelpButton}
            isBackNavigable={false}
            navigation={navigation}
          />

          <ScrollView
            style={styles.container}
            scrollEventThrottle={32}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={{backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR}}>
              <MonthlyCalendar handleClick={this.dailySchedule} />
            </View>
            <View style={styles.block}>
              {Array.isArray(schedules) ? (
                schedules.map((schedule, index) => {
                  if (schedule.temporary) {
                    return null;
                  } else {
                    let totalTime = 0;
                    schedule.zones.forEach((item) => {
                      totalTime += item.duration;
                    });
                    const calHours = Math.floor(totalTime / 3600);
                    const calMinutes = Math.floor((totalTime % 3600) / 60);
                    const calSeconds = Math.floor((totalTime % 3600) % 60);
                    let timeString = calHours ? `${calHours} H` : '';
                    timeString = calMinutes
                      ? `${timeString} ${calMinutes} M`
                      : '0 M';
                    timeString = calSeconds
                      ? `${timeString} ${calSeconds} S`
                      : `${timeString} 0 S`;
                    return (
                      <View key={schedule._id} style={styles.card}>
                        <View style={styles.eyeContainer}>
                          <Icon
                            name="event"
                            size={36}
                            color={schedule.enabled ? '#1976D2' : '#9E9E9E'}
                          />
                        </View>
                        <TouchableOpacity
                          onPress={() => this.handleClick(index)}
                          style={{
                            flex: 8,
                            margin: 9,
                            flexDirection: 'column',
                          }}>
                          <View style={{flexDirection: 'row'}}>
                            <Text
                              numberOfLines={1}
                              ellipsizeModel="middle"
                              style={styles.scheduleName}>
                              {schedule.name}
                            </Text>
                            <Icon
                              name={
                                schedule.enabled
                                  ? 'play-arrow'
                                  : 'play-disabled'
                              }
                              size={20}
                              color={
                                schedule.enabled
                                  ? Colors.PRIMARY_COLOR_DARK
                                  : Colors.DANGER
                              }
                            />
                          </View>
                          <Text style={styles.scheduleTime}>
                            {`Running for ${schedule.scheduleZoneNames.length} zones`}
                          </Text>
                          {schedule.timeSlot === 0 ? (
                            <Text
                              numberOfLines={1}
                              style={
                                styles.scheduleTime
                              }>{`${schedule.days}, End Before Sunrise `}</Text>
                          ) : null}
                          {schedule.timeSlot === 1 ? (
                            <Text
                              numberOfLines={1}
                              style={styles.scheduleTime}>{`${
                              schedule.days
                            }, End before ${moment(schedule.endTime).format(
                              'hh:mm A',
                            )} `}</Text>
                          ) : null}
                          {schedule.timeSlot === 2 ? (
                            <Text
                              numberOfLines={1}
                              style={
                                styles.scheduleTime
                              }>{`${schedule.days}, Start After Sunset `}</Text>
                          ) : null}
                          {schedule.timeSlot === 3 ? (
                            <Text
                              numberOfLines={1}
                              style={styles.scheduleTime}>{`${
                              schedule.days
                            }, Start at ${moment(schedule.startTime).format(
                              'hh:mm A',
                            )} `}</Text>
                          ) : null}
                          <Text style={styles.scheduleTime}>
                            {`Total Duration: ${timeString}`}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => this.handleClick(index)}
                          style={styles.editSchedule}>
                          <Icon
                            name="keyboard-arrow-right"
                            size={24}
                            color="#929292"
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  }
                })
              ) : (
                <Spinner visible={true} />
              )}
            </View>
          </ScrollView>
          <TouchableOpacity
            onPress={() => this.setState({selectSchedule: true})}
            style={styles.button}>
            <Icon name="add" size={30} color={'#fff'} />
          </TouchableOpacity>
          <Modal
            animationType="slide"
            transparent={false}
            visible={selectSchedule}
            onRequestClose={() => {
              this.setState({selectSchedule: false});
            }}>
            <SafeAreaView>
              <View style={styles.modalHeader}>
                <Icon
                  name="arrow-circle-down"
                  size={30}
                  color="#FFF"
                  onPress={() => this.setState({selectSchedule: false})}
                />
                <Subheading
                  style={{
                    textTransform: 'uppercase',
                    fontSize: 16,
                    color: '#fff',
                    fontWeight: 'bold',
                    letterSpacing: 1,
                  }}>
                  Create Schedule
                </Subheading>
                <Icon name="help-outline" size={30} color="#FFF" />
              </View>
              <View style={styles.modalView}>
                <Title>Which type of schedule would you like to create?</Title>
                <View style={styles.contentHolder}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.content}
                    onPress={() => this.createSchedule('FIXED')}>
                    <CommunityIcon
                      name="calendar-clock"
                      size={80}
                      color="#757575"
                    />
                    <Subheading style={{fontWeight: 'bold'}}>
                      Fixed Schedule
                    </Subheading>
                    <Caption>Water at a fixed time</Caption>
                  </TouchableOpacity>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.content}
                    onPress={() => this.createSchedule('SMART')}>
                    <CommunityIcon
                      name="calendar-star"
                      size={80}
                      color="#757575"
                    />
                    <Subheading style={{fontWeight: 'bold'}}>
                      Smart Schedule
                    </Subheading>
                    <Caption>Requires Sprinkler Plus Subscription</Caption>
                  </TouchableOpacity>
                </View>
              </View>
            </SafeAreaView>
          </Modal>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          child={Helpers.HelpButton}
          isBackNavigable={false}
          navigation={navigation}
          subtitle="No Device Found"
          title="Schedules"
        />
        <NoDevice navigation={navigation} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {allDevices} = state.devices;
  const {device} = state.activeDevice;
  const {schedules} = state.schedules;
  return {allDevices, device, schedules};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleScreen);
