/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';

const styles = StyleSheet.create({
  bullet: {
    color: Colors.PRIMARY_COLOR_DARK,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    color: '#9e9e9e',
    fontWeight: '600',
    fontSize: Fonts.FONT_SIZE_20,
  },
  instructions: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  row: {
    flexDirection: 'row',
    marginTop: Dimension.HEIGHT_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class WifiInstructionScreen extends Component {
  constructor(props) {
    super(props);
    this.handleNavigation = this.handleNavigation.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  handleNavigation() {
    const {navigation} = this.props;
    navigation.navigate('WifiReSetup', {
      onGoBack: () => this.showAlert(),
    });
  }

  showAlert() {
    alert('Error', 'Could not connect to Device');
  }

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Add Device"
          subtitle="Select Wifi"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Text style={styles.header}>
            Connect your Sprinkler in WiFi settings.
          </Text>
          <View style={styles.row}>
            <Text style={[styles.bullet, styles.instructions]}>1. </Text>
            <Text style={styles.instructions}>
              Tap home button, and open your Phone settings and select Wi-Fi
            </Text>
          </View>
          <View style={styles.row}>
            <Text style={[styles.bullet, styles.instructions]}>2. </Text>
            <Text style={styles.instructions}>
              Connect to the Wi-Fi network that matches your Sprinklr. It looks
              something like this below
            </Text>
          </View>
          <Image
            style={{alignSelf: 'center'}}
            source={require('../../../assets/images/wifi/wifi.png')}
          />
          <View style={styles.row}>
            <Text style={[styles.bullet, styles.instructions]}>3. </Text>
            <Text style={styles.instructions}>Come back to Sprinkler app.</Text>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <Helpers.CancelButton navigation={navigation} />
          <TouchableOpacity
            onPress={() => this.handleNavigation()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  return {controllerData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WifiInstructionScreen);
