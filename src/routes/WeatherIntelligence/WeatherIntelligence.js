import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, TouchableOpacity} from 'react-native';
import {Colors} from '../../constants';
import * as Helpers from '../../helpers/modules';
import Icon from 'react-native-vector-icons/Ionicons';
import {Subheading, Text, Title, Button, Caption} from 'react-native-paper';

const styles = StyleSheet.create({
  bodyHeader: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  scrollContainer: {
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  flexedView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  skipItem: {
    paddingVertical: 12,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
  },
  footer: {
    flexDirection: 'row',
  },
  footerBtn: {
    width: '100%',
    height: 60,
    borderColor: Colors.SECONDARY_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 0,
  },
});

class WeatherIntelligence extends Component {
  constructor(props) {
    super(props);
    this.state = {
      skips: [
        {
          name: 'Rain Skip',
          value: true,
          description:
            'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.',
        },
        {
          name: 'Freeze Skip',
          value: true,
          description:
            'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.',
        },
        {
          name: 'Wind Skip',
          value: true,
          description:
            'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.',
        },
        {
          name: 'Saturation Skip',
          value: true,
          description:
            'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.',
        },
        {
          name: 'Seasonal Skip',
          value: true,
          description:
            'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.',
        },
      ],
    };
  }
  render() {
    const {navigation} = this.props;
    const {skips} = this.state;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          title="Create Schedule"
          isBackNavigable={true}
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <View style={styles.bodyHeader}>
          <Title>Add Sprinkler Weather Intelligence</Title>
          <Subheading>
            Sprinkler uses local weather data to keep your schedule from running
            in suboptimal conditions
          </Subheading>
        </View>
        <ScrollView style={styles.scrollContainer}>
          {skips.map((item, index) => {
            return (
              <TouchableOpacity
                activeOpacity={0.5}
                key={index}
                onPress={() => {
                  this.setState({
                    skips: this.state.skips.map((skip) => {
                      if (skip.name === item.name) {
                        return {
                          ...skip,
                          value: !skip.value,
                        };
                      } else {
                        return skip;
                      }
                    }),
                  });
                }}
                style={[
                  styles.flexedView,
                  styles.skipItem,
                  {justifyContent: 'space-between'},
                ]}>
                <View>
                  <View style={[styles.flexedView]}>
                    <Title style={{marginRight: 12}}>{item.name}</Title>
                    <Icon
                      name="help-circle-outline"
                      size={18}
                      color={'#1E88E5'}
                    />
                  </View>
                  <Caption>Recommended</Caption>
                </View>
                <Icon
                  name={item.value ? 'checkmark-sharp' : ''}
                  size={30}
                  color={Colors.SECONDARY_COLOR}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View style={styles.footer}>
          <Button
            mode="contained"
            onPress={() => {
              navigation.navigate('SmartCycle');
            }}
            style={[styles.footerBtn]}
            color={Colors.SECONDARY_COLOR}>
            Next
          </Button>
        </View>
      </View>
    );
  }
}

export default WeatherIntelligence;
