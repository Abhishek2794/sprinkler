/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts} from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingHorizontal: 12,
  },

  formElement: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    paddingTop: Dimension.HEIGHT_20 / 2,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {},
  formLabel: {
    flex: 2,
    flexDirection: 'column',
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

class DeviceInfoScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation, device} = this.props;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          title="Device Info"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <ScrollView style={styles.container}>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Firmware Version</Text>
              <Text style={styles.formFieldValue}>
                {device.currentFirmware ? device.currentFirmware : 'N/A'}
              </Text>
            </View>
          </View>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Hardware Version</Text>
              <Text style={styles.formFieldValue}>
                {device.hardwareVersion ? device.hardwareVersion : 'N/A'}
              </Text>
            </View>
          </View>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Mac Address</Text>
              <Text style={styles.formFieldValue}>{device.mac ? device.mac : 'N/A'}</Text>
            </View>
          </View>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Serial Number</Text>
              <Text style={styles.formFieldValue}>{device.serialNumber}</Text>
            </View>
          </View>
          <View style={styles.formElement}>
            <View style={styles.formLabel}>
              <Text style={styles.formField}>Modal Name</Text>
              <Text style={styles.formFieldValue}>{device.model ? device.model : 'N/A'}</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  return {device};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeviceInfoScreen);
