import React from 'react';
import * as Helpers from '../../helpers/modules';
import {
  Subheading,
  DataTable,
  Caption,
  Switch,
  Snackbar,
} from 'react-native-paper';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {appConstants, Colors} from '../../constants';
import {logEvent, recordError} from '../../analytics';
import Spinner from 'react-native-loading-spinner-overlay';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import store from '../../helpers/store';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';

class NotificationPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      userPreferences: {},
      loading: true,
      error: '',
      toast: '',
    };
  }

  componentDidMount() {
    const {device} = this.props;
    let preference = {};
    try {
      if (device.notifications) {
        preference = device.notifications;
      }
      userServices
        .getNotificationTypes()
        .then((data) => {
          console.log(data);
          this.setState({
            notifications: data,
            loading: false,
            userPreferences: preference,
          });
        })
        .catch((error) => {
          this.setState({
            error: 'Error while fetching notification types ' + error,
            loading: false,
            userPreferences: preference,
          });
        });
    } catch (err) {
      this.setState({
        error: 'Error while fetching notification types ' + err,
        loading: false,
        userPreferences: preference,
      });
    }
  }

  onNotificationUpdate = () => {
    const {userPreferences} = this.state;
    const {device} = this.props;
    const payload = {
      ...device,
      notifications: userPreferences,
    };
    userServices
      .updateDevice(payload)
      .then(() => {
        logEvent('NotificationUpdate', {userPreferences});
        store.dispatch(appActions.updateActiveDeviceState(payload));
        this.setState({
          loading: false,
          toast: 'Notification settings are updated',
        });
      })
      .catch((err) => {
        store.dispatch(appActions.updateActiveDeviceState(device));
        this.setState({
          loading: false,
          error: 'Error while updating notification settings',
        });
        recordError('Error while updating notification settings', err);
      });
  };
  render() {
    const {navigation} = this.props;
    const {notifications, loading, userPreferences} = this.state;
    return (
      <View style={{flex: 1}}>
        <Helpers.StatusBar
          subtitle=""
          isBackNavigable
          child={Helpers.HelpButton}
          title="Notifications"
          navigation={navigation}
        />
        <Spinner visible={loading} />

        <View style={styles.continer}>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>
                <Subheading style={styles.tableHeader}>
                  Notification Type
                </Subheading>
              </DataTable.Title>
              <DataTable.Title numeric>
                <Subheading style={styles.tableHeader}> Status</Subheading>
              </DataTable.Title>
            </DataTable.Header>
            {notifications.map((item, index) => {
              return (
                <DataTable.Row key={index}>
                  <DataTable.Cell>{item}</DataTable.Cell>
                  <DataTable.Cell numeric>
                    <Switch
                      color={Colors.SECONDARY_COLOR}
                      value={userPreferences[item]}
                      onValueChange={() => {
                        this.setState(
                          {
                            userPreferences: {
                              ...userPreferences,
                              [item]: !userPreferences[item],
                            },
                            loading: true,
                          },
                          () => {
                            this.onNotificationUpdate();
                          },
                        );
                      }}
                    />
                  </DataTable.Cell>
                </DataTable.Row>
              );
            })}
          </DataTable>
          <Caption>
            Configure the type of push notifications you want to receive by
            toggling the status
          </Caption>
        </View>
        <Snackbar
          visible={this.state.error || this.state.toast}
          onDismiss={() => this.setState({error: '', toast: ''})}
          style={{backgroundColor: this.state.error ? '#d32f2f' : '#00796B'}}>
          {this.state.error || this.state.toast}
        </Snackbar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  continer: {
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  tableHeader: {
    fontSize: 15,
    color: '#000',
    fontWeight: 'bold',
  },
});

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  return {device, userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationPreference);
