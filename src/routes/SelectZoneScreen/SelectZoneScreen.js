/* eslint-disable no-return-assign */
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {Grayscale} from 'react-native-color-matrix-image-filters';
import Icon from 'react-native-vector-icons/AntDesign';
import alert from '../../components/alert';
import {appServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import store from '../../helpers/store';
import {Colors, Dimension, Fonts, appConstants} from '../../constants';
import {appActions} from '../../actions/app.actions';
import {userServices} from '../../services';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
  },
  durationControl: {
    alignItems: 'center',
    flexDirection: 'column',
    height: Dimension.HEIGHT_130,
    justifyContent: 'space-evenly',
    width: '30%',
  },
  switch: {
    alignSelf: 'flex-start',
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zone: {
    marginBottom: 10,
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    shadowColor: '#000',
    flexDirection: 'row',
    height: Dimension.HEIGHT_130,
    shadowOffset: {width: 0, height: 12},
    backgroundColor: Colors.TEXT_COLOR,
  },
  zoneContainer: {
    height: Dimension.HEIGHT_130,
    left: 0,
    position: 'absolute',
    top: 0,
    width: Dimension.WIDTH_338,
    zIndex: 2,
  },
  zoneContent: {
    flex: 1,
    flexDirection: 'row',
  },
  zoneControl: {
    height: Dimension.HEIGHT_130,
    paddingLeft: Dimension.WIDTH_20,
    paddingTop: Dimension.HEIGHT_20,
    width: '70%',
  },
  zoneDuration: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
  },
  zoneIndex: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_14,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zoneList: {
    marginTop: 10,
    alignSelf: 'center',
    flexDirection: 'column',
  },
  zoneName: {
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class SelectZoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zones: [],
      animating: false,
    };

    this.clearTimer = this.clearTimer.bind(this);
    this.onLongPress = this.onLongPress.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.selectZone = this.selectZone.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.updateZoneDuration = this.updateZoneDuration.bind(this);
  }

  componentDidMount() {
    const {zones} = this.props;
    this.setState({
      zones: zones.map((zone) => {
        return {
          ...zone,
          duration: 15,
        };
      }),
    });
  }

  onLongPress(index, state) {
    switch (state) {
      case appConstants.INCREMENT:
        this.interval = setInterval(
          this.startTimer,
          100,
          index,
          appConstants.INCREMENT,
        );
        break;
      case appConstants.DECREMENT:
        this.interval = setInterval(
          this.startTimer,
          100,
          index,
          appConstants.DECREMENT,
        );
        break;
      default:
        break;
    }
  }

  onSubmit() {
    const {navigation} = this.props;
    const {zones} = this.state;
    if (!zones.some((zone) => zone.selected === true)) {
      alert('ATTENTION', 'Select atleast one zone');
      return;
    }
    const {schedule} = this.props;
    const selectedZones = [];
    zones.forEach((zone) => {
      if (zone.selected) {
        selectedZones.push({
          id: zone._id,
          duration: zone.duration,
        });
      }
    });
    store.dispatch(
      appActions.createSchedule({...schedule, zones: selectedZones}),
    );
    navigation.navigate('SelectInterval');
  }

  selectZone(index, status) {
    const {zones} = this.state;
    zones[index].selected = status;
    this.setState({zones});
  }

  updateZoneDuration(index, zoneOperator) {
    const {zones} = this.state;
    switch (zoneOperator) {
      case appConstants.INCREMENT:
        zones[index].duration += 15;
        break;
      case appConstants.DECREMENT:
        if (zones[index].duration > 15) {
          zones[index].duration -= 15;
        }
        break;
      default:
        break;
    }
    this.setState({zones});
  }

  clearTimer() {
    clearInterval(this.interval);
  }

  startTimer(index, zoneOperator) {
    const {zones} = this.state;
    switch (zoneOperator) {
      case appConstants.INCREMENT:
        zones[index].duration += 15;
        break;
      case appConstants.DECREMENT:
        if (zones[index].duration > 15) {
          zones[index].duration -= 15;
        }
        break;
      default:
        break;
    }
    this.setState({zones});
  }

  zoneEnable = (zone, index) => {
    this.setState({animating: true});
    userServices.updateZone({...zone, enabled: true}).then(() => {
      const {device} = this.props;
      store.dispatch(appActions.fetchZone(device._id, zone.number));
      this.setState({animating: false});
      this.selectZone(index, true);
    });
  };

  toggleSwitch(zone, index) {
    if (zone.selected === true) {
      this.selectZone(index, false);
      return;
    }
    if (zone.enabled) {
      this.selectZone(index, true);
    } else {
      Alert.alert(
        'Confirmation',
        'Are you sure you want to enable this zone?',
        [
          {
            text: 'No',
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => this.zoneEnable(zone, index),
          },
        ],
      );
    }
  }

  getZoneImage = (zoneId) => {
    const {zones} = this.props;
    const zone = zones.find((item) => item._id === parseInt(zoneId));
    if (zone) return zone.defaultImage;
  };

  render() {
    const {zones, animating} = this.state;
    const {navigation} = this.props;
    if (zones) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            isBackNavigable
            title="Create Schedule"
            subtitle="Select zones"
            child={Helpers.HelpButton}
            navigation={navigation}
          />
          <ScrollView style={{flex: 1, marginBottom: 80}}>
            {animating ? <ActivityIndicator animating={animating} /> : null}
            <View style={styles.zoneList}>
              {zones.map((zone, index) => {
                const zoneImage = this.getZoneImage(zone._id);
                return (
                  <View key={zone.number} style={styles.zone}>
                    <View style={styles.zoneContainer}>
                      <View style={{flexDirection: 'row'}}>
                        <View style={styles.zoneControl}>
                          <Switch
                            style={styles.switch}
                            value={zone.selected}
                            onValueChange={() => this.toggleSwitch(zone, index)}
                            trackColor={Colors.PRIMARY_COLOR_DARK}
                          />
                          <Text style={styles.zoneName} numberOfLines={1}>
                            {zone.name}
                          </Text>
                          <Text style={styles.zoneIndex}>
                            {zone.number < 10
                              ? `Zone 0${zone.number}`
                              : `Zone ${zone.number}`}
                          </Text>
                        </View>
                        {/* {zone.selected ? (
                          <View style={styles.durationControl}>
                            <TouchableWithoutFeedback
                              onPressOut={() => this.clearTimer()}
                              onPressIn={() =>
                                this.onLongPress(index, appConstants.INCREMENT)
                              }
                              onPress={() =>
                                this.updateZoneDuration(
                                  index,
                                  appConstants.INCREMENT,
                                )
                              }>
                              <Icon
                                name="pluscircle"
                                size={30}
                                color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                              />
                            </TouchableWithoutFeedback>
                            <Text style={styles.zoneDuration}>{`${Math.floor(
                              zone.duration / 60,
                            )}:${
                              zone.duration % 60 === 0
                                ? '00'
                                : zone.duration % 60
                            }`}</Text>
                            <TouchableWithoutFeedback
                              onPressOut={() => this.clearTimer()}
                              onPressIn={() =>
                                this.onLongPress(index, appConstants.DECREMENT)
                              }
                              onPress={() =>
                                this.updateZoneDuration(
                                  index,
                                  appConstants.DECREMENT,
                                )
                              }>
                              <Icon
                                name="minuscircle"
                                size={30}
                                color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                              />
                            </TouchableWithoutFeedback>
                          </View>
                        ) : null} */}
                      </View>
                    </View>
                    <Grayscale key={zone.number} amount={zone.selected ? 0 : 1}>
                      <Image
                        resizeMode="cover"
                        style={{
                          width: Dimension.WIDTH_338,
                          height: Dimension.HEIGHT_130,
                        }}
                        source={
                          zoneImage
                            ? {uri: zoneImage}
                            : require('../../../assets/images/zone-default/zone-default.png')
                        }
                      />
                    </Grayscale>
                  </View>
                );
              })}
            </View>
          </ScrollView>
          <Helpers.Footer>
            <TouchableOpacity
              onPress={() => this.onSubmit()}
              style={styles.button}>
              <Text style={styles.text}>Next</Text>
            </TouchableOpacity>
          </Helpers.Footer>
        </View>
      );
    }
    return null;
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {zones} = state.zones;
  const {schedule} = state.scheduleCreate;
  return {device, schedule, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SelectZoneScreen);
