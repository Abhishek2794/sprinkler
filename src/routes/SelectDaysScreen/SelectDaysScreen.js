import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {appActions} from '../../actions/app.actions';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts, Strings} from '../../constants';
import store from '../../helpers/store';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  day: {
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    justifyContent: 'space-between',
    marginLeft: Dimension.WIDTH_36,
    marginRight: Dimension.WIDTH_36,
    paddingTop: Dimension.HEIGHT_20,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class SelectDaysScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: [],
    };
    this.selectedDays = new Set();
    this.selectedDays.add('Sunday');
    this.onSubmit = this.onSubmit.bind(this);
    this.handlePress = this.handlePress.bind(this);
  }

  componentDidMount() {
    const days = Strings.WEEK_DAYS.map((day) => ({name: day, selected: false}));
    days[0].selected = true;
    this.setState({days});
  }

  onSubmit() {
    if (this.selectedDays.size === 0) {
      alert('ATTENTION', 'Select atleast one day');
      return;
    }

    const {navigation, schedule} = this.props;
    store.dispatch(
      appActions.createSchedule({
        ...schedule,
        days: [...this.selectedDays].join(','),
      }),
    );
    navigation.navigate('SelectPeriod');
  }

  handlePress(index) {
    const {days} = this.state;
    if (days[index].selected === false) {
      days[index].selected = true;
      this.selectedDays.add(days[index].name);
      this.setState({days});
      return;
    }
    days[index].selected = false;
    this.selectedDays.delete(days[index].name);
    this.setState({days});
  }

  render() {
    const {navigation} = this.props;
    const {days} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Select Days"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <View style={{width: '100%'}}>
            {days.map((day, index) => (
              <TouchableOpacity
                key={day.name}
                onPress={() => this.handlePress(index)}>
                <View style={styles.listItem}>
                  <Text style={styles.day}>{day.name}</Text>
                  {day.selected && (
                    <Icon name="done" color={Colors.PRIMARY_COLOR} size={24} />
                  )}
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {schedule} = state.scheduleCreate;
  return {schedule};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SelectDaysScreen);
