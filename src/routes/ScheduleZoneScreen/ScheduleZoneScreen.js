/* eslint-disable prefer-const */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import {appActions} from '../../actions/app.actions';
import {appServices, userServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts, appConstants} from '../../constants';
import {generateCalenderPayload} from '../../helpers/create-calender';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
  },
  durationControl: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  zone: {
    elevation: 24,
    marginBottom: 30,
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    shadowColor: '#000',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_130,
    shadowOffset: {width: 0, height: 12},
    backgroundColor: Colors.TEXT_COLOR,
  },
  zoneContent: {
    flex: 1,
    flexDirection: 'row',
  },
  zoneControl: {
    flex: 4,
    paddingLeft: Dimension.WIDTH_20,
    paddingTop: Dimension.HEIGHT_20,
  },
  zoneDuration: {
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
  },
  zoneIndex: {
    fontWeight: '300',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_15,
  },
  zoneList: {
    marginTop: 10,
    alignSelf: 'center',
    flexDirection: 'column',
  },
  zoneName: {
    fontWeight: '500',
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    fontSize: Fonts.FONT_SIZE_20,
    marginTop: Dimension.HEIGHT_20 / 2,
  },
});

class ScheduleZoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedZones: [],
      tapped: false,
      zones: [],
    };
    this.clearTimer = this.clearTimer.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.selectZone = this.selectZone.bind(this);
    this.updateZoneDuration = this.updateZoneDuration.bind(this);
  }

  componentDidMount() {
    const {scheduleData} = this.props;
    const zones = this.props.zones.map((item) => {
      const selectedZone = scheduleData.zones.find(
        (ele) => ele.id === item._id,
      );
      if (selectedZone) {
        return {
          ...item,
          selected: true,
          duration: selectedZone.duration,
        };
      } else {
        return {...item};
      }
    });
    this.setState({zones});
  }

  onLongPress(index, state) {
    switch (state) {
      case appConstants.INCREMENT:
        this.interval = setInterval(
          this.startTimer,
          100,
          index,
          appConstants.INCREMENT,
        );
        break;
      case appConstants.DECREMENT:
        this.interval = setInterval(
          this.startTimer,
          100,
          index,
          appConstants.DECREMENT,
        );
        break;
      default:
        break;
    }
  }

  onSubmit() {
    this.setState({loading: true, tapped: true});
    let {zones} = this.state;
    const {scheduleData, device} = this.props;
    const updatedZones = [];
    zones.forEach(item => {
      if(item.selected) {
        updatedZones.push({id: item._id, duration: item.duration});
      }
    })
    const obj = {
      ...scheduleData,
      zones: updatedZones
    };
    userServices
      .updateSchedule(device._id, obj)
      .then((updatedSchedule) => {
        const {device, navigation} = this.props;
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(appActions.fetchSchedule(updatedSchedule));
        setTimeout(() => {
          console.log('Edit Schedule');
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                this.setState({loading: false});
                store.dispatch(appActions.fetchCalendarDates(id));
              });
          }
          navigation.goBack();
        }, 2000);
      })
      .catch((err) => {
        this.setState({loading: false, tapped: false});
        console.log(err);
      });
  }

  clearTimer() {
    clearInterval(this.interval);
  }

  selectZone(index, status) {
    let {zones} = this.state;
    zones = zones.map((item, i) => {
      if (index === i) {
        return {
          ...item,
          selected: status,
          duration: 30,
        };
      } else {
        return {
          ...item,
        };
      }
    });
    this.setState({zones});
  }

  updateZoneDuration(index, zoneOperator) {
    const {zones} = this.state;
    switch (zoneOperator) {
      case appConstants.INCREMENT:
        if (!zones[index].duration) {
          zones[index].duration = 15;
        }
        zones[index].duration += 15;
        break;
      case appConstants.DECREMENT:
        if (zones[index].duration > 15) {
          zones[index].duration -= 15;
        }
        break;
      default:
        break;
    }

    this.setState({zones});
  }

  startTimer(index, zoneOperator) {
    const {zones} = this.state;
    switch (zoneOperator) {
      case appConstants.INCREMENT:
        zones[index].duration += 15;
        break;
      case appConstants.DECREMENT:
        if (zones[index].duration > 15) {
          zones[index].duration -= 15;
        }
        break;
      default:
        break;
    }

    this.setState({zones});
  }

  getZoneImage = (zoneId) => {
    const {zones} = this.props;
    const zone = zones.find((item) => item._id === parseInt(zoneId));
    return zone.defaultImage;
  };

  render() {
    const {navigation} = this.props;
    const {loading, tapped, zones} = this.state;
    if (zones) {
      return (
        <View style={styles.container}>
          <Helpers.StatusBar
            title="Edit Schedule"
            isBackNavigable
            subtitle="Select zones"
            child={Helpers.HelpButton}
            navigation={navigation}
          />
          <Spinner visible={loading} />
          <ScrollView>
            <View style={styles.zoneList}>
              {zones.map((zone, index) => {
                const zoneImage = this.getZoneImage(zone._id);
                return (
                  <View key={zone.number} style={styles.zone}>
                    <ImageBackground
                      resizeMode="contain"
                      style={[
                        styles.container,
                        {
                          width: Dimension.WIDTH_338,
                          height: Dimension.HEIGHT_130,
                        },
                      ]}
                      source={
                        zoneImage
                          ? {uri: zoneImage}
                          : require('../../../assets/images/zone-default/zone-default.png')
                      }>
                      <LinearGradient
                        style={styles.container}
                        locations={[0.2, 1.0]}
                        colors={['rgba(0,0,0,0.0)', 'rgb(0,0,0)']}>
                        <View style={styles.zoneContent}>
                          <View style={styles.zoneControl}>
                            <View style={{width: Dimension.WIDTH_63}}>
                              <Switch
                                value={zone.selected}
                                onValueChange={() => {
                                  if (zone.selected === true) {
                                    this.selectZone(index, false);
                                  } else {
                                    this.selectZone(index, true);
                                  }
                                }}
                                trackColor={Colors.PRIMARY_COLOR_DARK}
                              />
                            </View>
                            <Text style={styles.zoneName}>{zone.name}</Text>
                            <Text style={styles.zoneIndex}>
                              {zone.number < 10
                                ? `Zone 0${zone.number}`
                                : `Zone ${zone.number}`}
                            </Text>
                          </View>
                          {zone.selected ? (
                            <View style={styles.durationControl}>
                              <TouchableWithoutFeedback
                                onPressOut={() => this.clearTimer()}
                                onPressIn={() =>
                                  this.onLongPress(
                                    index,
                                    appConstants.INCREMENT,
                                  )
                                }
                                onPress={() =>
                                  this.updateZoneDuration(
                                    index,
                                    appConstants.INCREMENT,
                                  )
                                }>
                                <Icon
                                  name="add-circle"
                                  size={34}
                                  color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                                />
                              </TouchableWithoutFeedback>
                              <Text style={styles.zoneDuration}>{`${Math.floor(
                                zone.duration / 60,
                              )}:${
                                zone.duration % 60 === 0
                                  ? '00'
                                  : zone.duration % 60
                              }`}</Text>
                              <TouchableWithoutFeedback
                                onPressOut={() => this.clearTimer()}
                                onPressIn={() =>
                                  this.onLongPress(
                                    index,
                                    appConstants.DECREMENT,
                                  )
                                }
                                onPress={() =>
                                  this.updateZoneDuration(
                                    index,
                                    appConstants.DECREMENT,
                                  )
                                }>
                                <Icon
                                  name="remove-circle"
                                  size={34}
                                  color={Colors.SWIPER_ACTIVE_DOT_COLOR}
                                />
                              </TouchableWithoutFeedback>
                            </View>
                          ) : null}
                        </View>
                      </LinearGradient>
                    </ImageBackground>
                  </View>
                );
              })}
            </View>
          </ScrollView>
          <Helpers.Footer>
            <TouchableOpacity
              disabled={tapped}
              onPress={() => this.onSubmit()}
              style={styles.button}>
              <Text style={styles.text}>Save</Text>
            </TouchableOpacity>
          </Helpers.Footer>
        </View>
      );
    }
    return <Spinner visible={true} />;
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleData} = state.schedule;
  const {zones} = state.zones;
  return {device, scheduleData, zones};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleZoneScreen);
