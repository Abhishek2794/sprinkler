/* eslint-disable spaced-comment */
/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {StyleSheet, View, Platform, Alert} from 'react-native';
import {appServices} from '../../services';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts} from '../../constants';
import WifiManager from 'react-native-wifi-reborn';
import {logEvent, recordError} from '../../analytics';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ProgressBar, Text, Subheading, Caption} from 'react-native-paper';

const styles = StyleSheet.create({
  bullet: {
    color: Colors.PRIMARY_COLOR_DARK,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    color: '#9e9e9e',
    fontWeight: '600',
    fontSize: Fonts.FONT_SIZE_20,
  },
  instructions: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  message: {
    alignSelf: 'center',
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_18,
    marginTop: Dimension.HEIGHT_20,
  },
  row: {
    flexDirection: 'row',
    marginTop: Dimension.HEIGHT_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  progress: {
    marginTop: 12,
  },
  image: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  sprinklerImage: {
    resizeMode: 'contain',
    flex: 0.3,
    aspectRatio: 1,
  },
});

const STEPS = [
  'Setting Up Network',
  'Getting response from controller',
  'Network success successful',
  'Registering controller',
  'Waiting for the response',
  'Setup Completed',
];

class SetupControllerScreen extends Component {
  isTimeout = false;
  constructor(props) {
    super(props);
    const {controllerData} = this.props;
    const {serialNumber} = controllerData;
    const wifiName = `Sprinkler${serialNumber.split('AMEBAA')[1]}`;
    this.state = {
      message: 0,
      retries: 0,
      wifiName,
      progress: 0.2,
    };
    this.checkDeviceStatus = this.checkDeviceStatus.bind(this);
    this.connectToController = this.connectToController.bind(this);
    this.setupController = this.setupController.bind(this);
  }

  componentDidMount() {
    this.isTimeout = true;
    this.connectToController();
  }

  checkTimeout = () => {
    setTimeout(() => {
      if (this.isTimeout) {
        Alert.alert(
          'Timeout Error',
          'App has not received any message from the server. Please troubleshoot if you keep receiving this error',
          [
            {
              text: 'TroubleShoot',
              onPress: async() => {
                if (Platform.OS === 'android') {
                  await WifiManager.forceWifiUsageWithOptions(false, {
                    noInternet: false,
                  });
                }
                this.props.navigation.navigate('TroubleShoot');
              },
            },
            {text: 'Retry', onPress: () => this.componentDidMount()},
          ],
        );
      }
    }, 24000);
  };

  setupController() {
    logEvent('SETUP_CONTROLLER', {screen: 'SETUP_CONTROLLER'});
    const {controllerData, navigation, userData} = this.props; // Getting response from controller
    const payload = {...controllerData, userId: userData._id};
    this.setState({message: 1, progress: 0.4});
    appServices
      .setControllerNetwork(payload)
      .then(async () => {
        this.isTimeout = false;
        const {serialNumber} = controllerData; // Network setup success
        this.setState({message: 2, progress: 0.6});
        if (Platform.OS === 'android') {
          await WifiManager.forceWifiUsageWithOptions(false, {
            noInternet: false,
          });
        }
        // Registering controller
        setTimeout(() => {
          this.setState({message: 3, progress: 0.8});
        }, 4000);
        setTimeout(() => {
          this.checkDeviceStatus(serialNumber);
        }, 30000);
      })
      .catch(async(err) => {
        this.isTimeout = false;
        if (Platform.OS === 'android') {
          await WifiManager.forceWifiUsageWithOptions(false, {
            noInternet: false,
          });
        }
        recordError('Error: Setup Controller', err);
        logEvent('ONBOARDING_FAILURE', {
          screen: 'ONBOARDING_FAILURE',
          reason: err,
        });
        navigation.navigate('DeviceNumberError');
      });
  }

  checkDeviceStatus(serialNumber) {
    const {navigation} = this.props;
    logEvent('CHECKING_DEVICE_STATUS', {screen: 'CHECKING_DEVICE_STATUS'});
    // Waiting for the response
    this.setState({message: 4, progress: 0.95});
    appServices
      .checkRegistrationStatus(serialNumber)
      .then((data) => {
        if (data._id) {
          this.setState({message: 5, progress: 1});
          logEvent('ONBOARDING_SUCCESS', {screen: 'ONBOARDING_SUCCESS'});
          navigation.navigate('SuccessMessage');
          return;
        }
        logEvent('ONBOARDING_FAILURE', {
          screen: 'ONBOARDING_FAILURE',
          error: 'Device not registered',
        });
        navigation.navigate('RegistrationFailure');
      })
      .catch((err) => {
        console.log(err);
      });
  }

  connectToController() {
    const {retries} = this.state;
    const {navigation} = this.props;
    if (retries === 2) {
      navigation.navigate('TroubleShoot');
      return;
    }
    this.checkTimeout();
    this.setupController(); // Setting Up Network
  }

  componentWillUnmount() {
    this.isTimeout = false;
  }

  render() {
    const {message} = this.state;
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Setup Controller"
          subtitle="Setup Controller"
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Subheading style={{textAlign: 'center'}}>
            Configuring controller and connecting with your account
          </Subheading>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name="warning" size={14} color="#FF6F00" />
            <Caption style={{marginLeft: 8}}>
              Please do not close the app
            </Caption>
          </View>
          <View style={{flex: 1, width: '100%'}}>
            <LottieView
              source={require('../../../assets/animation/Setup.json')}
              autoPlay
              loop
            />
          </View>
          <View>
            <Subheading
              style={{
                textTransform: 'uppercase',
                letterSpacing: 1.5,
                fontSize: 12,
                fontWeight: 'bold',
              }}>
              Setup Steps
            </Subheading>
            {STEPS.map((item, index) => {
              return (
                <View
                  style={{flexDirection: 'row', alignItems: 'center'}}
                  key={item}>
                  {index <= this.state.message ? (
                    <Icon
                      name="check-box"
                      color={Colors.PRIMARY_COLOR_DARK}
                      size={24}
                    />
                  ) : (
                    <Icon
                      name="check-box-outline-blank"
                      color={Colors.PRIMARY_COLOR}
                      size={24}
                    />
                  )}
                  <Text style={{marginTop: 4, marginLeft: 8}}>{item}</Text>
                </View>
              );
            })}
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
            }}>
            <View>
              <ProgressBar progress={this.state.progress} />
            </View>
            <Subheading style={{textAlign: 'center', marginTop: 8}}>
              Progress: {this.state.progress * 100}%
            </Subheading>
          </View>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {userData} = state.user;
  const {controllerData} = state.controller;
  const {status} = state.controllerRegistration;
  return {status, controllerData, userData};
};

export default connect(mapStateToProps, null)(SetupControllerScreen);
