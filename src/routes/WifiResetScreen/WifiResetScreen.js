import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Colors, Fonts, Dimension} from '../../constants';
import * as Helpers from '../../helpers/modules';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    color: '#9e9e9e',
    fontWeight: '600',
    fontSize: Fonts.FONT_SIZE_20,
  },
  instructions: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  resetImage: {
    alignSelf: 'center',
    height: Dimension.WIDTH_135,
    marginTop: Dimension.HEIGHT_20,
    width: Dimension.WIDTH_135,
  },
  row: {
    flexDirection: 'row',
    marginTop: Dimension.HEIGHT_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

export default class WifiResetScreen extends Component {
  constructor(props) {
    super(props);

    this.handleNavigation = this.handleNavigation.bind(this);
  }

  handleNavigation() {
    const {navigation} = this.props;
    navigation.navigate('NewHomeWifi');
  }

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Reset Wifi"
          subtitle="Reset Instructions"
          isBackNavigable
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <Text style={styles.header}>
            In order to reset Wifi, you need to reset the controller.
          </Text>
          <View style={styles.row}>
            <Text style={styles.instructions}>
              With the device powered on, hold the reset button for 10 seconds
            </Text>
          </View>
          <Image
            style={styles.resetImage}
            source={require('../../../assets/images/reset/reset.png')}
          />
        </Helpers.Card>
        <Helpers.Footer>
          <TouchableOpacity
            onPress={() => this.handleNavigation()}
            style={styles.button}>
            <Text style={styles.text}>Next</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}
