import moment from 'moment';
import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';
import {Text, Subheading, Caption, Button, TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {getImageViaKey, deleteImage} from '../../helpers/s3';
import Video from 'react-native-video';

const styles = StyleSheet.create({
  container: {},
  header: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 12,
    borderBottomWidth: 1.2,
    borderBottomColor: '#dadada',
  },
  list: {
    marginTop: 16,
  },
  image: {height: 60, width: 60, borderRadius: 8},
  listItem: {
    flexDirection: 'row',
    padding: 12,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 0.8,
    borderBottomColor: '#dadada',
  },
  bgImage: {
    height: 180,
  },
  imageInfo: {
    padding: 12,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 8,
    alignItems: 'center',
  },
  label: {
    minWidth: 90,
    textTransform: 'uppercase',
    opacity: 0.65,
  },
  videoPlayer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * (9 / 16),
    backgroundColor: 'black',
  },
});

const ZoneMultimedia = ({zone, onClose, onUpdate}) => {
  const [images, setImages] = useState([]);
  const [selectedImage, selectImage] = useState(null);
  const [imgName, updateName] = useState(null);
  useEffect(() => {
    (async () => {
      let zoneImage = [];
      zoneImage = zone.imageUrl.map(async (item) => {
        return await getImageViaKey(item.key);
      });
      zoneImage = await Promise.all(zoneImage);
      zoneImage = zoneImage.map((url, index) => {
        return {
          ...zone.imageUrl[index],
          url,
        };
      });
      setImages(zoneImage);
    })();
  }, []);
  const listItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() => selectImage(item)}>
        <Image
          style={styles.image}
          source={{
            uri: item.type.includes('video')
              ? 'https://cdn2.iconfinder.com/data/icons/social-media-2285/512/1_Youtube_colored_svg-512.png'
              : item.url,
          }}
        />
        <View>
          <Text>{item.title}</Text>
          <Caption>
            {moment(item.createdAt).format('Do MMM YYYY, hh:mm A')}
          </Caption>
        </View>
        <Icon
          name="star"
          size={24}
          color={item.isDefault ? '#1E88E5' : '#fff'}
        />
        <TouchableOpacity>
          <Icon name="chevron-right" size={24} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <View name="header" style={styles.header}>
        <TouchableOpacity
          onPress={() => {
            if (selectedImage) {
              selectImage(null);
            } else {
              onClose();
            }
          }}>
          <Icon name="arrow-back" size={24} />
        </TouchableOpacity>
        <Subheading>{zone.name} Multimedia</Subheading>
        <TouchableOpacity onPress={() => onClose()}>
          <Icon name="close" size={24} />
        </TouchableOpacity>
      </View>
      <View name="list">
        {selectedImage ? (
          <View>
            {selectedImage.type.includes('video') ? (
              <Video
                source={{uri: selectedImage.url}} // Can be a URL or a local file.
                // Store reference
                style={styles.videoPlayer}
                controls={true}
                resizeMode={'cover'}
                onError={(e) => console.log('Error while running video', e)} // Callback when video cannot be loaded
              />
            ) : (
              <Image source={{uri: selectedImage.url}} style={styles.bgImage} />
            )}
            <View style={styles.imageInfo}>
              <View style={styles.row}>
                <Text style={styles.label}>Title:</Text>
                {imgName !== null ? (
                  <View style={styles.row}>
                    <TextInput
                      value={imgName}
                      placeholder="Enter Image Name"
                      onChangeText={(text) => updateName(text)}
                    />
                    <Button
                      onPress={() => {
                        selectImage({...selectedImage, title: imgName});
                        const imgs = images.map((item) => {
                          if (item.key === selectedImage.key) {
                            return {
                              ...item,
                              title: imgName,
                            };
                          } else {
                            return {...item};
                          }
                        });
                        setImages(imgs);
                        const zoneImages = zone.imageUrl.map((item) => {
                          if (item.key === selectedImage.key) {
                            return {
                              ...item,
                              title: imgName,
                            };
                          } else {
                            return {...item};
                          }
                        });
                        onUpdate(zoneImages);
                        updateName(null);
                      }}>
                      Update
                    </Button>
                  </View>
                ) : (
                  <View style={styles.row}>
                    <Text>{selectedImage.title}</Text>
                    <TouchableOpacity
                      onPress={() => updateName(selectedImage.title)}>
                      <Icon
                        name="edit"
                        size={18}
                        style={{marginLeft: 16}}
                        color="#1976D2"
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
              <View style={styles.row}>
                <Text style={styles.label}>Added:</Text>
                <Text>
                  {moment(selectedImage.createdAt).format(
                    'Do MMM YYYY, hh:mm A',
                  )}
                </Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.label}>Default:</Text>
                <Button
                  icon={'star'}
                  mode="contained"
                  onPress={() => {
                    const updatedMeta = zone.imageUrl.map((item) => {
                      if (item.key === selectedImage.key) {
                        return {
                          ...item,
                          isDefault: true,
                        };
                      } else {
                        return {
                          ...item,
                          isDefault: false,
                        };
                      }
                    });
                    onUpdate(updatedMeta);
                    const zoneImgs = images.map((item) => {
                      if (item.key === selectedImage.key) {
                        return {
                          ...item,
                          isDefault: true,
                        };
                      } else {
                        return {
                          ...item,
                          isDefault: false,
                        };
                      }
                    });
                    selectImage({...selectedImage, isDefault: true});
                    setImages(zoneImgs);
                  }}
                  disabled={
                    selectedImage.isDefault ||
                    selectedImage.type.includes('video')
                  }>
                  Make Default
                </Button>
              </View>
              <View style={styles.row}>
                <Text style={styles.label}>Remove:</Text>
                <Button
                  icon={'delete'}
                  mode="contained"
                  onPress={async () => {
                    const response = await deleteImage(selectedImage.key);
                    console.log(response);
                    if (response) {
                      let updatedMeta = zone.imageUrl.filter(
                        (item) => item.key !== selectedImage.key,
                      );
                      if (updatedMeta.length) {
                        updatedMeta[0].isDefault = true;
                      }
                      onUpdate(updatedMeta);
                      updatedMeta = updatedMeta.map((item, index) => {
                        return {
                          ...item,
                          url: images[index].url,
                        };
                      });
                      setImages(updatedMeta);
                      selectImage(null);
                    }
                  }}>
                  Delete
                </Button>
              </View>
            </View>
          </View>
        ) : (
          <FlatList
            data={images}
            keyExtractor={(item) => item.key}
            renderItem={listItem}
          />
        )}
      </View>
    </View>
  );
};

export default ZoneMultimedia;
