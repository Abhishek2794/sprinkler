import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ActionCreators from '../../actions';
import alert from '../../components/alert';
import Cropper from '../../components/cropper';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import {Colors, Dimension} from '../../constants';
import {userServices} from '../../services';
import {logEvent, recordError} from '../../analytics';
import {PutObjectCommand} from '@aws-sdk/client-s3';
import {s3Client, s3Credentials} from '../../helpers/s3';
import {Chance} from 'chance';
import ImageResizer from 'react-native-image-resizer';
import Video from 'react-native-video';
import {Button} from 'react-native-paper';
import {StatusBar} from '../../components/statusBar';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.TEXT_COLOR,
    flex: 1,
    height: Dimension.HEIGHT,
    width: Dimension.WIDTH,
  },
  videoContainer: {
    backgroundColor: '#999',
    height: Dimensions.get('window').height,
  },
  statsContainer: {
    padding: 24,
  },
  statsHeader: {
    fontSize: 22,
    color: '#fff',
    marginBottom: 12,
    fontWeight: '600',
  },
  statsInfo: {
    fontSize: 16,
    color: '#fff',
    marginBottom: 6,
  },
  videoPlayer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * (9 / 16),
    backgroundColor: 'black',
  },
  uploadBtn: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 80,
    justifyContent: 'center',
    borderColor: Colors.SECONDARY_COLOR,
  },
});

class CropImageScreen extends Component {
  constructor(props) {
    super(props);
    const {route} = this.props;
    const image = route.params['image'];
    const video = route.params['video'];
    this.state = {
      showImageLoader: false,
      image,
      video,
    };
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onCancel() {
    const {navigation} = this.props;
    navigation.goBack();
  }
  async onSubmit(croppedImage) {
    this.setState({showImageLoader: true});
    const {device, zone} = this.props;
    try {
      let data;
      const isVideo = croppedImage?.type?.includes('video') || false;
      if (isVideo) {
        const videoSize = parseFloat(
          croppedImage.fileSize / (1024 * 1024),
        ).toFixed(2);
        if (videoSize > 5) {
          alert('Upload Denied: Video with size less than 5mb are accepted!');
          this.setState({showImageLoader: false});
          return;
        }
      }
      if (!isVideo) {
        const resizedImg = await ImageResizer.createResizedImage(
          croppedImage.path,
          croppedImage.width,
          croppedImage.height,
          'JPEG',
          30,
          0,
        );
        data = {
          fileName: resizedImg.name,
          uri: resizedImg.uri,
          type: this.state.image.type || 'image/jpeg',
          fileSize: resizedImg.size,
          height: resizedImg.height,
          width: resizedImg.width,
        };
      } else {
        data = croppedImage;
      }
      console.log('Data', data);
      let bucketParams = {
        Bucket: s3Credentials.bucket,
        Key: `${device.serialNumber}-${zone._id}-${Chance().string({
          length: 6,
          alpha: true,
        })}`,
        Body: data,
      };
      if (isVideo) {
        bucketParams['ContentType'] = croppedImage.type;
      }
      await s3Client.send(new PutObjectCommand(bucketParams));
      const payload = {
        title: isVideo ? 'Zone Video' : 'Zone Image',
        createdAt: new Date().getTime(),
        key: bucketParams.Key,
        isDefault: zone.imageUrl.length || isVideo ? false : true,
        type: data.type,
      };
      const images = zone.imageUrl.concat(payload);
      userServices
        .updateZone({...zone, imageUrl: images})
        .then(() => {
          logEvent('ZONE_UPDATE', {imageUpdate: true, screen: 'EDIT_ZONE'});
          store.dispatch(appActions.fetchZones(device._id, zone._id));
          this.props.navigation.goBack();
        })
        .catch((error) => {
          this.setState({showImageLoader: false});
          alert('Error', error);
          recordError('Error: Zone Image Upload', error);
        });
    } catch (err) {
      this.setState({showImageLoader: false});
      alert('Error', err);
      recordError('Error: Zone Image Upload', err);
      console.log('Error: Zone Image Upload', err);
    }
  }

  render() {
    const {navigation} = this.props;
    const {showImageLoader, image, video} = this.state;
    console.log(video);
    if (!video) {
      return (
        <View style={styles.container}>
          {image && !showImageLoader ? (
            <Cropper
              image={image}
              navigation={navigation}
              onCancel={this.onCancel}
              onSubmit={this.onSubmit}
            />
          ) : (
            <View
              style={{
                alignSelf: 'center',
                height: '120%',
                justifyContent: 'center',
              }}>
              <Spinner visible={showImageLoader} />
              <Text style={{color: '#fff'}}>
                Please Wait, Uploading Image...
              </Text>
            </View>
          )}
        </View>
      );
    } else {
      return (
        <View style={styles.videoContainer}>
          <StatusBar
            title="Upload Video"
            isBackNavigable
            navigation={navigation}
          />
          {showImageLoader ? (
            <View
              style={{
                alignSelf: 'center',
                height: '120%',
                justifyContent: 'center',
              }}>
              <Spinner visible={showImageLoader} />
              <Text style={{color: '#fff'}}>
                Please Wait, Uploading Video...
              </Text>
            </View>
          ) : (
            <View style={{height: '91%'}}>
              <Video
                source={{uri: video.uri}} // Can be a URL or a local file.
                // Store reference
                style={styles.videoPlayer}
                controls={true}
                resizeMode={'cover'}
                onError={(e) => console.log('Error while running video', e)} // Callback when video cannot be loaded
              />
              <View style={styles.statsContainer}>
                <Text style={styles.statsHeader}>Video Stats</Text>
                <Text style={styles.statsInfo}>FileName: {video.fileName}</Text>
                <Text style={styles.statsInfo}>
                  Size: {parseFloat(video.fileSize / (1024 * 1024)).toFixed(2)}{' '}
                  MB
                </Text>
              </View>
              <Button
                mode="contained"
                style={styles.uploadBtn}
                onPress={() => this.onSubmit(video)}>
                <Text style={{fontSize: 18}}>Upload Video</Text>
              </Button>
            </View>
          )}
        </View>
      );
    }
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zone} = state.zone;
  return {device, userData, zone};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CropImageScreen);
