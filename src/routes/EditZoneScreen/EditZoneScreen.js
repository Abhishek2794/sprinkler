/* eslint-disable import/no-unresolved */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Platform,
  BackHandler,
  Modal,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import * as ImagePicker from 'react-native-image-picker';
import ActionSheet from 'react-native-actionsheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import storage from '@react-native-firebase/storage';
import store from '../../helpers/store';
import alert from '../../components/alert';
import ZoneEnable from '../../components/zoneEnable';
import {appActions} from '../../actions/app.actions';
import {logglyServices, userServices} from '../../services';
import * as ActionCreators from '../../actions';
import {StatusBar} from '../../components/statusBar';
import {HelpButton} from '../../components/helpButton';
import {Colors, Fonts, Dimension} from '../../constants';
import DurationPicker from '../../components/durationPicker';
import {userActions} from '../../actions/user.actions';
import Permissions, {RESULTS, PERMISSIONS} from 'react-native-permissions';
import {logEvent} from '../../analytics';
import ZoneMultimedia from './ZoneMultimedia';

const styles = StyleSheet.create({
  contentBottomRight: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  contentCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  camera: {
    borderRadius: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimension.WIDTH_63,
    height: Dimension.HEIGHT_27,
    backgroundColor: '#fff',
    marginRight: 6,
  },
  cameraSection: {
    alignSelf: 'center',
    position: 'relative',
    width: Dimension.WIDTH_338,
    height: Dimension.HEIGHT_130,
    marginTop: Dimension.HEIGHT_20,
    marginBottom: Dimension.HEIGHT_20,
  },
  card: {
    alignSelf: 'center',
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 8,
    flexDirection: 'column',
    marginTop: 10,
    paddingLeft: Dimension.WIDTH_30,
    paddingRight: Dimension.WIDTH_30,
    paddingTop: Dimension.HEIGHT_20 / 2,
    position: 'relative',
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 7},
    shadowRadius: 18,
    width: Dimension.WIDTH_338,
  },
  circle: {
    alignSelf: 'center',
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'center',
    top: Dimension.HEIGHT_80,
    width: Dimension.WIDTH_115,
    height: Dimension.WIDTH_115,
    borderRadius: Dimension.WIDTH_115 / 2,
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  container: {
    flex: 1,
    height: '100%',
  },
  edit: {
    marginLeft: 5,
    fontWeight: 'bold',
    fontSize: Fonts.FONT_SIZE_12,
    color: '#000',
    letterSpacing: 1.5,
  },
  errorText: {
    marginTop: 10,
    marginBottom: 10,
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
  },
  formField: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontSize: Fonts.FONT_SIZE_14,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  imageField: {
    bottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    width: Dimension.WIDTH_317,
    height: Dimension.HEIGHT_130,
  },
  notePad: {
    fontSize: Fonts.FONT_SIZE_16,
    color: Colors.PRIMARY_COLOR_DARK,
    height: Dimension.HEIGHT_100,
    marginBottom: Dimension.HEIGHT_20,
    marginTop: Dimension.HEIGHT_20,
    paddingLeft: Dimension.WIDTH_20 / 2,
    paddingRight: Dimension.WIDTH_20 / 2,
    backgroundColor: 'rgba(126,211,33,0.12)',
  },
  pickerContainer: {
    bottom: 0,
    position: 'absolute',
    width: Dimension.WIDTH,
  },
  quickRun: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  runText: {
    top: 60,
    fontWeight: '300',
    position: 'absolute',
    fontSize: Fonts.FONT_SIZE_12,
    marginTop: Dimension.HEIGHT_20 / 2,
    color: Colors.PRIMARY_COLOR_DARK,
  },
});

class ZoneEditScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      modalVisible: false,
      tapped: false,
      visible: false,
      showImageLoader: false,
      zoneSetUpInfo: {},
      showMultimedia: false,
    };
    this.checkZoneEnabled = this.checkZoneEnabled.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.enableCamera = this.enableCamera.bind(this);
    this.enableStorage = this.enableStorage.bind(this);
    this.enableZone = this.enableZone.bind(this);
    this.onChangeValue = this.onChangeValue.bind(this);
    this.onClose = this.onClose.bind(this);
    this.uploadImage = this.uploadImage.bind(this);
    this.startQuickRun = this.startQuickRun.bind(this);
    this.pickImage = this.pickImage.bind(this);
  }

  componentDidMount() {
    logEvent('EDIT_ZONE', {screen: 'EDIT_ZONE'});
    userServices.getZoneSetupInfo().then((data) => {
      this.setState({
        zoneSetUpInfo: data,
      });
    });
    BackHandler.addEventListener('hardwareBackPress', this.handlePress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handlePress);
  }

  handlePress = () => {
    this.setState({
      modalVisible: false,
    });
  };

  onChangeValue(value) {
    const {zone} = this.props;
    this.setState({animating: true});
    logEvent('ZONE_UPDATE', {enabled: value, screen: 'EDIT_ZONE'});
    userServices.updateZone({...zone, enabled: value}).then(() => {
      const {device} = this.props;
      store.dispatch(appActions.fetchZones(device._id, zone._id));
      store.dispatch(appActions.fetchCalendarDates(device._id));
      this.setState({animating: false});
    });
  }

  onMasterValveUpdate(value) {
    const {zone} = this.props;
    this.setState({animating: true});
    logEvent('ZONE_MASTER_VALVE_UPDATE', {
      masterValve: value,
      screen: 'EDIT_ZONE',
    });
    userServices.updateZone({...zone, masterValve: value}).then(() => {
      const {device} = this.props;
      store.dispatch(appActions.fetchZones(device._id, zone._id));
      store.dispatch(appActions.fetchCalendarDates(device._id));
      this.setState({animating: false});
    });
  }

  onClose() {
    this.setState({visible: false});
  }

  checkZoneEnabled() {
    const {zone, device} = this.props;
    if (!device.isOnline) {
      alert(
        'Controller is offline',
        'Please turn on the controller device and make sure its connected with the wifi',
      );
      return;
    }
    if (zone.enabled) {
      this.setState({modalVisible: true});
      return;
    }
    this.setState({visible: true});
  }

  closeModal() {
    this.setState({modalVisible: false});
  }

  enableZone() {
    this.setState({animating: true, visible: false});
    const {zone} = this.props;
    zone.enabled = true;
    userServices.updateZone(zone).then(() => {
      this.setState({animating: false});
      alert('Alert', 'Zone Enabled');
      const {device} = this.props;
      store.dispatch(appActions.fetchZones(device._id, zone._id));
    });
  }

  startQuickRun(seconds) {
    this.setState({modalVisible: false, tapped: true});
    const {zone} = this.props;
    const {deviceId, _id, number} = zone;
    userServices
      .startQuickRun({
        zoneId: _id,
        zoneNumber: number,
        deviceId,
        duration: seconds,
      })
      .then(() => {
        logEvent('QUICK_RUN', {
          started: true,
          duration: seconds,
          screen: 'QUICK_RUN',
        });
        store.dispatch(userActions.dockView(false));
        const {navigation, userData} = this.props;
        logglyServices.logEvent(
          {user: userData.email, zone},
          'startZoneQuickRun',
        );
        navigation.navigate('ActiveSchedule');
        this.setState({modalVisible: false, tapped: false});
      })
      .catch(() => alert('Sprinkler', 'Device Offline'));
  }

  async enableCamera(source) {
    const camera =
      Platform.OS == 'ios'
        ? PERMISSIONS.IOS.CAMERA
        : PERMISSIONS.ANDROID.CAMERA;
    if (source) {
      this.enableStorage(source);
    } else {
      const status = await Permissions.check(camera);
      if (status === RESULTS.GRANTED) {
        this.enableStorage(source);
      } else {
        const permission = await Permissions.request(camera);
        switch (permission) {
          case RESULTS.UNAVAILABLE:
            alert(
              'This feature is not available (on this device / in this context)',
            );
            break;
          case RESULTS.DENIED:
            alert('Camera permission is denied');
            break;
          case RESULTS.LIMITED:
            alert('Camera permission is limited');
            break;
          case RESULTS.GRANTED:
            this.enableStorage(source);
            break;
          case RESULTS.BLOCKED:
            alert('The permission is denied and not requestable anymore');
            break;
          default:
            alert('Unexpected error while access the camera');
            break;
        }
      }
    }
  }

  async enableStorage(source) {
    const storage =
      Platform.OS == 'ios'
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE;
    const status = await Permissions.check(storage);
    if (status === RESULTS.GRANTED) {
      this.pickImage(source);
    } else {
      const permission = await Permissions.request(storage);
      switch (permission) {
        case RESULTS.UNAVAILABLE:
          alert(
            'This feature is not available (on this device / in this context)',
          );
          break;
        case RESULTS.DENIED:
          alert('Storage permission is denied');
          break;
        case RESULTS.LIMITED:
          alert('Storage permission is limited');
          break;
        case RESULTS.GRANTED:
          this.pickImage(source);
          break;
        case RESULTS.BLOCKED:
          alert('The permission is denied and not requestable anymore');
          break;
        default:
          alert('Unexpected error while access the Storage');
          break;
      }
    }
  }

  async pickImage(source) {
    if (source === 0) {
      ImagePicker.launchCamera({quality: 1}, (result) => {
        if (!result.didCancel) {
          this.cropImage(result, null);
        }
      });
    }
    if (source === 1) {
      ImagePicker.launchImageLibrary({quality: 1}, (result) => {
        console.log(result);
        if (!result.didCancel) {
          this.cropImage(result, null);
        }
      });
    }
    if (source === 2) {
      ImagePicker.launchCamera(
        {
          quality: 1,
          mediaType: 'video',
          durationLimit: 10,
          videoQuality: 'medium',
        },
        (result) => {
          if (!result.didCancel) {
            const ext = result.fileName.split('.').pop();
            this.cropImage(null, {...result, type: `video/${ext}`});
          }
        },
      );
    }
    if (source === 3) {
      ImagePicker.launchImageLibrary(
        {mediaType: 'video', videoQuality: 'medium'},
        (result) => {
          if (!result.didCancel) {
            const ext = result.fileName.split('.').pop();
            this.cropImage(null, {...result, type: `video/${ext}`});
          }
        },
      );
    }
  }

  cropImage(image, video) {
    const {navigation} = this.props;
    navigation.navigate('CropImageScreen', {image, video});
  }

  hideLoader() {
    this.setState({showImageLoader: false});
  }

  async uploadImage(croppedImage) {
    const imageName = croppedImage.split('/').pop();
    const [, fileName] = croppedImage.split('file://');
    this.setState({showImageLoader: true});
    const {zone} = this.props;
    const {currentUser} = auth();
    await storage()
      .ref(`user/${currentUser.uid}/zoneImages`)
      .child(imageName)
      .putFile(fileName, {contentType: 'mime'});
    const ref = storage()
      .ref(`user/${currentUser.uid}/zoneImages`)
      .child(imageName);
    ref.getDownloadURL().then((url) => {
      userServices
        .updateZone({...zone, imageUrl: url})
        .then(() => {
          zone.imageUrl = url;
          this.setState({showImageLoader: false});
          const {device} = this.props;
          store.dispatch(appActions.fetchZone(device._id, zone.number));
          store.dispatch(appActions.fetchZones(device._id));
        })
        .catch((error) => {
          this.setState({showImageLoader: false});
          alert('Error', error.toString());
        });
    });
  }

  onImgMetaUpdate(imgMeta) {
    const {zone} = this.props;
    this.setState({animating: true});
    userServices.updateZone({...zone, imageUrl: imgMeta}).then(() => {
      const {device} = this.props;
      store.dispatch(appActions.fetchZones(device._id, zone._id));
      this.setState({animating: false});
    });
  }

  render() {
    const {
      animating,
      modalVisible,
      tapped,
      visible,
      showImageLoader,
    } = this.state;
    const {navigation, zone} = this.props;
    if (!zone) {
      return <Spinner visible={true} />;
    }
    return (
      <View style={styles.container}>
        <StatusBar
          title={zone.name}
          child={HelpButton}
          isBackNavigable
          navigation={navigation}
        />
        <Spinner visible={animating} />

        <ZoneEnable
          closePopup={this.onClose}
          modalVisible={visible}
          onPress={this.enableZone}
        />
        <ActionSheet
          ref={(ref) => {
            this.ActionSheet = ref;
          }}
          title="Select Option"
          options={[
            'Take Photo',
            'Upload Phone From Library',
            'Shot 10 sec Video',
            'Upload Video From Library',
            'Cancel',
          ]}
          destructiveButtonIndex={4}
          onPress={(userAction) => this.enableCamera(userAction)}
        />
        <DurationPicker
          closePopup={this.closeModal}
          modalVisible={modalVisible}
          startQuickRun={this.startQuickRun}
        />
        <View style={styles.cameraSection}>
          <ImageBackground
            onLoad={() => this.setState({animating: false})}
            style={[styles.container, {height: Dimension.HEIGHT_130}]}
            source={
              zone.defaultImage
                ? {uri: zone.defaultImage}
                : require('../../../assets/images/zone-default/zone-default.png')
            }>
            {Array.isArray(zone.imageUrl) && zone.imageUrl.length ? (
              <TouchableOpacity
                style={[styles.camera, {margin: 4}]}
                onPress={() => this.setState({showMultimedia: true})}>
                <MaterialIcon name="note-multiple" size={20} />
                <Text style={styles.edit}>{zone.imageUrl.length}</Text>
              </TouchableOpacity>
            ) : null}
            <View
              style={[
                {flex: 1, marginBottom: 8},
                showImageLoader
                  ? styles.contentCenter
                  : styles.contentBottomRight,
              ]}>
              {showImageLoader ? (
                <ActivityIndicator size="large" />
              ) : (
                <TouchableOpacity
                  style={styles.camera}
                  onPress={() => this.ActionSheet.show()}>
                  <Icon name="edit" size={15} color={'#000'} />
                  <Text style={styles.edit}>EDIT</Text>
                </TouchableOpacity>
              )}
            </View>
          </ImageBackground>
        </View>
        <ScrollView>
          <View style={styles.card}>
            <TouchableOpacity
              disabled={tapped}
              style={styles.quickRun}
              onPress={() => this.checkZoneEnabled()}>
              <Text style={styles.formField}>Quick Run</Text>
              <Icon
                name="play-circle-filled"
                size={36}
                color={Colors.PRIMARY_COLOR_DARK}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.card}>
            <View style={styles.formElement}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Enabled</Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={zone.enabled}
                  trackColor={Colors.PRIMARY_COLOR_DARK}
                  onValueChange={() => this.onChangeValue(!zone.enabled)}
                />
              </View>
            </View>
            <View style={styles.formElement}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Master Valve</Text>
                <Text style={styles.formFieldValue} numberOfLines={2}>
                  {zone.name} master valve status
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Switch
                  value={zone.masterValve}
                  trackColor={Colors.PRIMARY_COLOR_DARK}
                  onValueChange={() => {
                    this.onMasterValveUpdate(!zone.masterValve);
                  }}
                />
              </View>
            </View>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() => navigation.navigate('EditZoneName')}>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Zone Name</Text>
                <Text style={styles.formFieldValue} numberOfLines={2}>
                  {zone.name}
                </Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() =>
                navigation.navigate('ZoneSetupScreen6', {
                  selectedValue: zone.vegetationType,
                  isEdit: true,
                  zone,
                  zoneSetUpInfo: this.state.zoneSetUpInfo,
                })
              }>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Vegetation Type</Text>
                <Text style={styles.formFieldValue}>{zone.vegetationType}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() =>
                navigation.navigate('ZoneSetupScreen7', {
                  selectedValue: zone.sprayHeadType,
                  isEdit: true,
                  zone,
                  zoneSetUpInfo: this.state.zoneSetUpInfo,
                })
              }>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Spray Head Type</Text>
                <Text style={styles.formFieldValue}>{zone.sprayHeadType}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() =>
                navigation.navigate('ZoneSetupScreen8', {
                  selectedValue: zone.soilType,
                  isEdit: true,
                  zone,
                  zoneSetUpInfo: this.state.zoneSetUpInfo,
                })
              }>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Soil Type</Text>
                <Text style={styles.formFieldValue}>{zone.soilType}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() =>
                navigation.navigate('ZoneSetupScreen9', {
                  selectedValue: zone.exposureType,
                  isEdit: true,
                  zone,
                  zoneSetUpInfo: this.state.zoneSetUpInfo,
                })
              }>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Exposure Type</Text>
                <Text style={styles.formFieldValue}>{zone.exposureType}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.formElement}
              onPress={() =>
                navigation.navigate('ZoneSetupScreen10', {
                  selectedValue: zone.slopeType,
                  isEdit: true,
                  zone,
                  zoneSetUpInfo: this.state.zoneSetUpInfo,
                })
              }>
              <View style={{flex: 2, flexDirection: 'column'}}>
                <Text style={styles.formField}>Slope Type</Text>
                <Text style={styles.formFieldValue}>{zone.slopeType}</Text>
              </View>
              <View style={styles.formEdit}>
                <Icon name="keyboard-arrow-right" size={24} color="#929292" />
              </View>
            </TouchableOpacity>
            <Text style={styles.formField}>Notes</Text>
            <TouchableOpacity onPress={() => navigation.navigate('EditNotes')}>
              <Text style={styles.notePad}>{zone.notes}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Modal
          animationType="slide"
          visible={this.state.showMultimedia}
          onRequestClose={() => this.setState({showMultimedia: false})}>
          <ZoneMultimedia
            onClose={() => this.setState({showMultimedia: false})}
            zone={zone}
            onUpdate={(data) => this.onImgMetaUpdate(data)}
          />
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  const {zone} = state.zone;
  return {device, userData, zone};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ZoneEditScreen);
