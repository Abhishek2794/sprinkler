import React from 'react';
import * as Helpers from '../../helpers/modules';
import {Subheading, Text, Caption} from 'react-native-paper';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {appConstants, Colors} from '../../constants';
import {logEvent, recordError} from '../../analytics';
import Spinner from 'react-native-loading-spinner-overlay';
import {userServices} from '../../services';
import {appActions} from '../../actions/app.actions';
import store from '../../helpers/store';
import {bindActionCreators} from 'redux';
import * as ActionCreators from '../../actions';
import Icon from 'react-native-vector-icons/MaterialIcons';

class MeasurementPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      preference: '1',
    };
  }

  componentDidMount() {
    this.setState({
      notifications: this.props.device.notifications,
    });
  }

  render() {
    const {navigation} = this.props;
    const {loading, preference} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Helpers.StatusBar
          subtitle=""
          isBackNavigable
          child={Helpers.HelpButton}
          title="Measurement Units"
          navigation={navigation}
        />
        <Spinner visible={loading} />
        <View style={styles.margin}>
          <Subheading>Select Measurement Preference</Subheading>
        </View>
        <TouchableOpacity
          style={[
            styles.margin,
            styles.borderBottom,
            {backgroundColor: preference === '1' ? '#d7d7d7' : '#fff'},
          ]}
          activeOpacity={0.5}
          onPress={() => this.setState({preference: '1'})}>
          <View>
            <Text style={{fontWeight: 'bold'}}>U.S. Standard</Text>
            <Caption>{`\u2109F`}, MPH, Inch</Caption>
          </View>
          <View>
            <Icon name="check" size={24} color="#000" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.margin,
            styles.borderBottom,
            {backgroundColor: preference === '2' ? '#d7d7d7' : '#fff'},
          ]}
          activeOpacity={0.5}
          onPress={() => this.setState({preference: '2'})}>
          <View>
            <Text style={{fontWeight: 'bold'}}>Metric</Text>
            <Caption>{`\u2109C`}, KPH, mm</Caption>
          </View>
          <View>
            <Icon name="check" size={24} color="#000" />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  margin: {
    paddingHorizontal: 12,
    paddingVertical: 16,
  },
  borderBottom: {
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {userData} = state.user;
  return {device, userData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MeasurementPreference);
