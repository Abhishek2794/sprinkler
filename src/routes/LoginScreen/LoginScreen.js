/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import alert from '../../components/alert';
import {Button, Subheading} from 'react-native-paper';
import * as ActionCreators from '../../actions';
import {Colors, Dimension, Fonts} from '../../constants';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import {userServices} from '../../services';
import NetInfo from '@react-native-community/netinfo';
import VersionInfo from 'react-native-version-info';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  errorText: {
    paddingLeft: 10,
    marginBottom: 10,
    fontWeight: '600',
    color: '#fff',
    textTransform: 'capitalize',
    fontSize: Fonts.FONT_SIZE_16,
  },
  form: {
    marginTop: 44,
    marginHorizontal: 24,
  },
  header: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: Fonts.FONT_SIZE_64,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  signup: {
    marginTop: 36,
  },
  text: {
    fontSize: 18,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  textInput: {
    borderWidth: 2,
    borderRadius: 8,
    fontWeight: '300',
    borderStyle: 'solid',
    width: Dimension.WIDTH_317,
    fontSize: Fonts.FONT_SIZE_20,
    height: Dimension.HEIGHT_50,
    paddingLeft: Dimension.WIDTH_20,
    marginBottom: Dimension.HEIGHT_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderColor: Colors.PRIMARY_COLOR_DARK,
  },
  toggleViewPassword: {
    left: 250,
    position: 'absolute',
    top: Dimension.HEIGHT_20 / 2,
  },
  loginBtn: {
    height: 56,
    justifyContent: 'center',
    fontSize: 20,
    width: '97%',
  },
});

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {},
      errors: {},
      viewPassword: false,
      visible: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.changeText = this.changeText.bind(this);
    this.handleNavigation = this.handleNavigation.bind(this);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  onSubmit() {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        Alert.alert(
          'No Network',
          'App is not able to connect with the network, check your network connection',
          [
            {
              text: 'Close',
            },
          ],
          {cancelable: false},
        );
      } else {
        Keyboard.dismiss();
        this.setState({visible: true});
        const {errors, form} = this.state;
        if (!('email' in form)) {
          errors.email = 'Email ID is required';
          this.setState({errors});
          this.setState({visible: false});
          return;
        }
        if (
          !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(
            form.email,
          )
        ) {
          errors.email = 'Email ID is invalid';
          this.setState({errors});
          this.setState({visible: false});
          return;
        }
        if (!('password' in form)) {
          errors.password = 'Password is required';
          this.setState({errors});
          this.setState({visible: false});
          return;
        }
        userServices
          .login({...form})
          .then(() => {
            this.setState({visible: false}, () => {
              store.dispatch(appActions.checkLogin(null));
            });
          })
          .catch((error) => {
            this.setState({visible: false}, () => {
              setTimeout(() => {
                alert('Error', error.message);
              }, 500);
            });
          });
      }
    });
  }

  changeText(text, field) {
    const {errors, form} = this.state;
    if (!text) {
      delete form[field];
      errors[field] = `${field} is required`;
      this.setState({errors, form});
      return;
    }
    form[field] = text.trim();
    delete errors[field];
    this.setState({errors, form});
  }

  handleNavigation(screen) {
    const {navigation} = this.props;
    navigation.navigate(screen);
  }

  render() {
    const {errors, viewPassword, visible} = this.state;
    const {login, error} = this.props;
    return (
      <LinearGradient
        locations={[0.1, 1.0]}
        style={styles.container}
        colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
        <Spinner visible={visible} />
        {login === false &&
          error !== '' &&
          alert('Error', error.split(':').pop())}
        <KeyboardAvoidingView
          behavior="height"
          keyboardVerticalOffset={10}
          styles={styles.container}>
          <Text style={styles.header}>Sprinkler</Text>
          <View style={styles.form}>
            <TextInput
              blurOnSubmit={false}
              placeholder="Email ID"
              returnKeyType="next"
              style={styles.textInput}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              onSubmitEditing={() => this.passwordInput.focus()}
              onChangeText={(text) => this.changeText(text, 'email')}
            />
            {errors.email ? (
              <Text style={styles.errorText}>{errors.email}</Text>
            ) : null}
            <View>
              <TextInput
                blurOnSubmit={false}
                returnKeyType="done"
                placeholder="Password"
                secureTextEntry={!viewPassword}
                style={[styles.textInput, {position: 'relative'}]}
                contextMenuHidden
                selectTextOnFocus={false}
                keyboardType="ascii-capable"
                placeholderTextColor={Colors.PLACEHOLDER_TEXT}
                ref={(input) => {
                  this.passwordInput = input;
                }}
                onChangeText={(text) => this.changeText(text, 'password')}
                onSubmitEditing={() => this.onSubmit()}
              />
              {errors.password ? (
                <Text style={styles.errorText}>{errors.password}</Text>
              ) : null}
              {viewPassword ? (
                <Button
                  onPress={() => this.setState({viewPassword: !viewPassword})}
                  style={styles.toggleViewPassword}
                  color="#fff">
                  Hide
                </Button>
              ) : (
                <Button
                  onPress={() => this.setState({viewPassword: !viewPassword})}
                  style={styles.toggleViewPassword}
                  color="#fff">
                  Show
                </Button>
              )}
              <Button
                mode="contained"
                onPress={this.onSubmit}
                color={Colors.SECONDARY_COLOR}
                style={styles.loginBtn}>
                <Text style={{fontSize: 18, letterSpacing: 1}}>Login</Text>
              </Button>
              <View style={styles.signup}>
                <Button
                  color={'#fff'}
                  mode="outlined"
                  onPress={() => this.handleNavigation('ForgotPassword')}>
                  Forgot Password
                </Button>
              </View>
              <View style={styles.signup}>
                <Text style={styles.text}>Don&#x27;t have an account?</Text>
                <Button
                  color={'#fff'}
                  mode="outlined"
                  style={{marginTop: 16}}
                  onPress={() => this.handleNavigation('SignUp')}>
                  Create Account
                </Button>
              </View>
              <View
                style={{
                  margin: 24,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Subheading style={{color: '#fff', fontSize: 16}}>
                  App Version: {VersionInfo.appVersion}
                </Subheading>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </LinearGradient>
    );
  }
}

const mapStateToProps = (state) => {
  const {login, error} = state.authentication;
  return {login, error};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
