/* eslint-disable no-param-reassign */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts, Strings, appConstants} from '../../constants';
import {appActions} from '../../actions/app.actions';
import {userServices, appServices} from '../../services';
import {generateCalenderPayload} from '../../helpers/create-calender';
import Spinner from 'react-native-loading-spinner-overlay';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  day: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
    justifyContent: 'space-between',
    marginLeft: Dimension.WIDTH_36,
    marginRight: Dimension.WIDTH_36,
    paddingTop: Dimension.HEIGHT_20,
    paddingBottom: Dimension.HEIGHT_20 / 2,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditDaysScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: [],
      loading: false,
    };
    this.selectedDays = new Set();
    this.onSubmit = this.onSubmit.bind(this);
    this.handlePress = this.handlePress.bind(this);
  }

  componentDidMount() {
    const {scheduleData} = this.props;
    let selectedDays = scheduleData?.days?.split(',') || [];
    this.selectedDays = new Set(selectedDays);
    if (
      this.selectedDays.has('Odd') ||
      this.selectedDays.has('Even') ||
      this.selectedDays.has('Interval') ||
      this.selectedDays.has('Everyday')
    ) {
      this.selectedDays.clear();
      this.selectedDays.add('Sunday');
      selectedDays = ['Sunday'];
    }
    const days = Strings.WEEK_DAYS.map((day) => ({name: day}));
    days.forEach((day) => {
      day.selected = selectedDays.includes(day.name);
    });
    this.setState({days});
  }

  onSubmit() {
    this.setState({loading: true});
    if (this.selectedDays.size === 0) {
      alert('ATTENTION', 'Select atleast one day');
      return;
    }

    const {device, navigation, scheduleData} = this.props;
    userServices
      .updateSchedule(device._id, {
        ...scheduleData,
        days: [...this.selectedDays].join(','),
      })
      .then((updatedSchedule) => {
        store.dispatch(appActions.fetchSchedules(device._id));
        store.dispatch(appActions.fetchSchedule(updatedSchedule));
        setTimeout(() => {
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                this.setState({loading: false});
                navigation.navigate('EditSchedule');
              });
          }
        }, 2000);
      });
  }

  handlePress(index) {
    const {days} = this.state;
    if (days[index].selected === false) {
      days[index].selected = true;
      this.selectedDays.add(days[index].name);
      this.setState({days});
      return;
    }
    days[index].selected = false;
    this.selectedDays.delete(days[index].name);
    this.setState({days});
  }

  render() {
    const {days, loading} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Select Days"
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Spinner visible={loading} />
        <Helpers.Card>
          <View>
            {days.map((day, index) => (
              <TouchableOpacity
                key={day.name}
                onPress={() => this.handlePress(index)}>
                <View style={styles.listItem}>
                  <Text style={styles.day}>{day.name}</Text>
                  {day.selected && (
                    <Icon name="done" color={Colors.PRIMARY_COLOR} size={24} />
                  )}
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <Helpers.CancelButton
            navigation={navigation}
            routeAction={appConstants.ROUTE_GO_BACK}
          />
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {scheduleData} = state.schedule;
  return {device, scheduleData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditDaysScreen);
