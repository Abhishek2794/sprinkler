import config from '../../helpers/config';
import {appServices} from '../../services';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function getLogs(device, callback) {
  try {
    AsyncStorage.getItem('@Sprinkler:HistoryLogs').then((data) => {
      let historyLogs = data;
      if (historyLogs) {
        historyLogs = JSON.parse(historyLogs);
        if (
          moment().diff(
            moment(historyLogs.fetchedOn).format('YYYY/MM/DD'),
            'days',
          ) === 0
        ) {
          const date = moment().subtract(1, 'days').format('YYYY/MM/DD');
          const timeStamp = moment().subtract(
            config.logHistoryDurationInDays,
            'days',
          );
          const logs = historyLogs.logs.filter(
            (item) => item.timestamp > timeStamp,
          );
          fetchData(device, date, logs, (result) => {
            callback(result);
          });
        } else {
          fetchData(
            device,
            historyLogs.fetchedOn,
            historyLogs.logs,
            (result) => {
              callback(result);
            },
          );
        }
      } else {
        const date = moment()
          .subtract(config.logHistoryDurationInDays, 'days')
          .format('YYYY/MM/DD');
        fetchData(device, date, null, (result) => {
          callback(result);
        });
      }
    });
  } catch (err) {
    console.log(err);
  }
}

function fetchData(device, date, previousLogs, callback) {
  appServices.fetchDeviceLogs(device._id, 0, date).then((response) => {
    const data = response;
    data.sort((x, y) => y.timestamp - x.timestamp);
    if (!previousLogs) {
      const payload = {
        logs: data,
        fetchedOn: moment().format('YYYY/MM/DD'),
      };
      AsyncStorage.setItem('@Sprinkler:HistoryLogs', JSON.stringify(payload));
      callback(data);
    } else {
      let updatedLogs = data.filter((item) => {
        const existingRecord = previousLogs.find(
          (oldLog) => item.timestamp === oldLog.timestamp,
        );
        if (existingRecord) {
          return false;
        } else {
          return true;
        }
      });
      updatedLogs = [...updatedLogs, ...previousLogs];
      const payload = {
        logs: updatedLogs,
        fetchedOn: moment().format('YYYY/MM/DD'),
      };
      AsyncStorage.setItem('@Sprinkler:HistoryLogs', JSON.stringify(payload));
      callback(updatedLogs);
    }
  });
}
