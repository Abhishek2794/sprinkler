/* eslint-disable import/no-extraneous-dependencies */
import moment from 'moment';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension} from '../../constants';
import Spinner from 'react-native-loading-spinner-overlay';
import {appServices} from '../../services';
import {
  WateringStatus,
  getWateringStatus,
} from '../../constants/WateringStatus';

const styles = StyleSheet.create({
  card: {
    backgroundColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderRadius: 8,
    flexDirection: 'row',
    height: Dimension.HEIGHT_80,
    marginBottom: Dimension.HEIGHT_20 / 2,
    shadowColor: 'rgba(203, 203, 203, 0.5)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 7},
    shadowRadius: 18,
    width: Dimension.WIDTH_338,
  },
  container: {
    flex: 1,
  },
  header: {
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  message: {
    alignSelf: 'center',
    color: Colors.HEADER_COLOR,
    fontSize: Fonts.FONT_SIZE_16,
  },
  footer: {
    paddingHorizontal: 12,
    paddingVertical: 16,
    backgroundColor: Colors.SCREEN_BACKGROUND,
  },
});

class HistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLogs: [],
      loading: true,
    };
  }

  componentDidMount() {
    const {device} = this.props;
    appServices
      .fetchDeviceLogs(device._id, 0, new Date())
      .then((response) => {
        this.setState({
          deviceLogs: response,
          loading: false,
        });
      })
      .catch((err) => {
        this.setState({
          deviceLogs: [],
          loading: false,
        });
      });
  }

  viewHistoryLogs = (logs) => {
    this.setState({
      deviceLogs: logs,
      loading: false,
    });
  };

  render() {
    const {deviceLogs, loading} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          subtitle=""
          title="History"
          child={Helpers.HelpButton}
          isBackNavigable
          navigation={navigation}
        />
        <View style={{marginTop: 10, alignItems: 'center', flex: 1}}>
          <Spinner visible={loading} />
          <FlatList
            contentContainerStyle={{flexGrow: 1}}
            data={deviceLogs}
            ListEmptyComponent={
              <Text style={{textAlignVertical: 'center', flex: 1}}>
                {loading
                  ? `Fetching controller history records`
                  : `No records found`}
              </Text>
            }
            keyExtractor={(item, index) => `${item._id}`}
            onEndReachedThreshold={0.7}
            renderItem={({item}) => (
              <View style={styles.card}>
                <View
                  style={{
                    flex: 2,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Icon
                    name={
                      item.status === WateringStatus.COMPLETED
                        ? 'watering-can-outline'
                        : 'clock-outline'
                    }
                    size={32}
                    color={
                      item.status === WateringStatus.COMPLETED
                        ? Colors.PRIMARY_COLOR_DARK
                        : Colors.OFFLINE_STATUS
                    }
                  />
                </View>
                <View style={{flex: 6, justifyContent: 'center'}}>
                  <Text style={styles.header}>
                    {item.name || `Quick Run`}
                    {`  [ ${getWateringStatus(item.status)} ]`}
                  </Text>
                  <Text style={{fontSize: Fonts.FONT_SIZE_14}}>
                    {moment(item.createdAt).format('Do MMM, hh:mm:ss A')}
                  </Text>
                  <Text style={{fontSize: Fonts.FONT_SIZE_12}}>
                    {item.description}
                  </Text>
                </View>
              </View>
            )}
            showsVerticalScrollIndicator={false}
          />
        </View>
        <View style={styles.footer}>
          <Text>Note: History records of past 30 days are shown</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {history} = state.logs;
  return {device, history};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
