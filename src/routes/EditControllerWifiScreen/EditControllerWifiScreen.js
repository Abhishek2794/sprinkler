/* eslint-disable class-methods-use-this */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, View} from 'react-native';
import WifiManager from 'react-native-wifi-reborn';
import {userServices} from '../../services';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Dimension, Fonts} from '../../constants';
import alert from '../../components/alert';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Subheading, Caption} from 'react-native-paper';


const styles = StyleSheet.create({
  bullet: {
    color: Colors.PRIMARY_COLOR_DARK,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    color: '#9e9e9e',
    fontWeight: '600',
    fontSize: Fonts.FONT_SIZE_20,
  },
  instructions: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_16,
  },
  message: {
    alignSelf: 'center',
    color: Colors.TEXT_COLOR,
    fontSize: Fonts.FONT_SIZE_18,
    marginTop: Dimension.HEIGHT_20,
  },
  row: {
    flexDirection: 'row',
    marginTop: Dimension.HEIGHT_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditControllerWifiScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.setupController = this.setupController.bind(this);
  }

  componentDidMount() {
    this.setupController();
  }

  setupController() {
    const {controllerData, device} = this.props;
    userServices
      .sendNetworkDetails({
        ...controllerData,
        serialNumber: device.serialNumber,
      })
      .then(async () => {
        if (Platform.OS === 'android') {
          await WifiManager.forceWifiUsageWithOptions(false, {
            noInternet: false,
          });
        }
        setTimeout(() => {
          const {navigation} = this.props;
          navigation.popToTop();
        }, 3000);
      })
      .catch((err) => {
        alert('Error while connecting', err.toString());
      });
  }

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Reset Network"
          subtitle=""
          isBackNavigable={false}
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Helpers.Card>
          <Subheading style={{textAlign: 'center'}}>
            Configuring controller and connecting with your account
          </Subheading>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name="warning" size={14} color="#FF6F00" />
            <Caption style={{marginLeft: 8}}>
              Please do not close the app
            </Caption>
          </View>
          <View style={{flex: 1, width: '100%'}}>
            <LottieView
              source={require('../../../assets/animation/Setup.json')}
              autoPlay
              loop
            />
          </View>
        </Helpers.Card>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {controllerData} = state.controller;
  return {controllerData, device};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditControllerWifiScreen);
