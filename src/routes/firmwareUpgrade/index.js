import React from 'react';
import {Subheading, Text, Button, Snackbar} from 'react-native-paper';
import {View, Modal, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {appServices} from '../../services';

class FirmwareUpgrade extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      toast: '',
      error: '',
    };
  }
  closeDialog = () => {
    this.props.onClose();
  };
  updateFirmware = () => {
    this.setState({
      loading: true,
    });
    appServices
      .upgradeFirmware(this.props.deviceId)
      .then((data) => {
        console.log('data', data);
        this.setState(
          {
            loading: false,
            toast: data,
          },
          () => {
            setTimeout(() => {
              this.closeDialog();
            }, 4000);
          },
        );
      })
      .catch((err) => {
        this.setState({
          loading: false,
          error: err,
        });
      });
  };
  render() {
    const {device} = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.visible}
        onDismiss={this.closeDialog}
        onRequestClose={this.closeDialog}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.header}>
              <Subheading>Update Firmware</Subheading>
              <TouchableOpacity activeOpacity={0.5} onPress={this.closeDialog}>
                <Icon name="close" size={24} />
              </TouchableOpacity>
            </View>
            <View style={styles.body}>
              <Image
                source={require('../../../assets/images/upgrade/firmware.jpeg')}
                style={styles.image}
              />
              <Text style={{textAlign: 'center'}}>
                You are required to update your device's firmware. We have some
                amazing new features which will help you to use sprinkler device
                in a better way.
              </Text>
            </View>
            <View style={styles.footer}>
              {device.isOnline ? (
                <Button
                  loading={this.state.loading}
                  icon="update"
                  onPress={this.updateFirmware}>
                  Update Now
                </Button>
              ) : (
                <View style={styles.warning}>
                  <FontAwesomeIcon name="warning" size={24} color="#EF6C00" />
                  <Subheading style={{marginLeft: 8, color: '#D84315'}}>
                    Please switch on the device !
                  </Subheading>
                </View>
              )}
            </View>
          </View>
        </View>
        <Snackbar
          visible={this.state.error || this.state.toast}
          onDismiss={() => this.setState({error: '', toast: ''})}
          style={{backgroundColor: this.state.error ? '#d32f2f' : '#00796B'}}>
          {this.state.error || this.state.toast}
        </Snackbar>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
  },
  modalView: {
    margin: 20,
    backgroundColor: '#fff',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  header: {
    paddingVertical: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#999',
    borderBottomWidth: 0.5,
    paddingHorizontal: 16,
  },
  body: {
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderBottomColor: '#999',
    borderBottomWidth: 0.5,
  },
  image: {
    alignSelf: 'center',
    height: 220,
    width: 220,
  },
  footer: {
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  warning: {
    flexDirection: 'row',
  },
});

export default FirmwareUpgrade;
