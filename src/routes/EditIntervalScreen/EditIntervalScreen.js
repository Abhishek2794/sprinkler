import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {Colors, Fonts, Dimension, appConstants} from '../../constants';
import {userServices, appServices} from '../../services';
import {generateCalenderPayload} from '../../helpers/create-calender';
import Spinner from 'react-native-loading-spinner-overlay';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
  },
  header: {
    marginBottom: 5,
    color: '#9e9e9e',
    fontWeight: '500',
    fontSize: Fonts.FONT_SIZE_20,
  },
  radioText: {
    marginTop: -5,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class EditIntervalScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: '',
      selectedIndex: -1,
      loader: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    const {scheduleData} = this.props;
    const {days} = scheduleData;
    switch (days) {
      case 'Monday,Wednesday,Friday,Sunday':
        this.setState({days, selectedIndex: 0});
        break;
      case 'Tuesday,Thursday,Saturday':
        this.setState({days, selectedIndex: 1});
        break;
      case 'Everyday':
        this.setState({days, selectedIndex: 2});
        break;
      default:
        this.setState({selectedIndex: 3});
        break;
    }
  }

  onSubmit() {
    this.setState({loader: true});
    const {days, selectedIndex} = this.state;
    const {navigation} = this.props;
    if (selectedIndex === 3) {
      navigation.navigate('EditDays');
      return;
    }
    const {device, scheduleData} = this.props;
    userServices
      .updateSchedule(device._id, {...scheduleData, days})
      .then((updatedSchedule) => {
        store.dispatch(appActions.fetchSchedule(updatedSchedule));
        store.dispatch(appActions.fetchSchedules(device._id));
        setTimeout(() => {
          const payload = generateCalenderPayload();
          if (payload) {
            appServices
              .createCalenderForController(device.serialNumber, payload.data)
              .then(() => {
                this.setState({loader: false});
                navigation.goBack();
              });
          }
        }, 2000);
      });
  }

  render() {
    const {navigation} = this.props;
    const {selectedIndex, loader} = this.state;
    return (
      <View style={styles.container}>
        <Spinner visible={loader} />
        <Helpers.StatusBar
          isBackNavigable
          title="Create Schedule"
          subtitle="Select Interval"
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <Helpers.Card>
          <View>
            <Text
              style={
                styles.header
              }>{`Which days do you want to${'\n'}run this schedule?`}</Text>
            <RadioGroup
              size={21}
              selectedIndex={selectedIndex}
              style={{marginTop: Dimension.HEIGHT_20}}
              onSelect={(index, value) => {
                switch (value) {
                  case 0:
                    this.setState({
                      days: 'Monday,Wednesday,Friday,Sunday',
                      selectedIndex: value,
                    });
                    break;
                  case 1:
                    this.setState({
                      days: 'Tuesday,Thursday,Saturday',
                      selectedIndex: value,
                    });
                    break;
                  case 2:
                    this.setState({days: 'Everyday', selectedIndex: value});
                    break;
                  case 3:
                    this.setState({days: '', selectedIndex: value});
                    break;
                  default:
                    break;
                }
              }}
              color={Colors.PRIMARY_COLOR_DARK}>
              <RadioButton value={0}>
                <Text style={styles.radioText}>Odd Interval</Text>
              </RadioButton>
              <RadioButton value={1}>
                <Text style={styles.radioText}>Even Interval</Text>
              </RadioButton>
              <RadioButton value={2}>
                <Text style={styles.radioText}>Every day</Text>
              </RadioButton>
              <RadioButton value={3}>
                <Text style={styles.radioText}>Specific days</Text>
              </RadioButton>
            </RadioGroup>
          </View>
        </Helpers.Card>
        <Helpers.Footer>
          <Helpers.CancelButton
            navigation={navigation}
            routeAction={appConstants.ROUTE_GO_BACK}
          />
          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={styles.button}>
            <Text style={styles.text}>
              {selectedIndex === 3 ? 'Next' : 'Save'}
            </Text>
          </TouchableOpacity>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {device} = state.activeDevice;
  const {schedule} = state.scheduleCreate;
  const {scheduleData} = state.schedule;
  return {device, schedule, scheduleData};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditIntervalScreen);
