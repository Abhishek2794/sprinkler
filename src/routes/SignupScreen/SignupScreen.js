/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-useless-escape */
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  View,
} from 'react-native';
import {Button, Subheading} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import alert from '../../components/alert';
import * as ActionCreators from '../../actions';
import {appActions} from '../../actions/app.actions';
import {userServices} from '../../services/user.services';
import {Colors, Dimension, Fonts} from '../../constants';
import {Checkbox} from 'react-native-paper';
import * as Helpers from '../../helpers/modules';
import VersionInfo from 'react-native-version-info';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
  },
  errorText: {
    alignSelf: 'center',
    paddingLeft: 10,
    marginBottom: 10,
    fontWeight: '600',
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    width: Dimension.WIDTH_317,
  },
  form: {
    marginTop: 44,
    marginHorizontal: 24,
  },
  header: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: Fonts.FONT_SIZE_64,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  login: {
    marginTop: Dimension.HEIGHT_50,
  },
  text: {
    fontSize: 18,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  textInput: {
    borderWidth: 2,
    borderRadius: 8,
    fontWeight: '300',
    borderStyle: 'solid',
    width: Dimension.WIDTH_317,
    fontSize: Fonts.FONT_SIZE_20,
    height: Dimension.HEIGHT_50,
    paddingLeft: Dimension.WIDTH_20,
    marginBottom: Dimension.HEIGHT_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
    borderColor: Colors.PRIMARY_COLOR_DARK,
  },
  toggleViewPassword: {
    left: 250,
    position: 'absolute',
    top: 90,
  },
  loginBtn: {
    height: 56,
    justifyContent: 'center',
    fontSize: 20,
    width: '97%',
  },
});

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {},
      errors: {},
      viewPassword: false,
      validPassword: false,
      loader: false,
      showPasswordHint: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.changeText = this.changeText.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  validatePassword = (password) => {
    this.setState({
      showPasswordHint: true,
    });
    const {errors} = this.state;
    let validPassword = false;
    if (password) {
      if (/.*[A-Z].*/.test(password)) {
        errors.isCapital = true;
        this.setState({errors});
        validPassword = true;
      } else {
        errors.isCapital = false;
        this.setState({errors});
        validPassword = false;
      }
      if (/\d+/.test(password)) {
        errors.isNumber = true;
        this.setState({errors});
        validPassword = true;
      } else {
        errors.isNumber = false;
        this.setState({errors});
        validPassword = false;
      }
      if (/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(password)) {
        errors.isSpecialChar = true;
        this.setState({errors});
        validPassword = true;
      } else {
        errors.isSpecialChar = false;
        this.setState({errors});
        validPassword = false;
      }
      if (password.length >= 6) {
        errors.isStrong = true;
        this.setState({errors});
        validPassword = true;
      } else {
        validPassword = false;
        errors.isStrong = false;
        this.setState({errors});
      }
    } else {
      this.setState({errors: {}});
    }
    if (validPassword) {
      this.setState({errors, validPassword: true});
      return true;
    } else {
      return false;
    }
  };

  onSubmit() {
    Keyboard.dismiss();
    this.setState({loader: true});
    const {errors, form} = this.state;

    Object.keys(errors).forEach((prop) => delete errors[prop]);
    if (!('email' in form)) {
      errors.email = 'Email ID is required';
      this.setState({errors, loader: false});
      return;
    }
    if (
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(
        form.email,
      )
    ) {
      Reflect.deleteProperty(form, 'email');
      errors.email = 'Invalid Email ID';
      this.setState({errors, form, loader: false});
      return;
    }
    if (!('password' in form)) {
      errors.password = 'Password is required';
      this.setState({errors, loader: false});
      return;
    }
    if (!this.validatePassword(form.password)) {
      return;
    }
    userServices
      .register({...form})
      .then(() => {
        this.setState({loader: false}, () => {
          store.dispatch(appActions.checkLogin(null));
        });
      })
      .catch((error) => {
        this.setState({loader: false}, () => {
          setTimeout(() => {
            alert('Sign Up Failed', error.message);
          }, 500);
        });
      });
  }

  changeText(text, field) {
    const {form} = this.state;
    form[field] = text;
    this.setState({form});
  }

  handleClick() {
    const {navigation} = this.props;
    navigation.navigate('Login');
  }

  render() {
    const {errors, viewPassword, loader: loader} = this.state;
    const {navigation} = this.props;
    return (
      <LinearGradient
        locations={[0.1, 1.0]}
        style={{flex: 1}}
        colors={[Colors.PRIMARY_COLOR, Colors.SECONDARY_COLOR]}>
        <Spinner loader={loader} />
        <Helpers.StatusBar
          title="Create Account"
          isBackNavigable
          subtitle=""
          child={Helpers.HelpButton}
          navigation={navigation}
        />
        <KeyboardAvoidingView
          styles={styles.container}>
          <ScrollView style={styles.form}>
            <TextInput
              blurOnSubmit={false}
              placeholder="Email ID"
              returnKeyType="next"
              style={styles.textInput}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              onFocus={() => {
                this.setState({showPasswordHint: false});
              }}
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              onSubmitEditing={() => this.passwordInput.focus()}
              onChangeText={(text) => this.changeText(text, 'email')}
            />
            {errors.email ? (
              <Text style={styles.errorText}>{errors.email}</Text>
            ) : null}
            <TextInput
              blurOnSubmit={false}
              returnKeyType="next"
              placeholder="Password"
              secureTextEntry={!viewPassword}
              style={[styles.textInput, {position: 'relative'}]}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              ref={(input) => {
                this.passwordInput = input;
              }}
              onSubmitEditing={() => this.onSubmit()}
              onChangeText={(text) => {
                this.validatePassword(text);
                this.changeText(text, 'password');
              }}
            />
            {this.state.showPasswordHint ? (
              <View style={{position: 'relative', bottom: 20}}>
                <Checkbox.Item
                  label="One Captial Char"
                  status={errors.isCapital ? 'checked' : 'unchecked'}
                  color="#fff"
                  labelStyle={{color: '#fff'}}
                />
                <Checkbox.Item
                  label="One Special Char"
                  status={errors.isSpecialChar ? 'checked' : 'unchecked'}
                  color="#fff"
                  labelStyle={{color: '#fff'}}
                />
                <Checkbox.Item
                  label="One Number"
                  status={errors.isNumber ? 'checked' : 'unchecked'}
                  color="#fff"
                  labelStyle={{color: '#fff'}}
                />
                <Checkbox.Item
                  label="Strong Password"
                  status={errors.isStrong ? 'checked' : 'unchecked'}
                  color="#fff"
                  labelStyle={{color: '#fff'}}
                />
              </View>
            ) : null}
            {viewPassword ? (
              <Button
                onPress={() => this.setState({viewPassword: !viewPassword})}
                style={styles.toggleViewPassword}
                color="#fff">
                Hide
              </Button>
            ) : (
              <Button
                onPress={() => this.setState({viewPassword: !viewPassword})}
                style={styles.toggleViewPassword}
                color="#fff">
                Show
              </Button>
            )}
            <TextInput
              blurOnSubmit={false}
              placeholder="Name"
              returnKeyType="next"
              style={styles.textInput}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="ascii-capable"
              onFocus={() => {
                this.setState({showPasswordHint: false});
              }}
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              onSubmitEditing={() => this.passwordInput.focus()}
              onChangeText={(text) => this.changeText(text, 'username')}
            />
            <TextInput
              blurOnSubmit={false}
              placeholder="Phone Number"
              returnKeyType="next"
              style={styles.textInput}
              contextMenuHidden
              selectTextOnFocus={false}
              keyboardType="phone-pad"
              onFocus={() => {
                this.setState({showPasswordHint: false});
              }}
              placeholderTextColor={Colors.PLACEHOLDER_TEXT}
              onSubmitEditing={() => this.passwordInput.focus()}
              onChangeText={(text) => this.changeText(text, 'phone')}
            />
            <Button
              mode="contained"
              onPress={this.onSubmit}
              color={Colors.SECONDARY_COLOR}
              style={styles.loginBtn}>
              <Text style={{fontSize: 18, letterSpacing: 1}}>
                Create Account
              </Text>
            </Button>
            <View style={styles.login}>
              <Text style={styles.text}>Already have an account?</Text>
              <Button
                color={'#fff'}
                mode="outlined"
                style={{marginTop: 16}}
                onPress={this.handleClick}>
                Sign In
              </Button>
            </View>
            <View
              style={{
                margin: 24,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Subheading style={{color: '#fff', fontSize: 16}}>
                App Version: {VersionInfo.appVersion}
              </Subheading>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </LinearGradient>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(null, mapDispatchToProps)(SignupScreen);
