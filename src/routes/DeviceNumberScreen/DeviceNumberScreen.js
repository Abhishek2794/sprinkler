import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import store from '../../helpers/store';
import {appActions} from '../../actions/app.actions';
import * as ActionCreators from '../../actions';
import * as Helpers from '../../helpers/modules';
import {appServices} from '../../services';
import {Colors, Fonts, Dimension} from '../../constants';
import {recordError, logEvent} from '../../analytics';
import {Button, Subheading, TextInput, Snackbar} from 'react-native-paper';

const styles = StyleSheet.create({
  box: {
    flex: 2,
    flexDirection: 'column',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  container: {
    flex: 1,
  },
  errorText: {
    color: Colors.DANGER,
    fontSize: Fonts.FONT_SIZE_16,
    marginTop: Dimension.HEIGHT_20 / 2,
    marginBottom: Dimension.HEIGHT_20 / 2,
  },
  formEdit: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formElement: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dfdfe4',
  },
  formField: {
    color: '#9e9e9e',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_12,
  },
  formFieldValue: {
    borderWidth: 2,
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    borderColor: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
  hint: {
    color: '#929292',
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    marginTop: Dimension.HEIGHT_20,
  },
  scan: {
    alignSelf: 'center',
    marginTop: Dimension.HEIGHT_70,
  },
  text: {
    fontWeight: '300',
    fontSize: Fonts.FONT_SIZE_20,
    color: Colors.SWIPER_ACTIVE_DOT_COLOR,
  },
});

class DeviceNumberScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      error: '',
      number: '',
    };
    AsyncStorage.getItem('@Sprinkler:deviceNumber').then((value) =>
      this.setState({number: value}),
    );
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  onChangeText(number) {
    if (number) {
      this.setState({number, error: false});
      return;
    }
    this.setState({number, error: true});
  }

  onSubmit() {
    const {number} = this.state;
    const {allDevices} = this.props;
    if (!number) {
      return;
    }
    this.setState({animating: true, error: ''});
    this.isRemotelyDisabled();
  }

  isRemotelyDisabled() {
    const {number} = this.state;
    const {controllerData, navigation} = this.props;
    appServices
      .isRemotelyDisabled(`AMEBAA${number}`)
      .then((response) => {
        this.setState({animating: false});
        const {active} = response;
        if (active) {
          store.dispatch(
            appActions.setControllerData({
              ...controllerData,
              serialNumber: `AMEBAA${number}`,
            }),
          );
          AsyncStorage.setItem('@Sprinkler:deviceNumber', number);
          navigation.navigate('DeviceName');
          return;
        }
        navigation.navigate('RemotelyDisabled');
      })
      .catch((err) => {
        console.log(err);
        this.setState({animating: false});
        recordError('Error: Device Serial Number', err);
        navigation.navigate('DeviceNumberError');
      });
  }

  render() {
    const {navigation} = this.props;
    const {animating, error, number} = this.state;
    return (
      <View style={styles.container}>
        <Helpers.StatusBar
          title="Add Device"
          isBackNavigable
          subtitle="Serial Number"
          child={Helpers.HelpButton}
          navigation={navigation}
        />

        <Spinner visible={animating} />

        <Helpers.Card>
          <View style={styles.formElement}>
            <View style={styles.box}>
              <TextInput
                autoFocus
                maxLength={10}
                returnKeyType="done"
                keyboardType="numeric"
                value={number}
                onChangeText={(text) => this.onChangeText(text)}
                label="Device Serial Number"
                placeholder="Enter Number"
                mode="outlined"
              />
            </View>
          </View>
          <Subheading>
            Your device number is printed on the front of the controller.
          </Subheading>
          <Snackbar
            style={{backgroundColor: Colors.DANGER}}
            visible={error}
            onDismiss={() => this.setState({error: ''})}>
            {error}
          </Snackbar>
        </Helpers.Card>
        <Helpers.Footer>
          <Button
            style={{width: '100%', justifyContent: 'center', borderRadius: 0}}
            onPress={() => this.onSubmit()}
            disabled={!number}
            mode="contained">
            <Subheading
              style={{color: '#fff', fontWeight: 'bold', letterSpacing: 2}}>
              Get Device Status
            </Subheading>
          </Button>
        </Helpers.Footer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {controllerData} = state.controller;
  const {allDevices} = state.devices;
  return {controllerData, allDevices};
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(ActionCreators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeviceNumberScreen);
