/* eslint-disable no-useless-escape */
/* eslint-disable no-console */
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import {config} from '../helpers/config';
import firebaseToken from '../helpers/token';
import authHeader from '../helpers/authHeader';

async function addManualSchedule(zone) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(zone),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/watering/start-multiple-run`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          response.json().then((data) => reject(data));
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function copyConfiguration(data) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(data),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/copyConfiguration`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject();
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function deleteRegistration(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/delete`, requestOptions)
      .then(async (response) => {
        if (!response.ok) {
          reject(response.status);
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function deRegisterDevice(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/deregister`, requestOptions)
      .then(async (response) => {
        if (!response.ok) {
          reject();
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

function login(user) {
  return new Promise((resolve, reject) => {
    const {email, password} = user;
    auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => resolve(res))
      .catch(() => reject(new Error('Username or password incorrect')));
  });
}

function logout(email) {
  return new Promise((resolve) => {
    if (email) {
      const topic = email.replace(/[&\/\\@#_,+()$~%.'":*?<>{}]/g, '-');
      messaging().unsubscribeFromTopic(topic);
    }
    auth().signOut();
    resolve();
  });
}

async function register(user) {
  const appConfig = await config();
  return new Promise((resolve, reject) => {
    const {email, password} = user;
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(async () => {
        const requestOptions = {
          method: 'POST',
          headers: authHeader(await firebaseToken()),
          body: JSON.stringify({...user, userPassword: password}),
        };
        fetch(`${appConfig.apiUrl}/auth/register`, requestOptions)
          .then((res) => {
            if (!res.ok) {
              reject(new Error('Registration Failed'));
              return;
            }
            resolve();
          })
          .catch((err) => reject(new Error(err)));
      })
      .catch(() => reject(new Error('Email ID already exists')));
  });
}

async function resetDeviceNetwork(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/user-controller/reset-network`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.status);
          return;
        }
        response.text().then((text) => {
          resolve(text);
        });
      })
      .catch((error) => reject(error));
  });
}

async function sendNetworkDetails(data) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    body: JSON.stringify(data),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.controllerUrl}/network/setup`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function skipZone(payload) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/watering/skip`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function startQuickRun(zone) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(zone),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/watering/start-quick-run`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.status);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function stopQuickRun(payload) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/watering/stop`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.status);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function removeDevice(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/user-controller/factory-reset`, requestOptions)
      .then(async (response) => {
        if (!response.ok) {
          reject(response.status);
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function updateZone(zone) {
  const appConfig = await config();
  const requestOptions = {
    method: 'PUT',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(zone),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/zones/update`, requestOptions)
      .then((response) => {
        console.log(response);
        if (!response.ok) {
          reject(response.json());
          return;
        }
        response.json().then(() => {
          resolve();
        });
      })
      .catch((error) => reject(error));
  });
}

async function updateDevice(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'PUT',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/user-controller/update`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.status);
          return;
        }
        response.json().then(() => {
          resolve();
        });
      })
      .catch((error) => reject(error));
  });
}

async function updateSchedule(deviceId, schedule) {
  const appConfig = await config();
  const scheduleObj = {...schedule, deviceId};
  const requestOptions = {
    method: 'PUT',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(scheduleObj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/schedules/update`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.status);
        } else {
          response.json().then((res) => {
            resolve(res.data);
          });
        }
      })
      .catch((error) => reject(error));
  });
}

async function createSchedule(deviceId, schedule) {
  const appConfig = await config();
  const scheduleObj = {...schedule, deviceId};
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(scheduleObj),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/schedules/add`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function deleteSchedule(schedule) {
  const appConfig = await config();
  const requestOptions = {
    body: JSON.stringify({_id: schedule._id}),
    method: 'DELETE',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/schedules/delete`, requestOptions)
      .then((response) => {
        console.log(response);
        if (!response.ok) {
          reject(response);
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function deleteScheduledZone(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile-schedule-zone/${id}`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function updateLocation(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/updateLocation`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
          return;
        }
        response.text().then(() => {
          resolve();
        });
      })
      .catch((error) => reject(error));
  });
}

async function updateScheduleZones(zones) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(zones),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/scheduleZone`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
          return;
        }
        response.text().then(() => {
          resolve();
        });
      })
      .catch((error) => reject(error));
  });
}

async function updateUserDetails(user) {
  const appConfig = await config();
  const requestOptions = {
    method: 'PUT',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(user),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/auth/editUser`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function sendResetMail(emailAddress) {
  return new Promise((resolve, reject) => {
    auth()
      .sendPasswordResetEmail(emailAddress)
      .then(() => resolve())
      .catch((error) => reject(error));
  });
}

async function stopManualSchedule(data) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(data),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/schedule/stop`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function getZoneSetupInfo() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/zones/setup-config`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          response.json().then((res) => {
            resolve(res.data);
          });
        }
      })
      .catch((error) => reject(error));
  });
}

async function getNotificationTypes() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/user-controller/notification-type`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response);
        } else {
          response.json().then((res) => {
            resolve(res.data);
          });
        }
      })
      .catch((error) => reject(error));
  });
}

export const userServices = {
  addManualSchedule,
  copyConfiguration,
  createSchedule,
  deleteRegistration,
  deleteSchedule,
  deleteScheduledZone,
  deRegisterDevice,
  login,
  logout,
  register,
  removeDevice,
  resetDeviceNetwork,
  sendNetworkDetails,
  sendResetMail,
  skipZone,
  startQuickRun,
  stopQuickRun,
  stopManualSchedule,
  updateDevice,
  updateLocation,
  updateSchedule,
  updateScheduleZones,
  updateUserDetails,
  updateZone,
  getZoneSetupInfo,
  getNotificationTypes,
};

export default userServices;
