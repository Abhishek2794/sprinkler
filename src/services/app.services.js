/* eslint-disable no-async-promise-executor */
/* eslint-disable no-console */
/* eslint-disable no-shadow */
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import {config} from '../helpers/config';
import firebaseToken from '../helpers/token';
import authHeader from '../helpers/authHeader';
import setCalendarDates from '../helpers/calculateDates';
import {Platform} from 'react-native';
import VersionInfo from 'react-native-version-info';

async function addController(controller) {
  const appConfig = await config();
  const updatedController = {
    ...controller,
    notifications: JSON.stringify({
      online: true,
      watering: true,
      weatherSkip: true,
      sensor: true,
    }),
  };
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(updatedController),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/add`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function boardUser() {
  await AsyncStorage.setItem('@Sprinkler:OnBoard', 'true');
}

async function checkBoarding() {
  const boarded = await AsyncStorage.getItem('@Sprinkler:OnBoard');
  return boarded;
}

async function checkDeviceToken() {
  const authToken = await AsyncStorage.getItem('@Sprinkler:deviceToken');
  return authToken;
}

async function checkRegistrationStatus(serialNumber) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/user-controller/registration-status?serialNumber=${serialNumber}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((res) => {
          resolve(res.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function checkSchedule(deviceId, scheduleId) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/schedule/isRunning?deviceId=${deviceId}&scheduleId=${scheduleId}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        resolve(response.json());
      })
      .catch((error) => reject(error));
  });
}

async function fetchActiveDevice() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/selected`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchCalendarDates(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/schedules/dates?deviceId=${id}`, requestOptions)
      .then((response) => {
        console.log(response);
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response
          .json()
          .then((res) => {
            const dates = setCalendarDates(res.data);
            resolve(dates);
          })
          .catch((error) => {
            reject(error);
          });
      })
      .catch((error) => reject(error));
  });
}

async function fetchCurrentSchedule() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/watering/current`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response
          .json()
          .then((json) => {
            resolve(json.data);
          })
          .catch((error) => {
            reject(error);
          });
      })
      .catch((error) => reject(error));
  });
}

async function fetchDeviceList() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/user-controller/list`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchDeviceLogs(id, page = 0, date) {
  console.log(id, date);
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    try {
      fetch(
        `${appConfig.apiUrl}/watering/logs?deviceId=${id}&date=${date}`,
        requestOptions,
      )
        .then((response) => {
          if (!response.ok) {
            reject(response.statusText);
            return;
          }
          response.json().then((json) => {
            resolve(json.data);
          });
        })
        .catch((error) => {
          reject(error);
          console.log(error);
        });
    } catch (e) {
      console.log(e);
    }
  });
}

async function fetchForecastData(latlng) {
  const appConfig = await config();
  return new Promise((resolve, reject) => {
    const {lat, lng} = latlng;
    fetch(
      `${appConfig.weatherApiUrl}/data/2.5/onecall?lat=${parseFloat(
        lat,
      )}&lon=${parseFloat(lng)}&exclude=hourly,minutely&appid=${
        appConfig.weatherApiKey
      }&units=imperial`,
    )
      .then((response) => resolve(response.json()))
      .catch((error) => reject(error));
  });
}

async function fetchDayWiseForecastData(data) {
  const appConfig = await config();
  return new Promise((resolve, reject) => {
    const {lat, lng, day, month} = data;
    fetch(
      `${appConfig.weatherApiUrl}/data/2.5/aggregated/day?lat=${parseFloat(
        lat,
      )}&lon=${parseFloat(lng)}&appid=${
        appConfig.weatherApiKey
      }&units=imperial&day=${day}&month=${month}`,
    )
      .then((response) => resolve(response.json()))
      .catch((error) => reject(error));
  });
}

async function fetchOpenWeatherData(latlng) {
  const appConfig = await config();
  return new Promise((resolve, reject) => {
    const {lat, lng} = latlng;
    fetch(
      `${appConfig.weatherApiUrl}/data/2.5/onecall?lat=${parseFloat(
        lat,
      )}&lon=${parseFloat(lng)}&exclude=hourly,minutely&appid=${
        appConfig.weatherApiKey
      }&units=imperial`,
    )
      .then((response) => resolve(response.json()))
      .catch((error) => reject(error));
  });
}

async function fetchSchedule(deviceId, scheduleId) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/schedule?scheduleId=${scheduleId}&deviceId=${deviceId}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function createCalenderForController(serialNumber, payload) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/watering/update-calendar?serialNumber=${serialNumber}`,
      requestOptions,
    )
      .then((response) => {
        console.log('createCalender resposne', response);
        if (!response.ok) {
          reject(response.statusText);
        } else {
          resolve();
        }
      })
      .catch((error) => reject(error));
  });
}

async function fetchScheduleHistory(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/watering/lastWatered?deviceId=${id}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchSchedules(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/schedules/list?deviceId=${id}`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then(async (json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchScheduleTimeTable(deviceId, timestamp) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/schedules/by-date?deviceId=${deviceId}&date=${timestamp}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchScheduledZones(id, scheduleId) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/scheduleZone/zones?scheduleId=${scheduleId}&deviceId=${id}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchTimeslot(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/schedule/timeSlot?deviceId=${id}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchUpcomingSchedules(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/schedules/upcoming?deviceId=${id}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          resolve([]);
          return;
        }
        response.json().then((json) => {
          console.log();
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchUserDetails() {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/auth/userInfo`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchWeatherData(latlng) {
  const appConfig = await config();
  return new Promise((resolve, reject) => {
    const {lat, lng} = latlng;
    fetch(
      `${appConfig.weatherApiUrl}/data/2.5/weather?lat=${parseFloat(
        lat,
      )}&lon=${parseFloat(lng)}&appid=${
        appConfig.weatherApiKey
      }&units=imperial`,
    )
      .then((response) => resolve(response.json()))
      .catch((error) => reject(error));
  });
}

async function fetchZone(id, zoneNumber) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/zone/${zoneNumber}?deviceId=${id}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function fetchZones(id) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/zones/all?deviceId=${id}`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function isRemotelyDisabled(number) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };
  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/controller/remotely-disabled?serialNumber=${number}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
}

async function registerForPushNotifications() {
  async function getToken() {
    let fcmToken = null;
    if (Platform.OS === 'ios') {
      await messaging().registerDeviceForRemoteMessages();
      fcmToken = await messaging().getToken();
    } else {
      fcmToken = await messaging().getToken();
    }
    await AsyncStorage.setItem('@Sprinkler:pushId', fcmToken);
    return fcmToken;
  }
  async function registerUserDevice(token) {
    await registerDevice({
      appType: Platform.OS === 'android' ? 'Android' : 'iOS',
      timezoneOffset: new Date().getTimezoneOffset(),
      deviceName: Platform.constants.Model || 'iPhone',
      messagingToken: token,
      deviceInfo: {
        name: VersionInfo.bundleIdentifier,
        version: VersionInfo.appVersion,
        os: Platform.OS,
        type: 'mobile',
      },
    });
  }
  const pushID = await AsyncStorage.getItem('@Sprinkler:pushId');
  if (pushID) {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      await registerUserDevice(pushID);
    } else {
      try {
        await messaging().requestPermission();
        const token = await getToken();
        await registerUserDevice(token);
      } catch (error) {
        console.log('rejected');
      }
    }
  } else {
    await messaging().requestPermission();
    const token = await getToken();
    await registerUserDevice(token);
  }
}

async function reverseGeocode(options) {
  const appConfig = await config();
  return new Promise(async (resolve, reject) => {
    fetch(
      `${appConfig.googleApiUrl}?key=${appConfig.googleApiKey}&latlng=${options.latitude},${options.longitude}`,
    )
      .then(async (result) => {
        const resultObject = await result.json();
        if (resultObject.status !== 'OK') {
          reject(new Error('An error occurred during geocoding.'));
          return;
        }
        resolve(
          resultObject.results.map((result) => {
            const address = {};
            result.address_components.forEach((component) => {
              if (component.types.includes('locality')) {
                address.city = component.long_name;
              }
              if (component.types.includes('street_address')) {
                address.street = component.long_name;
              }
              if (component.types.includes('administrative_area_level_1')) {
                address.region = component.long_name;
              }
              if (component.types.includes('country')) {
                address.country = component.long_name;
              }
              if (component.types.includes('postal_code')) {
                address.postalCode = component.long_name;
              }
              if (component.types.includes('point_of_interest')) {
                address.name = component.long_name;
              }
            });
            return {address, formatted_address: result.formatted_address};
          }),
        );
      })
      .catch((error) => {
        console.log('error', error);
        reject(new Error('Geocoding service unavailable'));
      });
  });
}

async function registerController(serialNumber) {
  const appConfig = await config();
  const requestOptions = {
    method: 'GET',
    headers: authHeader(await firebaseToken()),
  };

  return new Promise((resolve, reject) => {
    fetch(
      `${appConfig.apiUrl}/mobile/device/token?serialNumber=${serialNumber}`,
      requestOptions,
    )
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        response.json().then((json) => {
          resolve(json.data);
        });
      })
      .catch((error) => reject(error));
  });
}

async function setActiveDevice(device) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    headers: authHeader(await firebaseToken()),
    body: JSON.stringify(device),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.apiUrl}/mobile/device/select`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        resolve();
      })
      .catch((error) => reject(error));
  });
}

async function setControllerNetwork(controllerData) {
  const appConfig = await config();
  const requestOptions = {
    method: 'POST',
    body: JSON.stringify(controllerData),
  };

  return new Promise((resolve, reject) => {
    fetch(`${appConfig.controllerUrl}/network/setup`, requestOptions)
      .then((response) => {
        if (!response.ok) {
          reject(response.statusText);
          return;
        }
        resolve(response.json());
      })
      .catch((error) => reject(error));
  });
}

async function fetchNetworkList() {
  const appConfig = await config();
  try {
    const requestOptions = {
      method: 'GET',
    };
    return new Promise((resolve, reject) => {
      fetch(`${appConfig.controllerUrl}/network/ssids`, requestOptions)
        .then((response) => {
          if (!response.ok) {
            reject(response.statusText);
            return;
          }
          response.json().then((json) => {
            resolve(json.data);
          });
        })
        .catch((error) => reject(error));
    });
  } catch (e) {
    console.log(e);
  }
}

async function multipleQuickRun(payload) {
  const appConfig = await config();
  try {
    const requestOptions = {
      method: 'POST',
      headers: authHeader(await firebaseToken()),
      body: JSON.stringify(payload),
    };
    return new Promise((resolve, reject) => {
      fetch(`${appConfig.apiUrl}/watering/start-multiple-run`, requestOptions)
        .then((response) => {
          console.log(response);
          if (!response.ok) {
            reject(response.statusText);
            return;
          }
          resolve(response.json());
        })
        .catch((error) => reject(error));
    });
  } catch (err) {
    console.log(err);
  }
}

async function upgradeFirmware(deviceId) {
  const appConfig = await config();
  try {
    const requestOptions = {
      method: 'GET',
      headers: authHeader(await firebaseToken()),
    };
    return new Promise((resolve, reject) => {
      fetch(
        `${appConfig.apiUrl}/ota/request-firmware-upgrade?deviceId=${deviceId}`,
        requestOptions,
      )
        .then((response) => {
          if (!response.ok) {
            reject(response.statusText);
            return;
          }
          response.json().then((json) => {
            console.log('response', json);
            resolve(json.message);
          });
        })
        .catch((error) => reject(error));
    });
  } catch (err) {
    console.log(err);
  }
}

async function registerDevice(payload) {
  const appConfig = await config();
  try {
    const requestOptions = {
      method: 'PUT',
      headers: authHeader(await firebaseToken()),
      body: JSON.stringify(payload),
    };
    return new Promise((resolve, reject) => {
      fetch(`${appConfig.apiUrl}/auth/device`, requestOptions)
        .then((response) => {
          if (!response.ok) {
            reject(response.statusText);
            return;
          }
          resolve(response.json());
        })
        .catch((error) => reject(error));
    });
  } catch (err) {
    console.log(err);
  }
}

export const appServices = {
  addController,
  boardUser,
  checkBoarding,
  checkDeviceToken,
  checkRegistrationStatus,
  checkSchedule,
  fetchActiveDevice,
  fetchCalendarDates,
  fetchCurrentSchedule,
  fetchDeviceList,
  fetchDeviceLogs,
  fetchForecastData,
  fetchOpenWeatherData,
  fetchSchedule,
  fetchSchedules,
  fetchScheduleHistory,
  fetchScheduleTimeTable,
  fetchScheduledZones,
  fetchTimeslot,
  fetchUpcomingSchedules,
  fetchUserDetails,
  fetchWeatherData,
  fetchZone,
  fetchZones,
  isRemotelyDisabled,
  registerController,
  registerForPushNotifications,
  reverseGeocode,
  setActiveDevice,
  setControllerNetwork,
  fetchNetworkList,
  multipleQuickRun,
  upgradeFirmware,
  createCalenderForController,
  fetchDayWiseForecastData,
  registerDevice,
};

export default appServices;
