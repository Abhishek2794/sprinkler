import { config } from '../helpers/config';

async function logEvent(data, tag) {
    const appConfig = await config();
    const requestOptions = {
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
    };
    return new Promise((resolve, reject) => {
        fetch(`http://logs-01.loggly.com/bulk/${appConfig.logglyApiKey}/tag/${tag}`, requestOptions)
            .then((response) => {
                if (!response.ok) {
                    reject();
                    return;
                }
                response.json().then((json) => {
                    console.log("loggly", json);
                    resolve(json);
                });
            })
            .catch((error) => reject(error));
    });
}

export const logglyServices = {
    logEvent,
};

export default logglyServices;
