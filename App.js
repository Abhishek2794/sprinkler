import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {Alert} from 'react-native';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import store from './src/helpers/store';
import Navigator from './src/navigation';
import NetInfo from '@react-native-community/netinfo';
import {ModalPortal} from 'react-native-modals';
import watch from 'redux-watch';
import messaging from '@react-native-firebase/messaging';
import alert from './src/components/alert';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const watcher = watch(store.getState, 'login.isLoggedIn');
    this.loginListener = store.subscribe(
      watcher((newValue) => {
        if (newValue === true) {
          messaging().onNotificationOpenedApp((remoteMessage) => {
            console.log('onNotificationOpenedApp', remoteMessage);
          });

          // Check whether an initial notification is available
          messaging()
            .getInitialNotification()
            .then((remoteMessage) => {
              console.log('getInitialNotification', remoteMessage);
            });
          // Foreground state message handler
          this.notificationSubscription = messaging().onMessage(
            this.handleNotification,
          );
        }
      }),
    );
  }

  componentWillUnmount() {
    if (typeof this.notificationSubscription == 'function') {
      this.notificationSubscription();
    }
  }

  handleNotification(pushNotification) {
    const {data} = pushNotification;
    if (Object.keys(data).length) {
      alert(data.title, data.body);
    }
  }

  componentDidCatch(error) {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        Alert.alert(
          'No Network',
          'App is not able to connect with the network, check your network connection',
          [
            {
              text: 'Close',
            },
          ],
          {cancelable: false},
        );
      }
    });
  }

  render() {
    return (
      <NavigationContainer>
        <Provider store={store}>
          <Navigator />
          <ModalPortal />
        </Provider>
      </NavigationContainer>
    );
  }
}
